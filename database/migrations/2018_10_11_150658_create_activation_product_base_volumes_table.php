<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivationProductBaseVolumesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activation_product_base_volumes', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->double('base_volume');
            $table->string('activation_id');
            $table->string('location_id');
            $table->string('product_id');
            $table->string('admin_id');
            $table->string('date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activation_product_base_volumes');
    }
}

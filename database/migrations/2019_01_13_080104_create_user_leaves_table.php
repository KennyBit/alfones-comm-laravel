<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserLeavesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_leaves', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->string('admin_id');
            $table->string('date_from');
            $table->string('date_to');
            $table->string('reasons');
            $table->string('description');
            $table->string('attachment')->nullable();
            $table->integer('status')->default(0);
            $table->string('status_report')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_leaves');
    }
}

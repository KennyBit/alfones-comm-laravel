<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activations', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->string('name');
            $table->string('description');
            $table->string('client_id');
            $table->string('project_manager_id');
            $table->integer('ba_unit_cost')->default(0);
            $table->integer('team_leader_unit_cost')->default(0);
            $table->string('start_date');
            $table->string('end_date');
            $table->integer('status')->default(0);
            $table->integer('allow_tl_location')->default(0);
            $table->integer('enable_geo_fence')->default(0);
            $table->integer('allow_client_portal')->default(0);
            $table->timestamps();
        });

        DB::statement('ALTER Table activations add activation_id INTEGER NOT NULL UNIQUE AUTO_INCREMENT;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activations');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->string('activation_id');
            $table->string('location_id');
            $table->string('user_id');
            $table->string('customer_name');
            $table->string('customer_phone')->nullable();
            $table->string('customer_id')->nullable();
            $table->integer('product')->default(0);
            $table->string('product_id')->nullable();
            $table->integer('product_quantity')->default(0);
            $table->integer('product_value')->default(0);
            $table->string('extra_field_1')->nullable();
            $table->string('extra_field_2')->nullable();
            $table->integer('merchandise')->default(0);
            $table->string('merchandise_id')->nullable();
            $table->integer('merchandise_quantity')->default(0);
            $table->string('customer_feedback')->nullable();
            $table->string('image')->nullable();
            $table->string('image_caption')->nullable();
            $table->double('latitude')->nullable();
            $table->double('longitude')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports');
    }
}

<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expenses', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->integer('expense_type')->default(0);
            $table->string('activation_id')->nullable();
            $table->string('admin_id');
            $table->string('requisition_title');
            $table->string('required_date');
            $table->integer('requisition_status')->default(0);
            $table->integer('reconciliation_status')->default(0);
            $table->integer('submitted_status')->default(0);
            $table->timestamps();
        });

        DB::statement('ALTER Table expenses add expenses_id INTEGER NOT NULL UNIQUE AUTO_INCREMENT;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expenses');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->string('employee_no')->nullable();
            $table->string('name');
            $table->string('date_of_birth')->nullable();
            $table->integer('gender')->default(0);
            $table->integer('phone_number')->unique();
            $table->integer('national_id')->unique();
            $table->string('email')->unique();
            $table->string('password');
            $table->boolean('is_verified')->default(false);
            $table->string('firebase_token')->nullable();
            $table->string('profile_image')->nullable();
            $table->integer('role');
            $table->integer('permanent_status')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}

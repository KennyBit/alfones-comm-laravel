<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayslipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payslips', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->string('admin_id');
            $table->double('gross_salary')->default(0);
            $table->double('nssf_contibution')->default(0);
            $table->double('taxable_pay')->default(0);
            $table->double('personal_relief')->default(0);
            $table->double('insurance_relief')->default(0);
            $table->double('paye')->default(0);
            $table->double('nhif_contribution')->default(0);
            $table->double('net_pay')->default(0);
            $table->double('tax_rate')->default(0);
            $table->string('year')->default(0);
            $table->string('month')->default(0);
            $table->timestamps();
        });

        DB::statement('ALTER Table payslips add payslip_id INTEGER NOT NULL UNIQUE AUTO_INCREMENT;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payslips');
    }
}

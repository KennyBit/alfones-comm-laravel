<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->string('activation_id');
            $table->string('name');
            $table->string('description');
            $table->string('team_leader_id');
            $table->string('location_identifier')->nullable();
            $table->string('division_id')->nullable();
            $table->string('region_id')->nullable();
            $table->string('date');
            $table->string('duration')->default(0);
            $table->double('latitude')->nullable();
            $table->double('longitude')->nullable();
            $table->integer('enable_geo_fence')->default(0);
            $table->string('location_comments')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locations');
    }
}

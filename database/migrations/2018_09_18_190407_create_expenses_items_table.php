<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpensesItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expenses_items', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->string('expenses_id');
            $table->string('item_supplier');
            $table->string('item_name');
            $table->string('item_description');
            $table->string('item_unit_cost');
            $table->string('item_quantity');
            $table->string('item_days');
            $table->string('item_amount');
            $table->string('reconciliation_status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expenses_items');
    }
}

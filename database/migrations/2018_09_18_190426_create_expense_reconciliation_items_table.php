<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpenseReconciliationItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expense_reconciliation_items', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->string('expenses_id');
            $table->string('item_expenses_id');
            $table->string('item_supplier');
            $table->string('item_name');
            $table->string('item_description');
            $table->string('item_unit_cost');
            $table->string('item_quantity');
            $table->string('item_days');
            $table->string('item_amount');
            $table->string('item_attachment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expense_reconciliation_items');
    }
}

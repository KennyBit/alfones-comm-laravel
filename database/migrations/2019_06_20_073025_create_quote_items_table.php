<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuoteItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quote_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('item_name');
            $table->string('quote_id');
            $table->string('quantity');
            $table->string('description');
            $table->string('unit_cost');
            $table->string('total');
            $table->string('days');
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('quote_items');
    }

    
}

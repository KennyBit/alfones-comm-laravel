<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->string('name');
            $table->string('date_of_birth')->nullable();
            $table->integer('gender')->default(0);
            $table->string('phone_number')->unique();
            $table->integer('national_id')->default(0);
            $table->string('email')->unique();
            $table->string('password');
            $table->boolean('is_verified')->default(false);
            $table->string('firebase_token')->nullable();
            $table->string('profile_image')->nullable();
            $table->double('current_latitude')->nullable();
            $table->double('current_longitude')->nullable();
            $table->string('current_activation')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

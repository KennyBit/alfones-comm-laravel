<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivationOutletProductSummariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activation_outlet_product_summaries', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->string('activation_id');
            $table->string('location_id');
            $table->string('product_id');
            $table->string('product_price');
            $table->double('base_volume')->nullable();
            $table->double('quantity');
            $table->string('admin_id');
            $table->string('date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activation_outlet_product_summaries');
    }
}

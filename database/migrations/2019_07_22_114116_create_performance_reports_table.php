<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerformanceReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('performance_reports', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->string('admin_id');
            $table->string('start_date');
            $table->string('end_date');
            $table->integer('on_time')->default(0);
            $table->integer('in_full')->default(0);
            $table->integer('no_errors')->default(0);
            $table->integer('target_achievement')->default(0);
            $table->integer('overall_performance')->default(0);
            $table->double('performance_percentage')->default(0);
            $table->double('overall_attendance')->default(0);
            $table->double('percentage_overall_performance')->default(0);
            $table->double('cumulative_performance')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('performance_reports');
    }
}

<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'ba-list',
            'ba-details',
            'ba-create',
            'ba-edit',
            'ba-delete',
            'admin-list',
            'admin-details',
            'admin-create',
            'admin-edit',
            'admin-delete',
            'admin-permission',
            'client-list',
            'client-details',
            'client-create',
            'client-edit',
            'client-delete',
            'division-list',
            'division-details',
            'division-create',
            'division-edit',
            'division-delete',
            'region-list',
            'region-details',
            'region-create',
            'region-edit',
            'region-delete',
            'activation-list',
            'activation-details',
            'activation-create',
            'activation-edit',
            'activation-delete',
            'activation-product-list',
            'activation-product-details',
            'activation-product-create',
            'activation-product-edit',
            'activation-product-delete',
            'activation-merchandise-list',
            'activation-merchandise-details',
            'activation-merchandise-create',
            'activation-merchandise-edit',
            'activation-merchandise-delete',
            'activation-location-list',
            'activation-location-details',
            'activation-location-create',
            'activation-location-edit',
            'activation-location-delete',
            'activation-team-leader-list',
            'activation-team-leader-details',
            'activation-team-leader-create',
            'activation-team-leader-edit',
            'activation-team-leader-delete',
            'activation-user-list',
            'activation-user-details',
            'activation-user-create',
            'activation-user-edit',
            'activation-user-delete',
            'activation-target-list',
            'activation-target-details',
            'activation-target-create',
            'activation-target-edit',
            'activation-target-delete',
            'activation-reports-view',
            'activation-reports-pdf',
            'activation-reports-xls',
            'activation-payroll-view',
            'activation-payroll-generate',
            'expenses-list',
            'expenses-details',
            'expenses-approve',
            'expenses-printout',
            'leave-list',
            'leave-details',
            'leave-approve',
            'leave-printout',
            'payslip-list',
            'payslip-details',
            'payslip-create',
            'payslip-printout',
            'sms-notification-status'
        ];


        foreach ($permissions as $permission) {
            Permission::create(['name' => $permission]);
        }
    }
}

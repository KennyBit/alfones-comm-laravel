<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            'super-admin',
            'admin',
            'project-manager',
            'team-leader',
            'finance',
            'Supervisor',
            'Operations'
        ];


        foreach ($roles as $role) {
            Role::create(['name' => $role]);
        }
    }
}

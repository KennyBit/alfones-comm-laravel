<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/register', 'ApiController@authRegister');
Route::post('/login', 'ApiController@login')->name('login');
Route::post('/login-admin', 'ApiController@loginAdmin')->name('login-admin');
Route::post('/send-reset-code', 'ApiController@sendResetCode')->name('send-reset-code');
Route::post('/send-reset-code-admin', 'ApiController@sendResetCodeAdmin')->name('send-reset-code-admin');
Route::post('/check-reset-code', 'ApiController@checkResetCode')->name('check-reset-code');
Route::post('/check-reset-code-admin', 'ApiController@checkResetCodeAdmin')->name('check-reset-code-admin');
Route::post('/reset-password', 'ApiController@resetPassword')->name('reset-password');
Route::post('/reset-password-admin', 'ApiController@resetPasswordAdmin')->name('reset-password-admin');

/**
 * ADMIN ROUTES
 */
Route::post('/admin-admin-create', 'ApiController@adminCreate')->name('admin-admin-create');
Route::get('/admin-admin', 'ApiController@adminList')->name('admin-admin');
Route::get('/admin-admin-detail/{id}', 'ApiController@adminDetail')->name('admin-admin-detail');
Route::post('/admin-admin-edit', 'ApiController@adminEdit')->name('admin-admin-edit');
Route::post('/admin-admin-delete', 'ApiController@adminDelete')->name('admin-admin-delete');
Route::get('/admin-get-project-managers', 'ApiController@getProjectManager')->name('admin-get-project-managers');
Route::get('/admin-get-team-leader', 'ApiController@getTeamLeader')->name('admin-get-team-leader');
Route::get('/admin-get-activation-team-leader/{id}', 'ApiController@getActivationTeamLeader')->name('admin-get-activation-team-leader');


/**
 * CLIENT ROUTES
 */
Route::post('/admin-client-create', 'ApiController@createClient')->name('admin-client-create');
Route::get('/admin-client', 'ApiController@clientList')->name('admin-client');
Route::get('/admin-client-detail/{id}', 'ApiController@clientDetail')->name('admin-client-detail');
Route::post('/admin-client-edit', 'ApiController@clientEdit')->name('admin-client-edit');
Route::post('/admin-client-delete', 'ApiController@clientDelete')->name('admin-client-delete');
Route::get('/admin-get-clients', 'ApiController@getClient')->name('admin-get-clients');


/**
 * USER ROUTES
 */
Route::post('/admin-user-create', 'ApiController@createUser')->name('admin-user-create');
Route::get('/admin-user', 'ApiController@userList')->name('admin-user');
Route::get('/admin-user-detail/{id}', 'ApiController@userDetail')->name('admin-user-detail');
Route::post('/admin-user-edit', 'ApiController@userEdit')->name('admin-user-edit');
Route::post('/admin-user-delete', 'ApiController@userDelete')->name('admin-user-delete');


/**
 * DIVISION ROUTES
 */
Route::post('/admin-division-create', 'ApiController@createDivision')->name('admin-division-create');
Route::get('/admin-division', 'ApiController@divisionList')->name('admin-division');
Route::get('/admin-division-detail/{id}', 'ApiController@divisionDetail')->name('admin-division-detail');
Route::post('/admin-division-edit', 'ApiController@divisionEdit')->name('admin-division-edit');
Route::post('/admin-division-delete', 'ApiController@divisionDelete')->name('admin-division-delete');

/**
 * REGION ROUTES
 */
Route::post('/admin-region-create', 'ApiController@createRegion')->name('admin-region-create');
Route::get('/admin-region/{id}', 'ApiController@regionList')->name('admin-region');
Route::get('/admin-region-detail/{id}', 'ApiController@regionDetail')->name('admin-region-detail');
Route::post('/admin-region-edit', 'ApiController@regionEdit')->name('admin-region-edit');
Route::post('/admin-region-delete', 'ApiController@regionDelete')->name('admin-region-delete');

/**
 * ACTIVATION ROUTES
 */

Route::post('/admin-activation-create', 'ApiController@createActivation')->name('admin-activation-create');
Route::get('/admin-activation', 'ApiController@activationList')->name('admin-activation');
Route::get('/admin-activation-detail/{id}', 'ApiController@activationDetail')->name('admin-activation-detail');
Route::post('/admin-activation-edit', 'ApiController@activationEdit')->name('admin-activation-edit');
Route::post('/admin-activation-delete', 'ApiController@activationDelete')->name('admin-activation-delete');

Route::get('/admin-get-activation-products/{id}', 'ApiController@activationProduct')->name('admin-get-activation-products');
Route::get('/admin-get-activation-merchandises/{id}', 'ApiController@activationMerchandise')->name('admin-get-activation-merchandises');
Route::get('/admin-get-activation-locations/{id}', 'ApiController@activationLocation')->name('admin-get-activation-locations');
Route::get('/admin-get-activation-team-leaders/{id}', 'ApiController@activationTeamLeader')->name('admin-get-activation-team-leaders');
Route::get('/admin-get-activation-team-leader-create/{id}', 'ApiController@activationTeamLeaderCreate')->name('admin-get-activation-team-leader-create');
Route::get('/admin-get-activation-targets/{id}', 'ApiController@activationTarget')->name('admin-get-activation-targets');
Route::get('/admin-get-activation-users/{id}', 'ApiController@activationUsers')->name('admin-get-activation-users');

Route::post('/admin-get-activation-expenses', 'ApiController@getActivationExpenses')->name('admin-get-activation-expenses');
Route::get('/admin-get-activation-type/{id}', 'ApiController@getActivationType')->name('admin-get-activation-type');


/**
 * MOBILE APIS
 */
Route::post('/activation-sync-details', 'ApiController@activationSync')->name('activation-sync-details');
Route::post('/activation-get-products', 'ApiController@activationProducts')->name('activation-get-products');
Route::post('/activation-get-merchandises', 'ApiController@activationMerchandises')->name('activation-get-merchandises');
Route::post('/user-get-activation-details', 'ApiController@userCurrentActivationDetails')->name('user-get-activation-details');
Route::post('/user-activation-details-update', 'ApiController@userActivationDetailsUpdate')->name('user-activation-details-update');
Route::get('/active-activation-list', 'ApiController@activationActiveList')->name('active-activation-list');
Route::post('/user-get-team-leader-details', 'ApiController@userTeamLeaderDetails')->name('user-get-team-leader-details');
Route::post('/active-team-leader-list', 'ApiController@activationActiveTeamLeaders')->name('active-team-leader-list');
Route::post('/user-team-leader-details-update', 'ApiController@userTeamLeaderDetailUpdate')->name('user-team-leader-details-update');
Route::post('/user-get-previous-activations', 'ApiController@getUserActivations')->name('user-get-previous-activations');
Route::post('/activation-get-date', 'ApiController@getActivationDates')->name('activation-get-date');
Route::post('/activation-report-history', 'ApiController@getActivationReportHistory')->name('activation-report-history');
Route::post('/activation-report-history-summary', 'ApiController@getActivationReportHistorySummary')->name('activation-report-history-summary');
Route::post('/user-report-confirmation', 'ApiController@getUserReportConfirmation')->name('user-report-confirmation');
Route::post('/user-check-in', 'ApiController@getUserCheckIn')->name('user-check-in');
Route::post('/user-payment-history', 'ApiController@getUserPayment')->name('user-payment-history');
Route::post('/user-notifications', 'ApiController@getUserNotifications')->name('user-notifications');
Route::post('/notification-read-status-update', 'ApiController@updateNotificationReadStatus')->name('notification-read-status-update');
Route::post('/activation-report-create', 'ApiController@createActivationRecord')->name('activation-report-create');
Route::post('/activation-attachment-report-create', 'ApiController@createActivationAttachmentRecord')->name('activation-attachment-report-create');
Route::post('/activation-report-edit', 'ApiController@editActivationRecord')->name('activation-report-edit');
Route::post('/activation-report-delete', 'ApiController@deleteActivationRecord')->name('activation-report-delete');

Route::post('/operations-previous-activations', 'ApiController@getPreviousActivations')->name('operations-previous-activations');
Route::post('/project-manager-previous-activations', 'ApiController@getProjectManagerActivations')->name('project-manager-previous-activations');
Route::post('/team-leader-previous-activations', 'ApiController@getTeamLeaderActivations')->name('team-leader-previous-activations');
Route::post('/project-manager-location-report-history', 'ApiController@getProjectManagerLocationReportHistory')->name('project-manager-location-report-history');
Route::post('/team-leader-location-report-history', 'ApiController@getTeamLeaderLocationReportHistory')->name('location-report-history');
Route::post('/project-manager-user-location-report-history', 'ApiController@getProjectManagerUserLocationReportHistory')->name('project-manager-user-location-report-history');
Route::post('/team-leader-user-location-report-history', 'ApiController@getTeamLeaderUserLocationReportHistory')->name('team-leader-user-location-report-history');
Route::post('/team-leader-check-in', 'ApiController@getTeamLeaderCheckIn')->name('team-leader-check-in');
Route::post('/team-leader-check-in-update', 'ApiController@teamLeaderCheckInUpdate')->name('team-leader-check-in-update');
Route::post('/team-leader-activation-location', 'ApiController@teamLeaderActivationLocation')->name('team-leader-activation-location');
Route::post('/team-leader-activation-product', 'ApiController@teamLeaderActivationProduct')->name('team-leader-activation-product');
Route::post('/team-leader-activation-user-location', 'ApiController@teamLeaderActivationUserLocation')->name('team-leader-activation-user-location');
Route::post('/team-leader-activation-users', 'ApiController@teamLeaderActivationUsers')->name('team-leader-activation-users');
Route::post('/team-leader-user-location-update', 'ApiController@teamLeaderActivationUsersUpdate')->name('team-leader-user-location-update');

Route::post('/activation-pending-expenses', 'ApiController@activationPendingExpenses')->name('activation-pending-expenses');
Route::post('/activation-processed-expenses', 'ApiController@activationProcessedExpenses')->name('activation-processed-expenses');
Route::post('/activation-expenses-items', 'ApiController@activationExpensesItems')->name('activation-expenses-items');
Route::post('/activation-expenses-item-create', 'ApiController@activationExpensesItemCreate')->name('activation-expenses-item-create');
Route::post('/activation-expenses-item-edit', 'ApiController@activationExpensesItemEdit')->name('activation-expenses-item-edit');
Route::post('/activation-expenses-item-delete', 'ApiController@activationExpensesItemDelete')->name('activation-expenses-item-delete');
Route::post('/activation-expenses-create', 'ApiController@activationExpensesCreate')->name('activation-expenses-create');
Route::post('/activation-expenses-submitted', 'ApiController@activationExpensesSubmitted')->name('activation-expenses-submitted');
Route::post('/activation-reconcile-item', 'ApiController@createReconciliationItem')->name('activation-reconcile-item');
Route::post('/expense-give-amount', 'ApiController@expenseGiveAmount')->name('expense-give-amount');

Route::post('/general-pending-expenses', 'ApiController@generalPendingExpenses')->name('general-pending-expenses');
Route::post('/general-processed-expenses', 'ApiController@generalProcessedExpenses')->name('general-processed-expenses');
Route::post('/general-expenses-items', 'ApiController@generalExpensesItems')->name('general-expenses-items');
Route::post('/general-expenses-item-create', 'ApiController@generalExpensesItemCreate')->name('general-expenses-item-create');
Route::post('/general-expenses-item-edit', 'ApiController@generalExpensesItemEdit')->name('general-expenses-item-edit');
Route::post('/general-expenses-item-delete', 'ApiController@generalExpensesItemDelete')->name('general-expenses-item-delete');
Route::post('/general-expenses-create', 'ApiController@generalExpensesCreate')->name('general-expenses-create');
Route::post('/general-expenses-submitted', 'ApiController@generalExpensesSubmitted')->name('general-expenses-submitted');
Route::post('/general-reconcile-item', 'ApiController@createReconciliationItem')->name('general-reconcile-item');

Route::post('/activation-reports-confirm', 'ApiController@activationReportConfirm')->name('activation-reports-confirm');
Route::post('/activation-check-reports-confirm', 'ApiController@activationCheckReportConfirm')->name('activation-check-reports-confirm');

Route::post('/submit-outlet-sales', 'ApiController@submitOutletSales')->name('submit-outlet-sales');
Route::post('/submit-outlet-stocks', 'ApiController@submitOutletStock')->name('submit-outlet-stocks');

Route::post('/create-location-mobile', 'ApiController@createLocationMobile')->name('create-location-mobile');
Route::get('/activation-division-mobile', 'ApiController@divisionListMobile')->name('activation-division-mobile');
Route::post('/activation-region-mobile', 'ApiController@regionListMobile')->name('activation-region-mobile');


/**
 * REPORTS GRAPH
 */
Route::get('/admin-interaction-graph/{id}', 'ApiController@interactionGraph')->name('admin-interaction-graph');
Route::get('/admin-sales-graph/{id}', 'ApiController@salesGraph')->name('admin-sales-graph');
Route::get('/admin-product-sales-graph/{id}', 'ApiController@productSalesGraph')->name('admin-product-sales-graph');
Route::get('/admin-merchandise-graph/{id}', 'ApiController@merchandiseGraph')->name('admin-merchandise-graph');
Route::get('/admin-merchandise-distribution-graph/{id}', 'ApiController@merchandiseDistributionGraph')->name('admin-merchandise-distribution-graph');


/**
 * PRODUCT ROUTES
 */
Route::post('/admin-product-create', 'ApiController@createProduct')->name('admin-product-create');
Route::get('/admin-product', 'ApiController@index')->name('admin-product');
Route::get('/admin-product-detail/{id}', 'ApiController@productDetail')->name('admin-product-detail');
Route::post('/admin-product-edit', 'ApiController@productEdit')->name('admin-product-edit');
Route::delete('/admin-product-delete', 'ApiController@productDelete')->name('admin-product-delete');


/**
 * MERCHANDISE ROUTES
 */
Route::post('/admin-merchandise-create', 'ApiController@createMerchandise')->name('admin-merchandise-create');
Route::get('/admin-merchandise', 'ApiController@index')->name('admin-merchandise');
Route::get('/admin-merchandise-detail/{id}', 'ApiController@merchandiseDetail')->name('admin-merchandise-detail');
Route::post('/admin-merchandise-edit', 'ApiController@merchandiseEdit')->name('admin-merchandise-edit');
Route::delete('/admin-merchandise-delete', 'ApiController@merchandiseDelete')->name('admin-merchandise-delete');


/**
 * LOCATION / ASSIGNMENT ROUTES
 */
Route::post('/admin-location-create', 'ApiController@createLocation')->name('admin-location-create');
Route::get('/admin-location', 'ApiController@index')->name('admin-location');
Route::get('/admin-location-detail/{id}', 'ApiController@locationDetail')->name('admin-location-detail');
Route::post('/admin-location-edit', 'ApiController@locationEdit')->name('admin-location-edit');
Route::delete('/admin-location-delete', 'ApiController@locationDelete')->name('admin-location-delete');

/**
 * TEAM LEADER ROUTES
 */
Route::post('/admin-team-leader-create', 'ApiController@createTeamLeader')->name('admin-team-leader-create');
Route::get('/admin-team-leader', 'ApiController@index')->name('admin-team-leader');
Route::get('/admin-team-leader-detail/{id}', 'ApiController@teamleaderDetail')->name('admin-team-leader-detail');
Route::post('/admin-team-leader-edit', 'ApiController@teamleaderEdit')->name('admin-team-leader-edit');
Route::delete('/admin-team-leader-delete', 'ApiController@teamleaderDelete')->name('admin-team-leader-delete');

/**
 * TARGET ROUTES
 */
Route::post('/admin-target-create', 'ApiController@createTarget')->name('admin-target-create');
Route::get('/admin-target', 'ApiController@index')->name('admin-target');
Route::get('/admin-target-detail/{id}', 'ApiController@targetDetails')->name('admin-target-detail');
Route::post('/admin-target-edit', 'ApiController@targetEdit')->name('admin-target-edit');
Route::delete('/admin-target-delete', 'ApiController@targetDelete')->name('admin-target-delete');

/**
 * ACTIVATION TYPE ROUTES
 */
Route::post('/admin-activation-type-create', 'ApiController@createActivationType')->name('admin-activation-type-create');
Route::get('/admin-activation-type-detail/{id}', 'ApiController@activationTypeDetails')->name('admin-activation-type-detail');
Route::post('/admin-activation-type-edit', 'ApiController@activationTypeEdit')->name('admin-activation-type-edit');
Route::delete('/admin-activation-type-delete', 'ApiController@activationTypeDelete')->name('admin-activation-type-delete');

/**
 * ACTIVATION PRODUCT STOCK
 */
Route::post('/admin-get-activation-location-stock', 'ApiController@getActivationLocationStock')->name('admin-get-activation-location-stock');
Route::get('/admin-get-activation-location-stock-details/{id}', 'ApiController@getActivationLocationStockDetails')->name('admin-get-activation-location-stock-details');
Route::post('/admin-activation-location-stock-edit', 'ApiController@getActivationLocationStockEdit')->name('admin-activation-location-stock-edit');
Route::post('/admin-activation-location-stock-delete', 'ApiController@getActivationLocationStockDelete')->name('admin-activation-location-stock-delete');


/**
 * ACTIVATION PRODUCT SUMMARY
 */
Route::post('/admin-get-activation-location-product-summary', 'ApiController@getActivationLocationProductSummary')->name('admin-get-activation-location-product-summary');
Route::get('/admin-get-activation-location-product-summary-details/{id}', 'ApiController@getActivationLocationProductSummaryDetails')->name('admin-get-activation-location-product-summary-details');
Route::post('/admin-activation-location-product-summary-edit', 'ApiController@getActivationLocationProductSummaryEdit')->name('admin-activation-location-product-summary-edit');
Route::post('/admin-activation-location-product-summary-delete', 'ApiController@getActivationLocationProductSummaryDelete')->name('admin-activation-location-product-summary-delete');


/**
 * LEAVE ROUTES
 */
Route::post('/admin-leave-create', 'ApiController@createLeave')->name('admin-leave-create');
Route::get('/admin-leave', 'ApiController@getLeave')->name('admin-leave');
Route::post('/admin-user-leave', 'ApiController@getUserLeave')->name('admin-user-leave');
Route::post('/admin-user-leave-summary', 'ApiController@userLeaveSummary')->name('admin-user-leave-summary');
Route::get('/admin-leave-details/{id}', 'ApiController@leaveDetail')->name('admin-leave-details');
Route::post('/admin-leave-edit', 'ApiController@editLeave')->name('admin-leave-edit');
Route::post('/admin-leave-status-edit', 'ApiController@editLeaveStatus')->name('admin-leave-status-edit');
Route::post('/admin-leave-upload-attachment', 'ApiController@uploadLeaveAttachment')->name('admin-leave-upload-attachment');

/**
 * PAYSLIP ROUTES
 */
Route::post('/admin-payslip', 'ApiController@getPayslips')->name('admin-payslip');
Route::post('/admin-user-payslip', 'ApiController@getUserPayslip')->name('admin-user-payslip');
Route::post('/admin-payslip-create', 'ApiController@createPayslip')->name('admin-payslip-create');

Route::get('/admin-permanent_employees', 'ApiController@getPermanentEmployees')->name('admin-permanent_employees');

Route::get('/image-get-reconciliation/{fileName}', 'ApiController@getReconciliationImage')->name('image-get-reconciliation');
Route::get('/image-get-leave/{fileName}', 'ApiController@getLeaveAttachment')->name('image-get-leave');
Route::get('/image-get-activation/{fileName}', 'ApiController@getActivationAttachment')->name('image-get-activation');
Route::get('/file-get-payslip/{fileName}', 'ApiController@getPayslip')->name('file-get-payslip');


/**
 * PLANNER ROUTES
 */
Route::post('/admin-planner-event-update', 'Api\PlannerEventController@updateEvent')->name('admin-planner-event-update');
Route::post('/admin-get-planner-events', 'Api\PlannerEventController@getDayEvents')->name('admin-get-planner-events');
Route::post('/admin-get-currrent-event', 'Api\PlannerEventController@getCurrentUserEvent')->name('admin-get-currrent-event');
Route::get('/send-work-planner', 'Api\PlannerEventController@sendWorkPlanner')->name('send-work-planner');

/**
 *  PERFORMACE REPORTS
 */
Route::post('/admin-performance-create', 'ApiController@createPerformance')->name('admin-performance-create');
Route::get('/admin-performance', 'ApiController@performanceList')->name('admin-performance');
Route::get('/admin-performance-detail/{id}', 'ApiController@performanceDetails')->name('admin-performance-detail');
Route::post('/admin-performance-edit', 'ApiController@performanceEdit')->name('admin-performance-edit');
Route::post('/admin-performance-delete', 'ApiController@performanceDelete')->name('admin-performance-delete');
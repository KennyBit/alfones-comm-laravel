<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('admin')->group(function () {
    Route::get('/', 'Admin\HomeController@index')->name('admin.dashboard');
    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::get('/logout', 'Auth\AdminLoginController@logout')->name('admin-logout');

    /**
     * ADMIN ROUTES
     */
    Route::get('/admin','Admin\AdminController@index')->name('admin.admin');
    Route::get('/admin-details/{id}','Admin\AdminController@show')->name('admin.admin.details');
    Route::get('/admin-permissions/{id}','Admin\AdminController@adminPermissions')->name('admin.admin.permissions');
    Route::post('/admin-permissions-edit/{id}','Admin\AdminController@adminPermissionsEdit')->name('admin.admin.permissions.edit');
    Route::get('/admin-permissions-quick-edit/{id}/{role}','Admin\AdminController@adminPermissionsQuickEdit')->name('admin.admin.permissions.quick.edit');

    /**
     * CLIENT ROUTES
     */
    Route::get('/client','Admin\ClientController@index')->name('admin.client');
    Route::get('/client-details/{id}','Admin\ClientController@show')->name('admin.client.details');

   /**
    * USER ROUTES
    */
    Route::get('/user','Admin\UserController@index')->name('admin.user');
    Route::get('/user-details/{id}','Admin\UserController@show')->name('admin.user.details');

    /**
     * ACTIVATION ROUTES
     */
    Route::get('/activation','Admin\ActivationController@index')->name('admin.activation');
    Route::get('/activation-details/{id}','Admin\ActivationController@show')->name('admin.activation.details');
    Route::get('/activation-reports/{id}','Admin\ActivationController@reports')->name('admin.activation.reports');

    Route::post('/interaction-details/{id}','Admin\ActivationController@interactionDetails')->name('interaction-details');
    Route::post('/sales-details','Admin\ActivationController@salesDetails')->name('sales-details');
    Route::post('/sales-entries-edit/{id}','Admin\ActivationController@salesEntryEdit')->name('sales-entries-edit');
    Route::post('/merchandise-details','Admin\ActivationController@merchandiseDetails')->name('merchandise-details');


    Route::get('/interaction-summary-report/{id}','Admin\ActivationController@interactionSummaryReport')->name('interaction-summary-report');
    Route::get('/interaction-ba-report/{id}','Admin\ActivationController@interactionBaReport')->name('interaction-ba-report');
    Route::get('/interaction-entries-report/{id}','Admin\ActivationController@interactionEntriesReport')->name('interaction-entries-report');
    Route::get('/interaction-details-summary-report/{id}/{date}','Admin\ActivationController@interactionDetailsSummaryReport')->name('interaction-details-summary-report');
    Route::get('/interaction-details-ba-report/{id}/{date}','Admin\ActivationController@interactionDetailsBaReport')->name('interaction-details-ba-report');
    Route::get('/interaction-details-entries-report/{id}/{date}','Admin\ActivationController@interactionDetailsEntriesReport')->name('interaction-details-entries-report');

    Route::get('/sales-summary-report/{id}','Admin\ActivationController@salesSummaryReport')->name('sales-summary-report');
    Route::get('/sales-ba-report/{id}','Admin\ActivationController@salesBaReport')->name('sales-ba-report');
    Route::get('/sales-entries-report/{id}','Admin\ActivationController@salesEntriesReport')->name('sales-entries-report');
    Route::get('/sales-details-summary-report/{id}/{date}','Admin\ActivationController@salesDetailsSummaryReport')->name('sales-details-summary-report');
    Route::get('/sales-details-ba-report/{id}/{date}','Admin\ActivationController@salesDetailsBaReport')->name('sales-details-ba-report');
    Route::get('/sales-details-entries-report/{id}/{date}','Admin\ActivationController@salesDetailsEntriesReport')->name('sales-details-entries-report');
    Route::get('/sales-details-summary-entries-raw-report/{id}/{date}','Admin\ActivationController@salesDetailsSummaryRawReport')->name('sales-details-summary-entries-raw-report');
    Route::get('/sales-summary-entries-raw-report/{id}','Admin\ActivationController@salesSummaryRawReport')->name('sales-summary-entries-raw-report');

    Route::get('/merchandise-summary-report/{id}','Admin\ActivationController@merchandiseSummaryReport')->name('merchandise-summary-report');
    Route::get('/merchandise-ba-report/{id}','Admin\ActivationController@merchandiseBaReport')->name('merchandise-ba-report');
    Route::get('/merchandise-entries-report/{id}','Admin\ActivationController@merchandiseEntriesReport')->name('merchandise-entries-report');
    Route::get('/merchandise-details-summary-report/{id}/{date}','Admin\ActivationController@merchandiseDetailsSummaryReport')->name('merchandise-details-summary-report');
    Route::get('/merchandise-details-ba-report/{id}/{date}','Admin\ActivationController@merchandiseDetailsBaReport')->name('merchandise-details-ba-report');
    Route::get('/merchandise-details-entries-report/{id}/{date}','Admin\ActivationController@merchandiseDetailsEntriesReport')->name('merchandise-details-entries-report');

    Route::get('/ba-performance-report/{id}','Admin\ActivationController@baPerformanceReport')->name('ba-performance-report');
    Route::get('/activation-payroll-report/{id}','Admin\ActivationController@activationPayroll')->name('activation-payroll-report');
    Route::post('/activation-payroll-selected-payroll','Admin\ActivationController@activationSelectedPayroll')->name('activation-payroll-selected-payroll');



    /**
     * PRODUCTS ROUTES
     */
    Route::get('/product','Admin\ProductController@index')->name('admin.product');
    Route::get('/product-details/{id}/{activation_id}','Admin\ProductController@show')->name('admin.product.details');

    /**
     * MERCHANDISE ROUTES
     */
    Route::get('/merchandise','Admin\ProductController@index')->name('admin.merchandise');
    Route::get('/merchandise-details/{id}/{activation_id}','Admin\MerchandiseController@show')->name('admin.merchandise.details');

    /**
     * LOCATION ROUTES
     */
    Route::get('/location','Admin\LocationController@index')->name('admin.location');
    Route::get('/location-details/{id}/{activation_id}','Admin\LocationController@show')->name('admin.location.details');

    /**
     * TARGETS ROUTES
     */
    Route::get('/target','Admin\ActivationTargetController@index')->name('admin.target');
    Route::get('/target-details/{id}/{activation_id}','Admin\ActivationTargetController@show')->name('admin.target.details');

    /**
     * ACTVATION TYPE ROUTES
     */
    Route::get('/activation-type','Admin\ActivationTypeController@index')->name('admin.activation.type');
    Route::get('/activation-type-details/{id}/{activation_id}','Admin\ActivationTypeController@show')->name('admin.activation.type.details');


    /**
     * DIVISION ROUTES
     */
    Route::get('/division','Admin\DivisionController@index')->name('admin.division');
    Route::get('/division-details/{id}','Admin\DivisionController@show')->name('admin.division.details');


    /**
     * REGION ROUTES
     */
    Route::get('/region/{id}','Admin\RegionController@index')->name('admin.region');
    Route::get('/region-details/{id}/{division_id}','Admin\RegionController@show')->name('admin.region.details');


    /**
     * STOCK ROUTES
     */
    Route::get('/stock-details/{id}','Admin\ActivationProductStockController@show')->name('admin.region.details');


    /**
     * STOCK ROUTES
     */
    Route::get('/product-summary-details/{id}','Admin\ActivationOutletProductSummaryController@show')->name('admin.region.details');

    /**
     * EXPENSES ROUTES
     */
    Route::get('/expenses','Admin\ExpensesController@index')->name('admin.expenses');
    Route::get('/expenses-details/{id}','Admin\ExpensesController@show')->name('admin.expenses.details');
    Route::get('/expenses-requisition-pdf/{id}','Admin\ExpensesController@expensesReportPdf')->name('admin.expenses.requisition.pdf');
    Route::get('/expense-requisition-status/{id}/{requisition_status}','Admin\ExpensesController@requisitionStatusChange')->name('admin.expense.requisition.status');
    Route::get('/expense-reconciliation-status/{id}/{reconciliation_status}','Admin\ExpensesController@reconciliationStatusChange')->name('admin.expense.reconciliation.status');
    Route::get('/expense-give-amount/{id}','Admin\ExpensesController@giveAmount')->name('admin.expense.give.amount');


    /**
     * LEAVE ROUTES
     */
    Route::get('/leaves','Admin\LeaveController@index')->name('admin.leaves');
    Route::get('/leave-details/{id}','Admin\LeaveController@show')->name('admin.leave.details');
    Route::get('/leave-status/{id}/{status}','Admin\LeaveController@leaveStatusChange')->name('admin.leave.status');

    /**
     * PAYSLIP ROUTES
     */
    Route::get('/payslips','Admin\PayslipController@index')->name('admin.payslips');
    Route::get('/create-payslip','Admin\PayslipController@create')->name('admin.payslip.create');
    Route::get('/payslip-details/{id}','Admin\PayslipController@show')->name('admin.payslip.details');
    Route::get('/admin.payslip.pdf/{id}','Admin\PayslipController@generatePDF')->name('admin.payslip.pdf');

    // IMAGES GET
    Route::get('/image-get-reconciliation/{fileName}', 'ApiController@getReconciliationImage')->name('image-get-reconciliation');
    Route::get('/image-get-leave/{fileName}', 'ApiController@getLeaveAttachment')->name('image-get-leave');

    /**
     * ADMIN PROFILE
     */

    Route::get('/admin-profile','Admin\ProfileController@index')->name('admin.profile');
    Route::post('/admin-password-change','Admin\ProfileController@passwordChange')->name('admin.password.change');


    // PLANNERS ROUTES
    // Route::get('/planners','Admin\PlannersController@index')->name('admin.planners');


   
    // QUOTES ROUTES
   

    Route::get('/quotes','Admin\QuotesController@index')->name('admin.quotes');
    Route::post('/quotes', 'Admin\QuotesController@store')->name('admin.quotes');
    Route::get('/quote/{id}','Admin\QuotesController@show')->name('admin.quote.show'); 
    Route::get('/quoteEdit/{id}','Admin\QuotesController@quoteEdit')->name('admin.quote.edit');
    Route::put('/quoteUpdate/{id}','Admin\QuotesController@quoteUpdate')->name('admin.quote.update');
    Route::delete('/quoteDelete/{id}','Admin\QuotesController@delete');
    Route::post('/quoteItems/{id}','Admin\QuotesController@quoteItem');
    Route::put('/agencyFee/{id}', 'Admin\QuotesController@agencyFee')->name('admin.quote.agencyFee');
    Route::get('/pdf/{id}', 'Admin\QuotesController@downloadPDF');

    /**
     * PERFORMANCE ROUTES
     */
    Route::get('/performance-reports','Admin\PerformanceReportController@index')->name('admin.performance.reports');
    Route::get('/performance-report-details/{id}','Admin\PerformanceReportController@show')->name('admin.performance.details');
    Route::get('/performance-report-xls/{start_date}/{end_date}','Admin\PerformanceReportController@downloadReport')->name('admin.performance.report.xls');


});

Route::prefix('client')->group(function () {
    Route::get('/', 'Client\ActivationController@index')->name('client.dashboard');
    Route::get('/login', 'Auth\ClientLoginController@showLoginForm')->name('client.login');
    Route::post('/login', 'Auth\ClientLoginController@login')->name('client.login.submit');
    Route::get('/logout', 'Auth\ClientLoginController@logout')->name('client-logout');

    /**
     * ACTIVATION ROUTES
     */
    Route::get('/activation','Client\ActivationController@index')->name('client.activation');
    Route::get('/activation-details/{id}','Client\ActivationController@show')->name('client.activation.details');
    Route::get('/activation-reports/{id}','Client\ActivationController@reports')->name('client.activation.reports');

    Route::post('/client-interaction-details/{id}','Client\ActivationController@interactionDetails')->name('client.interaction-details');
    Route::post('/client-sales-details','Client\ActivationController@salesDetails')->name('client.sales-details');
    Route::post('/merchandise-details','Client\ActivationController@merchandiseDetails')->name('client.merchandise-details');

    

});

/**
 * LOCATION MAP
 */
Route::get('/google-maps-location/{id}','MapController@index')->name('google-maps-location');

/**
 * IMAGE
 */
Route::get('/activation-image/{fileName}','ImageController@getActivationImage')->name('activation-image');



/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');


window.base_url = "http://192.81.215.21/api/";
window.base_url_web = "http://192.81.215.21";
window.base_url_web_admin = "http://192.81.215.21/admin/";

// window.base_url = "http://localhost/alfones-hub/api/";
// window.base_url_web = "http://localhost/alfones-hub";
// window.base_url_web_admin = "http://localhost/alfones-hub/admin/";

// window.base_url = "http://localhost/hub/api/";
// window.base_url_web = "http://localhost/hub";
// window.base_url_web_admin = "http://localhost/hub/admin/";
window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('admin-component', require('./components/admin/AdminComponent.vue'));
Vue.component('admin-details-component', require('./components/admin/AdminDetailsComponent.vue'));
Vue.component('division-component', require('./components/division/DivisionComponent.vue'));
Vue.component('division-details-component', require('./components/division/DivisionDetailsComponent.vue'));
Vue.component('region-component', require('./components/division/region/RegionComponent.vue'));
Vue.component('region-details-component', require('./components/division/region/RegionDetailsComponent.vue'));
Vue.component('client-component', require('./components/client/ClientComponent.vue'));
Vue.component('client-details-component', require('./components/client/ClientDetailsComponent.vue'));
Vue.component('user-component', require('./components/user/UserComponent.vue'));
Vue.component('user-details-component', require('./components/user/UserDetailsComponent.vue'));
Vue.component('activation-component', require('./components/activation/ActivationComponent.vue'));
Vue.component('activation-details-component', require('./components/activation/ActivationDetailsComponent.vue'));
Vue.component('activation-product-component', require('./components/activation/product/ProductComponent.vue'));
Vue.component('activation-product-details-component', require('./components/activation/product/ProductEditComponent.vue'));
Vue.component('activation-type-component', require('./components/activation/activation-type/ActivationTypeComponent.vue'));
Vue.component('activation-type-details-component', require('./components/activation/activation-type/ActivationTypeEditComponent.vue'));
Vue.component('activation-merchandise-component', require('./components/activation/merchandise/MerchandiseComponent.vue'));
Vue.component('activation-merchandise-details-component', require('./components/activation/merchandise/MerchandiseEditComponent.vue'));
Vue.component('activation-location-component', require('./components/activation/location/LocationComponent.vue'));
Vue.component('activation-location-details-component', require('./components/activation/location/LocationEditComponent.vue'));
Vue.component('activation-team-leader-component', require('./components/activation/team-leader/TeamLeaderComponent.vue'));
Vue.component('activation-user-component', require('./components/activation/user/ActivationUserComponent.vue'));
Vue.component('activation-target-component', require('./components/activation/target/TargetComponent.vue'));
Vue.component('activation-target-details-component', require('./components/activation/target/TargetDetailsComponent.vue'));
Vue.component('activation-product-stock-component', require('./components/activation/reports/sales/ProductStockComponent.vue'));
Vue.component('activation-product-stock-details-component', require('./components/activation/reports/sales/ProductStockDetailsComponent.vue'));
Vue.component('activation-product-summary-component', require('./components/activation/reports/sales/OutletSummaryComponent.vue'));
Vue.component('activation-product-summary-details-component', require('./components/activation/reports/sales/OutletSummaryDetailsComponent.vue'));
Vue.component('activation-expenses-component', require('./components/expenses/ExpensesComponent.vue'));
Vue.component('expenses-give-amount-component', require('./components/expenses/ExpenseGiveAmount.vue'));
Vue.component('payslip-component', require('./components/payslips/PayslipComponent.vue'));
Vue.component('create-payslip-component', require('./components/payslips/CreatePayslipComponent.vue'));
Vue.component('performance-component', require('./components/performance/PerformanceComponent.vue'));
Vue.component('performance-details-component', require('./components/performance/PerformanceDetailsComponent.vue'));


Vue.component('interaction-chart-component', require('./components/activation/reports/interactions/InteractionSummaryComponent.vue'));
Vue.component('sales-chart-component', require('./components/activation/reports/sales/SalesSummaryComponent.vue'));
Vue.component('product-sales-chart-component', require('./components/activation/reports/sales/ProductSalesChartComponent.vue'));
Vue.component('merchandise-chart-component', require('./components/activation/reports/merchandise/MerchandiseSummaryComponent.vue'));
Vue.component('merchandise-distribution-chart-component', require('./components/activation/reports/merchandise/MerchandiseDistributionChartComponent.vue'));



const app = new Vue({
    el: '#app'
});

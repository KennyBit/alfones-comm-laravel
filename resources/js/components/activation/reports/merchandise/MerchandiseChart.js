import axios from 'axios'
import { Line } from 'vue-chartjs'

export default ({
    extends: Line,

    props: {
        activation: String,
    },

    mounted () {

        let uri = base_url+`admin-merchandise-graph/`+this.activation;
        axios.get(uri)
            .then(resp => {
                this.merchandise = resp.data.merchandise
                this.labels = resp.data.labels
                this.setUpGraph()
            })
    },
    data () {
        return {
            merchandise: [],
            labels: []
        }
    },

    methods: {
        setUpGraph() {
            this.renderChart({
                labels: this.labels,
                datasets: [
                    { label: 'Merchandise (Last 7 days)', backgroundColor: '#0f9125', data: this.merchandise }
                ]
            }, {responsive: false, maintainAspectRatio: false})
        }
    },
})
import { Doughnut } from 'vue-chartjs'

export default ({
    extends: Doughnut,

    props: {
        activation: String,
    },

    mounted () {

        let uri = base_url+`admin-merchandise-distribution-graph/`+this.activation;
        axios.get(uri)
            .then(resp => {
                this.distributions = resp.data.distributions
                this.labels = resp.data.labels
                this.setUpGraph()
            })
    },
    data () {
        return {
            distributions: [],
            labels: []
        }
    },

    methods: {
        setUpGraph() {
            this.renderChart({
                labels: this.labels,
                datasets: [
                    { label: 'Distribution by merchandise',  backgroundColor: ['#d44814','#ff0d20', '#9c9c9c', '#d4ce00', '#d44814','#5a86d4','#bad400'], data: this.distributions }
                ]
            }, {responsive: false, maintainAspectRatio: false})
        }
    },
})
import { Doughnut } from 'vue-chartjs'

export default ({
    extends: Doughnut,

    props: {
        activation: String,
    },

    mounted () {

        let uri = base_url+`admin-product-sales-graph/`+this.activation;
        axios.get(uri)
            .then(resp => {
                this.sales = resp.data.sales
                this.labels = resp.data.labels
                this.setUpGraph()
            })
    },
    data () {
        return {
            sales: [],
            labels: []
        }
    },

    methods: {
        setUpGraph() {
            this.renderChart({
                labels: this.labels,
                datasets: [
                    { label: 'Sales by products',  backgroundColor: ['#d4ce00', '#d44814','#5a86d4','#9c9c9c','#0f9125','#ff0d20','#bad400'], data: this.sales }
                ]
            }, {responsive: false, maintainAspectRatio: false})
        }
    },
})
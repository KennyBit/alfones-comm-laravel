import axios from 'axios'
import { Line } from 'vue-chartjs'

export default ({
    extends: Line,

    props: {
        activation: String,
    },

    mounted () {

        let uri = base_url+`admin-sales-graph/`+this.activation;
        axios.get(uri)
            .then(resp => {
                this.sales = resp.data.sales
                this.labels = resp.data.labels
                this.setUpGraph()
            })
    },
    data () {
        return {
            sales: [],
            labels: []
        }
    },

    methods: {
        setUpGraph() {
            this.renderChart({
                labels: this.labels,
                datasets: [
                    { label: 'Sales (Last 7 days)', backgroundColor: '#5a86d4', data: this.sales }
                ]
            }, {responsive: false, maintainAspectRatio: false})
        }
    },
})
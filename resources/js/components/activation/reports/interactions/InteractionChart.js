import axios from 'axios'
import { Line } from 'vue-chartjs'

export default ({
    extends: Line,

    props: {
        activation: String,
    },

    mounted () {

        let uri = base_url+`admin-interaction-graph/`+this.activation;
        axios.get(uri)
            .then(resp => {
                this.interactions = resp.data.interactions
                this.labels = resp.data.labels
                this.setUpGraph()
            })
    },
    data () {
        return {
            interactions: [],
            labels: []
        }
    },

    methods: {
        setUpGraph() {
            this.renderChart({
                labels: this.labels,
                datasets: [
                    { label: 'Interactions (Last 7 days)', backgroundColor: '#f87979', data: this.interactions }
                ]
            }, {responsive: false, maintainAspectRatio: false})
        }
    },
})
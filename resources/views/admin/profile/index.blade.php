@extends('app-admin')
@section('content')
    <div class="content p-4">
            <h2 class="mb-4">Admin profile</h2>

            <div class="row">
                <div class="col-5">
                    <div class="card mb-4">
                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <form action="{{ route('admin.password.change') }}" method="post">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-sm">
                                        <div class="form-group">
                                            <label for="password">New password</label>
                                            <input type="password" class="form-control" id="password" name="password" aria-describedby="name_help" placeholder="Enter password">
                                            <small id="name_help" class="form-text text-muted">Enter password</small>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm">
                                        <button type="submit" class="btn btn-primary">Change password</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection

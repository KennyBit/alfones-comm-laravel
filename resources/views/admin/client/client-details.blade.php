@extends('app-admin')
@section('content')

    @can('client-details')
        <div class="content p-4">
            <h2 class="mb-4">Client edit</h2>

            <div class="row">
                <div class="col-5">
                    <div class="card mb-4">
                        <client-details-component client="{{ $client }}" can_edit="{{ $canEdit }}" can_delete="{{ $canDelete }}"></client-details-component>
                    </div>
                </div>
            </div>

        </div>
    @endcan

@endsection

@extends('app-admin')
@section('content')

    <div class="content p-4">
        <h2 class="mb-4">Client</h2>

        <div class="card mb-4">
            <div class="card-body">
                Monitor your clients here,
            </div>
        </div>

        <div class="row">
            @can('client-create')
                <div class="col-5">
                    <div class="card mb-4">
                        <client-component></client-component>
                    </div>

                </div>
            @endcan

            @can('client-list')
                <div class="col-7">
                    <div class="card mb-4">
                        <div class="card-body">
                            <table id="example" class="table table-hover" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th style="width: 40%;">Name</th>
                                    <th>Email</th>
                                    <th>Address</th>
                                    @can('client-details')
                                        <th class="actions">Actions</th>
                                    @endcan
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($clients as $client)
                                    <tr>
                                        <td>{{ $loop->iteration  }}</td>
                                        <td style="width: 40%;">{{ $client->company_name }}</td>
                                        <td>{{ $client->address }}</td>
                                        <td>{{ $client->email }}</td>

                                        @can('client-details')
                                            <td>
                                                <a href="{{ route('admin.client.details', ['id' => $client->id]) }}" class="btn btn-icon btn-pill btn-primary" data-toggle="tooltip" title="Edit"><i class="fa fa-fw fa-edit"></i></a>
                                            </td>
                                        @endcan
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            @endcan

        </div>

    </div>


@endsection

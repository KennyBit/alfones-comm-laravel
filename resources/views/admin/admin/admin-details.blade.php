@extends('app-admin')
@section('content')

    @can('admin-details')
        <div class="content p-4">
            <h2 class="mb-4">Admin edit</h2>

            <div class="row">
                <div class="col-5">
                    <div class="card mb-4">
                        <admin-details-component admin="{{ $admin }}" can_edit="{{ $canEdit }}" can_delete="{{ $canDelete }}" can_permission="{{ $canPermission }}"></admin-details-component>
                    </div>
                </div>
                <div class="col-7">
                    <div class="card mb-4">
                        <div class="card-body">
                            <table id="example" class="table table-hover" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th style="width: 5%;">#</th>
                                    <th style="width: 30%;">Log name</th>
                                    <th style="width: 45%;">Log description</th>
                                    <th style="width: 20%;">Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($logs as $log)
                                    <tr>
                                        <td>{{ $loop->iteration  }}</td>
                                        <td>{{ $log->log_name }}</td>
                                        <td>{{ $log->description }}</td>
                                        <td>{{ $log->created_at }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    @endcan

@endsection

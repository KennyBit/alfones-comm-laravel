@extends('app-admin')
@section('content')

    <div class="content p-4">
        <h2 class="mb-4">Admin permissions</h2>

        <div class="row">
            <div class="col-6">
                <div class="card mb-6">
                    <div class="card-body">
                        <a href="{{ route('admin.admin.permissions.quick.edit', ['id'=>$admin, 'role'=>'1']) }}">Super admin</a> | <a href="{{ route('admin.admin.permissions.quick.edit', ['id'=>$admin, 'role'=>'2']) }}">Admin</a> | <a href="{{ route('admin.admin.permissions.quick.edit', ['id'=>$admin, 'role'=>'3']) }}">Project Manager</a> | <a
                                href="{{ route('admin.admin.permissions.quick.edit', ['id'=>$admin, 'role'=>'4']) }}">Team leader</a> | <a href="{{ route('admin.admin.permissions.quick.edit', ['id'=>$admin, 'role'=>'5']) }}">Finance</a>
                        <form method="POST" action="{{ route('admin.admin.permissions.edit', [$admin]) }}">
                            {{ csrf_field() }}
                            @foreach ($permissions as $permission)
                                <div class="row" style="margin-top: 10px;">
                                    <div class="col-sm-3">
                                        {{ $permission['name'] }}
                                    </div>
                                    @if($permission['has_permission'] == 1)
                                        <div class="col-sm-2">
                                             <input type="checkbox" name="user_permissions[]" value="{{ $permission['name'] }}" checked>
                                        </div>
                                    @else
                                        <div class="col-sm-2">
                                             <input type="checkbox" name="user_permissions[]" value="{{ $permission['name'] }}">
                                        </div>
                                    @endif
                                </div>
                            @endforeach
                            <div class="row" style="margin-top: 10px;">
                                <div class="col-sm">
                                    <button type="submit" class="btn btn-primary">Update permissions</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>

    </div>

@endsection

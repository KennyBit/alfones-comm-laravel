@extends('app-admin')
@section('content')

    <div class="content p-4">
        <h2 class="mb-4">Admins</h2>

        <div class="card mb-4">
            <div class="card-body">
                Monitor your admins here,
            </div>
        </div>

        <div class="row">
            @can('admin-create')
                <div class="col-5">
                    <div class="card mb-4">
                        <admin-component></admin-component>
                    </div>

                </div>
            @endcan

            @can('admin-list')
                <div class="col-7">
                    <div class="card mb-4">
                        <div class="card-body">
                            <table id="example" class="table table-hover" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th style="width: 40%;">Name</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    @can('admin-details')
                                        <th class="actions">Actions</th>
                                    @endcan
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($admins as $admin)
                                    <tr>
                                        <td>{{ $loop->iteration  }}</td>
                                        <td style="width: 40%;">{{ $admin->name }}</td>
                                        <td>{{ $admin->email }}</td>
                                        <td>
                                            @if($admin->role =='1')
                                                Super Admin
                                            @elseif($admin->role =='2')
                                                Admin
                                            @elseif($admin->role =='3')
                                                Project manager
                                            @elseif($admin->role =='4')
                                                Team leader
                                            @elseif($admin->role =='5')
                                                Finance
                                            @endif
                                        </td>
                                        @can('admin-details')
                                            <td>
                                                <a href="{{ route('admin.admin.details', ['id' => $admin->id]) }}" class="btn btn-icon btn-pill btn-primary" data-toggle="tooltip" title="Edit"><i class="fa fa-fw fa-edit"></i></a>
                                            </td>
                                        @endcan
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            @endcan

        </div>

    </div>


@endsection

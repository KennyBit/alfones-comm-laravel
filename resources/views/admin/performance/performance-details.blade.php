@extends('app-admin')
@section('content')

    @can('division-list')
        <div class="content p-4">
            <h2 class="mb-4">Performance edit</h2>

            <div class="row">
                <div class="col-5">
                    <div class="card mb-4">
                        <performance-details-component performance="{{ $performance }}" can_edit="{{ $canEdit }}" can_delete="{{ $canDelete }}"></performance-details-component>
                    </div>
                </div>
            </div>

        </div>
    @endcan

@endsection

@extends('app-admin')
@section('content')

    <div class="content p-4">
        <h2 class="mb-4">Performance Analysis</h2>

        <performance-component can_list="{{ $canList }}" can_create="{{ $canCreate }}" can_details="{{ $canDetails }}"></performance-component>

    </div>


@endsection

@extends('app-admin')
@section('content')

    @can('expenses-list')

        <div class="content p-4">
            <h2 class="mb-4">Expenses Centre</h2>

            <div class="row">

                <div class="col-10">
                    <div class="card mb-4">
                        <div class="card-body">
                            <h5 class="card-title">Recent expenses</h5>
                            <table id="example1" class="table table-hover" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th style="width: 30%;">Requisition title</th>
                                    <th style="width: 25%;">User</th>
                                    <th>Amount</th>
                                    <th>Required date</th>
                                    <th>Status</th>
                                    <th>Type</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($recent_expenses as $recent_expense)
                                    <tr>
                                        <td>{{ $recent_expense['expenses_id']  }}</td>

                                        @can('expenses-details')
                                            <td style="width: 30%;"><a href="{{ route('admin.expenses.details', [$recent_expense['id']]) }}">{{ $recent_expense['requisition_title'] }}</a></td>
                                        @else
                                            <td style="width: 30%;">{{ $recent_expense['requisition_title'] }}</td>
                                        @endcan

                                        <td style="width: 25%;"> {{ $recent_expense['admin_name'] }} </td>
                                        <td>  {{ $recent_expense['amount'] }} </td>
                                        <td> {{ $recent_expense['required_date'] }} </td>
                                        <td>
                                            @if ($recent_expense['requisition_status'] === 1)

                                                <span class="badge badge-success">Approved</span>

                                            @elseif ($recent_expense['requisition_status'] === 0)

                                                <span class="badge badge-warning">Pending</span>

                                            @elseif ($recent_expense['requisition_status'] === 2)

                                                <span class="badge badge-danger">Declined</span>

                                            @endif
                                        </td>
                                        <td>
                                            @if ($recent_expense['expense_type'] === 1)
                                                 {{ $recent_expense['activation_name'] }}
                                            @else
                                                General Expenses
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab_container">
                <input id="tab1" type="radio" name="tabs" checked>
                <label for="tab1" class="label-tab"><span>Activation Expenses</span></label>

                <input id="tab2" type="radio" name="tabs">
                <label for="tab2" class="label-tab"><span>General Expenses</span></label>

                <section id="content1" class="tab-content">
                    <h4 class="mb-4" style="color: #c7254e">Activation Expenses</h4>

                    <activation-expenses-component></activation-expenses-component>

                </section>

                <section id="content2" class="tab-content">
                    <h4 class="mb-4" style="color: #c7254e">General Expenses</h4>

                    <div class="row">
                        <div class="col-10">
                            <div class="card mb-4">
                                <div class="card-body">
                                    <table id="example1" class="table table-hover" cellspacing="0" width="100%">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th style="width: 30%;">Requisition title</th>
                                            <th style="width: 25%;">User</th>
                                            <th>Amount</th>
                                            <th>Required date</th>
                                            <th>Status</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($general_expenses as $general_expense)
                                            <tr>
                                                <td>{{ $general_expense['expenses_id']  }}</td>

                                                @can('expenses-details')
                                                    <td style="width: 30%;"><a href="{{ route('admin.expenses.details', [$general_expense['id']]) }}">{{ $general_expense['requisition_title'] }}</a></td>
                                                @else
                                                    <td style="width: 30%;">{{ $general_expense['requisition_title'] }}</td>
                                                @endcan

                                                <td style="width: 25%;"> {{ $general_expense['admin_name'] }} </td>
                                                <td>  {{ $general_expense['amount'] }} </td>
                                                <td> {{ $general_expense['required_date'] }} </td>
                                                <td>
                                                    @if ($general_expense['requisition_status'] === 1)

                                                        <span class="badge badge-success">Approved</span>

                                                    @elseif ($general_expense['requisition_status'] === 0)

                                                        <span class="badge badge-warning">Pending</span>

                                                    @elseif ($general_expense['requisition_status'] === 2)

                                                        <span class="badge badge-danger">Declined</span>

                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </section>

            </div>
        </div>

    @endcan

@endsection

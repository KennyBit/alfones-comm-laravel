@extends('app-admin')
@section('content')

    @can('expenses-details')
        <div class="content p-4">
            <h2 class="mb-4">Give Amount</h2>

            <div class="row">
                <div class="col-5">
                    <div class="card mb-4">
                        <expenses-give-amount-component expense_id="{{ $expense_id }}"></expenses-give-amount-component>
                    </div>
                </div>
            </div>

        </div>
    @endcan

@endsection

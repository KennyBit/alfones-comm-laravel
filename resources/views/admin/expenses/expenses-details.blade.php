@extends('app-admin')
@section('content')

    @can('expenses-details')

        <div class="content p-4">
        <h2 class="mb-4">Expenses no #{{ $expenses->expenses_id }}</h2>
        
            <div class="row">
            <div class="col-8">
                <div class="card mb-6">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm">
                                <h5><b>User details</b></h5>
                                <hr>

                                Name
                                <br><b> {{ $admin->name }}</b><br><br>
                                Phone number
                                <br><b> {{ $admin->phone_number }}</b><br><br>
                                Email
                                <br><b> {{ $admin->email }}</b>
                            </div>
                            <div class="col-sm">
                                <h5><b>Expenses details</b></h5>
                                <hr>
                                <b>Expense #{{ $expenses->expenses_id }}</b><br>

                                @can('expenses-printout')
                                     <a href="{{ route('admin.expenses.requisition.pdf', [$expenses->id]) }}" target="_blank">Print requistion</a>
                                @endcan


                                <br><br>
                                Total amount <br>
                                <b>Kshs {{ $expenses_amount }}</b>

                                <br><br>
                                Requisition status <br>

                                @if ($expenses->requisition_status === 1)

                                    <span class="badge badge-success">Approved</span>

                                @elseif ($expenses->requisition_status === 0)

                                    <span class="badge badge-warning">Pending</span>

                                @elseif ($expenses->requisition_status === 2)

                                    <span class="badge badge-danger">Declined</span>

                                @endif
                            </div>
                        </div>

                        <br><br>
                        <div class="row">
                            <div class="col-sm">
                                <h5><b>Expenses items</b></h5>
                                <table class="table table-hover" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>Supplier</th>
                                        <th style="width: 20%;">Item</th>
                                        <th style="width: 25%;">Description</th>
                                        <th>Quantity</th>
                                        <th>Unit Cost</th>
                                        <th>Days</th>
                                        <th>Total</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($expenses_items as $expenses_item)
                                        <tr>
                                            <td>{{ $loop->iteration  }}</td>
                                            <td> {{ $expenses_item->item_supplier }} </td>
                                            <td> {{ $expenses_item->item_name }} </td>
                                            <td> {{ $expenses_item->item_description }} </td>
                                            <td> {{ $expenses_item->item_quantity }} </td>
                                            <td> {{ $expenses_item->item_unit_cost }} </td>
                                            <td> {{ $expenses_item->item_days }} </td>
                                            <td> {{ $expenses_item->item_amount }} </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        @if ($expenses->requisition_status === 1)
                            <br><br>
                            <div class="row">
                                <div class="col-sm">
                                    <h5><b>Reconciliation items</b></h5>
                                    <table class="table table-hover" cellspacing="0" width="100%">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th>Supplier</th>
                                            <th style="width: 20%;">Item</th>
                                            <th style="width: 25%;">Description</th>
                                            <th>Quantity</th>
                                            <th>Unit Cost</th>
                                            <th>Days</th>
                                            <th>Total</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($reconciliation_items as $reconciliation_item)
                                            <tr>
                                                <td>{{ $loop->iteration  }}</td>
                                                <td> {{ $reconciliation_item->item_supplier }} </td>
                                                <td>
                                                    {{ $reconciliation_item->item_name }} <br>
                                                    <a href="">View attachment</a>
                                                </td>
                                                <td> {{ $reconciliation_item->item_description }} </td>
                                                <td> {{ $reconciliation_item->item_quantity }} </td>
                                                <td> {{ $reconciliation_item->item_unit_cost }} </td>
                                                <td> {{ $reconciliation_item->item_days }} </td>
                                                <td> {{ $reconciliation_item->item_amount }} </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <br>
                            Amount Given
                            <br><h4><b> Kshs {{ $balance }}</b></h4>
                            <br>
                            Balance
                            <br><h3><b> Kshs {{ $balance }}</b></h3>
                            <br>

                        @endif

                        @can('expenses-approve')
                             <br>
                            <div class="row">
                                <div class="col-sm">
                                    <div class="form-group">
                                        <label><b>Requisition status</b></label>
                                        @if ($expenses->requisition_status === 1)
                                            <h5><span class="badge badge-success">Approved</span></h5>
                                        @elseif ($expenses->requisition_status === 0)
                                            <h5><span class="badge badge-warning">Pending</span></h5>
                                        @elseif ($expenses->requisition_status === 2)
                                            <h5><span class="badge badge-danger">Declined</span></h5>
                                        @endif
                                        <br><br>
                                        <hr>
                                            @if ($expenses->requisition_status === 0)
                                                <a href="{{ route('admin.expense.requisition.status', ['id' => $expenses->id, 'requisition_status' => '1']) }}" class="btn btn-success">Approve</a>
                                                <a href="{{ route('admin.expense.requisition.status', ['id' => $expenses->id, 'requisition_status' => '2']) }}" class="btn btn-danger">Decline</a>
                                            @endif
                                    </div>
                                </div>
                                @if ($expenses->requisition_status === 1)
                                    <div class="col-sm">
                                        <div class="form-group">
                                            <label><b>Reconciliation status</b></label>
                                            @if ($expenses->reconciliation_status === 1)
                                                <h5><span class="badge badge-success">Approved</span></h5>
                                            @elseif ($expenses->reconciliation_status === 0)
                                                <h5><span class="badge badge-warning">Pending</span></h5>
                                            @elseif ($expenses->reconciliation_status === 2)
                                                <h5><span class="badge badge-danger">Declined</span></h5>
                                            @endif
                                            <br><br>
                                            <hr>
                                            @if ($expenses->reconciliation_status === 0)
                                                <a href="{{ route('admin.expense.give.amount', ['id' => $expenses->id]) }}" class="btn btn-primary">Give Amount</a>
                                                <a href="{{ route('admin.expense.reconciliation.status', ['id' => $expenses->id, 'reconciliation_status' => '1']) }}" class="btn btn-success">Approve</a>
                                                <a href="{{ route('admin.expense.reconciliation.status', ['id' => $expenses->id, 'reconciliation_status' => '2']) }}" class="btn btn-danger">Decline</a>
                                            @endif
                                        </div>
                                    </div>
                                @endif
                            </div>
                        @endcan
                    </div>
                </div>

            </div>
        </div>

    </div>

    @endcan

@endsection

@extends('app-admin')
@section('content')

    <div class="content p-4">
        <h2 class="mb-4">Users (Brand ambassadors)</h2>

        <div class="card mb-4">
            <div class="card-body">
                Monitor your users here,
            </div>
        </div>

        <div class="row">
            @can('ba-create')
                <div class="col-5">
                    <div class="card mb-4">
                        <user-component></user-component>
                    </div>

                </div>
            @endcan

            @can('ba-list')
                <div class="col-7">
                    <div class="card mb-4">
                        <div class="card-body">
                            <table id="example" class="table table-hover" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th style="width: 40%;">Name</th>
                                    <th>Phone number</th>
                                    <th>Address</th>
                                    @can('ba-details')
                                        <th class="actions">Actions</th>
                                    @endcan
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($users as $user)
                                    <tr>
                                        <td>{{ $loop->iteration  }}</td>
                                        <td style="width: 40%;">{{ $user->name }}</td>
                                        <td>{{ $user->phone_number }}</td>
                                        <td>{{ $user->email }}</td>

                                        @can('ba-details')
                                            <td>
                                                <a href="{{ route('admin.user.details', ['id' => $user->id]) }}" class="btn btn-icon btn-pill btn-primary" data-toggle="tooltip" title="Edit"><i class="fa fa-fw fa-edit"></i></a>
                                            </td>
                                        @endcan
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            @endcan

        </div>

    </div>


@endsection

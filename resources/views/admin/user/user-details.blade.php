@extends('app-admin')
@section('content')

    @can('admin-details')
        <div class="content p-4">
            <h2 class="mb-4">Admin edit</h2>

            <div class="row">
                <div class="col-5">
                    <div class="card mb-4">
                        <user-details-component user="{{ $user }}" can_edit="{{ $canEdit }}" can_delete="{{ $canDelete }}"></user-details-component>
                    </div>
                </div>
            </div>

        </div>
    @endcan

@endsection

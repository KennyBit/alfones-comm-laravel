<nav class="navbar navbar-expand navbar-dark bg-primary">
    <a class="sidebar-toggle mr-3" href="#"><i class="fa fa-bars"></i></a>
    <a class="navbar-brand" href="#">Alfones Hub</a>

    <div class="navbar-collapse collapse">
        <ul class="navbar-nav ml-auto">

            @if (Auth::check())
                <li>
                    <a href="{{ route('admin-logout') }}" id="dd_user" class="nav-link dropdown-toggle"><i class="fa fa-user"></i> Logout </a>
                </li>
            @endif
        </ul>
    </div>
</nav>
<div class="sidebar sidebar-dark bg-dark">
    <ul class="list-unstyled">
        <li><a href="{{ route('client.dashboard') }}"><i class="fa fa-fw fa-home"></i> Home</a></li>
        <li><a href="{{ route('client.activation') }}"><i class="fa fa-fw fa-user"></i> Activations</a></li>
    </ul>
</div>
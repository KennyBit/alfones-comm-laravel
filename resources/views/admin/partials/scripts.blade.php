<script src="{{ asset('js/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ url('public/js/app.js') }}"></script>

{{-- <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('js/datatables.min.js') }}"></script>
<script src="{{ asset('js/moment.min.js') }}"></script>
<script src="{{ asset('js/fullcalendar.min.js') }}"></script>
<script src="{{ asset('js/bootadmin.min.js') }}"></script> --}}


<script>
    $(document).ready(function () {
        $('#example').DataTable(
            {
                "pageLength": 10
            }
        );
    });
    $(document).ready(function () {
        $('#example1').DataTable(
            {
                "pageLength": 10
            }
        );
    });
    $(document).ready(function () {
        $('#example2').DataTable(
            {
                "pageLength": 10
            }
        );
    });
    $(document).ready(function () {
        $('#example-product').DataTable(
            {
                "pageLength": 5
            }
        );
    });
    $(document).ready(function () {
        $('#example-orders').DataTable(
            {
                "pageLength": 5
            }
        );
    });
    $(document).ready(function () {
        $('#example-revenue').DataTable(
            {
                "pageLength": 5
            }
        );
    });
</script>

<script>
    (adsbygoogle = window.adsbygoogle || []).push({
        google_ad_client: "ca-pub-4097235499795154",
        enable_page_level_ads: true
    });
</script>


<script>
    // This example adds a user-editable rectangle to the map.
    // When the user changes the bounds of the rectangle,
    // an info window pops up displaying the new bounds.

    var rectangle;
    var map;
    var infoWindow;

    function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: 0.8359277234674408, lng:  37.819817382812516},
            zoom: 5
        });
        var card = document.getElementById('pac-card');
        var input = document.getElementById('pac-input');
        var types = document.getElementById('type-selector');
        var strictBounds = document.getElementById('strict-bounds-selector');

        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

        var autocomplete = new google.maps.places.Autocomplete(input);

        // Bind the map's bounds (viewport) property to the autocomplete object,
        // so that the autocomplete requests use the current map bounds for the
        // bounds option in the request.
        autocomplete.bindTo('bounds', map);

        // Set the data fields to return when the user selects a place.
        autocomplete.setFields(
            ['address_components', 'geometry', 'icon', 'name']);

        var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
            map: map,
            anchorPoint: new google.maps.Point(0, -29)
        });

        autocomplete.addListener('place_changed', function() {
            infowindow.close();
            marker.setVisible(false);
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.
                window.alert("No details available for input: '" + place.name + "'");
                return;
            }

            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);  // Why 17? Because it looks good.
            }
            marker.setPosition(place.geometry.location);
            marker.setVisible(true);

            var address = '';
            if (place.address_components) {
                address = [
                    (place.address_components[0] && place.address_components[0].short_name || ''),
                    (place.address_components[1] && place.address_components[1].short_name || ''),
                    (place.address_components[2] && place.address_components[2].short_name || '')
                ].join(' ');
            }

            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
            document.getElementById("latitude").value = latitude;
            document.getElementById("longitude").value = longitude;

            infowindowContent.children['place-icon'].src = place.icon;
            infowindowContent.children['place-name'].textContent = place.name;
            infowindowContent.children['place-address'].textContent = address;
            infowindow.open(map, marker);
        });

        // Sets a listener on a radio button to change the filter type on Places
        // Autocomplete.
        function setupClickListener(id, types) {
            var radioButton = document.getElementById(id);
            radioButton.addEventListener('click', function() {
                autocomplete.setTypes(types);
            });
        }

        setupClickListener('changetype-all', []);
        setupClickListener('changetype-address', ['address']);
        setupClickListener('changetype-establishment', ['establishment']);
        setupClickListener('changetype-geocode', ['geocode']);

        document.getElementById('use-strict-bounds')
            .addEventListener('click', function() {
                console.log('Checkbox clicked! New state=' + this.checked);
                autocomplete.setOptions({strictBounds: this.checked});
            });
    }

//    function initMap() {
//        map = new google.maps.Map(document.getElementById('map'), {
//            center: {lat: 0.8359277234674408, lng:  37.819817382812516},
//            zoom: 5
//        });
//
//        var bounds = {
//            north: 1.714607657346908,
//            south: -1.1076834333225414,
//            east: 39.6902333984375,
//            west: 36.281731445312516
//        };
//
//        // Define the rectangle and set its editable property to true.
//        rectangle = new google.maps.Rectangle({
//            bounds: bounds,
//            editable: true,
//            draggable: true
//        });
//
//        rectangle.setMap(map);
//
////        rectangle.addListener("click",getPoint());
//
//
//
//
//        // Add an event listener on the rectangle.
//        rectangle.addListener('bounds_changed', showNewRect);
//
//        // Define an info window on the map.
//        infoWindow = new google.maps.InfoWindow();
//    }

    //    function getPoint(e) {
    //        var clicked_ne = rectangle.getBounds().getNorthEast();
    //        alert(clicked_ne);
    //    }
    // Show the new coordinates for the rectangle in an info window.

    /** @this {google.maps.Rectangle} */
    function showNewRect(event) {
        var ne = rectangle.getBounds().getNorthEast();
        var sw = rectangle.getBounds().getSouthWest();
        document.getElementById("latitude").value = ne;
        document.getElementById("longitude").value = sw;

        var contentString = '<b>Rectangle moved.</b><br>' +
            'New north-east corner: ' + ne.lat() + ', ' + ne.lng() + '<br>' +
            'New south-west corner: ' + sw.lat() + ', ' + sw.lng();

        // Set the info window's content and position.
        infoWindow.setContent(contentString);
        infoWindow.setPosition(ne);

        infoWindow.open(map);
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCTozu2WHeCom_NlRG_IamkkRANGEyrPKE&libraries=places&callback=initMap"
        async defer></script>

<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script>
    $(function() {
    $( "#datepicker" ).datepicker();
  });
</script>
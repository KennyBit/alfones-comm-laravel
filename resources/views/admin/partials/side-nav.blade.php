<div class="sidebar sidebar-dark bg-dark">
    <ul class="list-unstyled">
        <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-fw fa-home"></i> Home</a></li>
    </ul>

    <ul class="list-unstyled">
        @can('admin-list')
                <li><a href="{{ route('admin.admin') }}"><i class="fa fa-fw fa-user"></i> Admin</a></li>
        @endcan

        @can('client-list')
                <li><a href="{{ route('admin.client') }}"><i class="fa fa-fw fa-user"></i> Clients</a></li>
        @endcan

        @can('ba-list')
                <li><a href="{{ route('admin.user') }}"><i class="fa fa-fw fa-user"></i> User (Brand Ambassadors)</a></li>
        @endcan
    </ul>
    <ul class="list-unstyled">
        @can('division-list')
            <li><a href="{{ route('admin.division') }}"><i class="fa fa-fw fa-user"></i> Divisions</a></li>
        @endcan

        @can('activation-list')
             <li><a href="{{ route('admin.activation') }}"><i class="fa fa-fw fa-user"></i> Activations</a></li>
        @endcan

        @can('expenses-list')
              <li><a href="{{ route('admin.expenses') }}"><i class="fa fa-fw fa-user"></i> Expenses</a></li>
        @endcan

        @can('leave-list')
            <li><a href="{{ route('admin.leaves') }}"><i class="fa fa-fw fa-user"></i> Leaves / Days off</a></li>
        @endcan

       
            <li><a href="{{ route('admin.quotes') }}"><i class="fa fa-fw fa-user"></i> Quotes</a></li>
       

        @can('payslip-list')
                <li><a href="{{ route('admin.payslips') }}"><i class="fa fa-fw fa-user"></i> Payslips</a></li>
        @endcan

        @can('admin-list')
            <li><a href="{{ route('admin.performance.reports') }}"><i class="fa fa-fw fa-chart-pie"></i> Performance Analysis</a></li>
        @endcan

            <li><a href="{{ route('admin.profile') }}"><i class="fa fa-fw fa-user"></i>Settings</a></li>

    </ul>
</div>
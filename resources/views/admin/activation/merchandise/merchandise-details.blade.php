@extends('app-admin')
@section('content')

    <div class="content p-4">
        <h2 class="mb-4">Merchandise edit</h2>

        <div class="row">
            <div class="col-5">
                <div class="card mb-4">
                    <activation-merchandise-details-component merchandise="{{ $merchandise }}" activation="{{ $activation }}" can_edit="{{ $canEdit }}" can_delete="{{ $canDelete }}"></activation-merchandise-details-component>
                </div>
            </div>
        </div>
    </div>

@endsection

@extends('app-admin')
@section('content')

    <div class="content p-4">
        <h2 class="mb-4">Activation type edit</h2>

        <div class="row">
            <div class="col-5">
                <div class="card mb-4">
                    <activation-type-details-component activation_type="{{ $activation_type }}" activation="{{ $activation }}" can_edit="{{ $canEdit }}" can_delete="{{ $canDelete }}"></activation-type-details-component>
                </div>
            </div>
        </div>
    </div>

@endsection

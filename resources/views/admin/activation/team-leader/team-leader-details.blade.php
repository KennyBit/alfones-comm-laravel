@extends('app-admin')
@section('content')

    <div class="content p-4">
        <h2 class="mb-4">Team leaders</h2>

        <div class="row">
            <div class="col-5">
                <div class="card mb-4">
                    <activation-team-leader-component activation="{{ $activation }}" activation="{{ $activation }}" can_edit="{{ $canEdit }}" can_delete="{{ $canDelete }}"></activation-team-leader-component>
                </div>
            </div>
        </div>
    </div>

@endsection

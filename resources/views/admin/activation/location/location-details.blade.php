@extends('app-admin')
@section('content')

    <div class="content p-4">
        <h2 class="mb-4">Location edit</h2>

        <div class="row">
            <div class="col-5">
                <div class="card mb-4">
                    <activation-location-details-component location="{{ $location }}" activation="{{ $activation }}" can_edit="{{ $canEdit }}" can_delete="{{ $canDelete }}"></activation-location-details-component>
                </div>
            </div>
        </div>
    </div>

@endsection

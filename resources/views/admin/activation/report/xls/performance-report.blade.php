<html>
<head>
    <style>
        table {
           border: 1px dashed #000;
        }
    </style>
</head>
    <body>
        <table>
            <tbody>
            <tr><th>ALFONES COMMUNICATIONS SOLUTIONS</th></tr>
            <tr><th>info@alfonesltd.com</th></tr>
            <tr></tr>
            <tr><th>{{ $start_date }} - {{ $end_date }}</th></tr>
            <tr><th>WEEKLY PERFORMANCE REPORT</th></tr>
            <tr style="background-color: #ffb53a; color: #FFFFFF;">
                <th>#</th>
                <th>Name</th>
                <th>On-time</th>
                <th>In-full</th>
                <th>No errors</th>
                <th>Target Achievement</th>
                <th>Overall Performance</th>
                <th>Percentage Performance</th>
                <th>Overall attendance</th>
                <th>Overall Performance</th>
                <th>Cumulative Performance</th>
            </tr>

            @foreach ($performances as $performance)
                <tr>

                    <td> </td>
                    <td> {{ $performance['name']  }}</td>

                    @if($performance['on_time'] == 'yes')
                        <td style="background-color: #22bd0e; color: #000000;"> yes </td>
                    @else
                        <td style="background-color: #ff3820; color: #000000;"> no </td>
                    @endif

                    @if($performance['in_full'] == 'yes')
                        <td style="background-color: #22bd0e; color: #000000;"> yes </td>
                    @else
                        <td style="background-color: #ff3820; color: #000000;"> no </td>
                    @endif

                    @if($performance['no_errors'] == 'yes')
                        <td style="background-color: #22bd0e; color: #000000;"> yes </td>
                    @else
                        <td style="background-color: #ff3820; color: #000000;"> no </td>
                    @endif

                    @if($performance['target_achievement'] == 'yes')
                        <td style="background-color: #22bd0e; color: #000000;"> yes </td>
                    @else
                        <td style="background-color: #ff3820; color: #000000;"> no </td>
                    @endif

                    <td> {{ $performance['overall_performance'] }} </td>
                    <td> {{ $performance['performance_percentage'] }} </td>
                    <td> {{ $performance['overall_attendance'] }} </td>
                    <td> {{ $performance['percentage_overall_performance'] }} </td>
                    <td> {{ $performance['cumulative_performance'] }} </td>

                </tr>
            @endforeach

            </tbody>
        </table>
    </body>
</html>
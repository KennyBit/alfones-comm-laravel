<html>
<head>
    <style>
        table {
           border: 1px dashed #000;
        }
    </style>
</head>
<body>
<table>
    <tbody>
    <tr><th>ALFONES COMMUNICATIONS SOLUTIONS</th></tr>
    <tr><th>info@alfonesltd.com</th></tr>
    <tr></tr>
    <tr><th>{{ $activation_name }}</th></tr>
    <tr><th>MERCHANDISE BA REPORT</th></tr>
    <tr style="background-color: #ffb53a; color: #FFFFFF;">
        <th>#</th>
        <th>Name</th>
        <th>Achievements</th>
    </tr>

    @foreach ($user_merchandises as $user_merchandise)
        <tr>
            <td>{{ $loop->iteration  }}</td>
            <td>
                {{ $user_merchandise['name'] }}
            </td>
            <td> {{ $user_merchandise['merchandise_count'] }} </td>
        </tr>
    @endforeach

    </tbody>
</table>
</body>
</html>
<html>
<head>
    <style>
        table {
           border: 1px dashed #000;
        }
    </style>
</head>
<body>
<table>
    <tbody>
    <tr><th>ALFONES COMMUNICATIONS SOLUTIONS</th></tr>
    <tr><th>info@alfonesltd.com</th></tr>
    <tr></tr>
    <tr><th>{{ $activation_name }}</th></tr>
    <tr><th>SALES BA REPORT - {{ $date }}</th></tr>
    <tr style="background-color: #ffb53a; color: #FFFFFF;">
        <th>#</th>
        <th>Name</th>
        <th>Achievements</th>
    </tr>

    @foreach ($user_sales as $user_sale)
        <tr>
            <td>{{ $loop->iteration  }}</td>
            <td>
                {{ $user_sale['name'] }}
            </td>
            <td> {{ $user_sale['sales_count'] }} </td>
        </tr>
    @endforeach

    </tbody>
</table>
</body>
</html>
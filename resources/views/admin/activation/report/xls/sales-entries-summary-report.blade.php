<html>
<head>
    <style>
        table {
           border: 1px dashed #000;
        }
    </style>
</head>
<body>
<table>
    <tbody>
    <tr><th>ALFONES COMMUNICATIONS SOLUTIONS</th></tr>
    <tr><th>info@alfonesltd.com</th></tr>
    <tr></tr>
    <tr><th>{{ $activation_name }}</th></tr>
    <tr><th>SALES ENTRIES SUMMARY REPORT</th></tr>
    <tr style="background-color: #ffb53a; color: #FFFFFF;">
        <th>Date</th>
        <th>Day</th>
        <th>Division</th>
        <th>Region</th>
        <th>Brand</th>
        <th>Outlet name</th>
        <th>KE numbers</th>
        <th>Price</th>
        <th>Opening stock</th>
        <th>Closing stock</th>
        <th>Base vol</th>
    </tr>

    @foreach ($entries as $entry)
        <tr>
            <td> {{ $entry['date'] }} </td>
            <td>{{ $entry['day'] }}</td>
            <td>{{ $entry['division'] }}</td>
            <td>{{ $entry['region'] }}</td>
            <td>{{ $entry['product_name'] }}</td>
            <td>{{ $entry['location'] }}</td>
            <td>{{ $entry['location_identifier'] }}</td>
            <td>{{ $entry['product_price'] }}</td>
            <td>{{ $entry['opening_stock'] }}</td>
            <td>{{ $entry['closing_stock'] }}</td>
            <td>{{ $entry['base_volume'] }}</td>
        </tr>
    @endforeach

    </tbody>
</table>
</body>
</html>
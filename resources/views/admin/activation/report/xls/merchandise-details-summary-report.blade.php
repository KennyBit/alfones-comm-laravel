<html>
<head>
    <style>
        table {
           border: 1px dashed #000;
        }
    </style>
</head>
<body>
<table>
    <tbody>
    <tr><th>ALFONES COMMUNICATIONS SOLUTIONS</th></tr>
    <tr><th>info@alfonesltd.com</th></tr>
    <tr></tr>
    <tr><th>{{ $activation_name }}</th></tr>
    <tr><th>MERCHANDISE SUMMARY REPORT - {{ $date }} </th></tr>
    <tr style="background-color: #ffb53a; color: #FFFFFF;">
        <th>#</th>
        <th>Location</th>
        <th>Distribution</th>
        <th>Team leader</th>
    </tr>

    @foreach ($total_merchandises as $total_merchandise)
        <tr>
            <td>{{ $loop->iteration  }}</td>
            <td>
                {{ $total_merchandise['name'] }}
            </td>
            <td>  {{ $total_merchandise['merchandise_count'] }} </td>
            <td> {{ $total_merchandise['team_leader_name'] }} </td>
        </tr>
    @endforeach

    </tbody>
</table>
</body>
</html>
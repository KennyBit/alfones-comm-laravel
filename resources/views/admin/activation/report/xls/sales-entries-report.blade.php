<html>
<head>
    <style>
        table {
           border: 1px dashed #000;
        }
    </style>
</head>
<body>
<table>
    <tbody>
    <tr><th>ALFONES COMMUNICATIONS SOLUTIONS</th></tr>
    <tr><th>info@alfonesltd.com</th></tr>
    <tr></tr>
    <tr><th>{{ $activation_name }}</th></tr>
    <tr><th>SALES ENTRIES REPORT</th></tr>
    <tr style="background-color: #ffb53a; color: #FFFFFF;">
        <th>#</th>
        <th>Customer name</th>
        <th>Phone number</th>
        <th>Product</th>
        <th>Quantity</th>
        <th>Value</th>
        <th>Feedback</th>
        <th>BA</th>
        <th>Location</th>
        <th>Date</th>
    </tr>

    @foreach ($entries as $entry)
        <tr>
            <td>{{ $loop->iteration  }}</td>
            <td> {{ $entry['customer_name'] }} </td>
            <td>{{ $entry['customer_phone'] }}</td>
            <td>{{ $entry['product_name'] }}</td>
            <td>{{ $entry['product_quantity'] }}</td>
            <td>{{ $entry['product_value'] }}</td>
            <td>{{ $entry['customer_feedback'] }}</td>
            <td>{{ $entry['user_name'] }}</td>
            <td>{{ $entry['location_name'] }}</td>
            <td>{{ $entry['created_at'] }}</td>
        </tr>
    @endforeach
    
    </tbody>
</table>
</body>
</html>
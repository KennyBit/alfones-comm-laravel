<html>
<head>
    <style>
        table {
           border: 1px dashed #000;
        }
    </style>
</head>
<body>
<table>
    <tbody>
    <tr><th>ALFONES COMMUNICATIONS SOLUTIONS</th></tr>
    <tr><th>info@alfonesltd.com</th></tr>
    <tr></tr>
    <tr><th>{{ $activation_name }}</th></tr>
    <tr><th>INTERACTIONS ENTRIES REPORT</th></tr>
    <tr style="background-color: #ffb53a; color: #FFFFFF;">
        <th>#</th>
        <th>Customer name</th>
        <th>Phone number</th>
        <th>Feedback</th>
        <th>BA</th>
        <th>Location</th>
        <th>Image</th>
        <th>Date</th>
    </tr>

    @foreach ($entries as $entry)
        <tr>
            <td>{{ $loop->iteration  }}</td>
            <td style="width: 20%;">
                {{ $entry['customer_name'] }}
            </td>
            <td>{{ $entry['customer_phone'] }}</td>
            <td>{{ $entry['customer_feedback'] }}</td>
            <td>{{ $entry['user_name'] }}</td>
            <td><a href="{{ route('google-maps-location', ['id'=>$entry['id']]) }}" target="_blank">{{ $entry['location_name'] }}</a></td>
            @if($entry['image'] == "")
                <td>No attachmemt</td>
            @else
                <td><a href="{{ $entry['image'] }}" target="_blank">View image</a></td>
            @endif
            <td>{{ $entry['created_at'] }}</td>
        </tr>
    @endforeach

    </tbody>
</table>
</body>
</html>
<html>
<head>
    <style>
        table {
           border: 1px dashed #000;
        }
    </style>
</head>
<body>
<table>
    <tbody>
    <tr><th>ALFONES COMMUNICATIONS SOLUTIONS</th></tr>
    <tr><th>info@alfonesltd.com</th></tr>
    <tr></tr>
    <tr><th>{{ $activation_name }}</th></tr>
    <tr><th>BRAND AMBASSADOR PERFORMANCE REPORT</th></tr>
    <tr style="background-color: #ffb53a; color: #FFFFFF;">
        <th>#</th>
        <th>Name</th>
        <th>Interactions</th>
        <th>Sales</th>
        <th>Merchandises</th>
    </tr>

    @foreach ($ba_performances as $ba_performance)
        <tr>
            <td>{{ $loop->iteration  }}</td>
            <td>
                {{ $ba_performance['name'] }}
            </td>
            <td> {{ $ba_performance['interaction_count'] }} </td>
            <td> {{ $ba_performance['sales_count'] }} </td>
            <td> {{ $ba_performance['merchandise_count'] }} </td>
        </tr>
    @endforeach

    </tbody>
</table>
</body>
</html>
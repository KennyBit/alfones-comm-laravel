<html>
<head>
    <style>
        table {
           border: 1px dashed #000;
        }
    </style>
</head>
    <body>
        <table>
            <tbody>
            <tr><th>ALFONES COMMUNICATIONS SOLUTIONS</th></tr>
            <tr><th>info@alfonesltd.com</th></tr>
            <tr></tr>
            <tr><th>{{ $name }}</th></tr>
            <tr><th>WEEKLY PLANNER</th></tr>
            <tr style="background-color: #ffb53a; color: #FFFFFF;">
                <th>#</th>
                <th>Monday</th>
                <th>Tuesday</th>
                <th>Wednesday</th>
                <th>Thursday</th>
                <th>Friday</th>
            </tr>


            @foreach ($planners as $planer)
                <tr>
                    <td> {{ $planer['time']  }}</td>
                    @foreach ($planer['data'] as $event)
                        <td> {{ $event['event'] }}</td>
                    @endforeach
                </tr>
            @endforeach

            </tbody>
        </table>
    </body>
</html>
<html>
<head>
    <style>
        table {
           border: 1px dashed #000;
        }
    </style>
</head>
<body>
<table>
    <tbody>
    <tr><th>ALFONES COMMUNICATIONS SOLUTIONS</th></tr>
    <tr><th>info@alfonesltd.com</th></tr>
    <tr></tr>
    <tr><th>{{ $activation_name }}</th></tr>
    <tr><th>SALES SUMMARY REPORT - {{ $date }} </th></tr>
    <tr style="background-color: #ffb53a; color: #FFFFFF;">
        <th>#</th>
        <th>Location</th>
        <th>Targets</th>
        <th>Achievements</th>
        <th>% Achievement</th>
        <th>Team leader</th>
    </tr>

    @foreach ($total_sales as $total_sale)
        <tr>
            <td> {{ $loop->iteration  }}</td>
            <td> {{ $total_sale['name'] }}</td>
            <td> {{ $total_sale['sales_target'] }} </td>
            <td> {{ $total_sale['sales_count'] }} </td>
            <td> {{ $total_sale['percentage_achievement'] }}% </td>
            <td> {{ $total_sale['team_leader_name'] }} </td>
        </tr>
    @endforeach

    </tbody>
</table>
</body>
</html>
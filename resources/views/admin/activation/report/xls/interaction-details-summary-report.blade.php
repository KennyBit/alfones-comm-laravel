<html>
<head>
    <style>
        table {
           border: 1px dashed #000;
        }
    </style>
</head>
<body>
<table>
    <tbody>
    <tr><th>ALFONES COMMUNICATIONS SOLUTIONS</th></tr>
    <tr><th>info@alfonesltd.com</th></tr>
    <tr></tr>
    <tr><th>{{ $activation_name }}</th></tr>
    <tr><th>INTERACTIONS SUMMARY REPORT - {{ $date }} </th></tr>
    <tr style="background-color: #ffb53a; color: #FFFFFF;">
        <th>#</th>
        <th>Location</th>
        <th>Targets</th>
        <th>Achievements</th>
        <th>% Achievement</th>
        <th>Team leader</th>
    </tr>

    @foreach ($total_interactions as $total_interaction)
        <tr>
            <td> {{ $loop->iteration  }} </td>
            <td> {{ $total_interaction['name'] }} </td>
            <td> {{ $total_interaction['interaction_target'] }} </td>
            <td> {{ $total_interaction['interaction_count'] }} </td>
            <td> {{ $total_interaction['percentage_achievement'] }} % </td>
            <td> {{ $total_interaction['team_leader_name'] }} </td>
        </tr>
    @endforeach

    </tbody>
</table>
</body>
</html>
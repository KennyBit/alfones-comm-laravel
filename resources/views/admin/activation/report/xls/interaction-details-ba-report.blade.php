<html>
<head>
    <style>
        table {
           border: 1px dashed #000;
        }
    </style>
</head>
<body>
<table>
    <tbody>
    <tr><th>ALFONES COMMUNICATIONS SOLUTIONS</th></tr>
    <tr><th>info@alfonesltd.com</th></tr>
    <tr></tr>
    <tr><th>{{ $activation_name }}</th></tr>
    <tr><th>INTERACTIONS BA REPORT - {{ $date }}</th></tr>
    <tr style="background-color: #ffb53a; color: #FFFFFF;">
        <th>#</th>
        <th>Name</th>
        <th>Achievements</th>
    </tr>

    @foreach ($user_interactions as $user_interaction)
        <tr>
            <td>{{ $loop->iteration  }}</td>
            <td style="width: 40%;">
                {{ $user_interaction['name'] }}
            </td>
            <td> {{ $user_interaction['interaction_count'] }} </td>
        </tr>
    @endforeach

    </tbody>
</table>
</body>
</html>
@extends('app-admin')
@section('content')

    <div class="content p-4">
        <h2 class="mb-4">Summary details edit</h2>

        <div class="row">
            <div class="col-5">
                <div class="card mb-4">
                    <activation-product-summary-details-component product_summary="{{ $product_summary }}" can_edit="{{ $canEdit }}" can_delete="{{ $canDelete }}"></activation-product-summary-details-component>
                </div>
            </div>
        </div>

    </div>

@endsection

@extends('app-admin')
@section('content')

    <div class="content p-4">
        <h2 class="mb-4">Stock edit</h2>

        <div class="row">
            <div class="col-5">
                <div class="card mb-4">
                    <activation-product-stock-details-component product_stock="{{ $product_stock }}" can_edit="{{ $canEdit }}" can_delete="{{ $canDelete }}"></activation-product-stock-details-component>
                </div>
            </div>
        </div>

    </div>

@endsection

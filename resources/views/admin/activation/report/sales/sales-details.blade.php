@extends('app-admin')
@section('content')

    @can('activation-reports-view')
        <div class="content p-4">
            <h2 class="mb-4">Sales reports - {{ $date }}</h2>

            <div class="tab_container">
                <input id="tab1" type="radio" name="tabs" checked>
                <label for="tab1" class="label-tab"><span>Total / Targets</span></label>

                <input id="tab2" type="radio" name="tabs">
                <label for="tab2" class="label-tab"><span>BA</span></label>

                <input id="tab3" type="radio" name="tabs">
                <label for="tab3" class="label-tab"><span>Entries</span></label>

                <input id="tab4" type="radio" name="tabs">
                <label for="tab4" class="label-tab"><span>Products</span></label>

                <input id="tab5" type="radio" name="tabs">
                <label for="tab5" class="label-tab"><span>Stocks (Team leader)</span></label>

                <input id="tab6" type="radio" name="tabs">
                <label for="tab6" class="label-tab"><span>Outlet summary (Team leader)</span></label>

                <section id="content1" class="tab-content">

                    <h4 class="mb-4" style="color: #c7254e">Total / Target</h4>

                    <div class="row">
                        <div class="col-10">
                            <div class="card-columns">
                                <div class="card bg-light">
                                    <div class="card-body text-center">
                                        <h3 class="card-title" style="color: #c7254e">{{ $sales_today_count }}</h3>
                                        <p class="card-text">Sales today</p>
                                    </div>
                                </div>
                                <div class="card bg-light">
                                    <div class="card-body text-center">
                                        <h3 class="card-title" style="color: #c7254e">{{ $sales_week_count }}</h3>
                                        <p class="card-text">Sales this week</p>

                                    </div>
                                </div>
                                <div class="card bg-light">
                                    <div class="card-body text-center">
                                        <h3 class="card-title" style="color: #c7254e">{{ $sales_total_count }}</h3>
                                        <p class="card-text">Total sales</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-10">
                            <div class="card mb-4">
                                <div class="card-body">
                                    <table id="example1" class="table table-hover" cellspacing="0" width="100%">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th style="width: 40%;">Location</th>
                                            <th>Targets</th>
                                            <th>Achievements</th>
                                            <th>% Achievement</th>
                                            <th>Team leader</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($total_sales as $total_sale)
                                            <tr>
                                                <td>{{ $loop->iteration  }}</td>
                                                <td style="width: 40%;">
                                                    {{ $total_sale['name'] }}
                                                </td>
                                                <td> {{ $total_sale['sales_target'] }} </td>
                                                <td>  {{ $total_sale['sales_count'] }} </td>
                                                <td> {{ $total_sale['percentage_achievement'] }}% </td>
                                                <td> {{ $total_sale['team_leader_name'] }} </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    @can('activation-reports-xls')
                        <div class="d-inline-block">
                            <div class="row">
                                <div class="col-sm">
                                    <a href="{{ route('sales-details-summary-report', ['id'=>$activation, 'date'=>$date]) }}" class="btn btn-warning">GENERATE REPORT</a>
                                </div>
                            </div>
                        </div>
                    @endcan

                </section>

                <section id="content2" class="tab-content">
                    <h4 class="mb-4" style="color: #c7254e">Sales by brand ambassador</h4>

                    <div class="row">
                        <div class="col-6">
                            <div class="card mb-4">
                                <div class="card-body">
                                    <table id="example2" class="table table-hover" cellspacing="0" width="100%">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th style="width: 50%;">Name</th>
                                            <th>Achievements</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($user_sales as $user_sale)
                                            <tr>
                                                <td>{{ $loop->iteration  }}</td>
                                                <td style="width: 40%;">
                                                    {{ $user_sale['name'] }}
                                                </td>
                                                <td> {{ $user_sale['sales_count'] }} </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    @can('activation-reports-xls')
                        <div class="d-inline-block">
                            <div class="row">
                                <div class="col-sm">
                                    <a href="{{ route('sales-details-ba-report', ['id'=>$activation, 'date'=>$date]) }}" class="btn btn-warning">GENERATE REPORT</a>
                                </div>
                            </div>
                        </div>
                    @endcan


                </section>

                <section id="content3" class="tab-content">
                    <h4 class="mb-4" style="color: #c7254e">Sales entries</h4>

                    <div class="row">
                        <div class="col-12">
                            <div class="card mb-4">
                                <div class="card-body">
                                    <table id="example2" class="table table-hover" cellspacing="0" width="100%">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th style="width: 20%;">Customer name</th>
                                            <th>Phone number</th>
                                            <th>Product</th>
                                            <th>Quantity</th>
                                            <th>Feedback</th>
                                            <th>BA</th>
                                            <th>Location</th>
                                            <th>Image</th>
                                            <th>Date</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($entries as $entry)
                                            <tr>
                                                <td>{{ $loop->iteration  }}</td>
                                                <td style="width: 20%;"> {{ $entry['customer_name'] }} </td>
                                                <td>{{ $entry['customer_phone'] }}</td>
                                                <td>{{ $entry['product_name'] }}</td>
                                                <td>{{ $entry['product_quantity'] }}</td>
                                                <td>{{ $entry['customer_feedback'] }}</td>
                                                <td>{{ $entry['user_name'] }}</td>
                                                <td><a href="{{ route('google-maps-location', ['id'=>$entry['id']]) }}" target="_blank">{{ $entry['location_name'] }}</a></td>
                                                @if($entry['image'] == "")
                                                    <td>No attachmemt</td>
                                                @else
                                                    <td><a href="{{ $entry['image'] }}" target="_blank">View image</a></td>
                                                @endif
                                                <td>{{ $entry['created_at'] }}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    @can('activation-reports-xls')
                        <div class="d-inline-block">
                            <div class="row">
                                <div class="col-sm">
                                    <a href="{{ route('sales-details-entries-report', ['id'=>$activation, 'date'=>$date]) }}" class="btn btn-warning">GENERATE REPORT</a>
                                </div>
                            </div>
                        </div>
                    @endcan


                </section>

                <section id="content4" class="tab-content">
                    <h4 class="mb-4" style="color: #c7254e">Sales by products</h4>

                    <div class="row">
                        <div class="col-6">
                            <div class="card mb-4">
                                <div class="card-body">
                                    <table id="example2" class="table table-hover" cellspacing="0" width="100%">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th style="width: 50%;">Name</th>
                                            <th>Sales</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($product_sales as $product_sale)
                                            <tr>
                                                <td>{{ $loop->iteration  }}</td>
                                                <td style="width: 40%;">
                                                    {{ $product_sale['name'] }}
                                                </td>
                                                <td> {{ $product_sale['sales_count'] }} </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </section>

                <section id="content5" class="tab-content input-display">
                    <h4 class="mb-4" style="color: #c7254e">Outlet Stocks</h4>

                    <activation-product-stock-component date="{{ $date }}" activation="{{ $activation }}"></activation-product-stock-component>

                </section>

                <section id="content6" class="tab-content">
                    <h4 class="mb-4" style="color: #c7254e">Outlet Summary</h4>

                    <activation-product-summary-component date="{{ $date }}" activation="{{ $activation }}"></activation-product-summary-component>

                    @can('activation-reports-xls')
                        <div class="d-inline-block">
                            <div class="row">
                                <div class="col-sm">
                                    <a href="{{ route('sales-details-summary-entries-raw-report', ['id'=>$activation, 'date'=>$date]) }}" class="btn btn-warning">GENERATE REPORT</a>
                                </div>
                            </div>
                        </div>
                    @endcan

                </section>


            </div>
        </div>

    @endcan

@endsection

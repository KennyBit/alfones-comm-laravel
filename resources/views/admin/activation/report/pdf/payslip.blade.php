<!DOCTYPE html>
<html>
<head>
    <title>Payslip</title>
    <style>
        *
        {
            margin:0;
            padding:0;
            font-family:Arial;
            font-size:10pt;
            color:#000;
        }
        body
        {
            width:100%;
            font-family:Arial;
            font-size:10pt;
            margin:0;
            padding:0;
        }

        p
        {
            margin:0;
            padding:0;
        }

        #wrapper
        {
            width:180mm;
            margin:0 15mm;
        }

        .page
        {
            height:297mm;
            width:210mm;
            page-break-after:always;
        }

        table
        {
            border-left: 1px solid #ccc;
            border-top: 1px solid #ccc;

            border-spacing:0;
            border-collapse: collapse;

        }

        table td
        {
            border-right: 1px solid #ccc;
            border-bottom: 1px solid #ccc;
            padding: 2mm;
        }

        table.heading
        {
            height:50mm;
        }

        h1.heading
        {
            font-size:12pt;
            color:#000;
            font-weight:normal;
        }

        h2.heading
        {
            font-size:9pt;
            color:#000;
            font-weight:normal;
        }

        hr
        {
            color:#ccc;
            background:#ccc;
        }

        #invoice_body
        {

        }

        #invoice_body , #invoice_total
        {
            width:100%;
        }
        #invoice_body table , #invoice_total table
        {
            width:100%;
            border-left: 1px solid #ccc;
            border-top: 1px solid #ccc;

            border-spacing:0;
            border-collapse: collapse;

            margin-top:5mm;
        }

        #invoice_body table td , #invoice_total table td
        {
            text-align:center;
            font-size:9pt;
            border-right: 1px solid #ccc;
            border-bottom: 1px solid #ccc;
            padding:2mm 0;
        }

        #invoice_body table td.mono  , #invoice_total table td.mono
        {
            font-family:monospace;
            text-align:right;
            padding-right:3mm;
            font-size:10pt;
        }

        #footer
        {
            width:180mm;
            margin:0 15mm;
            padding-bottom:3mm;
        }
        #footer table
        {
            width:100%;
            border-left: 1px solid #ccc;
            border-top: 1px solid #ccc;

            background:#eee;

            border-spacing:0;
            border-collapse: collapse;
        }
        #footer table td
        {
            width:25%;
            text-align:center;
            font-size:9pt;
            border-right: 1px solid #ccc;
            border-bottom: 1px solid #ccc;
        }
    </style>
</head>
<body>
<div id="wrapper">

    <div style="text-align: center;">
        <img width="160" src="{{ url('images/alfoneslogo.png') }}" alt="" />
    </div>
    <p style="text-align:center; font-weight:bold; padding-top:5mm;">EMPLOYEE PAYSLIP</p>
    <p style="text-align:center; font-weight:bold;">PAYSLIP No.{{ $payslip->payslip_id }}</p>

    <br>
    <table class="heading" style="width:90%;">
        <tr>
            <td style="width:80mm;  display: inline;">
                <h1 class="heading">ALFONES COMMUNICATIONS</h1>
                <h2 class="heading">
                    61682-00200  <br>
                    Nairobi – Kenya  <br>
                    Website : www.alfonesltd.com<br />
                    E-mail : info@alfonesltd.com<br />
                    <br><br>
                    <b> Name: {{ $admin->name }}</b> <br>
                    <b> Phone: {{ $admin->phone_number }}</b> <br>
                    <b> Email: {{ $admin->email }}</b> <br>

                    <br><br>
                    <b> Employee #: {{ $admin->employee_no }}</b> <br>
                    <b> Payslip #: {{ $payslip->payslip_id }}</b> <br>

                    @if($payslip->month == "1")
                        <b> Month / Year: JANUARY / {{ $payslip->year }}</b> <br>
                    @elseif($payslip->month == "2")
                        <b> Month / Year: FEBRUARY / {{ $payslip->year }}</b> <br>
                    @elseif($payslip->month == "3")
                        <b> Month / Year: MARCH / {{ $payslip->year }}</b> <br>
                    @elseif($payslip->month == "4")
                        <b> Month / Year: APRIL / {{ $payslip->year }}</b> <br>
                    @elseif($payslip->month == "5")
                        <b> Month / Year: MAY / {{ $payslip->year }}</b> <br>
                    @elseif($payslip->month == "6")
                        <b> Month / Year: JUNE / {{ $payslip->year }}</b> <br>
                    @elseif($payslip->month == "7")
                        <b> Month / Year: JULY / {{ $payslip->year }}</b> <br>
                    @elseif($payslip->month == "8")
                        <b> Month / Year: AUGUST / {{ $payslip->year }}</b> <br>
                    @elseif($payslip->month == "9")
                        <b> Month / Year: SEPTEMBER / {{ $payslip->year }}</b> <br>
                    @elseif($payslip->month == "10")
                        <b> Month / Year: OCTOBER / {{ $payslip->year }}</b> <br>
                    @elseif($payslip->month == "11")
                        <b> Month / Year: NOVEMBER / {{ $payslip->year }}</b> <br>
                    @elseif($payslip->month == "12")
                        <b> Month / Year: DECEMBER / {{ $payslip->year }}</b> <br>
                    @endif


                </h2>
            </td>
        </tr>
    </table>


    <table class="heading" style="width:90%; margin-top: 10px;">

        <tbody>
            <tr>
                <td style="width: 60%"><b>Gross Pay</b></td>
                <td style="text-align: right"> Kshs {{ $payslip->gross_salary }}</td>
            </tr>
        </tbody>
    </table>

    <table class="heading" style="width:90%; margin-top: 10px;">

        <tbody>
            <tr>
                <td colspan="2" style="background: #f1b0b7;"> Deductions </td>
            </tr>
            <tr>
                <td style="width: 60%"><b>NSSF Contribution</b></td>
                <td style="text-align: right">Kshs {{ $payslip->nssf_contibution }}</td>
            </tr>
            <tr>
                <td><b>Personal relief</b></td>
                <td style="text-align: right">Kshs {{ $payslip->personal_relief }}</td>
            </tr>
            <tr>
                <td><b>Paye</b></td>
                <td style="text-align: right">Kshs {{ $payslip->paye }}</td>
            </tr>
            <tr>
                <td><b>NHIF Contribution</b></td>
                <td style="text-align: right">Kshs {{ $payslip->nhif_contribution }}</td>
            </tr>
            <tr>
                <td><b>Total deducations</b></td>
                <td style="text-align: right">Kshs {{ $payslip->paye}}</td>
            </tr>
        </tbody>
    </table>

    <table class="heading" style="width:90%; margin-top: 10px;">

        <tbody>
        <tr>
            <td colspan="2" style="background: #f1b0b7;"> Net pay </td>
        </tr>
        <tr>
            <td style="width: 60%"><b>Total</b></td>
            <td style="text-align: right"> Kshs {{ $payslip->net_pay }}</td>
        </tr>
        </tbody>
    </table>



</div>

<htmlpagefooter name="footer">

    <div id="footer">
        <p style="text-align: center;">ALFONES COMMUNICATIONS SOLUTIONS</p> <b></b>
    </div>
</htmlpagefooter>
<sethtmlpagefooter name="footer" value="on" />

</body>
</html>
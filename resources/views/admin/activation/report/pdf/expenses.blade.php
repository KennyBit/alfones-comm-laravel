<!DOCTYPE html>
<html>
<head>
    <title>Expenses Requisition</title>
    <style>
        *
        {
            margin:0;
            padding:0;
            font-family:Arial;
            font-size:10pt;
            color:#000;
        }
        body
        {
            width:100%;
            font-family:Arial;
            font-size:10pt;
            margin:0;
            padding:0;
        }

        p
        {
            margin:0;
            padding:0;
        }

        #wrapper
        {
            width:180mm;
            margin:0 15mm;
        }

        .page
        {
            height:297mm;
            width:210mm;
            page-break-after:always;
        }

        table
        {
            border-left: 1px solid #ccc;
            border-top: 1px solid #ccc;

            border-spacing:0;
            border-collapse: collapse;

        }

        table td
        {
            border-right: 1px solid #ccc;
            border-bottom: 1px solid #ccc;
            padding: 2mm;
        }

        table.heading
        {
            height:50mm;
        }

        h1.heading
        {
            font-size:12pt;
            color:#000;
            font-weight:normal;
        }

        h2.heading
        {
            font-size:9pt;
            color:#000;
            font-weight:normal;
        }

        hr
        {
            color:#ccc;
            background:#ccc;
        }

        #invoice_body
        {

        }

        #invoice_body , #invoice_total
        {
            width:100%;
        }
        #invoice_body table , #invoice_total table
        {
            width:100%;
            border-left: 1px solid #ccc;
            border-top: 1px solid #ccc;

            border-spacing:0;
            border-collapse: collapse;

            margin-top:5mm;
        }

        #invoice_body table td , #invoice_total table td
        {
            text-align:center;
            font-size:9pt;
            border-right: 1px solid #ccc;
            border-bottom: 1px solid #ccc;
            padding:2mm 0;
        }

        #invoice_body table td.mono  , #invoice_total table td.mono
        {
            font-family:monospace;
            text-align:right;
            padding-right:3mm;
            font-size:10pt;
        }

        #footer
        {
            width:180mm;
            margin:0 15mm;
            padding-bottom:3mm;
        }
        #footer table
        {
            width:100%;
            border-left: 1px solid #ccc;
            border-top: 1px solid #ccc;

            background:#eee;

            border-spacing:0;
            border-collapse: collapse;
        }
        #footer table td
        {
            width:25%;
            text-align:center;
            font-size:9pt;
            border-right: 1px solid #ccc;
            border-bottom: 1px solid #ccc;
        }
    </style>
</head>
<body>
<div id="wrapper">

    <div style="text-align: center;">
        <img width="160" src="{{ url('images/alfoneslogo.png') }}" alt="" />
    </div>
    <p style="text-align:center; font-weight:bold; padding-top:5mm;">EXPENSES - {{ $expenses->requisition_title }}</p>

    @if($activation_name != "")
        <p style="text-align:center; font-weight:bold;">ACTIVATION - {{ $activation_name }}</p>
    @else
        <p style="text-align:center; font-weight:bold;">DESCRIPTION - {{ $expenses->requisition_title }}</p>
    @endif
    <br>
    <table class="heading" style="width:90%;">
        <tr>
            <td style="width:80mm;  display: inline;">
                <h1 class="heading">ALFONES COMMUNICATIONS</h1>
                <h2 class="heading">
                    61682-00200  <br>
                    Nairobi – Kenya  <br>
                    Website : www.alfonesltd.com<br />
                    E-mail : info@alfonesltd.com<br />
                    <br><br>
                    Name:  {{ $admin->name }} <br>
                    Phone:  {{ $admin->phone_number }} <br>
                    Email:  {{ $admin->email }} <br>
                </h2>
            </td>
        </tr>
    </table>


    <table class="heading" style="width:90%;">
        <tr>
            <th></th>
            <th>Supplier</th>
            <th style="width: 20%;">Item</th>
            <th style="width: 25%;">Description</th>
            <th>Quantity</th>
            <th>Unit Cost</th>
            <th>Days</th>
            <th>Total</th>
        </tr>
        <tbody>
        @foreach ($expenses_items as $expenses_item)
            <tr>
                <td>{{ $loop->iteration  }}</td>
                <td> {{ $expenses_item->item_supplier }} </td>
                <td> {{ $expenses_item->item_name }} </td>
                <td> {{ $expenses_item->item_description }} </td>
                <td> {{ $expenses_item->item_quantity }} </td>
                <td> {{ $expenses_item->item_unit_cost }} </td>
                <td> {{ $expenses_item->item_days }} </td>
                <td> {{ $expenses_item->item_amount }} </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <table class="heading" style="width:90%;">
        <tr>
            <td style="width:80mm;  display: inline;">
                <h2 class="heading">
                    Total amount:  {{ $expenses_amount }} <br>
                    Date:  {{ $expenses->created_at }} <br>
                </h2>
            </td>
        </tr>
    </table>


</div>

<htmlpagefooter name="footer">

    <div id="footer">
        <p style="text-align: center;">ALFONES COMMUNICATIONS SOLUTIONS</p> <b></b>
        <p style="text-align: center;"> Generated by {{ $user->first_name }} {{ $user->last_name }} | {{ $date }}</p>
    </div>
</htmlpagefooter>
<sethtmlpagefooter name="footer" value="on" />

</body>
</html>
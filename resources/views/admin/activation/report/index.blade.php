@extends('app-admin')
@section('content')

    @can('activation-reports-view')
        <div class="content p-4">
            <h2 class="mb-4">Activation reports</h2>

            <div class="tab_container">
                <input id="tab1" type="radio" name="tabs" checked>
                <label for="tab1" class="label-tab"><span>Interactions (P2P)</span></label>

                <input id="tab2" type="radio" name="tabs">
                <label for="tab2" class="label-tab"><span>Sales</span></label>

                <input id="tab3" type="radio" name="tabs">
                <label for="tab3" class="label-tab"><span>Merchandises</span></label>

                <input id="tab4" type="radio" name="tabs">
                <label for="tab4" class="label-tab"><span>BA Performance</span></label>

                <input id="tab5" type="radio" name="tabs">
                <label for="tab5" class="label-tab"><span>Payrolls</span></label>


                <section id="content1" class="tab-content input-display">
                    <h4 celass="mb-4" style="color: #c7254e">Interactions (Customer engagement)</h4>


                    <form method="POST" action="{{ route('interaction-details', ['id'=>$activation]) }}">
                        {{ csrf_field() }}
                        <div class="d-inline-block">
                            <div class="form-group">
                                <label for="email">Select date</label>
                                <input type="date" class="form-control" id="email" placeholder="Date" style="width: 300px;" name="date">
                            </div>
                        </div>
                        <div class="d-inline-block">
                            <div class="row">
                                <div class="col-sm">
                                    <button type="submit" class="btn btn-warning">Search</button>
                                </div>
                            </div>
                        </div>

                    </form>

                    <div class="row">
                        <div class="col-10">
                            <div class="card-columns">
                                <div class="card bg-light">
                                    <div class="card-body text-center">
                                        <h3 class="card-title" style="color: #c7254e">{{ $interaction_today_count }}</h3>
                                        <p class="card-text">Interactions today</p>
                                    </div>
                                </div>
                                <div class="card bg-light">
                                    <div class="card-body text-center">
                                        <h3 class="card-title" style="color: #c7254e">{{ $interaction_week_count }}</h3>
                                        <p class="card-text">Interaction this week</p>

                                    </div>
                                </div>
                                <div class="card bg-light">
                                    <div class="card-body text-center">
                                        <h3 class="card-title" style="color: #c7254e">{{ $interaction_total_count }}</h3>
                                        <p class="card-text">Total interactions</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-10">
                            <interaction-chart-component activation="{{ $activation }}"></interaction-chart-component>
                        </div>
                    </div>

                    @can('activation-reports-xls')
                        <br><br>
                        <div class="d-inline-block">
                            <div class="row">
                                <div class="col-sm">
                                    <a href="{{ route('interaction-summary-report', ['id'=>$activation]) }}" class="btn btn-warning">GENERATE LOCATION SUMMARY</a>
                                </div>
                            </div>
                        </div>

                        <div class="d-inline-block">
                            <div class="row">
                                <div class="col-sm">
                                    <a href="{{ route('interaction-ba-report', ['id'=>$activation]) }}" class="btn btn-danger">GENERATE BA SUMMARY</a>
                                </div>
                            </div>
                        </div>

                        <div class="d-inline-block">
                            <div class="row">
                                <div class="col-sm">
                                    <a href="{{ route('interaction-entries-report', ['id'=>$activation]) }}" class="btn btn-success">GENERATE ENTRIES SUMMARY</a>
                                </div>
                            </div>
                        </div>

                    @endcan

                </section>

                <section id="content2" class="tab-content input-display">
                    <h4 class="mb-4" style="color: #c7254e">Product / Service Sales</h4>

                    <form method="POST" action="{{ route('sales-details', ['id'=>$activation]) }}">
                        {{ csrf_field() }}
                        <div class="d-inline-block">
                            <div class="form-group">
                                <label for="email">Select date</label>
                                <input type="date" class="form-control" id="email" placeholder="Date" style="width: 300px;" name="date">
                            </div>
                        </div>
                        <div class="d-inline-block">
                            <div class="row">
                                <div class="col-sm">
                                    <button type="submit" class="btn btn-warning">Search</button>
                                </div>
                            </div>
                        </div>

                    </form>

                    <div class="row">
                        <div class="col-10">
                            <div class="card-columns">
                                <div class="card bg-light">
                                    <div class="card-body text-center">
                                        <h3 class="card-title" style="color: #c7254e">{{ $sales_today_count }}</h3>
                                        <p class="card-text">Sales today</p>
                                    </div>
                                </div>
                                <div class="card bg-light">
                                    <div class="card-body text-center">
                                        <h3 class="card-title" style="color: #c7254e">{{ $sales_week_count }}</h3>
                                        <p class="card-text">Sales this week</p>

                                    </div>
                                </div>
                                <div class="card bg-light">
                                    <div class="card-body text-center">
                                        <h3 class="card-title" style="color: #c7254e">{{ $sales_total_count }}</h3>
                                        <p class="card-text">Total sales</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-7">
                            <sales-chart-component activation="{{ $activation }}"></sales-chart-component>
                        </div>
                        <div class="col-4">
                            <product-sales-chart-component activation="{{ $activation }}"></product-sales-chart-component>
                        </div>
                    </div>

                    @can('activation-reports-xls')
                        <br><br>
                        <div class="d-inline-block">
                            <div class="row">
                                <div class="col-sm">
                                    <a href="{{ route('sales-summary-report', ['id'=>$activation]) }}" class="btn btn-warning">GENERATE LOCATION SUMMARY</a>
                                </div>
                            </div>
                        </div>

                        <div class="d-inline-block">
                            <div class="row">
                                <div class="col-sm">
                                    <a href="{{ route('sales-ba-report', ['id'=>$activation]) }}" class="btn btn-danger">GENERATE BA SUMMARY</a>
                                </div>
                            </div>
                        </div>

                        <div class="d-inline-block">
                            <div class="row">
                                <div class="col-sm">
                                    <a href="{{ route('sales-entries-report', ['id'=>$activation]) }}" class="btn btn-success">GENERATE ENTRIES </a>
                                </div>
                            </div>
                        </div>

                        <div class="d-inline-block">
                            <div class="row">
                                <div class="col-sm">
                                    <a href="{{ route('sales-summary-entries-raw-report', ['id'=>$activation]) }}" class="btn btn-warning">GENERATE SUMMARY</a>
                                </div>
                            </div>
                        </div>

                    @endcan
                </section>

                <section id="content3" class="tab-content input-display">
                    <h4 class="mb-4" style="color: #c7254e">Merchandise reconciliation</h4>

                    <form method="POST" action="{{ route('merchandise-details', ['id'=>$activation]) }}">
                        {{ csrf_field() }}
                        <div class="d-inline-block">
                            <div class="form-group">
                                <label for="email">Select date</label>
                                <input type="date" class="form-control" id="email" placeholder="Date" style="width: 300px;" name="date">
                            </div>
                        </div>
                        <div class="d-inline-block">
                            <div class="row">
                                <div class="col-sm">
                                    <button type="submit" class="btn btn-warning">Search</button>
                                </div>
                            </div>
                        </div>

                    </form>

                    <div class="row">
                        <div class="col-10">
                            <div class="card-columns">
                                <div class="card bg-light">
                                    <div class="card-body text-center">
                                        <h3 class="card-title" style="color: #c7254e">{{ $merchandise_today_count }}</h3>
                                        <p class="card-text">Distribution today</p>
                                    </div>
                                </div>
                                <div class="card bg-light">
                                    <div class="card-body text-center">
                                        <h3 class="card-title" style="color: #c7254e">{{ $merchandise_week_count }}</h3>
                                        <p class="card-text">Distribution this week</p>

                                    </div>
                                </div>
                                <div class="card bg-light">
                                    <div class="card-body text-center">
                                        <h3 class="card-title" style="color: #c7254e">{{ $merchandise_total_count }}</h3>
                                        <p class="card-text">Total distribution</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-7">
                            <merchandise-chart-component activation="{{ $activation }}"></merchandise-chart-component>
                        </div>
                        <div class="col-4">
                            <merchandise-distribution-chart-component activation="{{ $activation }}"></merchandise-distribution-chart-component>
                        </div>
                    </div>

                    @can('activation-reports-xls')
                        <br><br>
                        <div class="d-inline-block">
                            <div class="row">
                                <div class="col-sm">
                                    <a href="{{ route('merchandise-summary-report', ['id'=>$activation]) }}" class="btn btn-warning">GENERATE LOCATION SUMMARY</a>
                                </div>
                            </div>
                        </div>

                        <div class="d-inline-block">
                            <div class="row">
                                <div class="col-sm">
                                    <a href="{{ route('merchandise-ba-report', ['id'=>$activation]) }}" class="btn btn-danger">GENERATE BA SUMMARY</a>
                                </div>
                            </div>
                        </div>

                        <div class="d-inline-block">
                            <div class="row">
                                <div class="col-sm">
                                    <a href="{{ route('merchandise-entries-report', ['id'=>$activation]) }}" class="btn btn-success">GENERATE ENTRIES SUMMARY</a>
                                </div>
                            </div>
                        </div>
                    @endcan
                </section>

                <section id="content4" class="tab-content input-display">
                    <h4 class="mb-5" style="color: #c7254e">BA performance</h4>

                    <div class="row">
                        <div class="col-6">
                            <div class="card mb-5">
                                <div class="card-body">
                                    <table id="example2" class="table table-hover" cellspacing="0" width="100%">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th style="width: 40%;">Name</th>
                                            <th>Interactions</th>
                                            <th>Sales</th>
                                            <th>Merchandises</th>
                                            <th>Reports</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($ba_performances as $ba_performance)
                                            <tr>
                                                <td>{{ $loop->iteration  }}</td>
                                                <td style="width: 40%;">
                                                    {{ $ba_performance['name'] }}
                                                </td>
                                                <td> {{ $ba_performance['interaction_count'] }} </td>
                                                <td> {{ $ba_performance['sales_count'] }} </td>
                                                <td> {{ $ba_performance['merchandise_count'] }} </td>
                                                <td> View </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    @can('activation-reports-xls')
                        <br><br>
                        <div class="d-inline-block">
                            <div class="row">
                                <div class="col-sm">
                                    <a href="{{ route('ba-performance-report', ['id'=>$activation]) }}" class="btn btn-warning">GENERATE REPORT</a>
                                </div>
                            </div>
                        </div>
                    @endcan

                </section>

                <section id="content5" class="tab-content input-display">
                    @can('activation-payroll-view')
                        <h4 class="mb-4" style="color: #c7254e">Payroll</h4>

                        <div class="row">
                            <div class="col-6">
                                <div class="card mb-5">
                                    <div class="card-body">
                                        <table id="example2" class="table table-hover" cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th style="width: 40%;">Name</th>
                                                <th>Days</th>
                                                <th>Unit cost</th>
                                                <th>Total</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach ($payrolls as $payroll)
                                                <tr>
                                                    <td>{{ $loop->iteration  }}</td>
                                                    <td style="width: 40%;">
                                                        {{ $payroll['name'] }}
                                                    </td>
                                                    <td> {{ $payroll['total_days'] }} </td>
                                                    <td> Kshs {{ $payroll['unit_cost'] }} </td>
                                                    <td> Kshs {{ $payroll['total_cost'] }} </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        @can('activation-payroll-generate')
                            <br><br>
                            <form method="POST" action="{{ route('activation-payroll-selected-payroll') }}">
                                {{ csrf_field() }}
                                <input type="hidden" name="activation_id" value="{{ $activation }}">
                                <label for="email">Generate on selected dates</label><br>
                                <div class="d-inline-block">
                                    <div class="form-group">
                                        <label for="email">Start date</label>
                                        <input type="date" class="form-control" id="email" placeholder="Start date" style="width: 200px;" name="start_date">
                                    </div>
                                </div>
                                <div class="d-inline-block">
                                    <div class="form-group">
                                        <label for="email">End date</label>
                                        <input type="date" class="form-control" id="email" placeholder="End date" style="width: 200px;" name="end_date">
                                    </div>
                                </div>
                                <div class="d-inline-block">
                                    <div class="row">
                                        <div class="col-sm">
                                            <button type="submit" class="btn btn-warning">GENERATE</button>
                                        </div>
                                    </div>
                                </div>

                            </form>
                            <hr>
                            <div class="row">
                                <div class="col-sm">
                                    <label for="email">Generate full payroll</label><br>
                                    <a href="{{ route('activation-payroll-report', ['id'=>$activation]) }}" class="btn btn-success" target="_blank">GENERATE PAYROLL</a>
                                </div>
                            </div>
                        @endcan

                    @endcan

                </section>

            </div>
        </div>


    @endcan

@endsection

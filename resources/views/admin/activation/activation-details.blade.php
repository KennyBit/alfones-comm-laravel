@extends('app-admin')
@section('content')

    @can('activation-details')
        <div class="content p-4">
            <h2 class="mb-4">Activation settings</h2>

            <div class="tab_container">
                @can('activation-details')
                    <input id="tab1" type="radio" name="tabs" checked>
                    <label for="tab1" class="label-tab"><span>Activation</span></label>
                @endcan

                @can('activation-team-leader-list')
                    <input id="tab2" type="radio" name="tabs">
                    <label for="tab2" class="label-tab"><span>Team leaders</span></label>
                @endcan

                @can('ba-list')
                    <input id="tab3" type="radio" name="tabs">
                    <label for="tab3" class="label-tab"><span>Users</span></label>
                @endcan

                @can('activation-product-list')
                    <input id="tab4" type="radio" name="tabs">
                    <label for="tab4" class="label-tab"><span>Products</span></label>
                @endcan

                @can('activation-merchandise-list')
                    <input id="tab5" type="radio" name="tabs">
                    <label for="tab5" class="label-tab"><span>Merchandise</span></label>
                @endcan

                @can('activation-location-list')
                    <input id="tab6" type="radio" name="tabs">
                    <label for="tab6" class="label-tab"><span>Location</span></label>
                @endcan

                @can('activation-target-list')
                    <input id="tab7" type="radio" name="tabs">
                    <label for="tab7" class="label-tab"><span>Target</span></label>
                @endcan

                <section id="content1" class="tab-content input-display">
                    @can('activation-details')
                        <h4 class="mb-4" style="color: #c7254e">Activation settings</h4>
                        <div class="row">
                            <div class="col-5">
                                <div class="card mb-4">
                                    <activation-details-component activation="{{ $activation }}" can_edit="{{ $canEdit }}" can_delete="{{ $canDelete }}"></activation-details-component>
                                </div>
                            </div>
                        </div>

                        <h4 class="mb-4" style="color: #c7254e">Activation type</h4>
                        <activation-type-component activation="{{ $activation }}" can_list="{{ $canTeamLeaderList }}" can_create="{{ $canTeamLeaderCreate }}" can_details="{{ $canTeamLeaderDetails }}"></activation-type-component>

                    @endcan
                </section>

                <section id="content2" class="tab-content input-display">
                    @can('activation-team-leader-list')
                        <h4 class="mb-4" style="color: #c7254e">Team leaders settings</h4>
                        <activation-team-leader-component activation="{{ $activation }}" can_list="{{ $canTeamLeaderList }}" can_create="{{ $canTeamLeaderCreate }}" can_details="{{ $canTeamLeaderDetails }}"></activation-team-leader-component>
                    @endcan
                </section>

                <section id="content3" class="tab-content input-display">
                    @can('activation-user-list')
                        <h4 class="mb-4" style="color: #c7254e">Users settings</h4>
                        <activation-user-component activation="{{ $activation }}" can_list="{{ $canUserList }}" can_create="{{ $canUserCreate }}" can_details="{{ $canUserDetails }}"></activation-user-component>
                    @endcan
                </section>

                <section id="content4" class="tab-content input-display">
                    @can('activation-product-list')
                        <h4 class="mb-4" style="color: #c7254e">Products settings</h4>
                        <activation-product-component activation="{{ $activation }}" can_list="{{ $canProductList }}" can_create="{{ $canProductCreate }}" can_details="{{ $canProductDetails }}"></activation-product-component>
                    @endcan
                </section>

                <section id="content5" class="tab-content input-display">
                    @can('activation-merchandise-list')
                        <h4 class="mb-4" style="color: #c7254e">Merchandise settings</h4>
                        <activation-merchandise-component activation="{{ $activation }}" can_list="{{ $canMerchandiseList }}" can_create="{{ $canMerchandiseCreate }}" can_details="{{ $canMerchandiseDetails }}"></activation-merchandise-component>
                    @endcan
                </section>

                <section id="content6" class="tab-content input-display">
                    @can('activation-location-list')
                        <h4 class="mb-4" style="color: #c7254e">Location settings</h4>
                        <activation-location-component activation="{{ $activation }}" can_list="{{ $canLocationList }}" can_create="{{ $canLocationCreate }}" can_details="{{ $canLocationDetails }}"></activation-location-component>
                    @endcan
                </section>

                <section id="content7" class="tab-content input-display">
                    @can('activation-target-list')
                        <h4 class="mb-4" style="color: #c7254e">Target settings</h4>
                        <activation-target-component activation="{{ $activation }}" can_list="{{ $canTargetList }}" can_create="{{ $canTargetCreate }}" can_details="{{ $canTargetDetails }}"></activation-target-component>
                    @endcan
                </section>
            </div>
        </div>


    @endcan

@endsection

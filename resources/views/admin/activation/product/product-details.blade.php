@extends('app-admin')
@section('content')

    <div class="content p-4">
        <h2 class="mb-4">Product edit</h2>

        <div class="row">
            <div class="col-5">
                <div class="card mb-4">
                    <activation-product-details-component product="{{ $product }}" activation="{{ $activation }}" can_edit="{{ $canEdit }}" can_delete="{{ $canDelete }}"></activation-product-details-component>
                </div>
            </div>
        </div>
    </div>

@endsection

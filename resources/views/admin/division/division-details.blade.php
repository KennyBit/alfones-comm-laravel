@extends('app-admin')
@section('content')

    @can('division-details')
        <div class="content p-4">
            <h2 class="mb-4">Division edit</h2>

            <div class="row">
                <div class="col-5">
                    <div class="card mb-4">
                        <division-details-component division="{{ $division }}" can_edit="{{ $canEdit }}" can_delete="{{ $canDelete }}"></division-details-component>
                    </div>
                </div>
            </div>

        </div>
    @endcan

@endsection

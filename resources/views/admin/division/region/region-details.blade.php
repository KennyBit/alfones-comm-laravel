@extends('app-admin')
@section('content')

    @can('region-details')
        <div class="content p-4">
            <h2 class="mb-4">Region edit</h2>

            <div class="row">
                <div class="col-5">
                    <div class="card mb-4">
                        <region-details-component division="{{ $division }}" region="{{ $region }}" can_edit="{{ $canEdit }}" can_delete="{{ $canDelete }}"></region-details-component>
                    </div>
                </div>
            </div>

        </div>
    @endcan

@endsection


@extends('app-admin')
@section('content')

    <div class="content p-4">
        <h2 class="mb-4">Region</h2>

        <region-component division="{{ $division }}" can_list="{{ $canList }}" can_create="{{ $canCreate }}" can_details="{{ $canDetails }}"></region-component>

    </div>


@endsection

@extends('app-admin')
@section('content')

    <div class="content p-4">
        <h2 class="mb-4">Divisions</h2>

        <division-component can_list="{{ $canList }}" can_create="{{ $canCreate }}" can_details="{{ $canDetails }}"></division-component>

    </div>


@endsection

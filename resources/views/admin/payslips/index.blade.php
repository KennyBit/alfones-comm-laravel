@extends('app-admin')
@section('content')

    @can('payslip-list')

        <div class="content p-4">
            <h2 class="mb-4">Payslips Centre</h2>

            <div class="card mb-4">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm">
                            <a href="{{ route('admin.payslip.create') }}" class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i> Add payslip</a>
                        </div>
                    </div>
                </div>

            <div class="row">
                <div class="col-10">
                    <div class="card mb-4">
                        <div class="card-body">
                            <payslip-component></payslip-component>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        </div>

    @endcan

@endsection

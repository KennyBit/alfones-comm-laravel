@extends('app-admin')
@section('content')

    @can('payslip-details')

        <div class="content p-4">
        <h2 class="mb-4">Payslip no #{{ $payslip->payslip_id }}</h2>
        
            <div class="row">
            <div class="col-8">
                <div class="card mb-6">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm">
                                <h5><b>User details</b></h5>
                                <hr>

                                Full names
                                <br><b> {{ $admin->name }}</b><br><br>
                                Phone number
                                <br><b> {{ $admin->phone_number }}</b><br><br>
                                Email
                                <br><b> {{ $admin->email }}</b>
                            </div>
                            <div class="col-sm">
                                <h5><b>Payslip details</b></h5>
                                <hr>
                                <b>Expense #{{ $payslip->payslip_id }}</b><br>

                                @can('payslip-printout')
                                    <a href="{{ route('image-get-leave', ['id' => $leave->attachment]) }}" target="_blank">Print payslip</a>
                                @endcan

                                <br><br>
                                Employee no <br>
                                <b>{{ $admin->employee_no }}</b>
                            </div>
                        </div>

                        <br><br>
                        <div class="row">
                            <div class="col-sm">
                                <h5><b>Payslip items</b></h5>
                                <table class="table table-hover" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th>Item</th>
                                        <th>Amount</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>Gross Pay</td>
                                        <td>{{ $payslip->gross_salary }}</td>
                                    </tr>
                                    <tr>
                                        <td>NSSF Contribution</td>
                                        <td>{{ $payslip->nssf_contibution }}</td>
                                    </tr>
                                    <tr>
                                        <td>Taxable pay</td>
                                        <td>{{ $payslip->taxable_pay }}</td>
                                    </tr>
                                    <tr>
                                        <td>Personal relief</td>
                                        <td>{{ $payslip->personal_relief }}</td>
                                    </tr>
                                    <tr>
                                        <td>Insurance relief</td>
                                        <td>{{ $payslip->insurance_relief }}</td>
                                    </tr>
                                    <tr>
                                        <td>Paye</td>
                                        <td>{{ $payslip->paye }}</td>
                                    </tr>
                                    <tr>
                                        <td>NHIF Contribution</td>
                                        <td>{{ $payslip->nhif_contribution }}</td>
                                    </tr>
                                    <tr>
                                        <td>Net Pay</td>
                                        <td>{{ $payslip->net_pay }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>

    @endcan

@endsection

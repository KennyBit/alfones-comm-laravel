@extends('app-admin')
@section('content')

        <div class="content p-4">
            <h2 class="mb-4">Create payslip</h2>

            <div class="row">
                @can('payslip-create')
                    <div class="col-5">
                        <div class="card mb-4">
                            <create-payslip-component></create-payslip-component>
                        </div>

                    </div>
                @endcan
            </div>
        </div>



@endsection

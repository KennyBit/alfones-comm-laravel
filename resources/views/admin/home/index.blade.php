@extends('app-admin')
@section('content')
    <div class="content p-4">
        <h2 class="mb-4">Admin dashboard</h2>

        <div class="row">
            <div class="col-7">
                <div class="card-columns">
                    <div class="card bg-light">
                        <div class="card-body text-center">
                            <h3 class="card-title" style="color: #c7254e">{{ $total_activations }}</h3>
                            <p class="card-text">Total Activations</p>
                        </div>
                    </div>
                    <div class="card bg-light">
                        <div class="card-body text-center">
                            <h3 class="card-title" style="color: #c7254e">{{ $total_users }}</h3>
                            <p class="card-text">Users</p>

                        </div>
                    </div>
                    <div class="card bg-light">
                        <div class="card-body text-center">
                            <h3 class="card-title" style="color: #c7254e">{{ $total_clients }}</h3>
                            <p class="card-text">Clients</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-7">
                <h4 class="mb-4">Recent activations</h4>
                <div class="card mb-4">
                    <div class="card-body">
                        <table id="example" class="table table-hover" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th style="width: 40%;">Name</th>
                                <th>Start date</th>
                                <th>End date</th>
                                <th class="actions">Reports</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($activations as $activation)
                                <tr>
                                    <td>{{ $loop->iteration  }}</td>
                                    <td style="width: 40%;">{{ $activation->name }}</td>
                                    <td>{{ $activation->start_date }}</td>
                                    <td>{{ $activation->end_date }}</td>
                                    <td>
                                        <a href="{{ route('admin.activation.reports', ['id' => $activation->id]) }}" class="btn btn-icon btn-pill btn-primary" data-toggle="tooltip" title="Edit"><i class="fa fa-fw fa-edit"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>


@endsection

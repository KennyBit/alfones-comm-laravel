<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="noindex">

    <title>Alfones Communication Solutions</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    {{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"> --}}
    
    <style>
        .text-right {
        text-align: right;
        }
        table,
        th,
        td {
            border: 1px solid black;
            border-collapse: collapse;
        }
    
        th,
        td {
            padding: 5px;
            text-align: left;
        }
    </style>

</head>

<body class="login-page" style="background: white">

    <div>
        <div class="center-block">
            <img style="height: 30px; width: 100px;" src="http://alfonesltd.com/assets/images/alfoneslogo.png" alt="logo">
        </div>
        <div class="row">
            <div  class="col-sm-9 ">
                <strong>Alfones Communications Solutions</strong><br>
                Jamhuri Park Wanjigi Ave. <br>
                Nairobi, Kenya - 61682-00200,<br>
                Phone: +254 722 899646 <br>
                Email: info@alfonesld.com.com <br>
                <br>
            </div>

            
        </div>

        <div style="margin-bottom: 0px">&nbsp;</div>

        <div class="row">
            <div class="col-xs-6">
                <h4>User</h4>
                <address>
                    <strong>{{ $quote->admin->name or '' }}</strong><br>
                    <span>{{ $quote->admin->email or '' }}</span> <br>
                    <span>{{ $quote->admin->phone_number or '' }}</span>
                </address>
            </div>

            <div class="col-xs-5">
                <table style="width: 100%">
                    <tbody>
                        <tr>
                            <th>Quote Num:</th>
                            <td class="text-right">#{{ $quote->id}}</td>
                        </tr>
                        <tr>
                            <th>Date: </th>
                            <td class="text-right">{{ $quote->date }}</td>
                        </tr>
                    </tbody>
                </table>

                <div style="margin-bottom: 0px">&nbsp;</div>

                <table style="width: 100%; margin-bottom: 20px">
                    <tbody>
                        <tr class="well" style="padding: 5px">
                            <th style="padding: 5px">
                                <div> Total Amount </div>
                            </th>
                            <td style="padding: 5px" class="text-right"><strong> Ksh {{ $quoteTotals }}.00
                                </strong></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <table class="table">
            {{-- <thead style="background: #F5F5F5;">
                <tr>
                    <th>Item Name</th>
                    <th>Description</th>
                    <th>Unit Cost</th>
                    <th>Quantity</th>
                    <th>Days</th>
                    <th class="text-right">Total</th>
                    <th>Agency Fee</th>
                    <th>Cumulative Total</th>
                </tr>
            </thead> --}}
            <tbody>
                @foreach ($quoteItems as $quoteItem)
                <tr>
                    <th>Item Name</th>
                    <td><strong>{{ $quoteItem->item_name}}</strong></td>
                </tr>
                <tr>
                    <th>Description</th>
                    <td>{{ $quoteItem->description}}</td>
                </tr>

                <tr>
                    <th>Unit Cost</th>
                    <td>{{ $quoteItem->unit_cost}}.00</td>
                </tr>
                <tr>
                    <th>Quantity</th>
                    <td>{{ $quoteItem->quantity}}</td>
                </tr>
                <tr>
                    <th>No of days</th>
                    <td>{{$quoteItem->days}}</td>
                </tr>
                <tr>
                    <th>Total</th>
                    <td class="text-right">Ksh {{ $quoteTotals}}.00</td>
                </tr>
                <tr>
                    <th>Agency Percentage</th>
                    <td>{{$agencyFeesPercentage}}%</td>
                </tr>
                <tr>
                    <th>Agency Fees</th>
                    <td>Ksh {{ $agencyFees}}.00</td>
                </tr>
                <tr>
                    <th>Cumulative Cost</th>
                    <td>Ksh {{ $quoteTotals + $agencyFees}}.00</td>
                </tr>
                    
                @endforeach

            </tbody>
        </table>

        <div class="row">
            <div class="col-xs-6"></div>
            <div class="col-xs-5">
                <table style="width: 100%">
                    <tbody>
                        <tr class="well" style="padding: 5px">
                            <th style="padding: 5px">
                                <div> Amount Given </div>
                            </th>
                            <td style="padding: 5px" class="text-right"><strong> Ksh {{ $quoteTotals }}.00
                                </strong></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div style="margin-bottom: 0px">&nbsp;</div>


    </div>

</body>
</html>



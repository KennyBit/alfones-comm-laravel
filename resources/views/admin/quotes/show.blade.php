@extends('app-admin')
@section('content')
<div class="container-fluid">
    <div class="p-4">
        <div class="row">
            <div class="col-md-6">
                <h2>Create Quote Items</h2>
                <div class="card">
                    <div class="card-body">
                        {!! Form::open(array(
                        'method' => 'POST',
                        'action' => ['Admin\QuotesController@quoteItem', $quote->id])) !!}
                        <form action="/action_page.php">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        {{Form::label('item_name', 'Item Name')}}
                                        {{Form::text('item_name', '', ['class' => 'form-control', 'placeholder' => 'Item Name'])}}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        {{Form::label('quantity', 'quantity')}}
                                        {{Form::text('quantity', '', ['class' => 'form-control', 'textarea', 'placeholder' => 'Quantity'])}}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        {{Form::label('days', 'Days')}}
                                        {{Form::text('days', '', ['class' => 'form-control', 'placeholder' => 'Days'])}}
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        {{Form::label('unit_cost', 'Unit Cost')}}
                                        {{Form::text('unit_cost', '', ['class' => 'form-control', 'placeholder' => 'Unit cost'])}}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                {{Form::label('description', 'Description')}}
                                {{Form::textarea('description', '', ['class' => 'form-control', 'rows' => "3", 'placeholder' => 'Description'])}}
                            </div>
                            
                            {{Form::submit('Submit', ['id' => 'submit', 'class' => 'btn btn-primary'])}}
                            {!! Form::close() !!}
                        </form>
                    </div>
                    <div class="card-body">
                        {{-- <h4>Add Agency Fee</h4> --}}
                        @if ($agencyFee > 0)
                        {{null}}
                        @elseif ($agencyFee <= 2)
                        {!! Form::open(array(
                        'method' => 'PUT',
                        'action' => ['Admin\QuotesController@agencyFee', $quote->id])) !!}
                        <form action="/action_page.php">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        {{Form::label('agency_fees_percentage', 'Agency Fee Percentage')}} <b>%</b>
                                        {{Form::text('agency_fees_percentage', '', ['class' => 'form-control', 'placeholder' => 'Agency Fee Percentage'])}} 
                                    </div>
                                </div>
                            </div>
                        
                            {{Form::submit('Generate Agency Fee', ['id' => 'submit', 'class' => 'btn btn-primary'])}}
                            {!! Form::close() !!}
                        </form>
                        @endif
                    </div>
                </div>
                
                
             </div> 
            <div class="col-md-6">
                <h2>{{$quote->name}} Details</h2>
                <div class="card">
                    <div class="card-body">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Item Name</th>
                                    <th>Quantity</th>
                                    <th>Days</th>
                                    <th>Unit Cost</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            @if(count($quoteItems) > 0)
                            <tbody>
                                @foreach($quoteItems as $quoteItem)
                                <tr>
                                    <td>{{$quoteItem->id}}</td>
                                    <td>{{$quoteItem->item_name}}</td>
                                    <td>{{$quoteItem->quantity}}</td>
                                    <td>{{$quoteItem->days}}</td>
                                    <td>{{$quoteItem->unit_cost}}</td>
                                    <td>{{$quoteItem->total}}</td>
                                    <td>{{$quote->status}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                            @else
                            <p>No quotes found</p>
                            @endif
                        
                        </table>
                    </div>
                    <div class="card-body">
                        <b>Total Cost Without Agency fee:</b> {{$quoteTotals}} <br>
                        <b>Agency Fee:</b> {{$agencyFee}} <br>
                        @if($agencyFee <= 0)
                        {{null}}
                        @elseif($agencyFee > 0)
                        <b>Cumulative total:</b> {{$agencyFee + $quoteTotals}}
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
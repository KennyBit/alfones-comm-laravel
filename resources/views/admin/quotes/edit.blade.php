@extends('app-admin')
@section('content')

<div class="container">
    <h3>Edit Quote</h3>

    {{-- {!! Form::open(array(
    'style' => 'display: inline-block;',
    'method' => 'PUT',
    'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
    'action' => ['Admin\RequisitionsController@approve', $requisition->id])) !!} --}}

    {!! Form::model($quote, ['method' => 'PUT', 'route' => ['admin.quote.update', $quote->id]]) !!}
    <div class="row">
        <div class="col-xs-12 form-group">
            {!! Form::label('name', trans('Name').'*', ['class' => 'control-label']) !!}
            {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}    
        </div>
        <div class="col-xs-12 form-group">
            {!! Form::label('client_name', trans('Client').'*', ['class' => 'control-label']) !!}
            {!! Form::text('client_name', old('client_name'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
        </div>
        <div class="col-xs-12 form-group">
            {!! Form::label('date', trans('Date').'*', ['class' => 'control-label']) !!}
            {!! Form::text('date', old('date'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
        </div>
    </div>
    {!! Form::submit(trans('Submit'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
</div>

@endsection
@extends('app-admin')
@section('content')
<div class="container-fluid">
  <div class="p-4">
    <div class="row">
      <div class="col-md-5">
        <h2>Create Quote</h2>
        <div class="card">
          <div class="card-body">
            {!! Form::open(['action' => 'Admin\QuotesController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
            <form action="/action_page.php">
              <div class="form-group">
                {{Form::label('Name', 'Name')}}
                {{Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Quote Name'])}}
              </div>
              <div class="form-group">
                {{Form::label('client_name', 'Client')}}
                {{Form::text('client_name', '', ['class' => 'form-control', 'placeholder' => 'Client Name'])}}
              </div>
              <div class="form-group">
                  {{Form::label('Date', 'Date')}}
                  {!! Form::text('date', '', array('id' => 'datepicker', 'placeholder' => 'Select Date', 'class' => 'form-control')) !!}
              </div>
              {{Form::submit('Submit', ['id' => 'submit', 'class' => 'btn btn-primary'])}}
              {!! Form::close() !!}
            </form>
          </div>
        </div>
      </div>
      <div class="col-md-7">
        <h2>Quotes</h2>   
        <div class="card">
          <div class="card-body">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>User</th>
                  <th>Client</th>
                  <th>Date</th>
                  <th>Action</th>
                </tr>
              </thead>
              @if(count($quotes) > 0)
              <tbody>
                @foreach($quotes as $quote)
                <tr>
                  <td>{{$quote->id}}</td>
                  <td><a href="{{ route('admin.quote.show', [$quote['id']]) }}">{{$quote->name}}</a></td>
                  <td>{{$quote->admin->name}}</td>
                  <td>{{$quote->client_name}}</td>
                  <td>{{$quote->date}}</td>
                  <td>
                    <a href="{{ route('admin.quote.edit', [$quote['id']]) }}" class="btn btn-icon btn-pill btn-primary"
                    data-toggle="tooltip" title="Edit"><i class="fa fa-fw fa-edit"></i></a>
                    <a href="{{action('Admin\QuotesController@downloadPDF', $quote->id)}}" class="btn btn-icon btn-pill btn-success"
                    data-toggle="tooltip" title="Print"><i class="fa fa-fw fa-print"></i></a>
                      {!! Form::open(array(
                      'style' => 'display: inline-block;',
                      'method' => 'DELETE',
                      'onsubmit' => "return confirm('".trans("Are you sure you want to delete quote?")."');",
                      'action' => ['Admin\QuotesController@delete', $quote->id])) !!}
                      {{ Form::button('<i class="fa fa-trash"></i>', ['data-toggle' => 'tooltip', 'title' => 'Delete','type' => 'submit', 'class' => 'btn btn-icon btn-danger btn-pill'] )  }}
                      {!! Form::close() !!}
                  </td> 
                </tr>
                @endforeach
              </tbody>
              @else
              <p>No quotes found</p>
              @endif
            
            </table>
          </div>  
        </div>    
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">

    $('.date').datepicker({  

       format: 'mm-dd-yyyy'

     });  

</script>
@endsection
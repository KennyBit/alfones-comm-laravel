@extends('app-admin')
@section('content')

    @can('leave-details')

        <div class="content p-4">
        <h2 class="mb-4">Leave id #{{ $leave->id }}</h2>
        
            <div class="row">
            <div class="col-8">
                <div class="card mb-6">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm">
                                <h5><b>User details</b></h5>
                                <hr>

                                Name
                                <br><b> {{ $admin->name }}</b><br><br>
                                Phone number
                                <br><b> {{ $admin->phone_number }}</b><br><br>
                                Email
                                <br><b> {{ $admin->email }}</b>
                            </div>
                            <div class="col-sm">
                                <h5><b>Leave details</b></h5>
                                <hr>
                                Reasons <br>
                                <b>{{ $leave->reasons }}</b>
                                <br><br>
                                Description <br>
                                <b>{{ $leave->description }}</b>

                                <br><br>
                                Leave status <br>

                                @if ($leave->status === 1)

                                    <span class="badge badge-success">Approved</span>

                                @elseif ($leave->status === 0)

                                    <span class="badge badge-warning">Pending</span>

                                @elseif ($leave->status === 2)

                                    <span class="badge badge-danger">Declined</span>

                                @endif
                            </div>
                        </div>

                        <br><br>

                        @can('leave-approve')
                            <br>
                            <div class="row">
                                <div class="col-sm">
                                    <div class="form-group">

                                        @if ($leave->status === 0)
                                                <a href="{{ route('admin.leave.status', ['id' => $leave->id, 'status' => '1']) }}" class="btn btn-success">Approve</a>
                                                <a href="{{ route('admin.leave.status', ['id' => $leave->id, 'status' => '2']) }}" class="btn btn-danger">Decline</a>
                                            @elseif(($leave->status === 1))
                                                <a href="{{ route('image-get-leave', ['id' => $leave->attachment]) }}" target="_blank">View attachment</a>
                                            @endif
                                    </div>
                                </div>
                            </div>
                        @endcan
                    </div>
                </div>

            </div>
        </div>

    </div>

    @endcan

@endsection

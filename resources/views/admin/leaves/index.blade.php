@extends('app-admin')
@section('content')

    @can('leave-list')
        <div class="content p-4">
            <h2 class="mb-4">Leaves Centre</h2>

            <div class="tab_container">
                <input id="tab1" type="radio" name="tabs" checked>
                <label for="tab1" class="label-tab"><span>Recent Applications</span></label>

                <input id="tab2" type="radio" name="tabs">
                <label for="tab2" class="label-tab"><span>User Application</span></label>

                <section id="content1" class="tab-content">
                    <h4 class="mb-4" style="color: #c7254e">Recent Application</h4>

                    <div class="row">
                        <div class="col-8">
                            <div class="card mb-4">
                                <div class="card-body">
                                    <table id="example1" class="table table-hover" cellspacing="0" width="100%">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th style="width: 30%;">Reasons</th>
                                            <th style="width: 25%;">User</th>
                                            <th>Date From</th>
                                            <th>Date To</th>
                                            <th>Status</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($recent_leaves as $recent_leave)
                                            <tr>
                                                <td> {{ $loop->iteration }}</td>

                                                @can('expenses-details')
                                                    <td style="width: 30%;"><a href="{{ route('admin.leave.details', [$recent_leave['id']]) }}">{{ $recent_leave['reasons'] }}</a></td>
                                                @else
                                                    <td style="width: 30%;">{{ $recent_leave['reasons'] }}</td>
                                                @endcan

                                                <td style="width: 25%;"> {{ $recent_leave['admin_name'] }} </td>

                                                <td>  {{ $recent_leave['date_from'] }} </td>

                                                <td> {{ $recent_leave['date_to'] }} </td>

                                                <td>
                                                    @if ($recent_leave['status'] === 1)

                                                        <span class="badge badge-success">Approved</span>

                                                    @elseif ($recent_leave['status'] === 0)

                                                        <span class="badge badge-warning">Pending</span>

                                                    @elseif ($recent_leave['status'] === 2)

                                                        <span class="badge badge-danger">Declined</span>

                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </section>

                <section id="content2" class="tab-content">
                    <h4 class="mb-4" style="color: #c7254e">User Application</h4>

                    <div class="row">
                        <div class="col-8">
                            <div class="card mb-4">
                                <div class="card-body">
                                    <table id="example1" class="table table-hover" cellspacing="0" width="100%">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th style="width: 30%;">User</th>
                                            <th>Allowed days</th>
                                            <th>Applications</th>
                                            <th>Remaining</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($admins as $admin)
                                            <tr>
                                                <td> {{ $loop->iteration }}</td>

                                                <td style="width: 30%;">{{ $admin['admin_name'] }}</td>

                                                <td> {{ $admin['allowed_days'] }} </td>

                                                <td> {{ $admin['applications'] }} </td>

                                                <td> {{ $admin['remaining'] }} </td>

                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </section>

            </div>
        </div>
    @endcan


@endsection

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://bootadmin.net/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://bootadmin.net/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="https://bootadmin.net/css/datatables.min.css">
    <link rel="stylesheet" href="https://bootadmin.net/css/fullcalendar.min.css">
    <link rel="stylesheet" href="https://bootadmin.net/css/bootadmin.min.css">

    <title>Alfones Communications | Login</title>

</head>
<body class="bg-light">

<div class="bg"></div>

<div class="container h-100">
    <div class="row h-100 justify-content-center align-items-center">
        <div class="col-md-4">
            <h1 class="text-center mb-4">Client Area</h1>
            @if(session()->has('message'))
                <div class="alert alert-info">

                    <p style="color:#000;"> {{ session()->get('message') }}</p>

                </div>
            @endif
            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ route('client.login.submit') }}">
                        {{ csrf_field() }}
                        <div class="input-group mb-3">
                            <input type="email" class="form-control" placeholder="Email address" name="email" value="{{ old('email') }}"><br>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="input-group mb-3">
                            <input type="password" class="form-control" placeholder="Password" name="password"><br>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <div class="form-check mb-3">
                            <label class="form-check-label">
                                <input type="checkbox" name="remember" class="form-check-input" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                Remember Me
                            </label>
                        </div>

                        <div class="row">
                            <div class="col pr-2">
                                <button type="submit" class="btn btn-block btn-primary">Login</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://bootadmin.net/js/jquery.min.js"></script>
<script src="https://bootadmin.net/js/bootstrap.bundle.min.js"></script>
<script src="https://bootadmin.net/js/datatables.min.js"></script>
<script src="https://bootadmin.net/js/moment.min.js"></script>
<script src="https://bootadmin.net/js/fullcalendar.min.js"></script>
<script src="https://bootadmin.net/js/bootadmin.min.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-118868344-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-118868344-1');
</script>

<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
    (adsbygoogle = window.adsbygoogle || []).push({
        google_ad_client: "ca-pub-4097235499795154",
        enable_page_level_ads: true
    });
</script>

</body>

</html>

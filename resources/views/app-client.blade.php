<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
@include('admin.partials.header')

<body class="bg-light">

@include('admin.partials.top-nav')

<div id="app">

    <div class="d-flex">

        @include('admin.partials.client-side-nav')

        @yield('content')

    </div>


</div>
@include('admin.partials.scripts')

</body>


</html>
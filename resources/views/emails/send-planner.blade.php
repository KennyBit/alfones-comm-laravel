<head>
    <title> {{ $name }} | Weekly planner</title>
</head>

<body>
    Hi, <br> <br>
    Trust you are well. <br> <br>
    Please find attached the weekly planner for {{ $name }} ({{ $start_date }}). <br> <br>
    Kind regards.
</body>
@extends('app-client')
@section('content')

    {{--@can('admin-details')--}}
        <div class="content p-4">
            <h2 class="mb-4">Interaction reports - {{ $date }}</h2>

            <div class="tab_container">
                <input id="tab1" type="radio" name="tabs" checked>
                <label for="tab1" class="label-tab"><span>Total / Targets</span></label>

                <input id="tab2" type="radio" name="tabs">
                <label for="tab2" class="label-tab"><span>BA</span></label>

                <input id="tab3" type="radio" name="tabs">
                <label for="tab3" class="label-tab"><span>Entries</span></label>

                <section id="content1" class="tab-content">

                    <h4 class="mb-4" style="color: #c7254e">Total / Target</h4>

                    <div class="row">
                        <div class="col-10">
                            <div class="card-columns">
                                <div class="card bg-light">
                                    <div class="card-body text-center">
                                        <h3 class="card-title" style="color: #c7254e">{{ $interaction_today_count }}</h3>
                                        <p class="card-text">Interactions today</p>
                                    </div>
                                </div>
                                <div class="card bg-light">
                                    <div class="card-body text-center">
                                        <h3 class="card-title" style="color: #c7254e">{{ $interaction_week_count }}</h3>
                                        <p class="card-text">Interaction this week</p>

                                    </div>
                                </div>
                                <div class="card bg-light">
                                    <div class="card-body text-center">
                                        <h3 class="card-title" style="color: #c7254e">{{ $interaction_total_count }}</h3>
                                        <p class="card-text">Total interactions</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-10">
                            <div class="card mb-4">
                                <div class="card-body">
                                    <table id="example1" class="table table-hover" cellspacing="0" width="100%">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th style="width: 40%;">Location</th>
                                            <th>Targets</th>
                                            <th>Achievements</th>
                                            <th>% Achievement</th>
                                            <th>Team leader</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($total_interactions as $total_interaction)
                                            <tr>
                                                <td>{{ $loop->iteration  }}</td>
                                                <td style="width: 40%;">
                                                    {{ $total_interaction['name'] }}
                                                </td>
                                                <td> {{ $total_interaction['interaction_target'] }} </td>
                                                <td>  {{ $total_interaction['interaction_count'] }} </td>
                                                <td> {{ $total_interaction['percentage_achievement'] }} % </td>
                                                <td> {{ $total_interaction['team_leader_name'] }} </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section id="content2" class="tab-content">
                    <h4 class="mb-4" style="color: #c7254e">Interactions by brand ambassador</h4>

                    <div class="row">
                        <div class="col-6">
                            <div class="card mb-4">
                                <div class="card-body">
                                    <table id="example2" class="table table-hover" cellspacing="0" width="100%">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th style="width: 50%;">Name</th>
                                            <th>Achievements</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($user_interactions as $user_interaction)
                                            <tr>
                                                <td>{{ $loop->iteration  }}</td>
                                                <td style="width: 40%;">
                                                    {{ $user_interaction['name'] }}
                                                </td>
                                                <td> {{ $user_interaction['interaction_count'] }} </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </section>

                <section id="content3" class="tab-content">
                    <h4 class="mb-4" style="color: #c7254e">Interaction entries</h4>

                    <div class="row">
                        <div class="col-12">
                            <div class="card mb-4">
                                <div class="card-body">
                                    <table id="example2" class="table table-hover" cellspacing="0" width="100%">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th style="width: 20%;">Customer name</th>
                                            <th>Phone number</th>
                                            <th>Feedback</th>
                                            <th>BA</th>
                                            <th>Location</th>
                                            <th>Attachment</th>
                                            <th>Date</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($entries as $entry)
                                            <tr>
                                                <td>{{ $loop->iteration  }}</td>
                                                <td style="width: 20%;">
                                                    {{ $entry['customer_name'] }}
                                                </td>
                                                <td>{{ $entry['customer_phone'] }}</td>
                                                <td>{{ $entry['customer_feedback'] }}</td>
                                                <td>{{ $entry['user_name'] }}</td>
                                                <td><a href="{{ route('activation-map-location', ['id'=>$activation, 'latitude'=>$latitude, 'longitude'=>$longitude]) }}" target="_blank">{{ $entry['location_name'] }}</a></td>
                                                @if($entry['image'] == "")
                                                    <td>No attachmemt</td>
                                                @else
                                                    <td><a href="{{ $entry['image'] }}" target="_blank">View image</a></td>
                                                @endif
                                                <td>{{ $entry['created_at'] }}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>


                </section>


            </div>
        </div>


    {{--@endcan--}}

@endsection

@extends('app-client')
@section('content')

        <div class="content p-4">
            <h2 class="mb-4">Merchandise reports - {{ $date }}</h2>

            <div class="tab_container">
                <input id="tab1" type="radio" name="tabs" checked>
                <label for="tab1" class="label-tab"><span>Total / Targets</span></label>

                <input id="tab2" type="radio" name="tabs">
                <label for="tab2" class="label-tab"><span>BA</span></label>

                <input id="tab3" type="radio" name="tabs">
                <label for="tab3" class="label-tab"><span>Entries</span></label>

                <input id="tab4" type="radio" name="tabs">
                <label for="tab4" class="label-tab"><span>Merchandise</span></label>

                <section id="content1" class="tab-content">

                    <h4 class="mb-4" style="color: #c7254e">Total / Target</h4>

                    <div class="row">
                        <div class="col-10">
                            <div class="card-columns">
                                <div class="card bg-light">
                                    <div class="card-body text-center">
                                        <h3 class="card-title" style="color: #c7254e">{{ $merchandise_today_count }}</h3>
                                        <p class="card-text">Distribution today</p>
                                    </div>
                                </div>
                                <div class="card bg-light">
                                    <div class="card-body text-center">
                                        <h3 class="card-title" style="color: #c7254e">{{ $merchandise_week_count }}</h3>
                                        <p class="card-text">Distribution this week</p>

                                    </div>
                                </div>
                                <div class="card bg-light">
                                    <div class="card-body text-center">
                                        <h3 class="card-title" style="color: #c7254e">{{ $merchandise_total_count }}</h3>
                                        <p class="card-text">Total merchandise</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-10">
                            <div class="card mb-4">
                                <div class="card-body">
                                    <table id="example1" class="table table-hover" cellspacing="0" width="100%">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th style="width: 40%;">Location</th>
                                            <th>Distribution</th>
                                            <th>Team leader</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($total_merchandises as $total_merchandise)
                                            <tr>
                                                <td>{{ $loop->iteration  }}</td>
                                                <td style="width: 40%;">
                                                    {{ $total_merchandise['name'] }}
                                                </td>
                                                <td>  {{ $total_merchandise['merchandise_count'] }} </td>
                                                <td> {{ $total_merchandise['team_leader_name'] }} </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section id="content2" class="tab-content">
                    <h4 class="mb-4" style="color: #c7254e">Distribution by brand ambassador</h4>

                    <div class="row">
                        <div class="col-6">
                            <div class="card mb-4">
                                <div class="card-body">
                                    <table id="example2" class="table table-hover" cellspacing="0" width="100%">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th style="width: 50%;">Name</th>
                                            <th>Achievements</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($user_merchandises as $user_merchandise)
                                            <tr>
                                                <td>{{ $loop->iteration  }}</td>
                                                <td style="width: 40%;">
                                                    {{ $user_merchandise['name'] }}
                                                </td>
                                                <td> {{ $user_merchandise['merchandise_count'] }} </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </section>

                <section id="content3" class="tab-content">
                    <h4 class="mb-4" style="color: #c7254e">Distribution entries</h4>

                    <div class="row">
                        <div class="col-12">
                            <div class="card mb-4">
                                <div class="card-body">
                                    <table id="example2" class="table table-hover" cellspacing="0" width="100%">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th style="width: 20%;">Customer name</th>
                                            <th>Phone number</th>
                                            <th>Product</th>
                                            <th>Quantity</th>
                                            <th>Feedback</th>
                                            <th>BA</th>
                                            <th>Location</th>
                                            <th>Image</th>
                                            <th>Date</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($entries as $entry)
                                            <tr>
                                                <td>{{ $loop->iteration  }}</td>
                                                <td style="width: 20%;"> {{ $entry['customer_name'] }} </td>
                                                <td>{{ $entry['customer_phone'] }}</td>
                                                <td>{{ $entry['merchandise_name'] }}</td>
                                                <td>{{ $entry['merchandise_quantity'] }}</td>
                                                <td>{{ $entry['customer_feedback'] }}</td>
                                                <td>{{ $entry['user_name'] }}</td>
                                                <td><a href="{{ route('activation-map-location', ['id'=>$activation, 'latitude'=>$latitude, 'longitude'=>$longitude]) }}" target="_blank">{{ $entry['location_name'] }}</a></td>
                                                @if($entry['image'] == "")
                                                    <td>No attachmemt</td>
                                                @else
                                                    <td><a href="{{ $entry['image'] }}" target="_blank">View image</a></td>
                                                @endif
                                                <td>{{ $entry['created_at'] }}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>


                </section>

                <section id="content4" class="tab-content">
                    <h4 class="mb-4" style="color: #c7254e">Distribution by merchandise</h4>

                    <div class="row">
                        <div class="col-6">
                            <div class="card mb-4">
                                <div class="card-body">
                                    <table id="example2" class="table table-hover" cellspacing="0" width="100%">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th style="width: 50%;">Name</th>
                                            <th>Total</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($merchandise_distributions as $merchandise_distribution)
                                            <tr>
                                                <td>{{ $loop->iteration  }}</td>
                                                <td style="width: 40%;">
                                                    {{ $merchandise_distribution['name'] }}
                                                </td>
                                                <td> {{ $merchandise_distribution['sales_count'] }} </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </section>


            </div>
        </div>


@endsection

@extends('app-client')
@section('content')

    <div class="content p-4">
        <h2 class="mb-4">Activations (Campaigns)</h2>

        <div class="card mb-4">
            <div class="card-body">
                Monitor your activations here,
            </div>
        </div>

        <div class="row">
            {{--@can('activation-list')--}}
                <div class="col-7">
                    <div class="card mb-4">
                        <div class="card-body">
                            <table id="example" class="table table-hover" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th style="width: 40%;">Name</th>
                                    <th>Start date</th>
                                    <th>End date</th>
                                    {{--@can('activation-details')--}}
                                        <th class="actions">Reports</th>
                                    {{--@endcan--}}
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($activations as $activation)
                                    <tr>
                                        <td>{{ $loop->iteration  }}</td>
                                        <td style="width: 40%;">{{ $activation->name }}</td>
                                        <td>{{ $activation->start_date }}</td>
                                        <td>{{ $activation->end_date }}</td>

                                        {{--@can('activation-details')--}}
                                            <td>
                                                <a href="{{ route('client.activation.reports', ['id' => $activation->id]) }}" class="btn btn-icon btn-pill btn-primary" data-toggle="tooltip" title="Edit"><i class="fa fa-fw fa-edit"></i></a>
                                            </td>
                                        {{--@endcan--}}
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            {{--@endcan--}}

        </div>

    </div>


@endsection

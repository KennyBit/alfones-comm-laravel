<?php

namespace App;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

class ActivationUserLocation extends Model
{
    use Uuids;

    protected $fillable = [
        'activation_id','user_id', 'location_id'
    ];

    public $incrementing = false;
}

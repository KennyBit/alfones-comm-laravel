<?php

namespace App;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

class UserLeave extends Model
{
    use Uuids;

    protected $fillable = [
        'admin_id', 'date_from', 'date_to', 'reasons', 'description', 'attachment', 'status', 'status_report'
    ];

    public $incrementing = false;
}

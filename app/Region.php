<?php

namespace App;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    use Uuids;

    protected $fillable = [
        'division_id','name'
    ];

    public $incrementing = false;
}

<?php

namespace App;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

class ExpenseGiveAmount extends Model
{
    use Uuids;

    protected $fillable = [
        'expense_id', 'amount'
    ];

    public $incrementing = false;
}

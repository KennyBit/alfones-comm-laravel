<?php

namespace App;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

class ActivationProductStock extends Model
{
    use Uuids;

    protected $fillable = [
        'activation_id','opening_stock', 'closing_stock', 'location_id', 'product_id', 'admin_id', 'date',
    ];

    public $incrementing = false;
}

<?php

namespace App;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

class PerformanceReport extends Model
{
    use Uuids;

    protected $fillable = [
        'admin_id','start_date','end_date','on_time','in_full','no_errors','target_achievement','overall_performance',
        'performance_percentage','percentage_overall_performance', 'cumulative_performance', 'overall_attendance'
    ];

    public $incrementing = false;
}

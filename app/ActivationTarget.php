<?php

namespace App;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

class ActivationTarget extends Model
{
    use Uuids;

    protected $fillable = [
        'activation_id','location_id', 'interaction_target', 'sales_target', 'date'
    ];

    public $incrementing = false;
}

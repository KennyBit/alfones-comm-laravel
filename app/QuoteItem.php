<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class QuoteItem extends Model
{
     protected $fillable = [
        'item_name',
        'days',
        'unit_cost',
        'quote_id',
        'quantity',
        'total',
        
        
    ];

    public function quote()
    {
        return $this->belongsTo(Quote::class, 'quote_id');
    }
}


// <?php

// namespace App;

// use Illuminate\Database\Eloquent\Model;

// class DynamicField extends Model
// {
//     protected $fillable = [
//         'first_name', 'middle_name', 'last_name'
//     ];
// }

<?php

namespace App;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

class ActivationType extends Model
{
    use Uuids;

    protected $fillable = [
        'activation_id','name'
    ];

    public $incrementing = false;
}

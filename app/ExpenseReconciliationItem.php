<?php

namespace App;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

class ExpenseReconciliationItem extends Model
{
    use Uuids;

    protected $fillable = [
        'expenses_id', 'item_expenses_id','item_supplier', 'item_name', 'item_description', 'item_unit_cost', 'item_quantity', 'item_days','item_amount', 'item_attachment'
    ];

    public $incrementing = false;
}

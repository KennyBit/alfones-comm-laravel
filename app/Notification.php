<?php

namespace App;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    use Uuids;

    protected $fillable = [
        'message', 'user_id', 'is_read'
    ];

    public $incrementing = false;

}

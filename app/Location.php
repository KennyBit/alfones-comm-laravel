<?php

namespace App;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    use Uuids;

    protected $fillable = [
        'activation_id', 'name', 'description', 'team_leader_id', 'date', 'duration', 'latitude', 'longitude','enable_geo_fence', 'location_identifier', 'division_id', 'region_id', 'location_comments'
    ];

    public $incrementing = false;

    public function getRouteKeyName()
    {
        return 'uuid';
    }
}

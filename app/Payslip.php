<?php

namespace App;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

class Payslip extends Model
{
    use Uuids;

    protected $fillable = [
        'admin_id','gross_salary','nssf_contibution','taxable_pay','personal_relief','insurance_relief','paye','nhif_contribution','net_pay','tax_rate', 'month', 'year'
    ];

    public $incrementing = false;
}

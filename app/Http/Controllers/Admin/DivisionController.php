<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DivisionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {

        $user = Auth::user();
        $data = [
            'canList' => $user->hasPermissionTo('division-list'),
            'canCreate' => $user->hasPermissionTo('division-create'),
            'canDetails' => $user->hasPermissionTo('division-details'),
        ];

        return view('admin.division.index', $data);
    }

    public function show($id)
    {

        $user = Auth::user();
        $data = [
            'division' => $id,
            'canEdit' => $user->hasPermissionTo('division-edit'),
            'canDelete' => $user->hasPermissionTo('division-delete'),

        ];
        return view('admin.division.division-details', $data);
    }

}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use niklasravnsborg\LaravelPdf\Facades\Pdf;

class PayslipController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        return view('admin.payslips.index');
    }

    public function create()
    {
        return view('admin.payslips.create-payslip');
    }


    public function show(Request $request)
    {
        $payslip =  DB::table('payslips')
            ->where('id', $request->id)
            ->first();

        if($payslip){
            $admin = DB::table('admins')
                ->where('id', $payslip->admin_id)
                ->first();

            $data = [
                'admin'=>$admin,
                'payslip'=>$payslip,
            ];

            return view('admin.payslips.payslip-details', $data);
        }


    }


    public function generatePDF(Request $request){

        $payslip =  DB::table('payslips')
            ->where('id', $request->id)
            ->first();

        if($payslip){
            $admin = DB::table('admins')
                ->where('id', $payslip->admin_id)
                ->first();

            $data = [
                'admin'=>$admin,
                'payslip'=>$payslip,
            ];

            $pdf = PDF::loadView('admin.activation.report.pdf.payslip', $data);
            $path = public_path().'/printouts/invoices/'.$order->order_id.'.pdf';
            $pdf->save($path);

            return $pdf->stream('admin.activation.report.pdf.payslip');
        }

    }
}

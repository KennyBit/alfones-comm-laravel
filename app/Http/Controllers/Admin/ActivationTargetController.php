<?php

namespace App\Http\Controllers\Admin;

use App\ActivationTarget;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ActivationTargetController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function show(Request $request)
    {
        $user = Auth::user();
        $data = [
            'target' => $request->id,
            'activation' => $request->activation_id,
            'canEdit' => $user->hasPermissionTo('activation-target-edit'),
            'canDelete' => $user->hasPermissionTo('activation-target-delete'),
        ];
        return view('admin.activation.target.target-details', $data);
    }

}

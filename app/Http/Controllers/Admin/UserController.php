<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {

        $user = Auth::user();
        $users = DB::table('users')->get();
        $data = [
            'users' => $users,
            'canCreate' => $user->hasPermissionTo('ba-create'),
        ];

        return view('admin.user.index', $data);
    }

    public function show($id)
    {

        $logs = DB::table('activity_log')
            ->where('causer_id', $id)
            ->orderBy('created_at', 'DESC')
            ->get();

        $user = Auth::user();
        $data = [
            'logs' => $logs,
            'user' => $id,
            'canEdit' => $user->hasPermissionTo('ba-edit'),
            'canDelete' => $user->hasPermissionTo('ba-delete'),
        ];
        return view('admin.user.user-details', $data);
    }

}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(){

        return view('admin.profile.index');
    }

    public function passwordChange(Request $request){

        $user = Auth::user();

        $admin =  DB::table('admins')->where('id', $user->id)->first();
        if($admin) {

            $email = $admin->email;
            $admin_id = $admin->id;
            $update_password = DB::table('admins')
                ->where('id', $admin_id)
                ->update([ 'password'=> bcrypt($request->password)]);

            if($update_password){

                //SEND EMAIL
                $mail_controller = new MailController();
                $mail_controller->passwordReset($admin->name, $email);

                return redirect('admin/admin-profile')->with('status', 'Password has been chnaged');

            }else{

                return redirect('admin/admin-profile')->with('status', 'Password change was not successful');

            }
        }
        return redirect('admin/admin-profile')->with('status', 'User not found');
    }
}

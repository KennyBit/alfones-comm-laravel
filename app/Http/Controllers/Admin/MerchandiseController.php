<?php

namespace App\Http\Controllers\Admin;

use App\Merchandise;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MerchandiseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $merchandises = DB::table('merchandises')->get();
        $data = [
            'merchandises' => $merchandises,
            'canCreate' => $user->hasPermissionTo('admin-create'),
        ];

        return view('admin.merchandise.index', $data);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function show(Request $request)
    {

        $user = Auth::user();
        $data = [
            'merchandise' => $request->id,
            'activation' => $request->activation_id,
            'canEdit' => $user->hasPermissionTo('activation-merchandise-edit'),
            'canDelete' => $user->hasPermissionTo('activation-merchandise-delete'),
        ];
        return view('admin.activation.merchandise.merchandise-details', $data);
    }

}

<?php

namespace App\Http\Controllers\Admin;

use App\Activation;
use App\Report;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use niklasravnsborg\LaravelPdf\Facades\Pdf;


class ActivationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $activations = DB::table('activations')->get();
        $data = [
            'activations' => $activations,
        ];

         return view('admin.activation.index', $data);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $user = Auth::user();

        $data = [
            'activation' => $request->id,
            'canEdit' => $user->hasPermissionTo('activation-edit'),
            'canDelete' => $user->hasPermissionTo('activation-delete'),
            'canTeamLeaderList' => $user->hasPermissionTo('activation-team-leader-list'),
            'canTeamLeaderCreate' => $user->hasPermissionTo('activation-team-leader-create'),
            'canTeamLeaderDetails' => $user->hasPermissionTo('activation-team-leader-details'),
            'canProductList' => $user->hasPermissionTo('activation-product-list'),
            'canProductCreate' => $user->hasPermissionTo('activation-product-create'),
            'canProductDetails' => $user->hasPermissionTo('activation-product-details'),
            'canMerchandiseList' => $user->hasPermissionTo('activation-merchandise-list'),
            'canMerchandiseCreate' => $user->hasPermissionTo('activation-merchandise-create'),
            'canMerchandiseDetails' => $user->hasPermissionTo('activation-merchandise-details'),
            'canLocationList' => $user->hasPermissionTo('activation-location-list'),
            'canLocationCreate' => $user->hasPermissionTo('activation-location-create'),
            'canLocationDetails' => $user->hasPermissionTo('activation-location-details'),
            'canTargetList' => $user->hasPermissionTo('activation-target-list'),
            'canTargetCreate' => $user->hasPermissionTo('activation-target-create'),
            'canTargetDetails' => $user->hasPermissionTo('activation-target-details'),
            'canUserList' => $user->hasPermissionTo('activation-user-list'),
            'canUserCreate' => $user->hasPermissionTo('activation-user-create'),
            'canUserDetails' => $user->hasPermissionTo('activation-user-details'),
        ];
        return view('admin.activation.activation-details', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function reports(Request $request)
    {
        $activation_id = $request->id;
        $todays_date = date('Y-m-d');
        $last_seven_day=date("Y-m-d",strtotime("-7 day", strtotime($todays_date)));

        $interaction_today_count = DB::table('reports')
            ->where('activation_id',$activation_id)
            ->whereRaw('DATE(created_at) = ?', [$todays_date])
            ->count();

        $interaction_week_count = DB::table('reports')
            ->where('activation_id',$activation_id)
            ->whereBetween('created_at', [$last_seven_day, $todays_date])
            ->count();

        $interaction_total_count = DB::table('reports')
            ->where('activation_id',$activation_id)
            ->count();


        $sales_today_count = DB::table('reports')
            ->where('activation_id',$activation_id)
            ->where('product', 1)
            ->whereRaw('DATE(created_at) = ?', [$todays_date])
            ->sum('product_quantity');

        $sales_week_count = DB::table('reports')
            ->where('activation_id',$activation_id)
            ->where('product', 1)
            ->whereBetween('created_at', [$last_seven_day, $todays_date])
            ->sum('product_quantity');

        $sales_total_count = DB::table('reports')
            ->where('product', 1)
            ->where('activation_id',$activation_id)
            ->sum('product_quantity');


        $merchandise_today_count = DB::table('reports')
            ->where('activation_id',$activation_id)
            ->where('merchandise', 1)
            ->whereRaw('DATE(created_at) = ?', [$todays_date])
            ->sum('merchandise_quantity');

        $merchandise_week_count = DB::table('reports')
            ->where('activation_id',$activation_id)
            ->where('merchandise', 1)
            ->whereBetween('created_at', [$last_seven_day, $todays_date])
            ->sum('merchandise_quantity');

        $merchandise_total_count = DB::table('reports')
            ->where('merchandise', 1)
            ->where('activation_id',$activation_id)
            ->sum('merchandise_quantity');


        // Get ba achievements
        $ba_performances = array();
        $activation_users = DB::table('activation_users')
            ->where('activation_id',$activation_id)
            ->get();

        foreach ($activation_users as $activation_user) {

            $user = DB::table('users')
                ->where('id', $activation_user->user_id)
                ->first();

            $interaction_count = DB::table('reports')
                ->where('activation_id',$activation_id)
                ->where('user_id', $activation_user->user_id)
                ->count();

            $sales_count = DB::table('reports')
                ->where('activation_id',$activation_id)
                ->where('product', 1)
                ->where('user_id', $activation_user->user_id)
                ->sum('product_quantity');

            $merchandise_count = DB::table('reports')
                ->where('activation_id',$activation_id)
                ->where('merchandise', 1)
                ->where('user_id', $activation_user->user_id)
                ->sum('merchandise_quantity');


            if ($user) {
                array_push($ba_performances,
                    array(
                        'user_id' => $user->id,
                        'name' => $user->name,
                        'phone_number' => $user->phone_number,
                        'email' => $user->email,
                        'interaction_count' => $interaction_count,
                        'sales_count' => $sales_count,
                        'merchandise_count' => $merchandise_count
                    ));
            }
        }

        if(!empty($ba_performances)){
            foreach ($ba_performances as $key => $row)
            {
                $vc_array_name[$key] = $row['sales_count'];
            }
            array_multisort($vc_array_name, SORT_ASC, $ba_performances);

        }

        // Get payrolls
        $payrolls = array();
        $activation_users = DB::table('activation_users')
            ->where('activation_id',$activation_id)
            ->get();

        foreach ($activation_users as $activation_user) {

            $user = DB::table('users')
                ->where('id', $activation_user->user_id)
                ->first();

            if ($user) {

                // Get activation dates
                $activation = DB::table('activations')
                    ->where('id', $activation_id)
                    ->first();

                if($activation){
                    $start_date = $activation->start_date;
                    $end_date = $activation->end_date;
                    $ba_unit_cost = $activation->ba_unit_cost;

                    $days = 0;
                    $total_cost = 0;
                    while(strtotime($start_date)<=strtotime($end_date)) {

                            $check_in = DB::table('activation_check_ins')
                                ->where('activation_id',$activation_id)
                                ->where('user_id', $activation_user->user_id)
                                ->whereRaw('DATE(created_at) = ?', [$start_date])
                                ->first();

                            $report_confirmation = DB::table('activation_reports')
                                ->where('activation_id',$activation_id)
                                ->where('user_id', $activation_user->user_id)
                                ->whereRaw('DATE(created_at) = ?', [$start_date])
                                ->first();

                            if($check_in){
                                if($report_confirmation){

                                    $days = $days+1;
                                    $total_cost = $total_cost+$ba_unit_cost;
                                }
                            }

                        $start_date = date("Y-m-d", strtotime("+1 day", strtotime($start_date)));
                    }

                    array_push($payrolls,
                        array(
                            'user_id' => $user->id,
                            'name' => $user->name,
                            'unit_cost' => $ba_unit_cost,
                            'total_days' => $days,
                            'total_cost' => $total_cost
                        ));
                }

            }

        }


        $data = [
            'activation' => $request->id,
            'interaction_today_count' => $interaction_today_count,
            'interaction_week_count' => $interaction_week_count,
            'interaction_total_count' => $interaction_total_count,
            'sales_today_count' => $sales_today_count,
            'sales_week_count' => $sales_week_count,
            'sales_total_count' => $sales_total_count,
            'merchandise_today_count' => $merchandise_today_count,
            'merchandise_week_count' => $merchandise_week_count,
            'merchandise_total_count' => $merchandise_total_count,
            'ba_performances' => $ba_performances,
            'payrolls' => $payrolls,
        ];

        return view('admin.activation.report.index', $data);
    }


    public function interactionDetails(Request $request)
    {
        /**
         * Get request data
         */
        $activation_id = $request->id;
        $date = $request->date;

        // Get total/target achievements
        $total_interactions = array();
        $activation_locations = DB::table('locations')
            ->where('activation_id',$activation_id)
            ->get();

        foreach ($activation_locations as $activation_location) {

            $interaction_count = DB::table('reports')
                ->where('activation_id',$activation_id)
                ->where('location_id', $activation_location->id)
                ->whereRaw('DATE(created_at) = ?', [$date])
                ->count();

            $interaction_target = 0;
            $percentage_achievement = 0;
            $target = DB::table('activation_targets')
                ->where('activation_id', $activation_location->activation_id)
                ->where('location_id', $activation_location->id)
                ->where('date', $date)
                ->first();
            if($target){
                $interaction_target = $target->interaction_target;
            }

            if($interaction_target != 0){
                if($interaction_count !=0 ){
                    $percentage_achievement =  ($interaction_count/$interaction_target)*100;
                }
            }

            if ($activation_locations) {

                $team_leader_name = "";
                $team_leader = DB::table('admins')
                    ->where('id',$activation_location->team_leader_id)
                    ->first();
                if($team_leader){
                    $team_leader_name = $team_leader->name;
                }

                array_push($total_interactions,
                    array(
                        'id' => $activation_location->id,
                        'name' => $activation_location->name,
                        'team_leader_id' => $activation_location->team_leader_id,
                        'team_leader_name' => $team_leader_name,
                        'latitude' => $activation_location->latitude,
                        'longitude' => $activation_location->longitude,
                        'interaction_count' => $interaction_count,
                        'interaction_target' => $interaction_target,
                        'percentage_achievement' => $percentage_achievement
                    ));
            }
        }

        // Get ba achievements
        $user_interactions = array();
        $activation_users = DB::table('activation_users')
            ->where('activation_id',$activation_id)
            ->get();

        foreach ($activation_users as $activation_user) {

            $user = DB::table('users')
                ->where('id', $activation_user->user_id)
                ->first();

            $interaction_count = DB::table('reports')
                ->where('activation_id',$activation_id)
                ->where('user_id', $activation_user->user_id)
                ->whereRaw('DATE(created_at) = ?', [$date])
                ->count();

            if ($user) {
                array_push($user_interactions,
                    array(
                        'user_id' => $user->id,
                        'name' => $user->name,
                        'phone_number' => $user->phone_number,
                        'email' => $user->email,
                        'interaction_count' => $interaction_count
                    ));
            }
        }


        // Get entries
        $report_entries = array();
        $entries = DB::table('reports')
            ->where('activation_id',$activation_id)
            ->whereRaw('DATE(created_at) = ?', [$date])
            ->get();

        foreach ($entries as $entry) {

            $user_name = "";
            $user = DB::table('users')
                ->where('id', $entry->user_id)
                ->first();
            if($user){
                $user_name = $user->name;
            }

            $location_name = "";
            $activation_location = DB::table('locations')
                ->where('id',$entry->location_id)
                ->first();
            if($activation_location){
                $location_name =  $activation_location->name;
            }

            if ($user) {
                array_push($report_entries,
                    array(
                        'id' => $entry->id,
                        'user_name' => $user->name,
                        'customer_name' => $entry->customer_name,
                        'customer_phone' => $entry->customer_phone,
                        'customer_id' => $entry->customer_id,
                        'customer_feedback' => $entry->customer_feedback,
                        'location_name' => $location_name,
                        'latitude' => $entry->latitude,
                        'longitude' => $entry->longitude,
                        'image' => $entry->image,
                        'created_at' => $entry->created_at,

                    ));
            }
        }

        $interaction_today_count = DB::table('reports')
            ->where('activation_id',$activation_id)
            ->whereRaw('DATE(created_at) = ?', [$date])
            ->count();

        $todays_date = date('Y-m-d');
        $last_seven_day=date("Y-m-d",strtotime("-7 day", strtotime($todays_date)));

        $interaction_week_count = DB::table('reports')
            ->where('activation_id',$activation_id)
            ->whereBetween('created_at', [$last_seven_day, $todays_date])
            ->count();

        $interaction_total_count = DB::table('reports')
            ->where('activation_id',$activation_id)
            ->count();


        $data = [
            'activation' => $activation_id,
            'date' => $date,
            'total_interactions' => $total_interactions,
            'user_interactions' => $user_interactions,
            'entries' => $report_entries,
            'interaction_today_count' => $interaction_today_count,
            'interaction_week_count' => $interaction_week_count,
            'interaction_total_count' => $interaction_total_count,
        ];
        return view('admin.activation.report.interactions.interaction-details', $data);
    }

    public function salesDetails(Request $request)
    {
        /**
         * Get request data
         */
        $activation_id = $request->id;
        $date = $request->date;

        // Get total/target achievements
        $total_sales= array();
        $activation_locations = DB::table('locations')
            ->where('activation_id',$activation_id)
            ->get();

        foreach ($activation_locations as $activation_location) {

            $sales_count = DB::table('reports')
                ->where('activation_id',$activation_id)
                ->where('product', 1)
                ->where('location_id', $activation_location->id)
                ->whereRaw('DATE(created_at) = ?', [$date])
                ->sum('product_quantity');

            $sales_target = 0;
            $percentage_achievement = 0;
            $target = DB::table('activation_targets')
                ->where('activation_id', $activation_location->activation_id)
                ->where('location_id', $activation_location->id)
                ->where('date', $date)
                ->first();
            if($target){
                $sales_target = $target->sales_target;
            }

            if($sales_target != 0){
                if($sales_count !=0 ){
                    $percentage_achievement =  ($sales_count/$sales_target)*100;
                }
            }

            if ($activation_locations) {

                $team_leader_name = "";
                $team_leader = DB::table('admins')
                    ->where('id',$activation_location->team_leader_id)
                    ->first();
                if($team_leader){
                    $team_leader_name = $team_leader->name;
                }

                array_push($total_sales,
                    array(
                        'id' => $activation_location->id,
                        'name' => $activation_location->name,
                        'team_leader_id' => $activation_location->team_leader_id,
                        'team_leader_name' => $team_leader_name,
                        'latitude' => $activation_location->latitude,
                        'longitude' => $activation_location->longitude,
                        'sales_count' => $sales_count,
                        'sales_target' => $sales_target,
                        'percentage_achievement' => $percentage_achievement
                    ));
            }
        }

        // Get ba achievements
        $user_sales = array();
        $activation_users = DB::table('activation_users')
            ->where('activation_id',$activation_id)
            ->get();

        foreach ($activation_users as $activation_user) {

            $user = DB::table('users')
                ->where('id', $activation_user->user_id)
                ->first();

            $sales_count = DB::table('reports')
                ->where('activation_id',$activation_id)
                ->where('product', 1)
                ->where('user_id', $activation_user->user_id)
                ->whereRaw('DATE(created_at) = ?', [$date])
                ->sum('product_quantity');

            if ($user) {
                array_push($user_sales,
                    array(
                        'user_id' => $user->id,
                        'name' => $user->name,
                        'phone_number' => $user->phone_number,
                        'email' => $user->email,
                        'sales_count' => $sales_count
                    ));
            }
        }


        // Get entries
        $report_entries = array();
        $entries = DB::table('reports')
            ->where('activation_id',$activation_id)
            ->where('product', 1)
            ->whereRaw('DATE(created_at) = ?', [$date])
            ->get();

        foreach ($entries as $entry) {

            $user_name = "";
            $user = DB::table('users')
                ->where('id', $entry->user_id)
                ->first();
            if($user){
                $user_name = $user->name;
            }

            $location_name = "";
            $activation_location = DB::table('locations')
                ->where('id',$entry->location_id)
                ->first();
            if($activation_location){
                $location_name =  $activation_location->name;
            }

            $product_name = "";
            $activation_product = DB::table('products')
                ->where('id',$entry->product_id)
                ->first();
            if($activation_product){
                $product_name =  $activation_product->name;
            }

            if ($user) {
                array_push($report_entries,
                    array(
                        'id' => $entry->id,
                        'user_name' => $user->name,
                        'customer_name' => $entry->customer_name,
                        'customer_phone' => $entry->customer_phone,
                        'customer_id' => $entry->customer_id,
                        'customer_feedback' => $entry->customer_feedback,
                        'product_name' => $product_name,
                        'product_quantity' => $entry->product_quantity,
                        'product_value' => $entry->product_value,
                        'location_name' => $location_name,
                        'latitude' => $entry->latitude,
                        'longitude' => $entry->longitude,
                        'image' => $entry->image,
                        'created_at' => $entry->created_at,

                    ));
            }
        }

        // Get sales per product
        $product_sales = array();
        $activation_products = DB::table('products')
            ->where('activation_id',$activation_id)
            ->get();

        foreach ($activation_products as $activation_product) {

            $sales_count = DB::table('reports')
                ->where('activation_id',$activation_id)
                ->where('product', 1)
                ->where('product_id', $activation_product->id)
                ->whereRaw('DATE(created_at) = ?', [$date])
                ->sum('product_quantity');

                array_push($product_sales,
                    array(
                        'product_id' => $activation_product->id,
                        'name' => $activation_product->name,
                        'sales_count' => $sales_count
                    ));
        }

        $sales_today_count = DB::table('reports')
            ->where('activation_id',$activation_id)
            ->whereRaw('DATE(created_at) = ?', [$date])
            ->where('product', 1)
            ->sum('product_quantity');

        $todays_date = date('Y-m-d');
        $last_seven_day=date("Y-m-d",strtotime("-7 day", strtotime($todays_date)));

        $sales_week_count = DB::table('reports')
            ->where('activation_id',$activation_id)
            ->whereBetween('created_at', [$last_seven_day, $todays_date])
            ->where('product', 1)
            ->sum('product_quantity');

        $sales_total_count = DB::table('reports')
            ->where('activation_id',$activation_id)
            ->where('product', 1)
            ->sum('product_quantity');


        $data = [
            'activation' => $activation_id,
            'date' => $date,
            'total_sales' => $total_sales,
            'user_sales' => $user_sales,
            'product_sales' => $product_sales,
            'entries' => $report_entries,
            'sales_today_count' => $sales_today_count,
            'sales_week_count' => $sales_week_count,
            'sales_total_count' => $sales_total_count,
            'locations' => $activation_locations,
        ];
        return view('admin.activation.report.sales.sales-details', $data);
    }



    public function merchandiseDetails(Request $request)
    {
        /**
         * Get request data
         */
        $activation_id = $request->id;
        $date = $request->date;

        // Get total/target achievements
        $total_merchandise = array();
        $activation_locations = DB::table('locations')
            ->where('activation_id',$activation_id)
            ->get();

        foreach ($activation_locations as $activation_location) {

            $merchandise_count = DB::table('reports')
                ->where('activation_id',$activation_id)
                ->where('merchandise', 1)
                ->where('location_id', $activation_location->id)
                ->whereRaw('DATE(created_at) = ?', [$date])
                ->sum('merchandise_quantity');


            if ($activation_locations) {

                $team_leader_name = "";
                $team_leader = DB::table('admins')
                    ->where('id',$activation_location->team_leader_id)
                    ->first();
                if($team_leader){
                    $team_leader_name = $team_leader->name;
                }

                array_push($total_merchandise,
                    array(
                        'id' => $activation_location->id,
                        'name' => $activation_location->name,
                        'team_leader_id' => $activation_location->team_leader_id,
                        'team_leader_name' => $team_leader_name,
                        'latitude' => $activation_location->latitude,
                        'longitude' => $activation_location->longitude,
                        'merchandise_count' => $merchandise_count,
                    ));
            }
        }

        // Get ba achievements
        $user_merchandise = array();
        $activation_users = DB::table('activation_users')
            ->where('activation_id',$activation_id)
            ->get();

        foreach ($activation_users as $activation_user) {

            $user = DB::table('users')
                ->where('id', $activation_user->user_id)
                ->first();

            $merchandise_count = DB::table('reports')
                ->where('activation_id',$activation_id)
                ->where('merchandise', 1)
                ->where('user_id', $activation_user->user_id)
                ->whereRaw('DATE(created_at) = ?', [$date])
                ->sum('merchandise_quantity');

            if ($user) {
                array_push($user_merchandise,
                    array(
                        'user_id' => $user->id,
                        'name' => $user->name,
                        'phone_number' => $user->phone_number,
                        'email' => $user->email,
                        'merchandise_count' => $merchandise_count
                    ));
            }
        }


        // Get entries
        $report_entries = array();
        $entries = DB::table('reports')
            ->where('activation_id',$activation_id)
            ->where('merchandise', 1)
            ->whereRaw('DATE(created_at) = ?', [$date])
            ->get();

        foreach ($entries as $entry) {

            $user_name = "";
            $user = DB::table('users')
                ->where('id', $entry->user_id)
                ->first();
            if($user){
                $user_name = $user->name;
            }

            $location_name = "";
            $activation_location = DB::table('locations')
                ->where('id',$entry->location_id)
                ->first();
            if($activation_location){
                $location_name =  $activation_location->name;
            }

            $merchandise_name = "";
            $activation_merchandise = DB::table('merchandises')
                ->where('id',$entry->merchandise_id)
                ->first();
            if($activation_merchandise){
                $merchandise_name =  $activation_merchandise->name;
            }

            if ($user) {
                array_push($report_entries,
                    array(
                        'id' => $entry->id,
                        'user_name' => $user->name,
                        'customer_name' => $entry->customer_name,
                        'customer_phone' => $entry->customer_phone,
                        'customer_id' => $entry->customer_id,
                        'customer_feedback' => $entry->customer_feedback,
                        'merchandise_name' => $merchandise_name,
                        'merchandise_quantity' => $entry->merchandise_quantity,
                        'location_name' => $location_name,
                        'latitude' => $entry->latitude,
                        'longitude' => $entry->longitude,
                        'image' => $entry->image,
                        'created_at' => $entry->created_at,

                    ));
            }
        }

        // Get sales per merchandise
        $merchandise_sales = array();
        $activation_merchandises = DB::table('merchandises')
            ->where('activation_id',$activation_id)
            ->get();

        foreach ($activation_merchandises as $activation_merchandise) {

            $sales_count = DB::table('reports')
                ->where('activation_id',$activation_id)
                ->where('merchandise', 1)
                ->where('merchandise_id', $activation_merchandise->id)
                ->whereRaw('DATE(created_at) = ?', [$date])
                ->sum('merchandise_quantity');

            array_push($merchandise_sales,
                array(
                    'merchandise_id' => $activation_merchandise->id,
                    'name' => $activation_merchandise->name,
                    'sales_count' => $sales_count
                ));
        }

        $merchandise_today_count = DB::table('reports')
            ->where('activation_id',$activation_id)
            ->whereRaw('DATE(created_at) = ?', [$date])
            ->where('merchandise', 1)
            ->sum('merchandise_quantity');

        $todays_date = date('Y-m-d');
        $last_seven_day=date("Y-m-d",strtotime("-7 day", strtotime($todays_date)));

        $merchandise_week_count = DB::table('reports')
            ->where('activation_id',$activation_id)
            ->whereBetween('created_at', [$last_seven_day, $todays_date])
            ->where('merchandise', 1)
            ->sum('merchandise_quantity');

        $merchandise_total_count = DB::table('reports')
            ->where('activation_id',$activation_id)
            ->where('merchandise', 1)
            ->sum('merchandise_quantity');


        $data = [
            'activation' => $activation_id,
            'date' => $date,
            'total_merchandises' => $total_merchandise,
            'user_merchandises' => $user_merchandise,
            'merchandise_distributions' => $merchandise_sales,
            'entries' => $report_entries,
            'merchandise_today_count' => $merchandise_today_count,
            'merchandise_week_count' => $merchandise_week_count,
            'merchandise_total_count' => $merchandise_total_count,
        ];
        return view('admin.activation.report.merchandise.merchandise-details', $data);
    }



    public function interactionDetailsSummaryReport(Request $request){

        $activation_id = $request->id;
        $date = $request->date;

        // Get total/target achievements
        $total_interactions = array();
        $activation_locations = DB::table('locations')
            ->where('activation_id',$activation_id)
            ->get();

        foreach ($activation_locations as $activation_location) {

            $interaction_count = DB::table('reports')
                ->where('activation_id',$activation_id)
                ->where('location_id', $activation_location->id)
                ->whereRaw('DATE(created_at) = ?', [$date])
                ->count();

            $interaction_target = 0;
            $percentage_achievement = 0;
            $target = DB::table('activation_targets')
                ->where('activation_id', $activation_location->activation_id)
                ->where('location_id', $activation_location->id)
                ->where('date', $date)
                ->first();
            if($target){
                $interaction_target = $target->interaction_target;
            }

            if($interaction_target != 0){
                if($interaction_count !=0 ){
                    $percentage_achievement =  ($interaction_count/$interaction_target)*100;
                }
            }

            if ($activation_locations) {

                $team_leader_name = "";
                $team_leader = DB::table('admins')
                    ->where('id',$activation_location->team_leader_id)
                    ->first();
                if($team_leader){
                    $team_leader_name = $team_leader->name;
                }

                array_push($total_interactions,
                    array(
                        'id' => $activation_location->id,
                        'name' => $activation_location->name,
                        'team_leader_id' => $activation_location->team_leader_id,
                        'team_leader_name' => $team_leader_name,
                        'latitude' => $activation_location->latitude,
                        'longitude' => $activation_location->longitude,
                        'interaction_count' => $interaction_count,
                        'interaction_target' => $interaction_target,
                        'percentage_achievement' => $percentage_achievement
                    ));
            }
        }

        $activation_name = "";
        $activation = DB::table('activations')
            ->where('id', $activation_id)
            ->first();
        if($activation){
            $activation_name = $activation->name;
        }

        Excel::create('interaction-summary-report', function($excel) use ($activation_id, $activation_name, $date, $total_interactions) {
            $excel->sheet('interaction-summary-report', function($sheet) use ($activation_id, $activation_name, $date, $total_interactions) {
                $sheet->loadView('admin.activation.report.xls.interaction-details-summary-report')->with(['total_interactions' => $total_interactions, 'date'=> $date, 'activation_id'=>$activation_id, 'activation_name'=>$activation_name]);
            });
        })->download('xlsx');


    }

    public function interactionDetailsBaReport(Request $request){

        $activation_id = $request->id;
        $date = $request->date;

        // Get ba achievements
        $user_interactions = array();
        $activation_users = DB::table('activation_users')
            ->where('activation_id',$activation_id)
            ->get();

        foreach ($activation_users as $activation_user) {

            $user = DB::table('users')
                ->where('id', $activation_user->user_id)
                ->first();

            $interaction_count = DB::table('reports')
                ->where('activation_id',$activation_id)
                ->where('user_id', $activation_user->user_id)
                ->whereRaw('DATE(created_at) = ?', [$date])
                ->count();

            if ($user) {
                array_push($user_interactions,
                    array(
                        'user_id' => $user->id,
                        'name' => $user->name,
                        'phone_number' => $user->phone_number,
                        'email' => $user->email,
                        'interaction_count' => $interaction_count
                    ));
            }
        }

        $activation_name = "";
        $activation = DB::table('activations')
            ->where('id', $activation_id)
            ->first();
        if($activation){
            $activation_name = $activation->name;
        }

        Excel::create('interaction-ba-report', function($excel) use ($activation_name, $date, $user_interactions) {
            $excel->sheet('interaction-ba-report', function($sheet) use ($activation_name, $date, $user_interactions) {
                $sheet->loadView('admin.activation.report.xls.interaction-details-ba-report')->with(['user_interactions' => $user_interactions, 'date'=> $date, 'activation_name'=>$activation_name]);
            });
        })->download('xlsx');


    }

    public function interactionDetailsEntriesReport(Request $request){

        $activation_id = $request->id;
        $date = $request->date;

        $report_entries = array();
        $entries = DB::table('reports')
            ->where('activation_id',$activation_id)
            ->whereRaw('DATE(created_at) = ?', [$date])
            ->get();

        foreach ($entries as $entry) {

            $user_name = "";
            $user = DB::table('users')
                ->where('id', $entry->user_id)
                ->first();
            if($user){
                $user_name = $user->name;
            }

            $location_name = "";
            $activation_location = DB::table('locations')
                ->where('id',$entry->location_id)
                ->first();
            if($activation_location){
                $location_name =  $activation_location->name;
            }

            if ($user) {
                array_push($report_entries,
                    array(
                        'id' => $entry->id,
                        'user_name' => $user->name,
                        'customer_name' => $entry->customer_name,
                        'customer_phone' => $entry->customer_phone,
                        'customer_id' => $entry->customer_id,
                        'customer_feedback' => $entry->customer_feedback,
                        'location_name' => $location_name,
                        'latitude' => $entry->latitude,
                        'longitude' => $entry->longitude,
                        'image' => $entry->image,
                        'created_at' => $entry->created_at,

                    ));
            }
        }

        $activation_name = "";
        $activation = DB::table('activations')
            ->where('id', $activation_id)
            ->first();
        if($activation){
            $activation_name = $activation->name;
        }

        Excel::create('interaction-entries-report', function($excel) use ($activation_id, $activation_name, $date, $report_entries) {
            $excel->sheet('interaction-entries-report', function($sheet) use ($activation_id, $activation_name, $date, $report_entries) {
                $sheet->loadView('admin.activation.report.xls.interaction-details-entries-report')->with(['entries' => $report_entries, 'date'=> $date, 'activation_id'=>$activation_id, 'activation_name'=>$activation_name]);
            });
        })->download('xlsx');


    }

    public function interactionSummaryReport(Request $request){

        $activation_id = $request->id;

        // Get total/target achievements
        $total_interactions = array();
        $activation_locations = DB::table('locations')
            ->where('activation_id',$activation_id)
            ->get();

        foreach ($activation_locations as $activation_location) {

            $interaction_count = DB::table('reports')
                ->where('activation_id',$activation_id)
                ->where('location_id', $activation_location->id)
                ->count();

            $interaction_target = 0;
            $percentage_achievement = 0;
            $target = DB::table('activation_targets')
                ->where('activation_id', $activation_location->activation_id)
                ->where('location_id', $activation_location->id)
                ->first();
            if($target){
                $interaction_target = $target->interaction_target;
            }

            if($interaction_target != 0){
                if($interaction_count !=0 ){
                    $percentage_achievement =  ($interaction_count/$interaction_target)*100;
                }
            }

            if ($activation_locations) {

                $team_leader_name = "";
                $team_leader = DB::table('admins')
                    ->where('id',$activation_location->team_leader_id)
                    ->first();
                if($team_leader){
                    $team_leader_name = $team_leader->name;
                }

                array_push($total_interactions,
                    array(
                        'id' => $activation_location->id,
                        'name' => $activation_location->name,
                        'team_leader_id' => $activation_location->team_leader_id,
                        'team_leader_name' => $team_leader_name,
                        'latitude' => $activation_location->latitude,
                        'longitude' => $activation_location->longitude,
                        'interaction_count' => $interaction_count,
                        'interaction_target' => $interaction_target,
                        'percentage_achievement' => $percentage_achievement
                    ));
            }
        }

        $activation_name = "";
        $activation = DB::table('activations')
            ->where('id', $activation_id)
            ->first();
        if($activation){
            $activation_name = $activation->name;
        }

        $file_name = "Interaction summary report - $activation_name";
        Excel::create($file_name, function($excel) use ($activation_name, $total_interactions) {
            $excel->sheet('interaction-summary-report', function($sheet) use ($activation_name, $total_interactions) {
                $sheet->loadView('admin.activation.report.xls.interaction-summary-report')->with(['total_interactions' => $total_interactions, 'activation_name'=>$activation_name]);
            });
        })->download('xlsx');


    }

    public function interactionBaReport(Request $request){

        $activation_id = $request->id;
        $date = $request->date;

        // Get ba achievements
        $user_interactions = array();
        $activation_users = DB::table('activation_users')
            ->where('activation_id',$activation_id)
            ->get();

        foreach ($activation_users as $activation_user) {

            $user = DB::table('users')
                ->where('id', $activation_user->user_id)
                ->first();

            $interaction_count = DB::table('reports')
                ->where('activation_id',$activation_id)
                ->where('user_id', $activation_user->user_id)
                ->count();

            if ($user) {
                array_push($user_interactions,
                    array(
                        'user_id' => $user->id,
                        'name' => $user->name,
                        'phone_number' => $user->phone_number,
                        'email' => $user->email,
                        'interaction_count' => $interaction_count
                    ));
            }
        }

        $activation_name = "";
        $activation = DB::table('activations')
            ->where('id', $activation_id)
            ->first();
        if($activation){
            $activation_name = $activation->name;
        }

        $file_name = "Interaction summary by ba - $activation_name";
        Excel::create($file_name, function($excel) use ($activation_name, $user_interactions) {
            $excel->sheet('interaction-ba-report', function($sheet) use ($activation_name, $user_interactions) {
                $sheet->loadView('admin.activation.report.xls.interaction-ba-report')->with(['user_interactions' => $user_interactions, 'activation_name'=>$activation_name]);
            });
        })->download('xlsx');


    }

    public function interactionEntriesReport(Request $request){

        $activation_id = $request->id;

        $report_entries = array();
        $entries = DB::table('reports')
            ->where('activation_id',$activation_id)
            ->get();

        foreach ($entries as $entry) {

            $user_name = "";
            $user = DB::table('users')
                ->where('id', $entry->user_id)
                ->first();
            if($user){
                $user_name = $user->name;
            }

            $location_name = "";
            $activation_location = DB::table('locations')
                ->where('id',$entry->location_id)
                ->first();
            if($activation_location){
                $location_name =  $activation_location->name;
            }

            if ($user) {
                array_push($report_entries,
                    array(
                        'id' => $entry->id,
                        'user_name' => $user->name,
                        'customer_name' => $entry->customer_name,
                        'customer_phone' => $entry->customer_phone,
                        'customer_id' => $entry->customer_id,
                        'customer_feedback' => $entry->customer_feedback,
                        'location_name' => $location_name,
                        'latitude' => $entry->latitude,
                        'longitude' => $entry->longitude,
                        'image' => $entry->image,
                        'created_at' => $entry->created_at,

                    ));
            }
        }

        $activation_name = "";
        $activation = DB::table('activations')
            ->where('id', $activation_id)
            ->first();
        if($activation){
            $activation_name = $activation->name;
        }

        $file_name = "Interaction entries - $activation_name";
        Excel::create($file_name, function($excel) use ($activation_id, $activation_name, $report_entries) {
            $excel->sheet('interaction-entries-report', function($sheet) use ($activation_id, $activation_name, $report_entries) {
                $sheet->loadView('admin.activation.report.xls.interaction-entries-report')->with(['entries' => $report_entries, 'activation_name'=>$activation_name, 'activation_id'=>$activation_id]);
            });
        })->download('xlsx');


    }


    public function salesSummaryReport(Request $request){

        $activation_id = $request->id;

        // Get total/target achievements
        $total_sales= array();
        $activation_locations = DB::table('locations')
            ->where('activation_id',$activation_id)
            ->get();

        foreach ($activation_locations as $activation_location) {

            $sales_count = DB::table('reports')
                ->where('activation_id',$activation_id)
                ->where('product', 1)
                ->where('location_id', $activation_location->id)
                ->sum('product_quantity');

            $sales_target = 0;
            $percentage_achievement = 0;
            $target = DB::table('activation_targets')
                ->where('activation_id', $activation_location->activation_id)
                ->where('location_id', $activation_location->id)
                ->first();
            if($target){
                $sales_target = $target->sales_target;
            }

            if($sales_target != 0){
                if($sales_count !=0 ){
                    $percentage_achievement =  ($sales_count/$sales_target)*100;
                }
            }

            if ($activation_locations) {

                $team_leader_name = "";
                $team_leader = DB::table('admins')
                    ->where('id',$activation_location->team_leader_id)
                    ->first();
                if($team_leader){
                    $team_leader_name = $team_leader->name;
                }

                array_push($total_sales,
                    array(
                        'id' => $activation_location->id,
                        'name' => $activation_location->name,
                        'team_leader_id' => $activation_location->team_leader_id,
                        'team_leader_name' => $team_leader_name,
                        'latitude' => $activation_location->latitude,
                        'longitude' => $activation_location->longitude,
                        'sales_count' => $sales_count,
                        'sales_target' => $sales_target,
                        'percentage_achievement' => $percentage_achievement
                    ));
            }
        }

        $activation_name = "";
        $activation = DB::table('activations')
            ->where('id', $activation_id)
            ->first();
        if($activation){
            $activation_name = $activation->name;
        }

        $file_name = "Sales summary report - $activation_name";
        Excel::create($file_name, function($excel) use ($activation_name, $total_sales) {
            $excel->sheet('sales-summary-report', function($sheet) use ($activation_name, $total_sales) {
                $sheet->loadView('admin.activation.report.xls.sales-summary-report')->with(['total_sales' => $total_sales, 'activation_name'=>$activation_name]);
            });
        })->download('xlsx');


    }

    public function salesBaReport(Request $request){

        $activation_id = $request->id;

        $user_sales = array();
        $activation_users = DB::table('activation_users')
            ->where('activation_id',$activation_id)
            ->get();

        foreach ($activation_users as $activation_user) {

            $user = DB::table('users')
                ->where('id', $activation_user->user_id)
                ->first();

            $sales_count = DB::table('reports')
                ->where('activation_id',$activation_id)
                ->where('product', 1)
                ->where('user_id', $activation_user->user_id)
                ->sum('product_quantity');

            if ($user) {
                array_push($user_sales,
                    array(
                        'user_id' => $user->id,
                        'name' => $user->name,
                        'phone_number' => $user->phone_number,
                        'email' => $user->email,
                        'sales_count' => $sales_count
                    ));
            }
        }


        $activation_name = "";
        $activation = DB::table('activations')
            ->where('id', $activation_id)
            ->first();
        if($activation){
            $activation_name = $activation->name;
        }

        $file_name = "Sales summary report by ba - $activation_name";
        Excel::create($file_name, function($excel) use ($activation_name, $user_sales) {
            $excel->sheet('sales-ba-report', function($sheet) use ($activation_name, $user_sales) {
                $sheet->loadView('admin.activation.report.xls.sales-ba-report')->with(['user_sales' => $user_sales, 'activation_name'=>$activation_name]);
            });
        })->download('xlsx');


    }

    public function salesEntriesReport(Request $request){

        $activation_id = $request->id;

        $report_entries = array();
        $entries = DB::table('reports')
            ->where('activation_id',$activation_id)
            ->where('product', 1)
            ->get();

        foreach ($entries as $entry) {

            $user_name = "";
            $user = DB::table('users')
                ->where('id', $entry->user_id)
                ->first();
            if($user){
                $user_name = $user->name;
            }

            $location_name = "";
            $activation_location = DB::table('locations')
                ->where('id',$entry->location_id)
                ->first();
            if($activation_location){
                $location_name =  $activation_location->name;
            }

            $product_name = "";
            $activation_product = DB::table('products')
                ->where('id',$entry->product_id)
                ->first();
            if($activation_product){
                $product_name =  $activation_product->name;
            }

            if ($user) {
                array_push($report_entries,
                    array(
                        'id' => $entry->id,
                        'user_name' => $user->name,
                        'customer_name' => $entry->customer_name,
                        'customer_phone' => $entry->customer_phone,
                        'customer_id' => $entry->customer_id,
                        'customer_feedback' => $entry->customer_feedback,
                        'product_name' => $product_name,
                        'product_quantity' => $entry->product_quantity,
                        'product_value' => $entry->product_value,
                        'location_name' => $location_name,
                        'latitude' => $entry->latitude,
                        'longitude' => $entry->longitude,
                        'image' => $entry->image,
                        'created_at' => $entry->created_at,

                    ));
            }
        }

        $activation_name = "";
        $activation = DB::table('activations')
            ->where('id', $activation_id)
            ->first();
        if($activation){
            $activation_name = $activation->name;
        }

        $file_name = "Sales summary entries - $activation_name";
        Excel::create($file_name, function($excel) use ($activation_name, $report_entries) {
            $excel->sheet('sales-entries-report', function($sheet) use ($activation_name, $report_entries) {
                $sheet->loadView('admin.activation.report.xls.sales-entries-report')->with(['entries' => $report_entries, 'activation_name'=>$activation_name]);
            });
        })->download('xlsx');


    }

    public function salesDetailsSummaryReport(Request $request){

        $activation_id = $request->id;
        $date = $request->date;

        // Get total/target achievements
        $total_sales= array();
        $activation_locations = DB::table('locations')
            ->where('activation_id',$activation_id)
            ->get();

        foreach ($activation_locations as $activation_location) {

            $sales_count = DB::table('reports')
                ->where('activation_id',$activation_id)
                ->where('product', 1)
                ->where('location_id', $activation_location->id)
                ->whereRaw('DATE(created_at) = ?', [$date])
                ->sum('product_quantity');

            $sales_target = 0;
            $percentage_achievement = 0;
            $target = DB::table('activation_targets')
                ->where('activation_id', $activation_location->activation_id)
                ->where('location_id', $activation_location->id)
                ->where('date', $date)
                ->first();
            if($target){
                $sales_target = $target->sales_target;
            }

            if($sales_target != 0){
                if($sales_count !=0 ){
                    $percentage_achievement =  ($sales_count/$sales_target)*100;
                }
            }

            if ($activation_locations) {

                $team_leader_name = "";
                $team_leader = DB::table('admins')
                    ->where('id',$activation_location->team_leader_id)
                    ->first();
                if($team_leader){
                    $team_leader_name = $team_leader->name;
                }

                array_push($total_sales,
                    array(
                        'id' => $activation_location->id,
                        'name' => $activation_location->name,
                        'team_leader_id' => $activation_location->team_leader_id,
                        'team_leader_name' => $team_leader_name,
                        'latitude' => $activation_location->latitude,
                        'longitude' => $activation_location->longitude,
                        'sales_count' => $sales_count,
                        'sales_target' => $sales_target,
                        'percentage_achievement' => $percentage_achievement
                    ));
            }
        }

        $activation_name = "";
        $activation = DB::table('activations')
            ->where('id', $activation_id)
            ->first();
        if($activation){
            $activation_name = $activation->name;
        }

        Excel::create('sales-summary-report', function($excel) use ($activation_name, $total_sales, $date) {
            $excel->sheet('sales-summary-report', function($sheet) use ($activation_name, $total_sales, $date) {
                $sheet->loadView('admin.activation.report.xls.sales-details-summary-report')->with(['total_sales' => $total_sales, 'activation_name'=>$activation_name, 'date'=> $date]);
            });
        })->download('xlsx');


    }

    public function salesDetailsBaReport(Request $request){

        $activation_id = $request->id;
        $date = $request->date;

        $user_sales = array();
        $activation_users = DB::table('activation_users')
            ->where('activation_id',$activation_id)
            ->get();

        foreach ($activation_users as $activation_user) {

            $user = DB::table('users')
                ->where('id', $activation_user->user_id)
                ->first();

            $sales_count = DB::table('reports')
                ->where('activation_id',$activation_id)
                ->where('product', 1)
                ->where('user_id', $activation_user->user_id)
                ->whereRaw('DATE(created_at) = ?', [$date])
                ->sum('product_quantity');

            if ($user) {
                array_push($user_sales,
                    array(
                        'user_id' => $user->id,
                        'name' => $user->name,
                        'phone_number' => $user->phone_number,
                        'email' => $user->email,
                        'sales_count' => $sales_count
                    ));
            }
        }


        $activation_name = "";
        $activation = DB::table('activations')
            ->where('id', $activation_id)
            ->first();
        if($activation){
            $activation_name = $activation->name;
        }

        $file_name = "Daily sales report summary - $activation_name - $date";
        Excel::create($file_name, function($excel) use ($activation_name, $user_sales, $date) {
            $excel->sheet('sales-ba-report', function($sheet) use ($activation_name, $user_sales, $date) {
                $sheet->loadView('admin.activation.report.xls.sales-details-ba-report')->with(['user_sales' => $user_sales, 'activation_name'=>$activation_name, 'date' => $date]);
            });
        })->download('xlsx');


    }

    public function salesDetailsEntriesReport(Request $request){

        $activation_id = $request->id;
        $date = $request->date;

        // Get entries
        $report_entries = array();
        $entries = DB::table('reports')
            ->where('activation_id',$activation_id)
            ->where('product', 1)
            ->whereRaw('DATE(created_at) = ?', [$date])
            ->get();

        foreach ($entries as $entry) {

            $user_name = "";
            $user = DB::table('users')
                ->where('id', $entry->user_id)
                ->first();
            if($user){
                $user_name = $user->name;
            }

            $location_name = "";
            $activation_location = DB::table('locations')
                ->where('id',$entry->location_id)
                ->first();
            if($activation_location){
                $location_name =  $activation_location->name;
            }

            $product_name = "";
            $activation_product = DB::table('products')
                ->where('id',$entry->product_id)
                ->first();
            if($activation_product){
                $product_name =  $activation_product->name;
            }

            if ($user) {
                array_push($report_entries,
                    array(
                        'id' => $entry->id,
                        'user_name' => $user->name,
                        'customer_name' => $entry->customer_name,
                        'customer_phone' => $entry->customer_phone,
                        'customer_id' => $entry->customer_id,
                        'customer_feedback' => $entry->customer_feedback,
                        'product_name' => $product_name,
                        'product_quantity' => $entry->product_quantity,
                        'product_value' => $entry->product_value,
                        'location_name' => $location_name,
                        'latitude' => $entry->latitude,
                        'longitude' => $entry->longitude,
                        'image' => $entry->image,
                        'created_at' => $entry->created_at,

                    ));
            }
        }

        $activation_name = "";
        $activation = DB::table('activations')
            ->where('id', $activation_id)
            ->first();
        if($activation){
            $activation_name = $activation->name;
        }

        $file_name = "Daily sales report entries - $activation_name - $date";
        Excel::create($file_name, function($excel) use ($activation_name, $report_entries, $date) {
            $excel->sheet('sales-entries-report', function($sheet) use ($activation_name, $report_entries, $date) {
                $sheet->loadView('admin.activation.report.xls.sales-details-entries-report')->with(['entries' => $report_entries, 'activation_name'=>$activation_name, 'date' => $date]);
            });
        })->download('xlsx');


    }


    public function salesSummaryRawReport(Request $request){

        $activation_id = $request->id;
        // Get activation dates
        $activation =  DB::table('activations')
            ->where('id', $activation_id)
            ->first();

        $report_entries = array();

        if($activation) {
            $start_date = $activation->start_date;
            $end_date = $activation->end_date;

            while (strtotime($start_date) <= strtotime($end_date)) {

                // Get location details
                $activation_locations = DB::table('locations')
                    ->where('activation_id', $activation_id)
                    ->get();


                foreach ($activation_locations as $activation_location) {

                    // Get location id
                    $location_id = $activation_location->id;
                    $location_name = $activation_location->name;
                    $location_identifier = $activation_location->location_identifier;

                    $division_id = $activation_location->division_id;
                    $region_id = $activation_location->region_id;

                    $division_name = "";
                    $division = DB::table('divisions')
                        ->where('id', $division_id)
                        ->first();
                    if($division){
                        $division_name = $division->name;
                    }

                    $region_name = "";
                    $region = DB::table('regions')
                        ->where('id', $region_id)
                        ->first();
                    if($region){
                        $region_name = $region->name;
                    }


                    // Get products
                    $activation_products = DB::table('products')
                        ->where('activation_id', $activation_id)
                        ->get();

                    foreach ($activation_products as $activation_product) {

                        $product_id = $activation_product->id;
                        $product_name = $activation_product->name;


                        $base_volume = 0;
                        $product_quantity = 0;
                        $product_price = 0;
                        $activation_outlet_product_summary =  DB::table('activation_outlet_product_summaries')
                            ->where('product_id', $product_id)
                            ->where('activation_id', $activation_id)
                            ->where('location_id', $location_id)
                            ->where('date', $start_date)
                            ->first();

                        if($activation_outlet_product_summary){
                            $product_price = $activation_outlet_product_summary->product_price;
                            $product_quantity = $activation_outlet_product_summary->quantity;
                            $base_volume = $activation_outlet_product_summary->base_volume;
                        }


                        $opening_stock = 0;
                        $closing_stock = 0;
                        $activation_product_stocks =  DB::table('activation_product_stocks')
                            ->where('product_id', $product_id)
                            ->where('activation_id', $activation_id)
                            ->where('location_id', $location_id)
                            ->where('date', $start_date)
                            ->first();

                        if($activation_product_stocks){
                            $opening_stock = $activation_product_stocks->opening_stock;
                            $closing_stock = $activation_product_stocks->closing_stock;
                        }


                        array_push($report_entries,
                            array(
                                'date' => $start_date,
                                'day' => $dayOfWeek = date("l", strtotime($start_date)),
                                'division' => $division_name,
                                'region' => $region_name,
                                'location' => $location_name,
                                'location_identifier' => $location_identifier,
                                'product_name' => $product_name,
                                'product_price' => $product_price,
                                'product_quantity' => $product_quantity,
                                'base_volume' => $base_volume,
                                'opening_stock' => $opening_stock,
                                'closing_stock' => $closing_stock,


                            ));

                    }


                }


                $start_date = date ("Y-m-d", strtotime("+1 day", strtotime($start_date)));
            }
        }

        $activation_name = "";
        $activation = DB::table('activations')
            ->where('id', $activation_id)
            ->first();
        if($activation){
            $activation_name = $activation->name;
        }


        Excel::create('sales-summary-entries-report', function($excel) use ($activation_name, $report_entries) {
            $excel->sheet('sales-summary-entries-report', function($sheet) use ($activation_name, $report_entries) {
                $sheet->loadView('admin.activation.report.xls.sales-entries-summary-report')->with(['entries' => $report_entries, 'activation_name'=>$activation_name]);
            });
        })->download('xlsx');


    }

    public function salesDetailsSummaryRawReport(Request $request){

        $activation_id = $request->id;
        $date = $request->date;

        // Get activation dates
        $activation =  DB::table('activations')
            ->where('id', $activation_id)
            ->first();

        $report_entries = array();

        $start_date = $date;
        $end_date = $date;

        if($activation) {

            while (strtotime($start_date) <= strtotime($end_date)) {

                // Get location details
                $activation_locations = DB::table('locations')
                    ->where('activation_id', $activation_id)
                    ->get();


                foreach ($activation_locations as $activation_location) {

                    // Get location id
                    $location_id = $activation_location->id;
                    $location_name = $activation_location->name;
                    $location_identifier = $activation_location->location_identifier;

                    $division_id = $activation_location->division_id;
                    $region_id = $activation_location->region_id;

                    $division_name = "";
                    $division = DB::table('divisions')
                        ->where('id', $division_id)
                        ->first();
                    if($division){
                        $division_name = $division->name;
                    }

                    $region_name = "";
                    $region = DB::table('regions')
                        ->where('id', $region_id)
                        ->first();
                    if($region){
                        $region_name = $region->name;
                    }


                    // Get products
                    $activation_products = DB::table('products')
                        ->where('activation_id', $activation_id)
                        ->get();

                    foreach ($activation_products as $activation_product) {

                        $product_id = $activation_product->id;
                        $product_name = $activation_product->name;


                        $base_volume = 0;
                        $product_quantity = 0;
                        $product_price = 0;
                        $activation_outlet_product_summary =  DB::table('activation_outlet_product_summaries')
                            ->where('product_id', $product_id)
                            ->where('activation_id', $activation_id)
                            ->where('location_id', $location_id)
                            ->where('date', $start_date)
                            ->first();

                        if($activation_outlet_product_summary){
                            $product_price = $activation_outlet_product_summary->product_price;
                            $product_quantity = $activation_outlet_product_summary->quantity;
                            $base_volume = $activation_outlet_product_summary->base_volume;
                        }


                        $opening_stock = 0;
                        $closing_stock = 0;
                        $activation_product_stocks =  DB::table('activation_product_stocks')
                            ->where('product_id', $product_id)
                            ->where('activation_id', $activation_id)
                            ->where('location_id', $location_id)
                            ->where('date', $start_date)
                            ->first();

                        if($activation_product_stocks){
                            $opening_stock = $activation_product_stocks->opening_stock;
                            $closing_stock = $activation_product_stocks->closing_stock;
                        }


                        array_push($report_entries,
                            array(
                                'date' => $start_date,
                                'day' => $dayOfWeek = date("l", strtotime($start_date)),
                                'division' => $division_name,
                                'region' => $region_name,
                                'location' => $location_name,
                                'location_identifier' => $location_identifier,
                                'product_name' => $product_name,
                                'product_price' => $product_price,
                                'product_quantity' => $product_quantity,
                                'base_volume' => $base_volume,
                                'opening_stock' => $opening_stock,
                                'closing_stock' => $closing_stock,


                            ));

                    }


                }


                $start_date = date ("Y-m-d", strtotime("+1 day", strtotime($start_date)));
            }
        }

        $activation_name = "";
        $activation = DB::table('activations')
            ->where('id', $activation_id)
            ->first();
        if($activation){
            $activation_name = $activation->name;
        }


        $file_name = "Daily sales report entries - $activation_name - $date";
        Excel::create($file_name, function($excel) use ($date, $activation_name, $report_entries) {
            $excel->sheet('sales-entries-report', function($sheet) use ($date, $activation_name, $report_entries) {
                $sheet->loadView('admin.activation.report.xls.sales-details-entries-summary-report')->with(['entries' => $report_entries, 'date'=> $date, 'activation_name'=>$activation_name]);
            });
        })->download('xlsx');


    }


    public function merchandiseSummaryReport(Request $request){

        $activation_id = $request->id;

        // Get total/target achievements
        $total_merchandise = array();
        $activation_locations = DB::table('locations')
            ->where('activation_id',$activation_id)
            ->get();

        foreach ($activation_locations as $activation_location) {

            $merchandise_count = DB::table('reports')
                ->where('activation_id',$activation_id)
                ->where('merchandise', 1)
                ->where('location_id', $activation_location->id)
                ->sum('merchandise_quantity');


            if ($activation_locations) {

                $team_leader_name = "";
                $team_leader = DB::table('admins')
                    ->where('id',$activation_location->team_leader_id)
                    ->first();
                if($team_leader){
                    $team_leader_name = $team_leader->name;
                }

                array_push($total_merchandise,
                    array(
                        'id' => $activation_location->id,
                        'name' => $activation_location->name,
                        'team_leader_id' => $activation_location->team_leader_id,
                        'team_leader_name' => $team_leader_name,
                        'latitude' => $activation_location->latitude,
                        'longitude' => $activation_location->longitude,
                        'merchandise_count' => $merchandise_count,
                    ));
            }
        }

        $activation_name = "";
        $activation = DB::table('activations')
            ->where('id', $activation_id)
            ->first();
        if($activation){
            $activation_name = $activation->name;
        }


        $file_name = "Merchandise report summary - $activation_name";
        Excel::create($file_name, function($excel) use ($activation_name, $total_merchandise) {
            $excel->sheet('merchandise-summary-report', function($sheet) use ($activation_name, $total_merchandise) {
                $sheet->loadView('admin.activation.report.xls.merchandise-summary-report')->with(['total_merchandises' => $total_merchandise, 'activation_name'=>$activation_name]);
            });
        })->download('xlsx');


    }

    public function merchandiseBaReport(Request $request){

        $activation_id = $request->id;

        $user_merchandise = array();
        $activation_users = DB::table('activation_users')
            ->where('activation_id',$activation_id)
            ->get();

        foreach ($activation_users as $activation_user) {

            $user = DB::table('users')
                ->where('id', $activation_user->user_id)
                ->first();

            $merchandise_count = DB::table('reports')
                ->where('activation_id',$activation_id)
                ->where('merchandise', 1)
                ->where('user_id', $activation_user->user_id)
                ->sum('merchandise_quantity');

            if ($user) {
                array_push($user_merchandise,
                    array(
                        'user_id' => $user->id,
                        'name' => $user->name,
                        'phone_number' => $user->phone_number,
                        'email' => $user->email,
                        'merchandise_count' => $merchandise_count
                    ));
            }
        }

        $activation_name = "";
        $activation = DB::table('activations')
            ->where('id', $activation_id)
            ->first();
        if($activation){
            $activation_name = $activation->name;
        }

        $file_name = "Merchandise report summary by BA - $activation_name";
        Excel::create($file_name, function($excel) use ($activation_name, $user_merchandise) {
            $excel->sheet('merchandise-ba-report', function($sheet) use ($activation_name, $user_merchandise) {
                $sheet->loadView('admin.activation.report.xls.merchandise-ba-report')->with(['user_merchandises' => $user_merchandise, 'activation_name'=>$activation_name]);
            });
        })->download('xlsx');


    }

    public function merchandiseEntriesReport(Request $request){

        $activation_id = $request->id;

        $report_entries = array();
        $entries = DB::table('reports')
            ->where('activation_id',$activation_id)
            ->where('merchandise', 1)
            ->get();

        foreach ($entries as $entry) {

            $user_name = "";
            $user = DB::table('users')
                ->where('id', $entry->user_id)
                ->first();
            if($user){
                $user_name = $user->name;
            }

            $location_name = "";
            $activation_location = DB::table('locations')
                ->where('id',$entry->location_id)
                ->first();
            if($activation_location){
                $location_name =  $activation_location->name;
            }

            $merchandise_name = "";
            $activation_merchandise = DB::table('merchandises')
                ->where('id',$entry->merchandise_id)
                ->first();
            if($activation_merchandise){
                $merchandise_name =  $activation_merchandise->name;
            }

            if ($user) {
                array_push($report_entries,
                    array(
                        'id' => $entry->id,
                        'user_name' => $user->name,
                        'customer_name' => $entry->customer_name,
                        'customer_phone' => $entry->customer_phone,
                        'customer_id' => $entry->customer_id,
                        'customer_feedback' => $entry->customer_feedback,
                        'merchandise_name' => $merchandise_name,
                        'merchandise_quantity' => $entry->merchandise_quantity,
                        'location_name' => $location_name,
                        'latitude' => $entry->latitude,
                        'longitude' => $entry->longitude,
                        'image' => $entry->image,
                        'created_at' => $entry->created_at,

                    ));
            }
        }


        $activation_name = "";
        $activation = DB::table('activations')
            ->where('id', $activation_id)
            ->first();
        if($activation){
            $activation_name = $activation->name;
        }

        $file_name = "Merchandise report entries - $activation_name";
        Excel::create($file_name, function($excel) use ($activation_name, $report_entries) {
            $excel->sheet('merchandise-entries-report', function($sheet) use ($activation_name, $report_entries) {
                $sheet->loadView('admin.activation.report.xls.merchandise-entries-report')->with(['entries' => $report_entries, 'activation_name'=>$activation_name]);
            });
        })->download('xlsx');


    }

    public function merchandiseDetailsSummaryReport(Request $request){

        $activation_id = $request->id;
        $date = $request->date;

        $total_merchandise = array();
        $activation_locations = DB::table('locations')
            ->where('activation_id',$activation_id)
            ->get();

        foreach ($activation_locations as $activation_location) {

            $merchandise_count = DB::table('reports')
                ->where('activation_id',$activation_id)
                ->where('merchandise', 1)
                ->where('location_id', $activation_location->id)
                ->whereRaw('DATE(created_at) = ?', [$date])
                ->sum('merchandise_quantity');


            if ($activation_locations) {

                $team_leader_name = "";
                $team_leader = DB::table('admins')
                    ->where('id',$activation_location->team_leader_id)
                    ->first();
                if($team_leader){
                    $team_leader_name = $team_leader->name;
                }

                array_push($total_merchandise,
                    array(
                        'id' => $activation_location->id,
                        'name' => $activation_location->name,
                        'team_leader_id' => $activation_location->team_leader_id,
                        'team_leader_name' => $team_leader_name,
                        'latitude' => $activation_location->latitude,
                        'longitude' => $activation_location->longitude,
                        'merchandise_count' => $merchandise_count,
                    ));
            }
        }

        $activation_name = "";
        $activation = DB::table('activations')
            ->where('id', $activation_id)
            ->first();
        if($activation){
            $activation_name = $activation->name;
        }

        $file_name = "Daily merchandise report summary - $activation_name - $date";
        Excel::create($file_name, function($excel) use ($activation_name, $total_merchandise) {
            $excel->sheet('merchandise-summary-report', function($sheet) use ($activation_name, $total_merchandise) {
                $sheet->loadView('admin.activation.report.xls.merchandise-details-summary-report')->with(['total_merchandises' => $total_merchandise, 'activation_name'=>$activation_name]);
            });
        })->download('xlsx');


    }

    public function merchandiseDetailsBaReport(Request $request){

        $activation_id = $request->id;
        $date = $request->date;


        $user_merchandise = array();
        $activation_users = DB::table('activation_users')
            ->where('activation_id',$activation_id)
            ->get();

        foreach ($activation_users as $activation_user) {

            $user = DB::table('users')
                ->where('id', $activation_user->user_id)
                ->first();

            $merchandise_count = DB::table('reports')
                ->where('activation_id',$activation_id)
                ->where('merchandise', 1)
                ->where('user_id', $activation_user->user_id)
                ->whereRaw('DATE(created_at) = ?', [$date])
                ->sum('merchandise_quantity');

            if ($user) {
                array_push($user_merchandise,
                    array(
                        'user_id' => $user->id,
                        'name' => $user->name,
                        'phone_number' => $user->phone_number,
                        'email' => $user->email,
                        'merchandise_count' => $merchandise_count
                    ));
            }
        }

        $activation_name = "";
        $activation = DB::table('activations')
            ->where('id', $activation_id)
            ->first();
        if($activation){
            $activation_name = $activation->name;
        }

        $file_name = "Daily merchandise report by BA - $activation_name - $date";
        Excel::create($file_name, function($excel) use ($activation_name, $user_merchandise) {
            $excel->sheet('merchandise-ba-report', function($sheet) use ($activation_name, $user_merchandise) {
                $sheet->loadView('admin.activation.report.xls.merchandise-details-ba-report')->with(['user_merchandises' => $user_merchandise, 'activation_name'=>$activation_name]);
            });
        })->download('xlsx');


    }

    public function merchandiseDetailsEntriesReport(Request $request){

        $activation_id = $request->id;
        $date = $request->date;

        $report_entries = array();
        $entries = DB::table('reports')
            ->where('activation_id',$activation_id)
            ->where('merchandise', 1)
            ->whereRaw('DATE(created_at) = ?', [$date])
            ->get();

        foreach ($entries as $entry) {

            $user_name = "";
            $user = DB::table('users')
                ->where('id', $entry->user_id)
                ->first();
            if($user){
                $user_name = $user->name;
            }

            $location_name = "";
            $activation_location = DB::table('locations')
                ->where('id',$entry->location_id)
                ->first();
            if($activation_location){
                $location_name =  $activation_location->name;
            }

            $merchandise_name = "";
            $activation_merchandise = DB::table('merchandises')
                ->where('id',$entry->merchandise_id)
                ->first();
            if($activation_merchandise){
                $merchandise_name =  $activation_merchandise->name;
            }

            if ($user) {
                array_push($report_entries,
                    array(
                        'id' => $entry->id,
                        'user_name' => $user->name,
                        'customer_name' => $entry->customer_name,
                        'customer_phone' => $entry->customer_phone,
                        'customer_id' => $entry->customer_id,
                        'customer_feedback' => $entry->customer_feedback,
                        'merchandise_name' => $merchandise_name,
                        'merchandise_quantity' => $entry->merchandise_quantity,
                        'location_name' => $location_name,
                        'latitude' => $entry->latitude,
                        'longitude' => $entry->longitude,
                        'image' => $entry->image,
                        'created_at' => $entry->created_at,

                    ));
            }
        }

        $activation_name = "";
        $activation = DB::table('activations')
            ->where('id', $activation_id)
            ->first();
        if($activation){
            $activation_name = $activation->name;
        }

        $file_name = "Daily merchandise report entries - $activation_name - $date";
        Excel::create($file_name, function($excel) use ($activation_name, $report_entries) {
            $excel->sheet('merchandise-entries-report', function($sheet) use ($activation_name, $report_entries) {
                $sheet->loadView('admin.activation.report.xls.merchandise-details-entries-report')->with(['entries' => $report_entries, 'activation_name'=>$activation_name]);
            });
        })->download('xlsx');

    }

    public function baPerformanceReport(Request $request){

        $activation_id = $request->id;

        $ba_performances = array();
        $activation_users = DB::table('activation_users')
            ->where('activation_id',$activation_id)
            ->get();

        foreach ($activation_users as $activation_user) {

            $user = DB::table('users')
                ->where('id', $activation_user->user_id)
                ->first();

            $interaction_count = DB::table('reports')
                ->where('activation_id',$activation_id)
                ->where('user_id', $activation_user->user_id)
                ->count();

            $sales_count = DB::table('reports')
                ->where('activation_id',$activation_id)
                ->where('product', 1)
                ->where('user_id', $activation_user->user_id)
                ->sum('product_quantity');

            $merchandise_count = DB::table('reports')
                ->where('activation_id',$activation_id)
                ->where('merchandise', 1)
                ->where('user_id', $activation_user->user_id)
                ->sum('merchandise_quantity');


            if ($user) {
                array_push($ba_performances,
                    array(
                        'user_id' => $user->id,
                        'name' => $user->name,
                        'phone_number' => $user->phone_number,
                        'email' => $user->email,
                        'interaction_count' => $interaction_count,
                        'sales_count' => $sales_count,
                        'merchandise_count' => $merchandise_count
                    ));
            }
        }

        if(!empty($ba_performances)){
            foreach ($ba_performances as $key => $row)
            {
                $vc_array_name[$key] = $row['sales_count'];
            }
            array_multisort($vc_array_name, SORT_ASC, $ba_performances);

        }

        $activation_name = "";
        $activation = DB::table('activations')
            ->where('id', $activation_id)
            ->first();
        if($activation){
            $activation_name = $activation->name;
        }

        $file_name = "BA Performace report - $activation_name";
        Excel::create($file_name, function($excel) use ($activation_name, $ba_performances) {
            $excel->sheet('merchandise-entries-report', function($sheet) use ($activation_name, $ba_performances) {
                $sheet->loadView('admin.activation.report.xls.ba-performance')->with(['ba_performances' => $ba_performances, 'activation_name'=>$activation_name]);
            });
        })->download('xlsx');
    }

    public function activationPayroll(Request $request){

        $activation_id = $request->id;

        $payrolls = array();
        $activation_users = DB::table('activation_users')
            ->where('activation_id',$activation_id)
            ->get();

        foreach ($activation_users as $activation_user) {

            $user = DB::table('users')
                ->where('id', $activation_user->user_id)
                ->first();

            if ($user) {

                // Get activation dates
                $activation = DB::table('activations')
                    ->where('id', $activation_id)
                    ->first();

                if($activation){
                    $start_date = $activation->start_date;
                    $end_date = $activation->end_date;
                    $ba_unit_cost = $activation->ba_unit_cost;

                    $days = 0;
                    $total_cost = 0;
                    while(strtotime($start_date)<=strtotime($end_date)) {

                        $check_in = DB::table('activation_check_ins')
                            ->where('activation_id',$activation_id)
                            ->where('user_id', $activation_user->user_id)
                            ->whereRaw('DATE(created_at) = ?', [$start_date])
                            ->first();

                        $report_confirmation = DB::table('activation_reports')
                            ->where('activation_id',$activation_id)
                            ->where('user_id', $activation_user->user_id)
                            ->where('date', $start_date)
                            ->first();

                        if($check_in){
                            if($report_confirmation){

                                $days = $days+1;
                                $total_cost = $total_cost+$ba_unit_cost;
                            }
                        }

                        $start_date = date("Y-m-d", strtotime("+1 day", strtotime($start_date)));
                    }

                    array_push($payrolls,
                        array(
                            'user_id' => $user->id,
                            'name' => $user->name,
                            'phone_number' => $user->phone_number,
                            'unit_cost' => $ba_unit_cost,
                            'total_days' => $days,
                            'total_cost' => $total_cost
                        ));
                }

            }

        }

        $activation_name = "";
        $activation = DB::table('activations')
            ->where('id', $activation_id)
            ->first();
        if($activation){
            $activation_name = $activation->name;
        }

        $user = Auth::user();
        $current_time = Carbon::now()->toDateTimeString();
        $data = [
            'user'=>$user,
            'date'=>$current_time,
            'payrolls' => $payrolls,
            'activation_name' => $activation_name
        ];

        $pdf = PDF::loadView('admin.activation.report.pdf.payroll', $data);
        return $pdf->stream('admin.activation.report.pdf.payroll');

    }

    public function activationSelectedPayroll(Request $request){


        $activation_id = $request->activation_id;
        $beginning_date = $request->start_date;
        $closing_date = $request->end_date;

        $payrolls = array();
        $activation_users = DB::table('activation_users')
            ->where('activation_id', $activation_id)
            ->get();

        foreach ($activation_users as $activation_user) {

            $user = DB::table('users')
                ->where('id', $activation_user->user_id)
                ->first();

            if ($user) {

                // Get activation dates
                $activation = DB::table('activations')
                    ->where('id', $activation_id)
                    ->first();

                if($activation){

                    $start_date = $request->start_date;
                    $end_date = $request->end_date;

                    $ba_unit_cost = $activation->ba_unit_cost;
                    $days = 0;
                    $total_cost = 0;
                    while(strtotime($start_date)<=strtotime($end_date)) {
                        $check_in = DB::table('activation_check_ins')
                            ->where('activation_id', $activation_id)
                            ->where('user_id', $activation_user->user_id)
                            ->whereRaw('DATE(created_at) = ?', [$start_date])
                            ->first();

                        $report_confirmation = DB::table('activation_reports')
                            ->where('activation_id', $activation_id)
                            ->where('user_id', $activation_user->user_id)
                            ->where('date', $start_date)
                            ->first();


                        if($check_in){
                            if($report_confirmation){

                                $days = $days+1;
                                $total_cost = $total_cost+$ba_unit_cost;
                            }
                        }

                        $start_date = date("Y-m-d", strtotime("+1 day", strtotime($start_date)));
                    }

                    array_push($payrolls,
                        array(
                            'user_id' => $user->id,
                            'name' => $user->name,
                            'phone_number' => $user->phone_number,
                            'unit_cost' => $ba_unit_cost,
                            'total_days' => $days,
                            'total_cost' => $total_cost
                        ));
                }

            }

        }

        $activation_name = "";
        $activation = DB::table('activations')
            ->where('id', $activation_id)
            ->first();
        if($activation){
            $activation_name = $activation->name;
        }

        $user = Auth::user();
        $current_time = Carbon::now()->toDateTimeString();
        $data = [
            'user'=>$user,
            'date'=>$current_time,
            'payrolls' => $payrolls,
            'start_date' => $beginning_date,
            'end_date' => $closing_date,
            'activation_name' => $activation_name,
        ];


        $pdf = PDF::loadView('admin.activation.report.pdf.payroll-selected',
            $data,
            [],
            [
                'format' => 'A4-L',
                'orientation' => 'P'
            ]);
        return $pdf->stream('admin.activation.report.pdf.payroll-selected');

    }
}


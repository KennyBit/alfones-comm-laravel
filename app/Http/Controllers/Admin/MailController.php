<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    public function userWelcomeNote($first_name, $email, $password){

        $data = array(
            'email'=>$email,
            'password'=>$password,
            'first_name'=>$first_name,
        );

        Mail::send('admin.emails.user-welcome-note', $data, function($message) use ($data) {
            $message->to($data['email'], $data['first_name'])->subject
            ('Welcome Note');
            $message->from('alfonescomm@gmail.com','Welcome Note - Alfones Communications App');
        });
    }

    public function adminWelcomeNote($first_name, $email, $password, $role){

        $data = array(
            'email'=>$email,
            'password'=>$password,
            'first_name'=>$first_name,
            'role'=>$role,
        );

        Mail::send('admin.emails.admin-welcome-note', $data, function($message) use ($data) {
            $message->to($data['email'], $data['first_name'])->subject
            ('Welcome Note');
            $message->from('alfonescomm@gmail.com','Welcome Note - Alfones Communications App');
        });
    }

    public function clientWelcomeNote($first_name, $email, $password){

        $data = array(
            'email'=>$email,
            'password'=>$password,
            'first_name'=>$first_name,
        );

        Mail::send('admin.emails.client-welcome-note', $data, function($message) use ($data) {
            $message->to($data['email'], $data['first_name'])->subject
            ('Reset code');
            $message->from('alfonescomm@gmail.com','Welcome Note - Alfones Communications App');
        });
    }

    public function resetCode($first_name, $reset_code, $email){

        $data = array(
            'reset_code'=>$reset_code,
            'first_name'=>$first_name,
            'email'=>$email,
        );

        Mail::send('admin.emails.reset-code', $data, function($message) use ($data) {
            $message->to($data['email'], $data['first_name'])->subject
            ('Reset code');
            $message->from('alfonescomm@gmail.com','Reset code - Alfones Communications App');
        });
    }

    public function passwordReset($first_name, $email){

        $data = array(
            'first_name'=>$first_name,
            'email'=>$email,
        );

        Mail::send('admin.emails.password-reset', $data, function($message) use ($data) {
            $message->to($data['email'], $data['first_name'])->subject
            ('Password reset');
            $message->from('alfonescomm@gmail.com','Password reset - Alfones Communications App');
        });
    }
}

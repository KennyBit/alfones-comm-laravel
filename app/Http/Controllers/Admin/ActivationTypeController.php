<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ActivationTypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function show(Request $request)
    {
        $user = Auth::user();
        $data = [
            'activation_type' => $request->id,
            'activation' => $request->activation_id,
            'canEdit' => $user->hasPermissionTo('activation-target-edit'),
            'canDelete' => $user->hasPermissionTo('activation-target-delete'),
        ];
        return view('admin.activation.activation-type.activation-type-details', $data);
    }
}

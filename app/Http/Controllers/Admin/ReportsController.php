<?php
/**
 * Created by PhpStorm.
 * User: eugene
 * Date: 10/6/18
 * Time: 1:12 PM
 */

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Excel;


class ReportsController extends Controller
{
    public function view(): View
    {
        $activations = DB::table('activations')->get();
//        return view('admin.activation.report.xls.interaction-details-summary-report', [
//            'activations' => $activations
//        ]);

        $something = ['activations'=> $activations];

        Excel::create('Customer Data', function($excel) use ($something){
            $excel->setTitle('Customer Data');
            $excel->sheet('Customer Data', function($sheet) use ($something){
                $sheet->fromArray($something, null, 'A1', false, false);
            });
        })->download('xls');
    }
}
<?php

namespace App\Http\Controllers\Admin;

use App\Client;
use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ClientController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {

      $user = Auth::user();
        $clients = DB::table('clients')->get();
        $data = [
            'clients' => $clients,
            'canCreate' => $user->hasPermissionTo('client-create'),
        ];

        return view('admin.client.index', $data);
    }

    public function show($id)
    {

        $user = Auth::user();
        $data = [
            'client' => $id,
            'canEdit' => $user->hasPermissionTo('client-edit'),
            'canDelete' => $user->hasPermissionTo('client-delete'),

        ];
        return view('admin.client.client-details', $data);
    }

}

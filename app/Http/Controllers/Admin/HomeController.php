<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(){

        $total_activations = DB::table('activations')->count();
        $total_users = DB::table('users')->count();
        $total_clients = DB::table('clients')->count();

        $activations = DB::table('activations')
            ->orderBy('activation_id', 'DESC')
            ->limit(5)
            ->get();

        $data = [
            'total_activations' => $total_activations,
            'total_users' => $total_users,
            'total_clients' => $total_clients,
            'activations' => $activations,
        ];


        return view('admin.home.index', $data);
    }
}

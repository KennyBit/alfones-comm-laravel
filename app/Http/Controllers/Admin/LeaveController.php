<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class LeaveController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $recent_leaves = DB::table('user_leaves')
            ->orderBy('created_at', 'DESC')
            ->get();

        $result_recent = array();

        foreach ($recent_leaves as $recent_leave){

            $admin_name = "";
            $admin = DB::table('admins')
                ->where('id', $recent_leave->admin_id)
                ->first();
            if($admin){
                $admin_name = $admin->name;
            }

            array_push($result_recent,
                array(
                    'id' => $recent_leave->id,
                    'admin_name' => $admin_name,
                    'reasons' => $recent_leave->reasons,
                    'description' => $recent_leave->description,
                    'date_from' => $recent_leave->date_from,
                    'date_to' => $recent_leave->date_to,
                    'status' => $recent_leave->status,
                    'created_at' => $recent_leave->created_at,
                    'updated_at' => $recent_leave->updated_at,
                ));

        }

        $admins = DB::table('admins')
            ->where('permanent_status', '1')
            ->orderBy('created_at', 'DESC')
            ->get();

        $result_admins = array();
        foreach ($admins as $admin){

            $allowed_days = 30;
            $total_applications = DB::table('user_leaves')
                ->where('admin_id', $admin->id)
                ->count();
            $approved_applications = DB::table('user_leaves')
                ->where('admin_id', $admin->id)
                ->where('status', "1")
                ->count();
            $remaining_days = $allowed_days - $approved_applications;

            array_push($result_admins,
                array(
                    'id' => $admin->id,
                    'admin_name' => $admin->name,
                    'allowed_days' => $allowed_days,
                    'applications' => $total_applications,
                    'remaining' => $remaining_days,
                ));

        }

        $data = [
            'recent_leaves' => $result_recent,
            'admins' => $result_admins,

        ];
        return view('admin.leaves.index', $data);
    }


    public function show(Request $request)
    {

        $user_leave = DB::table('user_leaves')
            ->where('id', $request->id)
            ->first();
        if($user_leave){
            // Get user
            $admin = DB::table('admins')
                ->where('id',$user_leave->admin_id)
                ->first();

            $data = [
                'admin' => $admin,
                'leave' => $user_leave,
            ];

            return view('admin.leaves.leave-details', $data);
        }

    }

    public function leaveStatusChange(Request $request){
        //Get request

        $leave_id = $request->id;

        $leave =  DB::table('user_leaves')->where('id', $leave_id)->first();
        if($leave){
            /**
             * Update leave
             */
            $update = DB::table('user_leaves')
                ->where('id', $leave_id)
                ->update([
                    'status'=>$request->status,
                ]);


            if($update){

                if($request->status == 1){

                    return redirect()->route('admin.leave.details', ['id' => $leave_id]);


                }else if($request->status == 2) {

                    return redirect()->route('admin.leave.details', ['id' => $leave_id]);
                }


            }else{

                return redirect()->route('admin.leave.details', ['id' => $leave_id]);

            }

        }else{

            return redirect()->route('admin.leaves');
        }

        return false;
    }

}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ActivityLogController extends Controller
{
    //
    public function viewAdmin()
    {
        activity('View admin')->log('View admins');
    }
}

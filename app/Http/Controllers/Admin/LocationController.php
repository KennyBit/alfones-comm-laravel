<?php

namespace App\Http\Controllers\Admin;

use App\Location;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class LocationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $user = Auth::user();
        $locations = DB::table('locations')->get();
        $data = [
            'locations' => $locations,
            'canCreate' => $user->hasPermissionTo('admin-create'),
        ];

        return view('admin.location.index', $data);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {

        $user = Auth::user();
        $data = [
            'location' => $request->id,
            'activation' => $request->activation_id,
            'canEdit' => $user->hasPermissionTo('activation-location-edit'),
            'canDelete' => $user->hasPermissionTo('activation-location-delete'),
        ];
        return view('admin.activation.location.location-details', $data);
    }

}

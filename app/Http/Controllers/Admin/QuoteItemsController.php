<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Quote;
use App\QuoteItem;
use Illuminate\Support\Facades\DB;

class QuoteItemsController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

      public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $user = Auth::user();
        $quoteItems = QuoteItem::with('quote')->orderBy('created_at', 'desc')->get();
        $data = [
            'quoteItems' => $quoteItems,
            'canCreate' => $user->hasPermissionTo('admin-create'),
        ];
        
        // return response()->json($data);
        return view('admin.quote-items.index', compact('quoteItems'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.quotes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $quotes = Quote::with('admin')->orderBy('created_at', 'desc')->get();

        $quote = new Quote;
        $quote->name = $request->input('name');
        $quote->admin_id = Auth::user()->id;
        // $quote->admin_id = auth()->admin()->id;
        $quote->client_name = $request->input('client_name');
        $quote->date = $request->input('date');
        $quote->status = 0;
        $quote->save();

        return back();
        // return view('admin.quotes.index', compact('quotes', 'quote'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $quote = Quote::find($id);
        $quote_items = QuoteItem::with('quote')->get();

        return view('admin.quotes.show', compact('quote_items'));
    }

   public function agencyFee(Request $request, $id)
    {
        $quote = Quote::find($id);

        $quoteTotals = DB::table("quote_items")
            ->where('quote_id', $quote->id)
            ->sum('total');

        $quote->agency_fee_percentage = $request->input('agency_fee_percentage');
        $quote->agency_fee = $quote->agency_fee_percentage % $quoteTotals;
        $quote->save();
    }

    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

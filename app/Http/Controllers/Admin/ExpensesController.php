<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use niklasravnsborg\LaravelPdf\Facades\Pdf;

class ExpensesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {

        $general_expenses = DB::table('expenses')
            ->where('expense_type', '0')
            ->where('submitted_status', '1')
            ->orderBy('expenses_id', 'DESC')
            ->get();

        $result_general = array();

        foreach ($general_expenses as $general_expense){

            $admin_name = "";
            $admin = DB::table('admins')
                ->where('id',$general_expense->admin_id)
                ->first();
            if($admin){
                $admin_name = $admin->name;
            }

            // Get requisition amount
            $general_expense_sum = DB::table('expenses_items')
                ->where('expenses_id', $general_expense->id)
                ->sum('item_amount');


            array_push($result_general,
                array(
                    'id' => $general_expense->id,
                    'expenses_id' => $general_expense->expenses_id,
                    'admin_name' => $admin_name,
                    'requisition_title' => $general_expense->requisition_title,
                    'required_date' => $general_expense->required_date,
                    'requisition_status' => $general_expense->requisition_status,
                    'reconciliation_status' => $general_expense->reconciliation_status,
                    'submitted_status' => $general_expense->submitted_status,
                    'amount' => $general_expense_sum,
                    'created_at' => $general_expense->created_at,
                    'updated_at' => $general_expense->updated_at,
                ));

        }

        $recent_expenses = DB::table('expenses')
             ->where('submitted_status', '1')
            ->orderBy('expenses_id', 'DESC')
            ->limit(10)
            ->get();

        $result_recent = array();

        foreach ($recent_expenses as $recent_expense){

            $admin_name = "";
            $admin = DB::table('admins')
                ->where('id',$recent_expense->admin_id)
                ->first();
            if($admin){
                $admin_name = $admin->name;
            }

            // Get requisition amount
            $recent_expense_sum = DB::table('expenses_items')
                ->where('expenses_id', $recent_expense->id)
                ->sum('item_amount');

            $expense_type = $recent_expense->expense_type;
            $activation_name = "";
            if($expense_type == 1){
                $activation_id = $recent_expense->activation_id;
                $activation = DB::table('activations')
                    ->where('id', $activation_id)
                    ->first();
                $activation_name = $activation->name;
            }

            array_push($result_recent,
                array(
                    'id' => $recent_expense->id,
                    'expenses_id' => $recent_expense->expenses_id,
                    'expense_type' => $recent_expense->expense_type,
                    'activation_name' => $activation_name,
                    'admin_name' => $admin_name,
                    'requisition_title' => $recent_expense->requisition_title,
                    'required_date' => $recent_expense->required_date,
                    'requisition_status' => $recent_expense->requisition_status,
                    'reconciliation_status' => $recent_expense->reconciliation_status,
                    'submitted_status' => $recent_expense->submitted_status,
                    'amount' => $recent_expense_sum,
                    'created_at' => $recent_expense->created_at,
                    'updated_at' => $recent_expense->updated_at,
                ));


        }

        $data = [
            'general_expenses' => $result_general,
            'recent_expenses' => $result_recent,

        ];
        return view('admin.expenses.index', $data);
    }

    public function show(Request $request)
    {
        $expenses = DB::table('expenses')
            ->where('id', $request->id)
            ->first();
        if($expenses){
            // Get user
            $admin = DB::table('admins')
                ->where('id',$expenses->admin_id)
                ->first();

            // Get expenses items
            $expenses_items = DB::table('expenses_items')
                ->where('expenses_id', $request->id)
                ->get();

            $reconciliation_items = DB::table('expense_reconciliation_items')
                ->where('expenses_id', $request->id)
                ->get();

            $expense_amount = DB::table('expenses_items')
                ->where('expenses_id', $request->id)
                ->sum('item_amount');

            $amount_given = DB::table('expense_give_amounts')
                ->where('expense_id', $request->id)
                ->sum('amount');

            $reconciliation_amount = DB::table('expense_reconciliation_items')
                ->where('expenses_id', $request->id)
                ->sum('item_amount');

            $balance = $amount_given-$reconciliation_amount;

            $data = [
                'admin' => $admin,
                'expenses' => $expenses,
                'expenses_amount' => $expense_amount,
                'expenses_items' => $expenses_items,
                'reconciliation_items' => $reconciliation_items,
                'balance' => $balance,
            ];

            return view('admin.expenses.expenses-details', $data);
        }

        return false;
    }

    public function requisitionStatusChange(Request $request){
        //Get request

        $expense_id = $request->id;

        $expense =  DB::table('expenses')->where('id', $expense_id)->first();
        if($expense){
            /**
             * Update expense
             */
            $update = DB::table('expenses')
                ->where('id', $expense_id)
                ->update([
                    'requisition_status'=>$request->requisition_status,
                ]);


            if($update){

                if($request->requisition_status == 1){

                    return redirect()->route('admin.expenses.details', ['id' => $expense_id]);


                }else if($request->requisition_status == 2) {

                    return redirect()->route('admin.expenses.details', ['id' => $expense_id]);
                }


            }else{

                return redirect()->route('admin.expenses.details', ['id' => $expense_id]);

            }

        }else{

            return redirect()->route('admin.expenses');
        }

        return false;
    }

    public function reconciliationStatusChange(Request $request){
        //Get request

        $expense_id = $request->id;

        $expense =  DB::table('expenses')->where('id', $expense_id)->first();
        if($expense){
            /**
             * Update expense
             */
            $update = DB::table('expenses')
                ->where('id', $expense_id)
                ->update([
                    'reconciliation_status'=>$request->reconciliation_status,
                ]);


            if($update){

                if($request->reconciliation_status == 1){

                    return redirect()->route('admin.expenses.details', ['id' => $expense_id]);


                }else if($request->reconciliation_status == 2) {

                    return redirect()->route('admin.expenses.details', ['id' => $expense_id]);
                }


            }else{

                return redirect()->route('admin.expenses.details', ['id' => $expense_id]);

            }

        }else{

            return redirect()->route('admin.expenses');
        }

        return false;
    }

    public function expensesReportPdf(Request $request){

        $expenses = DB::table('expenses')
            ->where('id', $request->id)
            ->first();
        if($expenses){
            // Get user
            $admin = DB::table('admins')
                ->where('id',$expenses->admin_id)
                ->first();

            // Get expenses items
            $expenses_items = DB::table('expenses_items')
                ->where('expenses_id', $request->id)
                ->get();

            $expense_amount = DB::table('expenses_items')
                ->where('expenses_id', $request->id)
                ->sum('item_amount');

            $activation_name = "";
            $activation_id = $expenses->activation_id;
            if($activation_id != null || $activation_id != ""){
                $activation = DB::table('activations')
                    ->where('id', $activation_id)
                    ->first();
                $activation_name = $activation->name;

            }

            $user = Auth::user();
            $current_time = Carbon::now()->toDateTimeString();
            $data = [
                'user'=>$user,
                'date'=>$current_time,
                'admin' => $admin,
                'activation_name' => $activation_name,
                'expenses' => $expenses,
                'expenses_amount' => $expense_amount,
                'expenses_items' => $expenses_items,
            ];

            $file_name = 'Requisition #'.$expenses->expenses_id.'-'.$admin->name;
            $pdf = PDF::loadView('admin.activation.report.pdf.expenses', $data);
            return $pdf->stream($file_name);
        }

    }

    public function giveAmount(Request $request){

        $data = [
            'expense_id' => $request->id,
        ];

        return view('admin.expenses.give-amount', $data);
    }
}

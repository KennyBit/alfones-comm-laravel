<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ActivationOutletProductSummaryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function show($id)
    {

        $user = Auth::user();
        $data = [
            'product_summary' => $id,
            'canEdit' => $user->hasPermissionTo('client-edit'),
            'canDelete' => $user->hasPermissionTo('client-delete'),

        ];
        return view('admin.activation.report.sales.summary.summary-details', $data);
    }
}

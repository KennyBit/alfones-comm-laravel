<?php

namespace App\Http\Controllers\Admin;

use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $products = DB::table('products')->get();
        $data = [
            'products' => $products,
            'canCreate' => $user->hasPermissionTo('activation-product-create'),
        ];

      
         return view('admin.product.index', $data);
    }

    public function show(Request $request)
    {

        $user = Auth::user();

        $data = [
            'product' => $request->id,
            'activation' => $request->activation_id,
            'canEdit' => $user->hasPermissionTo('activation-product-edit'),
            'canDelete' => $user->hasPermissionTo('activation-product-delete'),
        ];
        return view('admin.activation.product.product-details', $data);
    }

}

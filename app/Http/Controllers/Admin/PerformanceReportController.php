<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class PerformanceReportController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $data = [
            'canList' => $user->hasPermissionTo('division-list'),
            'canCreate' => $user->hasPermissionTo('division-create'),
            'canDetails' => $user->hasPermissionTo('division-details'),
        ];

        return view('admin.performance.index', $data);
    }

    public function show($id)
    {

        $user = Auth::user();
        $data = [
            'performance' => $id,
            'canEdit' => $user->hasPermissionTo('division-edit'),
            'canDelete' => $user->hasPermissionTo('division-delete'),

        ];
        return view('admin.performance.performance-details', $data);
    }


    public function downloadReport(Request $request){

        $start_date = $request->start_date;
        $end_date = $request->end_date;

        $performances = array();
        $admins = DB::table('admins')
            ->where('permanent_status', '1')
            ->get();

        foreach ($admins as $admin) {

            $admin_name = $admin->name;

            $performance_report = DB::table('performance_reports')
                ->where('admin_id', $admin->id)
                ->where('start_date', $start_date)
                ->where('end_date', $end_date)
                ->first();

            $on_time = ""; $in_full = ""; $no_errors = ""; $target_achievement = ""; $overall_performance = ""; $performance_percentage = "";
            $percentage_overall_performance = ""; $cumulative_performance = ""; $overall_attendance = "";
            if($performance_report){

                if($performance_report->on_time == 1){
                    $on_time = "yes";
                }else{
                    $on_time = "no";
                }

                if($performance_report->in_full == 1){
                    $in_full = "yes";
                }else{
                    $in_full = "no";
                }

                if($performance_report->no_errors == 1){
                    $no_errors = "yes";
                }else{
                    $no_errors = "no";
                }

                if($performance_report->target_achievement == 1){
                    $target_achievement = "yes";
                }else{
                    $target_achievement = "no";
                }

                if($performance_report->overall_performance == 1){
                    $overall_performance = "yes";
                }else{
                    $overall_performance = "no";
                }

                $performance_percentage = $performance_report->performance_percentage;
                $overall_attendance = $performance_report->overall_attendance;
                $percentage_overall_performance = $performance_report->percentage_overall_performance;
                $cumulative_performance = $performance_report->cumulative_performance;

            }

            array_push($performances,
                array(
                    'name' => $admin_name,
                    'on_time' => $on_time,
                    'in_full' => $in_full,
                    'no_errors' => $no_errors,
                    'target_achievement' => $target_achievement,
                    'overall_performance' => $overall_performance,
                    'performance_percentage' => $performance_percentage,
                    'overall_attendance' => $overall_attendance,
                    'percentage_overall_performance' => $percentage_overall_performance,
                    'cumulative_performance' => $cumulative_performance,
                ));


        }


        $file_name = "Weekly performance analysis ($start_date - $end_date)";
        Excel::create($file_name, function($excel) use ($start_date, $end_date, $performances) {
            $excel->sheet('"Weekly performance analysis', function($sheet) use ($start_date, $end_date, $performances) {
                $sheet->loadView('admin.activation.report.xls.performance-report')->with(['start_date' => $start_date, 'end_date'=>$end_date, 'performances'=>$performances]);
            });
        })->download('xlsx');


    }
}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class RegionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(Request $request)
    {

        $user = Auth::user();
        $data = [
            'division' => $request->id,
            'canList' => $user->hasPermissionTo('region-list'),
            'canCreate' => $user->hasPermissionTo('region-create'),
            'canDetails' => $user->hasPermissionTo('region-details'),
        ];

        return view('admin.division.region.index', $data);
    }

    public function show(Request $request)
    {

        $user = Auth::user();
        $data = [
            'region' => $request->id,
            'division' => $request->division,
            'canEdit' => $user->hasPermissionTo('region-edit'),
            'canDelete' => $user->hasPermissionTo('region-delete'),

        ];
        return view('admin.division.region.region-details', $data);
    }
}

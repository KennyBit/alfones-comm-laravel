<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Quote;
use App\QuoteItem;
use App\AgencyFee;
use niklasravnsborg\LaravelPdf\Facades\Pdf;
use Illuminate\Support\Facades\DB;


class QuotesController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

      public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $user = Auth::user();
        $quotes = Quote::with('admin')->orderBy('created_at', 'desc')->get();
        return view('admin.quotes.index', compact('quotes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.quotes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $quotes = Quote::with('admin')->orderBy('created_at', 'desc')->get();

        $quote = new Quote;
        $quote->name = $request->input('name');
        $quote->admin_id = Auth::user()->id;
        // $quote->admin_id = auth()->admin()->id;
        $quote->client_name = $request->input('client_name');
        $quote->date = $request->input('date');
        $quote->status = 0;
        $quote->save();

        return back();
        // return view('admin.quotes.index', compact('quotes', 'quote'));
    }
    

    public function show($id)
    {
        $user = Auth::user();
        $quote = Quote::with('admin')->find($id);
        $quoteItems = QuoteItem::where('quote_id', $id)->get();
        $agencyFee = AgencyFee::where('quote_id', $id)->get();

        $quoteTotals = DB::table("quote_items")
            ->where('quote_id', $quote->id)
            ->sum('total');
        
        $agencyFee = DB::table('agency_fees')
            ->where('quote_id', $quote->id)
            ->value('agency_fees');

        return view('admin.quotes.show', compact('quote', 'quoteItems', 'quoteTotals', 'agencyFee'));
    }

      public function quoteItem(Request $request, $id)
    {
        $user = Auth::user();

        $quote = Quote::find($id);

        $quote_id = DB::table("quotes")
            ->where('id', $quote->id)
            ->value('id');

        $validatedData = $request->validate([
        'item_name' => 'required',
        'quantity' => 'required',
        ]);

        $quoteItem = new QuoteItem;
        $quoteItem->item_name = $request->input('item_name');
        $quoteItem->quote_id = $quote_id;
        $quoteItem->description = $request->input('description');
        $quoteItem->quantity = $request->input('quantity');
        $quoteItem->days = $request->input('days');
        $quoteItem->unit_cost = $request->input('unit_cost');
        $quoteItem ->total = $quoteItem->unit_cost * $quoteItem->quantity * $quoteItem->days;
        $quoteItem->save();

        return back();
        // return view('admin.quotes.index', compact('quotes', 'quote'));
    }

    public function agencyFee(Request $request, $id)
    {
        $quote = Quote::find($id);
        $agencyFee = AgencyFee::find($id);

        $quoteTotals = DB::table("quote_items")
            ->where('quote_id', $quote->id)
            ->sum('total');

        $quote_id = DB::table("quotes")
            ->where('id', $quote->id)
            ->value('id');

        $agencyFee = new AgencyFee;
        $agencyFee->agency_fees_percentage = $request->input('agency_fees_percentage');
        $agencyFee->agency_fees = $agencyFee->agency_fees_percentage / 100 * $quoteTotals;
        $agencyFee->quote_id = $quote_id;
        $agencyFee->save();

        return back();
    }

    public function downloadPDF($id){
        $quote = Quote::with('admin')->find($id);
        $quoteItems = DB::table('quote_items')->where('quote_id', $id)->get();
        $agencyFees = DB::table('agency_fees')->where('quote_id', $id)->value('agency_fees');
        $agencyFeesPercentage = DB::table('agency_fees')->where('quote_id', $id)->value('agency_fees_percentage');
        $quoteTotals = DB::table("quote_items")
            ->where('quote_id', $id)
            ->sum('total');

        $admin = DB::table("admins")
            ->where('id', $quote->admin_id)
            ->value('name');
        // $requisitionerNumber = DB::table("requisitions")
        //     ->value('id');

        $pdf = PDF::loadView('admin.quotes.pdf', compact('quote', 'quoteItems', 'quoteTotals', 'admin', 'agencyFees', 'agencyFeesPercentage'));
        return $pdf->download(''.' '.$quote->name.' '.'by'.' '.$admin.' '.'.pdf');

    }

    public function quoteEdit($id)
    {
        $quote = Quote::find($id);
        return view('admin.quotes.edit', compact('quote'));
    }

    public function quoteUpdate(Request $request, $id)
    {
         $quote = Quote::find($id);
        if($quote){
            if ($request->has('name'))
                {
                    $quote->name = $request->input('name');
                }
            if ($request->has('client_name'))
                {
                    $quote->client_name = $request->input('client_name');
                }    
            if ($request->has('date'))
                {
                    $quote->date = $request->input('date');
                }
            $quote->save();
            }    

        $quotes = Quote::with('admin')->orderBy('created_at', 'desc')->get();
        return view('admin.quotes.index', compact('quotes'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $quote = Quote::find($id);
        $quotes = Quote::with('admin')->orderBy('created_at', 'desc')->get();

            if($quote){        
                $quote->delete();
                return back();
            }

        return back();
    }
}

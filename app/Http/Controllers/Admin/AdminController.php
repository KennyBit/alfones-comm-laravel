<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {

        $user = Auth::user();
        $admins = DB::table('admins')->get();
        $data = [
            'admins' => $admins,
            'canCreate' => $user->hasPermissionTo('admin-create'),
        ];

        return view('admin.admin.index', $data);
    }

    public function show($id)
    {

        $logs = DB::table('activity_log')
            ->where('causer_id', $id)
            ->orderBy('created_at', 'DESC')
            ->get();

        $user = Auth::user();

        $data = [
            'logs' => $logs,
            'admin' => $id,
            'canEdit' => $user->hasPermissionTo('admin-edit'),
            'canDelete' => $user->hasPermissionTo('admin-delete'),
            'canPermission' => $user->hasPermissionTo('admin-permission')
        ];
        return view('admin.admin.admin-details', $data);
    }


    public function adminPermissions($id)
    {

        // Loop permissions
        $permissions = DB::table('permissions')
            ->get();
        $result = array();
        foreach ($permissions as $permission){

            // Check if user has permission
            $has_permission = 0;
            $user_permission = DB::table('model_has_permissions')
                ->where('model_id', $id)
                ->where('permission_id', $permission->id)
                ->first();
            if($user_permission){
                $has_permission = 1;
            }

            array_push($result,
                array(
                    'id' => $permission->id,
                    'name' => $permission->name,
                    'guard_name' => $permission->guard_name,
                    'has_permission' => $has_permission,
                ));


        }

        $user = Auth::user();
        $data = [
            'admin' => $id,
            'permissions' => $result,

        ];
        return view('admin.admin.admin-permissions', $data);
    }

    public function adminPermissionsEdit(Request $request)
    {

        Artisan::call('cache:clear');
        $id = $request->id;
        //Delete all user permissions
        $delete = DB::table('model_has_permissions')
            ->where('model_id', $id)
            ->delete();

        $post_permissions = $request->input('user_permissions');
        $user = Admin::where('id', $id)
            ->first();
        $user->syncPermissions($post_permissions);


        // Loop permissions
        $permissions = DB::table('permissions')
            ->get();
        $result = array();
        foreach ($permissions as $permission){

            // Check if user has permission
            $has_permission = 0;
            $user_permission = DB::table('model_has_permissions')
                ->where('model_id', $id)
                ->where('permission_id', $permission->id)
                ->first();
            if($user_permission){
                $has_permission = 1;
            }

            array_push($result,
                array(
                    'id' => $permission->id,
                    'name' => $permission->name,
                    'guard_name' => $permission->guard_name,
                    'has_permission' => $has_permission,
                ));


        }

        $user = Auth::user();
        $data = [
            'admin' => $id,
            'permissions' => $result,

        ];
        return view('admin.admin.admin-permissions', $data);
    }

    public function adminPermissionsQuickEdit(Request $request)
    {

        Artisan::call('cache:clear');
        $id = $request->id;
        $delete = DB::table('model_has_permissions')
            ->where('model_id', $id)
            ->delete();


        $permissions = array();
        if($request->role == 1){
            // Super admin permissions

            $role = "Super admin";

            $permissions = [
                'ba-list',
                'ba-details',
                'ba-create',
                'ba-edit',
                'ba-delete',
                'admin-list',
                'admin-details',
                'admin-create',
                'admin-edit',
                'admin-delete',
                'admin-permission',
                'client-list',
                'client-details',
                'client-create',
                'client-edit',
                'client-delete',
                'division-list',
                'division-details',
                'division-create',
                'division-edit',
                'division-delete',
                'region-list',
                'region-details',
                'region-create',
                'region-edit',
                'region-delete',
                'activation-list',
                'activation-details',
                'activation-create',
                'activation-edit',
                'activation-delete',
                'activation-product-list',
                'activation-product-details',
                'activation-product-create',
                'activation-product-edit',
                'activation-product-delete',
                'activation-merchandise-list',
                'activation-merchandise-details',
                'activation-merchandise-create',
                'activation-merchandise-edit',
                'activation-merchandise-delete',
                'activation-location-list',
                'activation-location-details',
                'activation-location-create',
                'activation-location-edit',
                'activation-location-delete',
                'activation-team-leader-list',
                'activation-team-leader-details',
                'activation-team-leader-create',
                'activation-team-leader-edit',
                'activation-team-leader-delete',
                'activation-target-list',
                'activation-target-details',
                'activation-target-create',
                'activation-target-edit',
                'activation-target-delete',
                'activation-reports-view',
                'activation-reports-pdf',
                'activation-reports-xls',
                'activation-payroll-view',
                'activation-payroll-generate',
                'expenses-list',
                'expenses-details',
                'expenses-approve',
                'expenses-printout',
                'sms-notification-status',
            ];

        }elseif ($request->role == 2){
            // Admin permissions

            $role = "Admin";

            $permissions = [
                'ba-list',
                'ba-details',
                'ba-create',
                'ba-edit',
                'ba-delete',
                'admin-list',
                'admin-details',
                'admin-create',
                'admin-edit',
                'admin-delete',
                'admin-permission',
                'client-list',
                'client-details',
                'client-create',
                'client-edit',
                'client-delete',
                'division-list',
                'division-details',
                'division-create',
                'division-edit',
                'division-delete',
                'region-list',
                'region-details',
                'region-create',
                'region-edit',
                'region-delete',
                'activation-list',
                'activation-details',
                'activation-create',
                'activation-edit',
                'activation-delete',
                'activation-product-list',
                'activation-product-details',
                'activation-product-create',
                'activation-product-edit',
                'activation-product-delete',
                'activation-merchandise-list',
                'activation-merchandise-details',
                'activation-merchandise-create',
                'activation-merchandise-edit',
                'activation-merchandise-delete',
                'activation-location-list',
                'activation-location-details',
                'activation-location-create',
                'activation-location-edit',
                'activation-location-delete',
                'activation-team-leader-list',
                'activation-team-leader-details',
                'activation-team-leader-create',
                'activation-team-leader-edit',
                'activation-team-leader-delete',
                'activation-target-list',
                'activation-target-details',
                'activation-target-create',
                'activation-target-edit',
                'activation-target-delete',
                'activation-reports-view',
                'activation-reports-pdf',
                'activation-reports-xls',
                'activation-payroll-view',
                'activation-payroll-generate',
                'expenses-list',
                'expenses-details',
                'expenses-approve',
                'expenses-printout',
                'sms-notification-status',
            ];

        }elseif ($request->role == 3){
            // Project manager

            $role = "Project manager";

            $permissions = [
                'ba-list',
                'ba-details',
                'ba-create',
                'ba-edit',
                'ba-delete',
                'admin-list',
                'admin-details',
                'admin-create',
                'admin-edit',
                'admin-delete',
                'admin-permission',
                'client-list',
                'client-details',
                'client-create',
                'client-edit',
                'client-delete',
                'activation-list',
                'activation-details',
                'activation-create',
                'activation-edit',
                'activation-delete',
                'activation-product-list',
                'activation-product-details',
                'activation-product-create',
                'activation-product-edit',
                'activation-product-delete',
                'activation-merchandise-list',
                'activation-merchandise-details',
                'activation-merchandise-create',
                'activation-merchandise-edit',
                'activation-merchandise-delete',
                'activation-location-list',
                'activation-location-details',
                'activation-location-create',
                'activation-location-edit',
                'activation-location-delete',
                'activation-team-leader-list',
                'activation-team-leader-details',
                'activation-team-leader-create',
                'activation-team-leader-edit',
                'activation-team-leader-delete',
                'activation-target-list',
                'activation-target-details',
                'activation-target-create',
                'activation-target-edit',
                'activation-target-delete',
                'activation-reports-view',
                'activation-reports-pdf',
                'activation-reports-xls',
                'activation-payroll-view',
                'activation-payroll-generate',
            ];

        }elseif ($request->role == 4){
            // Team leader
            $role = "Team leader";

            $permissions = [
                'ba-list',
                'ba-details',
                'admin-list',
                'admin-details',
                'admin-permission',
                'product-list',
                'product-details',
                'activation-list',
                'activation-details',
                'activation-product-list',
                'activation-product-details',
                'activation-merchandise-list',
                'activation-merchandise-details',
                'activation-location-list',
                'activation-location-details',
                'activation-team-leader-list',
                'activation-team-leader-details',
                'activation-target-list',
                'activation-target-details',
                'activation-reports-view',
            ];


        }elseif ($request->role == 5){
            // Finance
            $role = "Finance officer";

            $permissions = [
                'activation-payroll-view',
                'activation-payroll-generate',
                'expenses-list',
                'expenses-details',
                'expenses-approve',
                'expenses-printout',
            ];

        }elseif ($request->role == 6){
            // Supervisor
            $role = "Supervisor";

            $permissions = [
                'ba-list',
                'ba-details',
                'ba-create',
                'ba-edit',
                'ba-delete',
                'admin-list',
                'admin-details',
                'admin-permission',
                'product-list',
                'product-details',
                'activation-list',
                'activation-details',
                'activation-product-list',
                'activation-product-details',
                'activation-merchandise-list',
                'activation-merchandise-details',
                'activation-location-list',
                'activation-location-details',
                'activation-team-leader-list',
                'activation-team-leader-details',
                'activation-target-list',
                'activation-target-details',
                'activation-reports-view',
            ];


        }

        $user = Admin::where('id', $id)
            ->first();
        $user->syncPermissions($permissions);


        // Loop permissions
        $permissions = DB::table('permissions')
            ->get();
        $result = array();
        foreach ($permissions as $permission){

            // Check if user has permission
            $has_permission = 0;
            $user_permission = DB::table('model_has_permissions')
                ->where('model_id', $id)
                ->where('permission_id', $permission->id)
                ->first();
            if($user_permission){
                $has_permission = 1;
            }

            array_push($result,
                array(
                    'id' => $permission->id,
                    'name' => $permission->name,
                    'guard_name' => $permission->guard_name,
                    'has_permission' => $has_permission,
                ));


        }

        $user = Auth::user();
        $data = [
            'admin' => $id,
            'permissions' => $result,

        ];
        return view('admin.admin.admin-permissions', $data);
    }
}

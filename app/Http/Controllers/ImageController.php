<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;

class ImageController extends Controller
{
    public function getActivationImage($fileName){
        $path = public_path().'/images/activations/'.$fileName;
        return response()->file($path);

    }
}

<?php

namespace App\Http\Controllers\Api;

use App\PlannerEvent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;

class PlannerEventController extends Controller
{

    //Create/ update planner event
    public function updateEvent(Request $request)
    {
        //Check events
        $event_exist =  DB::table('planner_events')
            ->where('user_id', $request->user_id)
            ->where('date', $request->date)
            ->where('time', $request->time)
            ->first();

        if($event_exist){

            $conditions = ['user_id' => $request->user_id, 'date' => $request->date, 'time' => $request->time];
            $update = DB::table('clients')
                ->where($conditions)
                ->update(
                    [
                        'user_id'=>$request->user_id,
                        'date'=>$request->date,
                        'time'=>$request->time,
                        'event'=>$request->event,
                        'notes'=>$request->notes
                    ]);

            if($update){
                $json_array = array(
                    'success' => 1,
                );


                $response = $json_array;
                return json_encode($response);
            }else{
                /**
                 * client has failed to register
                 */
                $json_array = array(
                    'success' => 0,
                );

                $response = $json_array;
                return json_encode($response);
            }

        }else{

            $planner_object = new PlannerEvent();
            $planner_created = $planner_object->create([
                'user_id'=>$request->user_id,
                'date'=>$request->date,
                'time'=>$request->time,
                'event'=>$request->event,
                'notes'=>$request->notes,
            ]);

            if($planner_created){
                $json_array = array(
                    'success' => 1,
                );


                $response = $json_array;
                return json_encode($response);
            }else{
                /**
                 * client has failed to register
                 */
                $json_array = array(
                    'success' => 0,
                );

                $response = $json_array;
                return json_encode($response);
            }

        }


    }

    public function getDayEvents(Request $request){

        $planner_events = DB::table('planner_events')
            ->where('date',$request->date)
            ->where('user_id',$request->user_id)
            ->get();

        $result = array();

        foreach ($planner_events as $planner_event){

            array_push($result,
                array(
                    'id' => $planner_event->id,
                    'user_id' => $planner_event->user_id,
                    'date' => $planner_event->date,
                    'time' => $planner_event->time,
                    'event' => $planner_event->event,
                    'notes' => $planner_event->notes,
                    'status' => $planner_event->status,
                    'created_at' => $planner_event->created_at,
                    'updated_at ' => $planner_event->updated_at,
                ));

        }

        return json_encode(array("result" => $result));

    }

    public function getCurrentUserEvent(Request $request){

        $event_exist =  DB::table('planner_events')
            ->where('user_id', $request->user_id)
            ->where('date', $request->date)
            ->where('time', $request->time)
            ->first();

        if($event_exist){

            $json_array = array(
                'success' => 1,
                'event' => $event_exist->event,
            );

            $response = $json_array;
            return json_encode($response);
        }else{
            /**
             * User has failed to register
             */
            $json_array = array(
                'success' => 0,
            );

            $response = $json_array;
            return json_encode($response);
        }

    }

    public function sendWorkPlanner(){

        $admins = DB::table('admins')
            ->where('permanent_status', '1')
            ->get();


        foreach ($admins as $admin){

            $planners = array();

            $user_id = $admin->id;
            $name = $admin->name;

            for($time = 1; $time <= 10; $time++){

                $daily_events = array();

                $current_date = date('d-m-Y');
                $start_date = $current_date;
                $first_date = date('d-m-Y',strtotime($current_date . "+1 days"));

//                $start_date = date('d-m-Y',strtotime($current_date . "+1 days"));
                $end_date = date('d-m-Y',strtotime($start_date . "+4 days"));

                while (strtotime($start_date) <= strtotime($end_date)) {

                        $event = "--";
                        $event_exist =  DB::table('planner_events')
                            ->where('user_id', $user_id)
                            ->where('date', $start_date)
                            ->where('time', $time)
                            ->first();
                        if($event_exist){
                            $event = $event_exist->event;
                        }

                        array_push($daily_events,
                            array(
                                'event' => $event,
                            ));


                        $start_date = date ("d-m-Y", strtotime("+1 day", strtotime($start_date)));
                    }

                    $formatted_time = "";
                    if($time == 1){
                        $formatted_time = "8:00 AM";
                    }else if ($time == 2){
                        $formatted_time = "9:00 AM";
                    }else if ($time == 3){
                        $formatted_time = "10:00 AM";
                    }else if ($time == 4){
                        $formatted_time = "11:00 AM";
                    }else if ($time == 5){
                        $formatted_time = "12:00 PM";
                    }else if ($time == 6){
                        $formatted_time = "1:00 PM";
                    }else if ($time == 7){
                        $formatted_time = "2:00 PM";
                    }else if ($time == 8){
                        $formatted_time = "3:00 PM";
                    }else if ($time == 9){
                        $formatted_time = "4:00 PM";
                    }else if ($time == 10){
                        $formatted_time = "5:00 PM";
                    }

                    array_push($planners,
                        array(
                            'time' => $formatted_time,
                            'data' => $daily_events,
                        ));

            }


            $storage_path = public_path().'/planners/';

            $name_stripped = str_replace(' ', '', $name);

            $title_name = "Weekly Planner - $name - $start_date";

            $file_name = "Planner-$name_stripped-$start_date";

            Excel::create($file_name, function($excel) use ($name, $planners) {
                $excel->sheet('weekly-planner', function($sheet) use ($name, $planners) {
                    $sheet->loadView('admin.activation.report.xls.weekly-planner')->with(['name' => $name, 'planners'=> $planners]);
                });
            })->store('xlsx', $storage_path);

            // Send email

            $path = public_path().'/planners/'.$file_name.'.xlsx';
            $data = array(
                'path'=>$path,
                'name'=>$name,
                'start_date'=>$first_date,
                'file_name'=>$file_name,
                'title_name'=>$title_name,
            );

            Mail::send('emails.send-planner', $data, function($message) use ($data) {
                $message->to('keyneskabunge@alfonesltd.com', 'Keynes Kabunge')
                    ->subject($data['title_name']);
                $message->cc('amundia@alfonesltd.com');
                $message->attach($data['path']);
                $message->from('alfonescomm@gmail.com', $data['name']);

            });

        }


    }

}

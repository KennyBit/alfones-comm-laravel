<?php

namespace App\Http\Controllers\Client;

use App\Activation;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\DB;

class ActivationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:client');
    }

    public function index()
    {
        $user = Auth::user();
        //$user = Auth::user();
        $activations = DB::table('activations')
            ->where('client_id', $user->id)
            ->where('allow_client_portal', '1')
            ->get();
        $data = [
            'activations' => $activations,
        ];

      
         return view('client.activation.index', $data);
    }

    public function activationList()
    {
        $activations =  DB::table('activations')
            ->where('allow_client_portal', '1')
            ->orderBy('created_at', 'DESC')
            ->get();

        return json_encode($activations);
    }

    public function activationDetail(Request $request)
    {
        $activation =  DB::table('activations')->where('id', $request->id)->first();
        return json_encode($activation);
    }

    public function activationProducts(Request $request)
    {
        $products =  DB::table('products')->where('activation_id', $request->id)->get();
        return json_encode($products);
    }

    public function activationMerchandise(Request $request)
    {
        $merchandises =  DB::table('merchandises')->where('activation_id', $request->id)->get();
        return json_encode($merchandises);
    }

    public function activationLocation(Request $request){

        $locations =  DB::table('locations')->where('activation_id', $request->id)->get();

        $result = array();

        foreach ($locations as $location){

            $team_leader = DB::table('admins')
                ->where('id',$location->team_leader_id)
                ->first();

            $team_leader_name = "";
            if($team_leader){
                $team_leader_name =$team_leader->name;
            }

            array_push($result,
                array(
                    'id' => $location->id,
                    'activation_id' => $location->activation_id,
                    'name' => $location->name,
                    'team_leader' => $team_leader_name,
                    'date' => $location->date,
                    'duration' => $location->duration,
                    'created_at' => $location->created_at,
                    'updated_at' => $location->updated_at,
                ));

        }

        return json_encode($result);

    }

    public function activationTeamLeader(Request $request)
    {
        $activation_team_leaders =  DB::table('activation_team_leaders')->where('activation_id', $request->id)->get();

        $result = array();

        foreach ($activation_team_leaders as $activation_team_leader){

            $team_leader = DB::table('admins')
                ->where('id',$activation_team_leader->admin_id)
                ->first();

            if($team_leader){
                array_push($result,
                    array(
                        'id' => $team_leader->id,
                        'name' => $team_leader->name,
                        'phone_number' => $team_leader->phone_number,
                        'email' => $team_leader->email,
                        'created_at' => $team_leader->created_at,
                        'updated_at' => $team_leader->updated_at,
                    ));
            }



        }

        return json_encode($result);
    }

    public function activationTeamLeaderCreate(Request $request)
    {

        $team_leaders = DB::table('admins')
            ->where('role',4)
            ->get();

        $result = array();

        foreach ($team_leaders as $team_leader){

            $team_leader_check = DB::table('activation_team_leaders')
                ->where('admin_id',$team_leader->id)
                ->first();

            if(!$team_leader_check){
                array_push($result,
                    array(
                        'id' => $team_leader->id,
                        'name' => $team_leader->name,
                        'phone_number' => $team_leader->phone_number,
                        'email' => $team_leader->email,
                        'created_at' => $team_leader->created_at,
                        'updated_at' => $team_leader->updated_at,
                    ));
            }


        }

        return json_encode($result);
    }

    public function activationTarget(Request $request)
    {
        $activation_targets =  DB::table('activation_targets')->where('activation_id', $request->id)->get();

        $result = array();

        foreach ($activation_targets as $activation_target){

            $location_name = "";
            $location = DB::table('locations')
                ->where('id',$activation_target->location_id)
                ->first();
            if($location){
                $location_name = $location->name;
            }

            array_push($result,
                array(
                    'id' => $activation_target->id,
                    'location_name' => $location_name,
                    'date' => $activation_target->date,
                    'interaction_target' => $activation_target->interaction_target,
                    'sales_target' => $activation_target->sales_target,
                    'created_at' => $activation_target->created_at,
                    'updated_at' => $activation_target->updated_at,
                ));


        }

        return json_encode($result);
    }

    public function activationExpenses(Request $request){

        $activation_expenses = DB::table('expenses')
            ->where('activation_id', $request->activation_id)
            ->where('submitted_status', '1')
            ->orderBy('expenses_id', 'DESC')
            ->get();

        $result_activation = array();

        foreach ($activation_expenses as $activation_expense){

            $admin_name = "";
            $admin = DB::table('admins')
                ->where('id',$activation_expense->admin_id)
                ->first();
            if($admin){
                $admin_name = $admin->name;
            }

            // Get requisition amount
            $activation_expense_sum = DB::table('expenses_items')
                ->where('expenses_id', $activation_expense->id)
                ->sum('item_amount');


            array_push($result_activation,
                array(
                    'id' => $activation_expense->id,
                    'expenses_id' => $activation_expense->expenses_id,
                    'admin_name' => $admin_name,
                    'requisition_title' => $activation_expense->requisition_title,
                    'required_date' => $activation_expense->required_date,
                    'requisition_status' => $activation_expense->requisition_status,
                    'reconciliation_status' => $activation_expense->reconciliation_status,
                    'submitted_status' => $activation_expense->submitted_status,
                    'amount' => $activation_expense_sum,
                    'created_at' => $activation_expense->created_at,
                    'updated_at' => $activation_expense->updated_at,
                ));


        }
        return json_encode($result_activation);

    }

    public function reports(Request $request)
    {
        $activation_id = $request->id;
        $todays_date = date('Y-m-d');
        $last_seven_day=date("Y-m-d",strtotime("-7 day", strtotime($todays_date)));

        $interaction_today_count = DB::table('reports')
            ->where('activation_id',$activation_id)
            ->whereRaw('DATE(created_at) = ?', [$todays_date])
            ->count();

        $interaction_week_count = DB::table('reports')
            ->where('activation_id',$activation_id)
            ->whereBetween('created_at', [$last_seven_day, $todays_date])
            ->count();

        $interaction_total_count = DB::table('reports')
            ->where('activation_id',$activation_id)
            ->count();


        $sales_today_count = DB::table('reports')
            ->where('activation_id',$activation_id)
            ->where('product', 1)
            ->whereRaw('DATE(created_at) = ?', [$todays_date])
            ->sum('product_quantity');

        $sales_week_count = DB::table('reports')
            ->where('activation_id',$activation_id)
            ->where('product', 1)
            ->whereBetween('created_at', [$last_seven_day, $todays_date])
            ->sum('product_quantity');

        $sales_total_count = DB::table('reports')
            ->where('product', 1)
            ->where('activation_id',$activation_id)
            ->sum('product_quantity');


        $merchandise_today_count = DB::table('reports')
            ->where('activation_id',$activation_id)
            ->where('merchandise', 1)
            ->whereRaw('DATE(created_at) = ?', [$todays_date])
            ->sum('merchandise_quantity');

        $merchandise_week_count = DB::table('reports')
            ->where('activation_id',$activation_id)
            ->where('merchandise', 1)
            ->whereBetween('created_at', [$last_seven_day, $todays_date])
            ->sum('merchandise_quantity');

        $merchandise_total_count = DB::table('reports')
            ->where('merchandise', 1)
            ->where('activation_id',$activation_id)
            ->sum('merchandise_quantity');


        // Get ba achievements
        $ba_performances = array();
        $activation_users = DB::table('activation_users')
            ->where('activation_id',$activation_id)
            ->get();

        foreach ($activation_users as $activation_user) {

            $user = DB::table('users')
                ->where('id', $activation_user->user_id)
                ->first();

            $interaction_count = DB::table('reports')
                ->where('activation_id',$activation_id)
                ->where('user_id', $activation_user->user_id)
                ->count();

            $sales_count = DB::table('reports')
                ->where('activation_id',$activation_id)
                ->where('product', 1)
                ->where('user_id', $activation_user->user_id)
                ->sum('product_quantity');

            $merchandise_count = DB::table('reports')
                ->where('activation_id',$activation_id)
                ->where('merchandise', 1)
                ->where('user_id', $activation_user->user_id)
                ->sum('merchandise_quantity');


            if ($user) {
                array_push($ba_performances,
                    array(
                        'user_id' => $user->id,
                        'name' => $user->name,
                        'phone_number' => $user->phone_number,
                        'email' => $user->email,
                        'interaction_count' => $interaction_count,
                        'sales_count' => $sales_count,
                        'merchandise_count' => $merchandise_count
                    ));
            }
        }

        if(!empty($ba_performances)){
            foreach ($ba_performances as $key => $row)
            {
                $vc_array_name[$key] = $row['sales_count'];
            }
            array_multisort($vc_array_name, SORT_ASC, $ba_performances);

        }

        // Get payrolls
        $payrolls = array();
        $activation_users = DB::table('activation_users')
            ->where('activation_id',$activation_id)
            ->get();

        foreach ($activation_users as $activation_user) {

            $user = DB::table('users')
                ->where('id', $activation_user->user_id)
                ->first();

            if ($user) {

                // Get activation dates
                $activation = DB::table('activations')
                    ->where('id', $activation_id)
                    ->first();

                if($activation){
                    $start_date = $activation->start_date;
                    $end_date = $activation->end_date;
                    $ba_unit_cost = $activation->ba_unit_cost;

                    $days = 0;
                    $total_cost = 0;
                    while(strtotime($start_date)<=strtotime($end_date)) {

                            $check_in = DB::table('activation_check_ins')
                                ->where('activation_id',$activation_id)
                                ->where('user_id', $activation_user->user_id)
                                ->whereRaw('DATE(created_at) = ?', [$start_date])
                                ->first();

                            $report_confirmation = DB::table('activation_reports')
                                ->where('activation_id',$activation_id)
                                ->where('user_id', $activation_user->user_id)
                                ->whereRaw('DATE(created_at) = ?', [$start_date])
                                ->first();

                            if($check_in){
                                if($report_confirmation){

                                    $days = $days+1;
                                    $total_cost = $total_cost+$ba_unit_cost;
                                }
                            }

                        $start_date = date("Y-m-d", strtotime("+1 day", strtotime($start_date)));
                    }

                    array_push($payrolls,
                        array(
                            'user_id' => $user->id,
                            'name' => $user->name,
                            'unit_cost' => $ba_unit_cost,
                            'total_days' => $days,
                            'total_cost' => $total_cost
                        ));
                }

            }

        }


        $data = [
            'activation' => $request->id,
            'interaction_today_count' => $interaction_today_count,
            'interaction_week_count' => $interaction_week_count,
            'interaction_total_count' => $interaction_total_count,
            'sales_today_count' => $sales_today_count,
            'sales_week_count' => $sales_week_count,
            'sales_total_count' => $sales_total_count,
            'merchandise_today_count' => $merchandise_today_count,
            'merchandise_week_count' => $merchandise_week_count,
            'merchandise_total_count' => $merchandise_total_count,
            'ba_performances' => $ba_performances,
            'payrolls' => $payrolls,
        ];

        return view('client.activation.report.index', $data);
    }

    public function interactionGraph(Request $request){
        // Get date of seven days ago
        $beginning_date = Carbon::now()->subWeek()->toDateString();

        // Get todays date
        $todays_date = Carbon::today()->toDateString();

        $array_labels = array();
        $array_interactions = array();
        while (strtotime($beginning_date) <= strtotime($todays_date)) {

            $interaction_today_count = DB::table('reports')
                ->where('activation_id', $request->id)
                ->whereRaw('DATE(created_at) = ?', [$beginning_date])
                ->count();

            array_push($array_labels, $beginning_date);
            array_push($array_interactions, $interaction_today_count);

            $beginning_date = date ("Y-m-d", strtotime("+1 day", strtotime($beginning_date)));
        }


        $json_array = array(

            'labels' => $array_labels,
            'interactions' => $array_interactions,
        );
        $response = $json_array;
        return json_encode($response);
    }


    public function interactionDetails(Request $request)
    {
        /**
         * Get request data
         */
        $activation_id = $request->id;
        $date = $request->date;

        // Get total/target achievements
        $total_interactions = array();
        $activation_locations = DB::table('locations')
            ->where('activation_id',$activation_id)
            ->get();

        foreach ($activation_locations as $activation_location) {

            $interaction_count = DB::table('reports')
                ->where('activation_id',$activation_id)
                ->where('location_id', $activation_location->id)
                ->whereRaw('DATE(created_at) = ?', [$date])
                ->count();

            $interaction_target = 0;
            $percentage_achievement = 0;
            $target = DB::table('activation_targets')
                ->where('activation_id', $activation_location->activation_id)
                ->where('location_id', $activation_location->id)
                ->where('date', $date)
                ->first();
            if($target){
                $interaction_target = $target->interaction_target;
            }

            if($interaction_target != 0){
                if($interaction_count !=0 ){
                    $percentage_achievement =  ($interaction_count/$interaction_target)*100;
                }
            }

            if ($activation_locations) {

                $team_leader_name = "";
                $team_leader = DB::table('admins')
                    ->where('id',$activation_location->team_leader_id)
                    ->first();
                if($team_leader){
                    $team_leader_name = $team_leader->name;
                }

                array_push($total_interactions,
                    array(
                        'id' => $activation_location->id,
                        'name' => $activation_location->name,
                        'team_leader_id' => $activation_location->team_leader_id,
                        'team_leader_name' => $team_leader_name,
                        'latitude' => $activation_location->latitude,
                        'longitude' => $activation_location->longitude,
                        'interaction_count' => $interaction_count,
                        'interaction_target' => $interaction_target,
                        'percentage_achievement' => $percentage_achievement
                    ));
            }
        }

        // Get ba achievements
        $user_interactions = array();
        $activation_users = DB::table('activation_users')
            ->where('activation_id',$activation_id)
            ->get();

        foreach ($activation_users as $activation_user) {

            $user = DB::table('users')
                ->where('id', $activation_user->user_id)
                ->first();

            $interaction_count = DB::table('reports')
                ->where('activation_id',$activation_id)
                ->where('user_id', $activation_user->user_id)
                ->whereRaw('DATE(created_at) = ?', [$date])
                ->count();

            if ($user) {
                array_push($user_interactions,
                    array(
                        'user_id' => $user->id,
                        'name' => $user->name,
                        'phone_number' => $user->phone_number,
                        'email' => $user->email,
                        'interaction_count' => $interaction_count
                    ));
            }
        }


        // Get entries
        $report_entries = array();
        $entries = DB::table('reports')
            ->where('activation_id',$activation_id)
            ->whereRaw('DATE(created_at) = ?', [$date])
            ->get();

        foreach ($entries as $entry) {

            $user_name = "";
            $user = DB::table('users')
                ->where('id', $entry->user_id)
                ->first();
            if($user){
                $user_name = $user->name;
            }

            $location_name = "";
            $activation_location = DB::table('locations')
                ->where('id',$entry->location_id)
                ->first();
            if($activation_location){
                $location_name =  $activation_location->name;
            }

            if ($user) {
                array_push($report_entries,
                    array(
                        'id' => $entry->id,
                        'user_name' => $user->name,
                        'customer_name' => $user_name,
                        'customer_phone' => $entry->customer_phone,
                        'customer_id' => $entry->customer_id,
                        'customer_feedback' => $entry->customer_feedback,
                        'location_name' => $location_name,
                        'latitude' => $entry->latitude,
                        'longitude' => $entry->longitude,
                        'image' => $entry->image,
                        'created_at' => $entry->created_at,

                    ));
            }
        }

        $interaction_today_count = DB::table('reports')
            ->where('activation_id',$activation_id)
            ->whereRaw('DATE(created_at) = ?', [$date])
            ->count();

        $todays_date = date('Y-m-d');
        $last_seven_day=date("Y-m-d",strtotime("-7 day", strtotime($todays_date)));

        $interaction_week_count = DB::table('reports')
            ->where('activation_id',$activation_id)
            ->whereBetween('created_at', [$last_seven_day, $todays_date])
            ->count();

        $interaction_total_count = DB::table('reports')
            ->where('activation_id',$activation_id)
            ->count();


        $data = [
            'date' => $date,
            'total_interactions' => $total_interactions,
            'user_interactions' => $user_interactions,
            'entries' => $report_entries,
            'interaction_today_count' => $interaction_today_count,
            'interaction_week_count' => $interaction_week_count,
            'interaction_total_count' => $interaction_total_count,
        ];
        return view('client.activation.report.interactions.interaction-details', $data);
    }

    public function salesDetails(Request $request)
    {
        /**
         * Get request data
         */
        $activation_id = $request->id;
        $date = $request->date;

        // Get total/target achievements
        $total_sales= array();
        $activation_locations = DB::table('locations')
            ->where('activation_id',$activation_id)
            ->get();

        foreach ($activation_locations as $activation_location) {

            $sales_count = DB::table('reports')
                ->where('activation_id',$activation_id)
                ->where('product', 1)
                ->where('location_id', $activation_location->id)
                ->whereRaw('DATE(created_at) = ?', [$date])
                ->sum('product_quantity');

            $sales_target = 0;
            $percentage_achievement = 0;
            $target = DB::table('activation_targets')
                ->where('activation_id', $activation_location->activation_id)
                ->where('location_id', $activation_location->id)
                ->where('date', $date)
                ->first();
            if($target){
                $sales_target = $target->sales_target;
            }

            if($sales_target != 0){
                if($sales_count !=0 ){
                    $percentage_achievement =  ($sales_count/$sales_target)*100;
                }
            }

            if ($activation_locations) {

                $team_leader_name = "";
                $team_leader = DB::table('admins')
                    ->where('id',$activation_location->team_leader_id)
                    ->first();
                if($team_leader){
                    $team_leader_name = $team_leader->name;
                }

                array_push($total_sales,
                    array(
                        'id' => $activation_location->id,
                        'name' => $activation_location->name,
                        'team_leader_id' => $activation_location->team_leader_id,
                        'team_leader_name' => $team_leader_name,
                        'latitude' => $activation_location->latitude,
                        'longitude' => $activation_location->longitude,
                        'sales_count' => $sales_count,
                        'sales_target' => $sales_target,
                        'percentage_achievement' => $percentage_achievement
                    ));
            }
        }

        // Get ba achievements
        $user_sales = array();
        $activation_users = DB::table('activation_users')
            ->where('activation_id',$activation_id)
            ->get();

        foreach ($activation_users as $activation_user) {

            $user = DB::table('users')
                ->where('id', $activation_user->user_id)
                ->first();

            $sales_count = DB::table('reports')
                ->where('activation_id',$activation_id)
                ->where('product', 1)
                ->where('user_id', $activation_user->user_id)
                ->whereRaw('DATE(created_at) = ?', [$date])
                ->sum('product_quantity');

            if ($user) {
                array_push($user_sales,
                    array(
                        'user_id' => $user->id,
                        'name' => $user->name,
                        'phone_number' => $user->phone_number,
                        'email' => $user->email,
                        'sales_count' => $sales_count
                    ));
            }
        }


        // Get entries
        $report_entries = array();
        $entries = DB::table('reports')
            ->where('activation_id',$activation_id)
            ->where('product', 1)
            ->whereRaw('DATE(created_at) = ?', [$date])
            ->get();

        foreach ($entries as $entry) {

            $user_name = "";
            $user = DB::table('users')
                ->where('id', $entry->user_id)
                ->first();
            if($user){
                $user_name = $user->name;
            }

            $location_name = "";
            $activation_location = DB::table('locations')
                ->where('id',$entry->location_id)
                ->first();
            if($activation_location){
                $location_name =  $activation_location->name;
            }

            $product_name = "";
            $activation_product = DB::table('products')
                ->where('id',$entry->product_id)
                ->first();
            if($activation_product){
                $product_name =  $activation_product->name;
            }

            if ($user) {
                array_push($report_entries,
                    array(
                        'id' => $entry->id,
                        'user_name' => $user->name,
                        'customer_name' => $user_name,
                        'customer_phone' => $entry->customer_phone,
                        'customer_id' => $entry->customer_id,
                        'customer_feedback' => $entry->customer_feedback,
                        'product_name' => $product_name,
                        'product_quantity' => $entry->product_quantity,
                        'location_name' => $location_name,
                        'latitude' => $entry->latitude,
                        'longitude' => $entry->longitude,
                        'image' => $entry->image,
                        'created_at' => $entry->created_at,

                    ));
            }
        }

        // Get sales per product
        $product_sales = array();
        $activation_products = DB::table('products')
            ->where('activation_id',$activation_id)
            ->get();

        foreach ($activation_products as $activation_product) {

            $sales_count = DB::table('reports')
                ->where('activation_id',$activation_id)
                ->where('product', 1)
                ->where('product_id', $activation_product->id)
                ->whereRaw('DATE(created_at) = ?', [$date])
                ->sum('product_quantity');

                array_push($product_sales,
                    array(
                        'product_id' => $activation_product->id,
                        'name' => $activation_product->name,
                        'sales_count' => $sales_count
                    ));
        }

        $sales_today_count = DB::table('reports')
            ->where('activation_id',$activation_id)
            ->whereRaw('DATE(created_at) = ?', [$date])
            ->where('product', 1)
            ->sum('product_quantity');

        $todays_date = date('Y-m-d');
        $last_seven_day=date("Y-m-d",strtotime("-7 day", strtotime($todays_date)));

        $sales_week_count = DB::table('reports')
            ->where('activation_id',$activation_id)
            ->whereBetween('created_at', [$last_seven_day, $todays_date])
            ->where('product', 1)
            ->sum('product_quantity');

        $sales_total_count = DB::table('reports')
            ->where('activation_id',$activation_id)
            ->where('product', 1)
            ->sum('product_quantity');


        $data = [
            'date' => $date,
            'total_sales' => $total_sales,
            'user_sales' => $user_sales,
            'product_sales' => $product_sales,
            'entries' => $report_entries,
            'sales_today_count' => $sales_today_count,
            'sales_week_count' => $sales_week_count,
            'sales_total_count' => $sales_total_count,
        ];
        return view('client.activation.report.sales.sales-details', $data);
    }

    public function salesGraph(Request $request){
        // Get date of seven days ago
        $beginning_date = Carbon::now()->subWeek()->toDateString();

        // Get todays date
        $todays_date = Carbon::today()->toDateString();

        $array_labels = array();
        $array_sales = array();
        while (strtotime($beginning_date) <= strtotime($todays_date)) {

            $sale_today_count = DB::table('reports')
                ->where('activation_id', $request->id)
                ->where('product', 1)
                ->whereRaw('DATE(created_at) = ?', [$beginning_date])
                ->sum('product_quantity');

            array_push($array_labels, $beginning_date);
            array_push($array_sales, $sale_today_count);

            $beginning_date = date ("Y-m-d", strtotime("+1 day", strtotime($beginning_date)));
        }


        $json_array = array(

            'labels' => $array_labels,
            'sales' => $array_sales,
        );
        $response = $json_array;
        return json_encode($response);
    }

    public function productSalesGraph(Request $request){
        $array_labels = array();
        $array_sales = array();

        // Get sales per product
        $product_sales = array();
        $activation_products = DB::table('products')
            ->where('activation_id',$request->id)
            ->get();

        foreach ($activation_products as $activation_product) {

            $sales_count = DB::table('reports')
                ->where('activation_id', $request->id)
                ->where('product', 1)
                ->where('product_id', $activation_product->id)
                ->sum('product_quantity');

            array_push($array_labels, $activation_product->name);
            array_push($array_sales, $sales_count);

        }

        $json_array = array(

            'labels' => $array_labels,
            'sales' => $array_sales,
        );
        $response = $json_array;
        return json_encode($response);
    }

    public function merchandiseDetails(Request $request)
    {
        /**
         * Get request data
         */
        $activation_id = $request->id;
        $date = $request->date;

        // Get total/target achievements
        $total_merchandise = array();
        $activation_locations = DB::table('locations')
            ->where('activation_id',$activation_id)
            ->get();

        foreach ($activation_locations as $activation_location) {

            $merchandise_count = DB::table('reports')
                ->where('activation_id',$activation_id)
                ->where('merchandise', 1)
                ->where('location_id', $activation_location->id)
                ->whereRaw('DATE(created_at) = ?', [$date])
                ->sum('merchandise_quantity');


            if ($activation_locations) {

                $team_leader_name = "";
                $team_leader = DB::table('admins')
                    ->where('id',$activation_location->team_leader_id)
                    ->first();
                if($team_leader){
                    $team_leader_name = $team_leader->name;
                }

                array_push($total_merchandise,
                    array(
                        'id' => $activation_location->id,
                        'name' => $activation_location->name,
                        'team_leader_id' => $activation_location->team_leader_id,
                        'team_leader_name' => $team_leader_name,
                        'latitude' => $activation_location->latitude,
                        'longitude' => $activation_location->longitude,
                        'merchandise_count' => $merchandise_count,
                    ));
            }
        }

        // Get ba achievements
        $user_merchandise = array();
        $activation_users = DB::table('activation_users')
            ->where('activation_id',$activation_id)
            ->get();

        foreach ($activation_users as $activation_user) {

            $user = DB::table('users')
                ->where('id', $activation_user->user_id)
                ->first();

            $merchandise_count = DB::table('reports')
                ->where('activation_id',$activation_id)
                ->where('merchandise', 1)
                ->where('user_id', $activation_user->user_id)
                ->whereRaw('DATE(created_at) = ?', [$date])
                ->sum('merchandise_quantity');

            if ($user) {
                array_push($user_merchandise,
                    array(
                        'user_id' => $user->id,
                        'name' => $user->name,
                        'phone_number' => $user->phone_number,
                        'email' => $user->email,
                        'merchandise_count' => $merchandise_count
                    ));
            }
        }


        // Get entries
        $report_entries = array();
        $entries = DB::table('reports')
            ->where('activation_id',$activation_id)
            ->where('merchandise', 1)
            ->whereRaw('DATE(created_at) = ?', [$date])
            ->get();

        foreach ($entries as $entry) {

            $user_name = "";
            $user = DB::table('users')
                ->where('id', $entry->user_id)
                ->first();
            if($user){
                $user_name = $user->name;
            }

            $location_name = "";
            $activation_location = DB::table('locations')
                ->where('id',$entry->location_id)
                ->first();
            if($activation_location){
                $location_name =  $activation_location->name;
            }

            $merchandise_name = "";
            $activation_merchandise = DB::table('merchandises')
                ->where('id',$entry->merchandise_id)
                ->first();
            if($activation_merchandise){
                $merchandise_name =  $activation_merchandise->name;
            }

            if ($user) {
                array_push($report_entries,
                    array(
                        'id' => $entry->id,
                        'user_name' => $user->name,
                        'customer_name' => $user_name,
                        'customer_phone' => $entry->customer_phone,
                        'customer_id' => $entry->customer_id,
                        'customer_feedback' => $entry->customer_feedback,
                        'merchandise_name' => $merchandise_name,
                        'merchandise_quantity' => $entry->merchandise_quantity,
                        'location_name' => $location_name,
                        'latitude' => $entry->latitude,
                        'longitude' => $entry->longitude,
                        'created_at' => $entry->created_at,

                    ));
            }
        }

        // Get sales per merchandise
        $merchandise_sales = array();
        $activation_merchandises = DB::table('merchandises')
            ->where('activation_id',$activation_id)
            ->get();

        foreach ($activation_merchandises as $activation_merchandise) {

            $sales_count = DB::table('reports')
                ->where('activation_id',$activation_id)
                ->where('merchandise', 1)
                ->where('merchandise_id', $activation_merchandise->id)
                ->whereRaw('DATE(created_at) = ?', [$date])
                ->sum('merchandise_quantity');

            array_push($merchandise_sales,
                array(
                    'merchandise_id' => $activation_merchandise->id,
                    'name' => $activation_merchandise->name,
                    'sales_count' => $sales_count
                ));
        }

        $merchandise_today_count = DB::table('reports')
            ->where('activation_id',$activation_id)
            ->whereRaw('DATE(created_at) = ?', [$date])
            ->where('merchandise', 1)
            ->sum('merchandise_quantity');

        $todays_date = date('Y-m-d');
        $last_seven_day=date("Y-m-d",strtotime("-7 day", strtotime($todays_date)));

        $merchandise_week_count = DB::table('reports')
            ->where('activation_id',$activation_id)
            ->whereBetween('created_at', [$last_seven_day, $todays_date])
            ->where('merchandise', 1)
            ->sum('merchandise_quantity');

        $merchandise_total_count = DB::table('reports')
            ->where('activation_id',$activation_id)
            ->where('merchandise', 1)
            ->sum('merchandise_quantity');


        $data = [
            'date' => $date,
            'total_merchandises' => $total_merchandise,
            'user_merchandises' => $user_merchandise,
            'merchandise_distributions' => $merchandise_sales,
            'entries' => $report_entries,
            'merchandise_today_count' => $merchandise_today_count,
            'merchandise_week_count' => $merchandise_week_count,
            'merchandise_total_count' => $merchandise_total_count,
        ];
        return view('client.activation.report.merchandise.merchandise-details', $data);
    }

    public function merchandiseGraph(Request $request){
        // Get date of seven days ago
        $beginning_date = Carbon::now()->subWeek()->toDateString();

        // Get todays date
        $todays_date = Carbon::today()->toDateString();

        $array_labels = array();
        $array_merchandise = array();
        while (strtotime($beginning_date) <= strtotime($todays_date)) {

            $merchandise_today_count = DB::table('reports')
                ->where('activation_id', $request->id)
                ->where('merchandise', 1)
                ->whereRaw('DATE(created_at) = ?', [$beginning_date])
                ->sum('merchandise_quantity');

            array_push($array_labels, $beginning_date);
            array_push($array_merchandise, $merchandise_today_count);

            $beginning_date = date ("Y-m-d", strtotime("+1 day", strtotime($beginning_date)));
        }


        $json_array = array(

            'labels' => $array_labels,
            'merchandise' => $array_merchandise,
        );
        $response = $json_array;
        return json_encode($response);
    }

    public function merchandiseDistributionGraph(Request $request){
        $array_labels = array();
        $array_distributions = array();

        // Get distributions per merchandise
        $merchandise_distributions = array();
        $activation_merchandises = DB::table('merchandises')
            ->where('activation_id',$request->id)
            ->get();

        foreach ($activation_merchandises as $activation_merchandise) {

            $distributions_count = DB::table('reports')
                ->where('activation_id', $request->id)
                ->where('merchandise', 1)
                ->where('merchandise_id', $activation_merchandise->id)
                ->sum('merchandise_quantity');

            array_push($array_labels, $activation_merchandise->name);
            array_push($array_distributions, $distributions_count);

        }

        $json_array = array(

            'labels' => $array_labels,
            'distributions' => $array_distributions,
        );
        $response = $json_array;
        return json_encode($response);
    }
}


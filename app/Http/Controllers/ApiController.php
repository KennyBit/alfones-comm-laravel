<?php

namespace App\Http\Controllers;

use App\Activation;
use App\ActivationCheckIn;
use App\ActivationOutletProductSummary;
use App\ActivationProductStock;
use App\ActivationReport;
use App\ActivationTarget;
use App\ActivationTeamLeader;
use App\ActivationType;
use App\ActivationUser;
use App\ActivationUserLocation;
use App\Admin;
use App\Client;
use App\Division;
use App\ExpenseGiveAmount;
use App\ExpenseReconciliationItem;
use App\Expenses;
use App\ExpensesItem;
use App\Http\Controllers\Admin\ActivityLogController;
use App\Http\Controllers\Admin\MailController;
use App\library\AfricasTalkingGateway;
use App\library\AfricasTalkingGatewayException;
use App\Location;
use App\Merchandise;
use App\PasswordResetCode;
use App\Payslip;
use App\PerformanceReport;
use App\Product;
use App\Region;
use App\Report;
use App\User;
use App\UserLeave;
use App\UserPlanner;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use niklasravnsborg\LaravelPdf\Facades\Pdf;
use Response;
use App\Quote;

class ApiController extends Controller
{
    public function authRegister(Request $request){
        //Get request

        //Check if email exists
        $email = $request->email;
        $email_check =  DB::table('users')->where('email', $email)->first();
        if($email_check){
            /**
             * User has failed to register
             */
            $json_array = array(
                'success' => 2,
            );

            $response = $json_array;
            return json_encode($response);
        }

        //Check if phone exists
        $phone_number = $request->phone_number;
        $phone_number_check =  DB::table('users')->where('phone_number', $phone_number)->first();
        if($phone_number_check){
            /**
             * User has failed to register
             */
            $json_array = array(
                'success' => 3,
            );

            $response = $json_array;
            return json_encode($response);
        }

        //Check if national exists
        $national_id = $request->national_id;
        $national_id_check =  DB::table('users')->where('national_id', $national_id)->first();
        if($national_id_check){
            /**
             * User has failed to register
             */
            $json_array = array(
                'success' => 3,
            );

            $response = $json_array;
            return json_encode($response);
        }

        if(substr($phone_number,0, 3) == "254"){

            $phone_number = $request->phone_number;

        }else if(substr($phone_number,0, 2) == "07"){

            $phone_number = substr_replace($phone_number,"",0, 2);
            $phone_number = "2547$phone_number";

        }else if(substr($phone_number,0, 1) == "7"){

            $phone_number = substr_replace($phone_number,"",0, 1);
            $phone_number = "2547$phone_number";

        }else if(substr($phone_number,0, 4) == "null0"){

            $phone_number = substr_replace($phone_number,"",0, 4);
            $phone_number = "254$phone_number";

        }else if(substr($phone_number,0, 4) == "null7"){

            $phone_number = substr_replace($phone_number,"",0, 3);
            $phone_number = "254$phone_number";
        }

        $password = rand(1000,9999);

        $user_object = new User();
        $user_created = $user_object->create([
            'name'=>$request->name,
            'date_of_birth'=>$request->date_of_birth,
            'national_id'=>$request->national_id,
            'phone_number'=>$phone_number,
            'email'=>$request->email,
            'password'=> bcrypt($password),
        ]);

        if($user_created){
            /**
             * User has successfully registered
             */

            //SEND EMAIL
            $mail_controller = new MailController();
            $mail_controller->userWelcomeNote($request->name, $email, $password);

            //SEND SMS
            $message = "Welcome to Alfones App\nEmail: $email\nPassword:$password";
            $this->sendSMS($phone_number, $message);

            $json_array = array(
                'success' => 1,
                'password' => $password,
            );

            $response = $json_array;
            return json_encode($response);
        }else{
            /**
             * User has failed to register
             */
            $json_array = array(
                'success' => 0,
            );

            $response = $json_array;
            return json_encode($response);
        }

    }


    /**
     * @param Request $request
     * @return mixed
     *
     * Send verification code
     */
    public function sendResetCode(Request $request)
    {
        $email = $request->email;
        $user =  DB::table('users')->where('email', $email)->first();

        if($user){

            $user_id = $user->id;
            $reset_code = rand(100000,999999);
            $password_reset_code = new PasswordResetCode();
            $code_created = $password_reset_code->create([
                'user_id'=>$user_id,
                'code'=>$reset_code,
                'status'=> false,
            ]);

            if($code_created){

//                //SEND EMAIL
                $mail_controller = new MailController();
                $mail_controller->resetCode($user->name, $reset_code, $email);
//
//                //SEND SMS
                $message = "Alfones App\nReset code: $reset_code";
                $this->sendSMS($user->phone_number, $message);

                $json_array = array(
                    'success' => 1,
                );

                $response = $json_array;
                return json_encode($response);

            }else{

                $json_array = array(
                    'success' => 0,
                );

                $response = $json_array;
                return json_encode($response);

            }

        } else {

            $json_array = array(
                'success' => 2,
                'message' => 'no user',
            );

            $response = $json_array;
            return json_encode($response);
        }

    }

    public function sendResetCodeAdmin(Request $request)
    {
        $email = $request->email;
        $admin =  DB::table('admins')->where('email', $email)->first();

        if($admin){

            $admin_id = $admin->id;
            $reset_code = rand(100000,999999);
            $password_reset_code = new PasswordResetCode();
            $code_created = $password_reset_code->create([
                'user_id'=>$admin_id,
                'code'=>$reset_code,
                'status'=> false,
            ]);

            if($code_created){

                //SEND EMAIL
                $mail_controller = new MailController();
                $mail_controller->resetCode($admin->name, $reset_code, $email);

                //SEND SMS
                $message = "Alfones App\nReset code: $reset_code";
                $this->sendSMS($admin->phone_number, $message);

                $json_array = array(
                    'success' => 1,
                );

                $response = $json_array;
                return json_encode($response);

            }else{

                $json_array = array(
                    'success' => 0,
                );

                $response = $json_array;
                return json_encode($response);

            }

        } else {

            $json_array = array(
                'success' => 2,
                'message' => 'no user',
            );

            $response = $json_array;
            return json_encode($response);
        }

    }

    public function checkResetCode(Request $request){
        $email = $request->email;
        $reset_code = $request->reset_code;
        $user =  DB::table('users')->where('email', $email)->first();

        if($user) {

            $user_id = $user->id;

            $conditions = ['user_id' => $user_id, 'code' => $reset_code];
            $check_reset=  DB::table('password_reset_codes')
                ->where($conditions)
                ->orderBy('created_at','DESC')
                ->first();

            if($check_reset){

                $status = $check_reset->status;
                if($status==1){

                    $json_array = array(
                        'success' => 0,
                        'message' => 'failed to update',
                    );

                    $response = $json_array;
                    return json_encode($response);

                }else{

                    $json_array = array(
                        'success' => 1,
                        'message' => 'Successfu1',
                    );

                    $response = $json_array;
                    return json_encode($response);
                }

            }else{

                $json_array = array(
                    'success' => 2,
                    'message' => 'Code not found',
                );

                $response = $json_array;
                return json_encode($response);

            }

        }else{

            $json_array = array(
                'success' => 3,
                'message' => 'User not found',
            );

            $response = $json_array;
            return json_encode($response);
        }

    }

    public function checkResetCodeAdmin(Request $request){
        $email = $request->email;
        $reset_code = $request->reset_code;
        $admin =  DB::table('admins')->where('email', $email)->first();

        if($admin) {

            $admin_id = $admin->id;

            $conditions = ['user_id' => $admin_id, 'code' => $reset_code];
            $check_reset=  DB::table('password_reset_codes')
                ->where($conditions)
                ->orderBy('created_at','DESC')
                ->first();

            if($check_reset){

                $status = $check_reset->status;
                if($status==1){

                    $json_array = array(
                        'success' => 0,
                        'message' => 'failed to update',
                    );

                    $response = $json_array;
                    return json_encode($response);

                }else{

                    $json_array = array(
                        'success' => 1,
                        'message' => 'Successfu1',
                    );

                    $response = $json_array;
                    return json_encode($response);
                }

            }else{

                $json_array = array(
                    'success' => 2,
                    'message' => 'Code not found',
                );

                $response = $json_array;
                return json_encode($response);

            }

        }else{

            $json_array = array(
                'success' => 3,
                'message' => 'User not found',
            );

            $response = $json_array;
            return json_encode($response);
        }

    }

    public function resetPassword(Request $request){

        $email = $request->email;
        $user =  DB::table('users')->where('email', $email)->first();

        if($user) {

            $user_id = $user->id;
            $update_password = DB::table('users')
                ->where('id', $user_id)
                ->update([ 'password'=> bcrypt($request->password)]);

            if($update_password){

                //SEND EMAIL
                $mail_controller = new MailController();
                $mail_controller->passwordReset($user->name, $email);

                $user_id = $user->id;
                $json_array = array(
                    'success' => 1,
                );

                $response = $json_array;
                return json_encode($response);

            }else{

                $json_array = array(
                    'success' => 0,
                    'message' => 'failed to update',
                );

                $response = $json_array;
                return json_encode($response);

            }
        }

        return false;
    }

    public function resetPasswordAdmin(Request $request){

        $email = $request->email;
        $admin =  DB::table('admins')->where('email', $email)->first();

        if($admin) {

            $admin_id = $admin->id;
            $update_password = DB::table('admins')
                ->where('id', $admin_id)
                ->update([ 'password'=> bcrypt($request->password)]);

            if($update_password){

                //SEND EMAIL
                $mail_controller = new MailController();
                $mail_controller->passwordReset($admin->name, $email);

                $admin_id = $admin->id;
                $json_array = array(
                    'success' => 1,
                );

                $response = $json_array;
                return json_encode($response);

            }else{

                $json_array = array(
                    'success' => 0,
                    'message' => 'failed to update',
                );

                $response = $json_array;
                return json_encode($response);

            }
        }

        return false;
    }


    /**
     * @param Request $request
     * @return mixed
     *
     * Login user api
     */
    public function login(Request $request)
    {
        $email = $request->email;
        $password = $request->input('password');
        $user =  DB::table('users')->where('email', $email)->first();

        if($user){
            if (Hash::check($password, $user->password)) {

                $json_array = array(
                    'success' => 1,
                    'user_id' => $user->id,
                    'name' => $user->name,
                    'email' => $user->email,
                    'phone_number' => $user->phone_number,
                    'national_id' => $user->national_id,
                    'date_of_birth' => $user->date_of_birth,
                );

                $response = $json_array;
                return json_encode($response);

            } else {

                $json_array = array(
                    'success' => 0,
                );

                $response = $json_array;
                return json_encode($response);
            }
        }else{

            $json_array = array(
                'success' => 0,
            );

            $response = $json_array;
            return json_encode($response);
        }



    }


    public function loginAdmin(Request $request)
    {
        $email = $request->email;
        $password = $request->input('password');
        $admin =  DB::table('admins')->where('email', $email)->first();

        if($admin){
            if (Hash::check($password, $admin->password)) {

                $json_array = array(
                    'success' => 1,
                    'user_id' => $admin->id,
                    'name' => $admin->name,
                    'email' => $admin->email,
                    'phone_number' => $admin->phone_number,
                    'role' => $admin->role,
                    'permanent_status' => $admin->permanent_status,
                );

                $response = $json_array;
                return json_encode($response);

            } else {

                $json_array = array(
                    'success' => 0,
                );

                $response = $json_array;
                return json_encode($response);
            }
        }else{

            $json_array = array(
                'success' => 0,
            );

            $response = $json_array;
            return json_encode($response);
        }



    }

    public function activationSync(Request $request){

        //Define global variables
        $activation_id = "";
        $activation_name = "";
        $activation_date = "";
        $activation_status = "";
        $client_name = "";
        $location_id = "";
        $location_name = "";
        $latitude = "";
        $longitude = "";
        $enable_geo_fence = "";

        $user_id = $request->user_id;
        $user =  DB::table('users')->where('id', $user_id)->first();
        if($user){
            $current_activation = $user->current_activation;
            // Get activation details
            $activation =  DB::table('activations')->where('id', $current_activation)->first();

            if($activation){
                $activation_id = $activation->id;
                $activation_name = $activation->name;
                $activation_date = $activation->start_date;
                $activation_status = $activation->status;


                //Get client
                $client =  DB::table('clients')->where('id', $activation->client_id)->first();
                if($client){
                    $client_name = $client->company_name;
                }

                // Get assignment details
                $activation_user_location =  DB::table('activation_user_locations')
                    ->where('activation_id', $current_activation)
                    ->where('user_id', $user->id)
                    ->orderBy('activation_user_location_id', 'DESC')
                    ->first();

                if($activation_user_location){
                    $location_id = $activation_user_location->location_id;
                    $location =  DB::table('locations')->where('id', $location_id)->first();

                    if($location){
                        $location_id = $location->id;
                        $location_name = $location->name;
                        $latitude = $location->latitude;
                        $longitude = $location->longitude;
                        $enable_geo_fence = $location->enable_geo_fence;
                    }

                }
            }

        }

        $json_array = array(
            'activation_id' => $activation_id,
            'activation_name' => $activation_name,
            'activation_date' => $activation_date,
            'activation_status' => $activation_status,
            'client_name' => $client_name,
            'location_id' => $location_id,
            'location_name' => $location_name,
            'latitude' => $latitude,
            'longitude' => $longitude,
            'enable_geo_fence' => $enable_geo_fence,
        );

        $response = $json_array;
        return json_encode($response);
    }

    public function activationProducts(Request $request)
    {
        $products =  DB::table('products')->where('activation_id', $request->activation_id)->get();
        return json_encode(array("result" => $products));
    }

    public function activationMerchandises(Request $request)
    {
        $merchandises =  DB::table('merchandises')->where('activation_id', $request->activation_id)->get();
        return json_encode(array("result" => $merchandises));
    }

    public function userCurrentActivationDetails(Request $request){

        //Define global variables
        $activation_id = "";
        $activation_name = "";
        $activation_date = "";
        $activation_status = "";
        $client_name = "";

        $user_id = $request->user_id;
        $user =  DB::table('users')->where('id', $user_id)->first();
        if($user){
            $current_activation = $user->current_activation;
            // Get activation details
            $activation =  DB::table('activations')->where('id', $current_activation)->first();

            if($activation){
                $activation_id = $activation->id;
                $activation_name = $activation->name;
                $activation_date = $activation->start_date;
                $activation_status = $activation->status;

                //Get client
                $client =  DB::table('clients')->where('id', $activation->client_id)->first();
                if($client){
                    $client_name = $client->company_name;
                }
            }

        }

        $json_array = array(
            'activation_id' => $activation_id,
            'activation_name' => $activation_name,
            'activation_date' => $activation_date,
            'activation_status' => $activation_status,
            'activation_company' => $client_name,
        );

        $response = $json_array;
        return json_encode($response);
    }

    public function activationActiveList()
    {
        $activations =  DB::table('activations')
            ->where('status','0')
            ->orderBy('activation_id', 'DESC')
            ->get();

        $result = array();

        foreach ($activations as $activation){

            $activation_id = $activation->id;
            $activation_name = $activation->name;
            $activation_date = $activation->start_date;
            $activation_status = $activation->status;

            array_push($result,
                array(
                    'id' => $activation->id,
                    'activation_id' => $activation_id,
                    'activation_name' => $activation_name,
                    'activation_date' => $activation_date,
                    'activation_status' => $activation_status,
                ));
        }

        return json_encode(array("result" => $result));
    }

    public function userActivationDetailsUpdate(Request $request){

        $activation_id = "";
        $activation_name = "";
        $activation_date = "";
        $activation_status = "";
        $client_name = "";
        $success = "";

        // Get activation id from spinner name
        $user_id = $request->user_id;
        $activation_name = $request->activation_name;

        $activation =  DB::table('activations')
            ->where('name', $activation_name)
            ->first();
        if($activation){

            $activation_id = $activation->id;
            $activation_name = $activation->name;
            $activation_date = $activation->start_date;
            $activation_status = $activation->status;

            //Get client
            $client =  DB::table('clients')->where('id', $activation->client_id)->first();
            if($client){
                $client_name = $client->company_name;
            }

            // Check if brand ambassador already exists in the activation
            $activation_user =  DB::table('activation_users')
                ->where('user_id', $user_id)
                ->where('activation_id', $activation_id)
                ->first();

            if(!$activation_user){
                // Insert into activation user
                $user_object = new ActivationUser();
                $user_created = $user_object->create([
                    'user_id'=>$user_id,
                    'activation_id'=>$activation_id
                ]);

                if($user_created){
                    $success = 1;
                }
                else{
                    $success = 0;
                }
            }else{
                $success = 2;
            }

            // Update user current activation
            $update_current_activation = DB::table('users')
                ->where('id', $user_id)
                ->update([ 'current_activation'=> $activation_id]);

        }

        $json_array = array(
            'success' => $success,
            'activation_id' => $activation_id,
            'activation_name' => $activation_name,
            'activation_date' => $activation_date,
            'activation_status' => $activation_status,
            'activation_company' => $client_name,
        );

        $response = $json_array;
        return json_encode($response);


    }

    public function userTeamLeaderDetails(Request $request){

        //Define global variables
        $team_leader_id = "";
        $team_leader_name = "";
        $team_leader_phone = "";
        $success = "0";

        $user_id = $request->user_id;
        $activation_id = $request->activation_id;

        $activation_user =  DB::table('activation_users')
            ->where('user_id', $user_id)
            ->where('activation_id', $activation_id)
            ->first();

        if($activation_user){
            $team_leader_id = $activation_user->admin_id;
            if($team_leader_id != ""){
                $admin =  DB::table('admins')
                    ->where('id', $team_leader_id)
                    ->first();
                if($admin){
                    $success = "1";
                    $team_leader_id = $admin->id;
                    $team_leader_name = $admin->name;
                    $team_leader_phone = $admin->phone_number;
                }
            }else{
                $success = "3";
            }

        }else{
            $success = "2";
        }

        $json_array = array(
            'success' => $success,
            'team_leader_id' => $team_leader_id,
            'team_leader_name' => $team_leader_name,
            'team_leader_phone' => $team_leader_phone,
        );

        $response = $json_array;
        return json_encode($response);
    }

    public function activationActiveTeamLeaders(Request $request){

        $user_id = $request->user_id;
        $user =  DB::table('users')
            ->where('id', $user_id)
            ->first();

        $result = array();
        if($user){
            $activation_id = $user->current_activation;
            // Get team leaders in activation
            $activation_team_leaders =  DB::table('activation_team_leaders')
                ->where('activation_id', $activation_id)
                ->get();
            foreach ($activation_team_leaders as $activation_team_leader){

                $admin =  DB::table('admins')
                    ->where('id', $activation_team_leader->admin_id)
                    ->first();
                if($admin){
                    $team_leader_id = $admin->id;
                    $team_leader_name = $admin->name;
                    $team_leader_phone = $admin->phone_number;

                    array_push($result,
                        array(
                            'team_leader_id' => $team_leader_id,
                            'team_leader_name' => $team_leader_name,
                            'team_leader_phone' => $team_leader_phone,
                        ));
                }

            }


        }

        return json_encode(array("result" => $result));

    }

    public function userTeamLeaderDetailUpdate(Request $request){

        $team_leader_id = "";
        $team_leader_name = "";
        $team_leader_phone = "";
        $success = "0";

        $user_id = $request->user_id;
        $team_leader_name = $request->team_leader_name;

        $admin =  DB::table('admins')
            ->where('name', $team_leader_name)
            ->first();
        if($admin){

            $team_leader_id = $admin->id;
            $team_leader_name = $admin->name;
            $team_leader_phone = $admin->phone_number;

            $update_activation_user = DB::table('activation_users')
                ->where('user_id', $user_id)
                ->update([ 'admin_id'=> $team_leader_id]);
            if($update_activation_user){
                $success = 1;
            }
        }

        $json_array = array(
            'success' => $success,
            'team_leader_id' => $team_leader_id,
            'team_leader_name' => $team_leader_name,
            'team_leader_phone' => $team_leader_phone,
        );

        $response = $json_array;
        return json_encode($response);

    }

    public function getUserActivations(Request $request)
    {
        $user_id = $request->user_id;

        $activation_users =  DB::table('activation_users')
            ->where('user_id',$user_id)
            ->get();

        $result = array();

        foreach ($activation_users as $activation_user){

            $activation =  DB::table('activations')
                ->where('id', $activation_user->activation_id)
                ->where('status', 0)
                ->first();

            if($activation) {

                $activation_id = $activation->id;
                $activation_name = $activation->name;
                $activation_description = $activation->description;
                $activation_start_date = $activation->start_date;
                $activation_end_date = $activation->end_date;
                $activation_status = $activation->status;

                //Get client
                $client_name ="";
                $client =  DB::table('clients')->where('id', $activation->client_id)->first();
                if($client){
                    $client_name = $client->company_name;
                }

                array_push($result,
                    array(
                        'id' => $activation->id,
                        'activation_id' => $activation_id,
                        'activation_name' => $activation_name,
                        'activation_description' => $activation_description,
                        'activation_start_date' => $activation_start_date,
                        'activation_end_date' => $activation_end_date,
                        'activation_company' => $client_name,
                        'activation_no' => $activation->activation_id,
                    ));
            }



        }

        if(!empty($result)){
            foreach ($result as $key => $row)
            {
                $vc_array_name[$key] = $row['activation_no'];
            }
            array_multisort($vc_array_name, SORT_ASC, $result);

        }

        return json_encode(array("result" => $result));
    }

    public function getActivationDates(Request $request){

        $activation_id = $request->activation_id;
        $activation =  DB::table('activations')
            ->where('id', $activation_id)
            ->first();

        $result = array();

        if($activation) {
            $start_date = $activation->start_date;
            $end_date = $activation->end_date;

            while (strtotime($start_date) <= strtotime($end_date)) {
                array_push($result,
                    array(
                        'date' => $start_date,
                    ));
                $start_date = date ("Y-m-d", strtotime("+1 day", strtotime($start_date)));
            }

        }

        return json_encode(array("result" => $result));
    }

    public function getActivationReportHistory(Request $request){

        $user_id =$request->user_id;
        $activation_id = $request->activation_id;
        $date = $request->date;

        $reports =  DB::table('reports')
            ->where('user_id', $user_id)
            ->where('activation_id', $activation_id)
            ->whereRaw('DATE(created_at) = ?', [$date])
            ->get();

        $result = array();

        foreach ($reports as $report) {

            //Get client
            $product_name = "";
            $product =  DB::table('products')->where('id', $report->product_id)->first();
            if ($product) {
                $product_name = $product->name;
            }

            $merchandise_name = "";
            $merchandise =  DB::table('merchandises')->where('id', $report->merchandise_id)->first();
            if ($merchandise) {
                $merchandise_name = $merchandise->name;
            }

            $user_name = "";
            $user =  DB::table('users')->where('id', $report->user_id)->first();
            if ($user) {
                $user_name = $user->name;
            }

            array_push($result,
                array(
                    'id' => $report->id,
                    'activation_id' => $report->activation_id,
                    'location_id' => $report->location_id,
                    'user_id' => $report->user_id,
                    'user_name' => $user_name,
                    'customer_name' => $report->customer_name,
                    'customer_phone' => $report->customer_phone,
                    'customer_id' => $report->customer_id,
                    'product' => $report->product,
                    'product_id' => $report->product_id,
                    'product_name' => $product_name,
                    'product_quantity' => $report->product_quantity,
                    'product_value' => $report->product_value,
                    'extra_field_1' => $report->extra_field_1,
                    'extra_field_2' => $report->extra_field_2,
                    'merchandise' => $report->merchandise,
                    'merchandise_id' => $report->merchandise_id,
                    'merchandise_name' => $merchandise_name,
                    'merchandise_quantity' => $report->merchandise_quantity,
                    'customer_feedback' => $report->customer_feedback,
                    'image' => $report->image,
                    'image_caption' => $report->image_caption,
                    'latitude' => $report->latitude,
                    'longitude' => $report->longitude,
                    'created_at' => $report->created_at,
                    'updated_at' => $report->updated_at,

                ));
        }

        return json_encode(array("result" => $result));

    }


    public function getActivationReportHistorySummary(Request $request){

        $user_id =$request->user_id;
        $activation_id = $request->activation_id;
        $date = $request->date;

        $interaction_count = DB::table('reports')
            ->where('activation_id',$activation_id)
            ->where('user_id', $user_id)
            ->whereRaw('DATE(created_at) = ?', [$date])
            ->count();

        $sales_count = DB::table('reports')
            ->where('activation_id',$activation_id)
            ->where('product', 1)
            ->where('user_id', $user_id)
            ->whereRaw('DATE(created_at) = ?', [$date])
            ->sum('product_quantity');

        $merchandise_count = DB::table('reports')
            ->where('activation_id',$activation_id)
            ->where('merchandise', 1)
            ->where('user_id', $user_id)
            ->whereRaw('DATE(created_at) = ?', [$date])
            ->sum('merchandise_quantity');

        $json_array = array(
            'interaction_count' => $interaction_count,
            'sales_count' => $sales_count,
            'merchandise_count' => $merchandise_count,
        );
        $response = $json_array;
        return json_encode($response);


    }

    public function getUserReportConfirmation(Request $request){

        $user_id = $request->user_id;
        $activation_name = $request->activation_name;

        $result = array();

        $activation =  DB::table('activations')
            ->where('name', $activation_name)
            ->first();
        if($activation){

            $activation_id = $activation->id;
            $start_date = $activation->start_date;
            $end_date = $activation->end_date;

            while (strtotime($start_date) <= strtotime($end_date)) {

                $activation_report =  DB::table('activation_reports')
                    ->where('user_id', $user_id)
                    ->where('activation_id', $activation_id)
                    ->where('date', $start_date)
                    ->first();

                if($activation_report){
                    $team_leader_name = "";
                    $team_leader =  DB::table('admins')
                        ->where('id', $activation_report->admin_id)
                        ->first();
                    if($team_leader){
                        $team_leader_name = $team_leader->name;
                    }

                    array_push($result,
                        array(
                            'id' => $activation_report->id,
                            'date' => $activation_report->date,
                            'team_leader' => $team_leader_name,
                            'activation_id' => $activation_report->activation_id,
                            'user_id' => $activation_report->user_id,
                        ));
                }


                $start_date = date ("Y-m-d", strtotime("+1 day", strtotime($start_date)));
            }



        }

        return json_encode(array("result" => $result));
    }

    public function getUserPayment(Request $request){

        $user_id = $request->user_id;
        $activation_name = $request->activation_name;

        $result = array();

        $activation =  DB::table('activations')
            ->where('name', $activation_name)
            ->first();
        if($activation){

            $activation_id = $activation->id;
            $start_date = $activation->start_date;
            $end_date = $activation->end_date;


            while (strtotime($start_date) <= strtotime($end_date)) {

                $ba_unit_cost = 0;

                $activation_check_in =  DB::table('activation_check_ins')
                    ->where('user_id', $user_id)
                    ->where('activation_id', $activation_id)
                    ->whereRaw('DATE(created_at) = ?', [$start_date])
                    ->first();

                $activation_report =  DB::table('activation_reports')
                    ->where('user_id', $user_id)
                    ->where('activation_id', $activation_id)
                    ->where('date', $start_date)
                    ->first();

                $check_in_status = 0;
                if($activation_check_in){
                    $check_in_status = 1;
                }

                $report_confirmation_status = 0;
                if($activation_report){
                    $report_confirmation_status = 1;
                }

                if($activation_check_in){
                    if($activation_report){
                        $ba_unit_cost = $activation->ba_unit_cost;
                    }
                }

                $team_leader_name = "";

                if($ba_unit_cost !=0){
                    array_push($result,
                        array(
                            'id' => "",
                            'date' => $start_date,
                            'amount' => $ba_unit_cost,
                            'checked_in' => $check_in_status,
                            'confirmed' => $report_confirmation_status,
                            'team_leader' => $team_leader_name,
                            'activation_id' => $activation_id,
                            'user_id' => $user_id,
                        ));

                }


                $start_date = date ("Y-m-d", strtotime("+1 day", strtotime($start_date)));
            }



        }

        return json_encode(array("result" => $result));
    }


    public function getUserCheckIn(Request $request){

        $user_id = $request->user_id;
        $activation_name = $request->activation_name;

        $result = array();

        $activation =  DB::table('activations')
            ->where('name', $activation_name)
            ->first();
        if($activation){

            $activation_id = $activation->id;
            $start_date = $activation->start_date;
            $end_date = $activation->end_date;

            while (strtotime($start_date) <= strtotime($end_date)) {

                $activation_check_in =  DB::table('activation_check_ins')
                    ->where('user_id', $user_id)
                    ->where('activation_id', $activation_id)
                    ->whereRaw('DATE(created_at) = ?', [$start_date])
                    ->first();

                if($activation_check_in){
                    $team_leader_name = "";
                    $team_leader =  DB::table('admins')
                        ->where('id', $activation_check_in->admin_id)
                        ->first();
                    if($team_leader){
                        $team_leader_name = $team_leader->name;
                    }


                    array_push($result,
                        array(
                            'id' => $activation_check_in->id,
                            'date' => $activation_check_in->created_at,
                            'team_leader' => $team_leader_name,
                            'activation_id' => $activation_check_in->activation_id,
                            'user_id' => $activation_check_in->user_id,
                        ));
                }


                $start_date = date ("Y-m-d", strtotime("+1 day", strtotime($start_date)));
            }



        }

        return json_encode(array("result" => $result));
    }

    public function getUserNotifications(Request $request){

        $user_id = $request->user_id;
        $result = array();
        $notifications =  DB::table('notifications')
            ->where('user_id', $user_id)
            ->orderBy('notification_id', 'DESC')
            ->get();

        foreach ($notifications as $notification){

            array_push($result,
                array(
                    'id' => $notification->id,
                    'message' => $notification->message,
                    'user_id' => $notification->user_id,
                    'read' => $notification->is_read,
                    'date' => $notification->created_at,
                ));

        }

        return json_encode(array("result" => $result));
    }

    public function updateNotificationReadStatus(Request $request){

        $id = $request->id;
        $update_notification_read = DB::table('notifications')
            ->where('id', $id)
            ->update([ 'is_read' => '1']);
        if($update_notification_read){
            $json_array = array(
                'success' => '1',
            );
            $response = $json_array;
            return json_encode($response);
        }else{
            $json_array = array(
                'success' => '0',
            );
            $response = $json_array;
            return json_encode($response);
        }

    }

    public function createActivationRecord(Request $request){

        $report_object = new Report();
        $report_created = $report_object->create([
            'user_id'=>$request->user_id,
            'activation_id'=>$request->activation_id,
            'location_id'=>$request->location_id,
            'customer_name'=>$request->customer_name,
            'customer_phone'=>$request->customer_phone,
            'customer_id'=>$request->customer_id,
            'product'=>$request->product,
            'product_id'=>$request->product_id,
            'product_quantity'=>$request->product_quantity,
            'product_value'=>$request->product_value,
            'extra_field_1'=>$request->extra_field_1,
            'extra_field_2'=>$request->extra_field_2,
            'merchandise'=>$request->merchandise,
            'merchandise_id'=>$request->merchandise_id,
            'merchandise_quantity'=>$request->merchandise_quantity,
            'customer_feedback'=>$request->customer_feedback,
            'latitude'=>$request->latitude,
            'longitude'=>$request->longitude,
        ]);
        if($report_created){
            $json_array = array(
                'success' => '1',
            );
            $response = $json_array;
            return json_encode($response);
        }else{
            $json_array = array(
                'success' => '0',
            );
            $response = $json_array;
            return json_encode($response);
        }

    }


    public function createActivationAttachmentRecord(Request $request){

        $item_attachment = "";
        if ($request->hasFile('image')) {
            $this->validate($request, [
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:4048',
            ]);

            $image = $request->file('image');
            $file_name = 'activation'.time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/images/activations/');
            $image->move($destinationPath, $file_name);

            $item_attachment = 'http://192.81.215.21/api/image-get-activation/'.$file_name;
        }

        $report_object = new Report();
        $report_created = $report_object->create([
            'user_id'=>$request->user_id,
            'activation_id'=>$request->activation_id,
            'location_id'=>$request->location_id,
            'customer_name'=>$request->customer_name,
            'customer_phone'=>$request->customer_phone,
            'customer_id'=>$request->customer_id,
            'product'=>$request->product,
            'product_id'=>$request->product_id,
            'product_quantity'=>$request->product_quantity,
            'product_value'=>$request->product_value,
            'extra_field_1'=>$request->extra_field_1,
            'extra_field_2'=>$request->extra_field_2,
            'merchandise'=>$request->merchandise,
            'merchandise_id'=>$request->merchandise_id,
            'merchandise_quantity'=>$request->merchandise_quantity,
            'customer_feedback'=>$request->customer_feedback,
            'latitude'=>$request->latitude,
            'longitude'=>$request->longitude,
            'image'=>$item_attachment,
        ]);
        if($report_created){
            $json_array = array(
                'success' => '1',
            );
            $response = $json_array;
            return json_encode($response);
        }else{
            $json_array = array(
                'success' => '0',
            );
            $response = $json_array;
            return json_encode($response);
        }

    }


    public function editActivationRecord(Request $request)
    {
        $reports =  DB::table('reports')->where('id', $request->id)->first();
        if($reports){
            /**
             * Update Activation
             */
            $update = DB::table('reports')
                ->where('id', $request->id)
                ->update([
                    'customer_name'=>$request->customer_name,
                    'customer_phone'=>$request->customer_phone,
                    'customer_id'=>$request->customer_id,
                    'product'=>$request->product,
                    'product_id'=>$request->product_id,
                    'product_quantity'=>$request->product_quantity,
                    'product_value'=>$request->product_value,
                    'extra_field_1'=>$request->extra_field_1,
                    'extra_field_2'=>$request->extra_field_2,
                    'merchandise'=>$request->merchandise,
                    'merchandise_id'=>$request->merchandise_id,
                    'merchandise_quantity'=>$request->merchandise_quantity,
                    'customer_feedback'=>$request->customer_feedback,
                ]);

            if($update){

                $json_array = array(
                    'success' => 1,
                );

                $response = $json_array;
                return json_encode($response);

            }else{

                $json_array = array(
                    'success' => 0,
                );

                $response = $json_array;
                return json_encode($response);

            }

        }else{

            $json_array = array(
                'success' => 2,
            );

            $response = $json_array;
            return json_encode($response);
        }
    }

    public function deleteActivationRecord(Request $request)
    {
        $reports =  DB::table('reports')->where('id', $request->id)->first();
        if($reports){
            /**
             * Delete Activation
             */
            $conditions = ['id' => $request->id];
            $delete = DB::table('reports')
                ->where($conditions)
                ->delete();

            if($delete){

                $json_array = array(
                    'success' => 1,
                );

                $response = $json_array;
                return json_encode($response);

            }else{

                $json_array = array(
                    'success' => 0,
                );

                $response = $json_array;
                return json_encode($response);

            }

        }else{

            $json_array = array(
                'success' => 2,
            );

            $response = $json_array;
            return json_encode($response);
        }
    }

    public function getPreviousActivations(Request $request)
    {

        $activations =  DB::table('activations')
            ->where('status', 0)
            ->get();

        $result = array();

        foreach ($activations as $activation){

            $activation_id = $activation->id;
            $activation_name = $activation->name;
            $activation_description = $activation->description;
            $activation_start_date = $activation->start_date;
            $activation_end_date = $activation->end_date;
            $activation_status = $activation->status;

            //Get client
            $client_name ="";
            $client =  DB::table('clients')->where('id', $activation->client_id)->first();
            if($client){
                $client_name = $client->company_name;
            }

            array_push($result,
                array(
                    'id' => $activation->id,
                    'activation_id' => $activation_id,
                    'activation_name' => $activation_name,
                    'activation_description' => $activation_description,
                    'activation_start_date' => $activation_start_date,
                    'activation_end_date' => $activation_end_date,
                    'activation_company' => $client_name,
                    'activation_no' => $activation->activation_id,
                ));



        }

        if(!empty($result)){
            foreach ($result as $key => $row)
            {
                $vc_array_name[$key] = $row['activation_no'];
            }
            array_multisort($vc_array_name, SORT_ASC, $result);

        }

        return json_encode(array("result" => $result));
    }

    public function getProjectManagerActivations(Request $request)
    {
        $user_id = $request->user_id;
        $activations =  DB::table('activations')
            ->where('project_manager_id', $user_id)
            ->where('status', 0)
            ->get();

        $result = array();

        foreach ($activations as $activation){

                $activation_id = $activation->id;
                $activation_name = $activation->name;
                $activation_description = $activation->description;
                $activation_start_date = $activation->start_date;
                $activation_end_date = $activation->end_date;
                $activation_status = $activation->status;

                //Get client
                $client_name ="";
                $client =  DB::table('clients')->where('id', $activation->client_id)->first();
                if($client){
                    $client_name = $client->company_name;
                }

                array_push($result,
                    array(
                        'id' => $activation->id,
                        'activation_id' => $activation_id,
                        'activation_name' => $activation_name,
                        'activation_description' => $activation_description,
                        'activation_start_date' => $activation_start_date,
                        'activation_end_date' => $activation_end_date,
                        'activation_company' => $client_name,
                        'activation_no' => $activation->activation_id,
                    ));



        }

        if(!empty($result)){
            foreach ($result as $key => $row)
            {
                $vc_array_name[$key] = $row['activation_no'];
            }
            array_multisort($vc_array_name, SORT_ASC, $result);

        }

        return json_encode(array("result" => $result));
    }

    public function getTeamLeaderActivations(Request $request)
    {
        $user_id = $request->user_id;

        $activation_team_leaders =  DB::table('activation_team_leaders')
            ->where('admin_id', $user_id)
            ->get();

        $result = array();

        foreach ($activation_team_leaders as $activation_team_leader){

            $activation =  DB::table('activations')
                ->where('id', $activation_team_leader->activation_id)
                ->where('status', 0)
                ->first();

            if($activation) {

                $activation_id = $activation->id;
                $activation_name = $activation->name;
                $activation_description = $activation->description;
                $activation_start_date = $activation->start_date;
                $activation_end_date = $activation->end_date;
                $activation_status = $activation->status;

                //Get client
                $client_name ="";
                $client =  DB::table('clients')->where('id', $activation->client_id)->first();
                if($client){
                    $client_name = $client->company_name;
                }

                array_push($result,
                    array(
                        'id' => $activation->id,
                        'activation_id' => $activation_id,
                        'activation_name' => $activation_name,
                        'activation_description' => $activation_description,
                        'activation_start_date' => $activation_start_date,
                        'activation_end_date' => $activation_end_date,
                        'activation_company' => $client_name,
                        'activation_no' => $activation->activation_id,
                    ));
            }



        }

        if(!empty($result)){
            foreach ($result as $key => $row)
            {
                $vc_array_name[$key] = $row['activation_no'];
            }
            array_multisort($vc_array_name, SORT_ASC, $result);

        }

        return json_encode(array("result" => $result));
    }

    public function getProjectManagerLocationReportHistory(Request $request)
    {
        $activation_id = $request->activation_id;
        $date = $request->date;

        $locations =  DB::table('locations')
            ->where('activation_id', $activation_id)
            ->get();

        $result = array();

        foreach ($locations as $location) {

            $interaction_count = DB::table('reports')
                ->where('activation_id', $activation_id)
                ->where('location_id', $location->id)
                ->whereRaw('DATE(created_at) = ?', [$date])
                ->count();

            $sales_count = DB::table('reports')
                ->where('activation_id', $activation_id)
                ->where('product', 1)
                ->where('location_id', $location->id)
                ->whereRaw('DATE(created_at) = ?', [$date])
                ->sum('product_quantity');

            $merchandise_count = DB::table('reports')
                ->where('activation_id', $activation_id)
                ->where('merchandise', 1)
                ->where('location_id', $location->id)
                ->whereRaw('DATE(created_at) = ?', [$date])
                ->sum('merchandise_quantity');

            $team_leader_name = "";
            $team_leader = DB::table('admins')->where('id', $location->team_leader_id)->first();
            if ($team_leader) {
                $team_leader_name = $team_leader->name;
            }


            array_push($result,
                array(
                    'id' => $location->id,
                    'activation_id' => $activation_id,
                    'location_id' => $location->id,
                    'location_name' => $location->name,
                    'team_leader_id' => $location->team_leader_id,
                    'team_leader_name' => $team_leader_name,
                    'interaction' => $interaction_count,
                    'sales' => $sales_count,
                    'merchandise' => $merchandise_count,
                    'created_at' => $location->activation_id,
                    'updated_at' => $location->activation_id,
                ));

        }

        return json_encode(array("result" => $result));
    }

    public function getTeamLeaderLocationReportHistory(Request $request)
    {
        $activation_id = $request->activation_id;
        $date = $request->date;
        $user_id = $request->user_id;

        $locations =  DB::table('locations')
            ->where('activation_id', $activation_id)
            ->where('team_leader_id', $user_id)
            ->get();

        $result = array();

        foreach ($locations as $location) {

            $interaction_count = DB::table('reports')
                ->where('activation_id', $activation_id)
                ->where('location_id', $location->id)
                ->whereRaw('DATE(created_at) = ?', [$date])
                ->count();

            $sales_count = DB::table('reports')
                ->where('activation_id', $activation_id)
                ->where('product', 1)
                ->where('location_id', $location->id)
                ->whereRaw('DATE(created_at) = ?', [$date])
                ->sum('product_quantity');

            $merchandise_count = DB::table('reports')
                ->where('activation_id', $activation_id)
                ->where('merchandise', 1)
                ->where('location_id', $location->id)
                ->whereRaw('DATE(created_at) = ?', [$date])
                ->sum('merchandise_quantity');

            $team_leader_name = "";
            $team_leader = DB::table('admins')->where('id', $location->team_leader_id)->first();
            if ($team_leader) {
                $team_leader_name = $team_leader->name;
            }


            array_push($result,
                array(
                    'id' => $location->id,
                    'activation_id' => $activation_id,
                    'location_id' => $location->id,
                    'location_name' => $location->name,
                    'team_leader_id' => $location->team_leader_id,
                    'team_leader_name' => $team_leader_name,
                    'interaction' => $interaction_count,
                    'sales' => $sales_count,
                    'merchandise' => $merchandise_count,
                    'created_at' => $location->activation_id,
                    'updated_at' => $location->activation_id,
                ));

        }

        return json_encode(array("result" => $result));
    }

    public function getProjectManagerLocationUserReportHistory(Request $request)
    {
        $activation_id = $request->activation_id;
        $date = $request->date;
        $location_id = $request->location_id;

        $activation_user_locations =  DB::table('activation_user_locations')
            ->where('activation_id', $activation_id)
            ->where('location_id', $location_id)
            ->get();

        $result = array();

        foreach ($activation_user_locations as $activation_user_location) {

            $interaction_count = DB::table('reports')
                ->where('activation_id', $activation_id)
                ->where('location_id', $location_id)
                ->where('user_id', $activation_user_location->user_id)
                ->whereRaw('DATE(created_at) = ?', [$date])
                ->count();

            $sales_count = DB::table('reports')
                ->where('activation_id', $activation_id)
                ->where('product', 1)
                ->where('location_id', $location_id)
                ->where('user_id', $activation_user_location->user_id)
                ->whereRaw('DATE(created_at) = ?', [$date])
                ->sum('product_quantity');

            $merchandise_count = DB::table('reports')
                ->where('activation_id', $activation_id)
                ->where('merchandise', 1)
                ->where('location_id', $location_id)
                ->where('user_id', $activation_user_location->user_id)
                ->whereRaw('DATE(created_at) = ?', [$date])
                ->sum('merchandise_quantity');

            $user_name = "";
            $user = DB::table('users')->where('id', $activation_user_location->user_id)->first();
            if ($user) {
                $user_name = $user->name;
            }


            array_push($result,
                array(
                    'id' => $activation_user_location->id,
                    'activation_id' => $activation_id,
                    'user_id' => $activation_user_location->user_id,
                    'user_name' => $user_name,
                    'interaction' => $interaction_count,
                    'sales' => $sales_count,
                    'merchandise' => $merchandise_count,
                    'created_at' => $activation_user_location->created_at,
                    'updated_at' => $activation_user_location->updated_at,
                ));

        }

        return json_encode(array("result" => $result));
    }

    public function getProjectManagerUserLocationReportHistory(Request $request)
    {
        $activation_id = $request->activation_id;
        $date = $request->date;
        $location_id = $request->location_id;

        $activation_user_locations =  DB::table('activation_user_locations')
            ->where('activation_id', $activation_id)
            ->where('location_id', $location_id)
            ->get();

        $result = array();

        foreach ($activation_user_locations as $activation_user_location) {

                $interaction_count = DB::table('reports')
                    ->where('activation_id', $activation_id)
                    ->where('location_id', $location_id)
                    ->where('user_id', $activation_user_location->user_id)
                    ->whereRaw('DATE(created_at) = ?', [$date])
                    ->count();

                $sales_count = DB::table('reports')
                    ->where('activation_id', $activation_id)
                    ->where('product', 1)
                    ->where('location_id', $location_id)
                    ->where('user_id', $activation_user_location->user_id)
                    ->whereRaw('DATE(created_at) = ?', [$date])
                    ->sum('product_quantity');

                $merchandise_count = DB::table('reports')
                    ->where('activation_id', $activation_id)
                    ->where('merchandise', 1)
                    ->where('location_id', $location_id)
                    ->where('user_id', $activation_user_location->user_id)
                    ->whereRaw('DATE(created_at) = ?', [$date])
                    ->sum('merchandise_quantity');

                $user_name = "";
                $user = DB::table('users')->where('id', $activation_user_location->user_id)->first();
                if ($user) {
                    $user_name = $user->name;
                }


                array_push($result,
                    array(
                        'id' => $activation_user_location->id,
                        'activation_id' => $activation_id,
                        'user_id' => $activation_user_location->user_id,
                        'user_name' => $user_name,
                        'interaction' => $interaction_count,
                        'sales' => $sales_count,
                        'merchandise' => $merchandise_count,
                        'created_at' => $activation_user_location->created_at,
                        'updated_at' => $activation_user_location->updated_at,
                    ));

        }

        return json_encode(array("result" => $result));
    }

    public function getTeamLeaderUserLocationReportHistory(Request $request)
    {
        $activation_id = $request->activation_id;
        $date = $request->date;
        $location_id = $request->location_id;
        $team_leader_id = $request->user_id;

        $activation_user_locations =  DB::table('activation_user_locations')
            ->where('activation_id', $activation_id)
            ->where('location_id', $location_id)
            ->get();


        $result = array();

        foreach ($activation_user_locations as $activation_user_location) {

            $team_leader_location =  DB::table('locations')
                ->where('activation_id', $activation_id)
                ->where('team_leader_id', $team_leader_id)
                ->first();

            if($team_leader_location){
                $interaction_count = DB::table('reports')
                    ->where('activation_id', $activation_id)
                    ->where('location_id', $location_id)
                    ->where('user_id', $activation_user_location->user_id)
                    ->whereRaw('DATE(created_at) = ?', [$date])
                    ->count();

                $sales_count = DB::table('reports')
                    ->where('activation_id', $activation_id)
                    ->where('product', 1)
                    ->where('location_id', $location_id)
                    ->where('user_id', $activation_user_location->user_id)
                    ->whereRaw('DATE(created_at) = ?', [$date])
                    ->sum('product_quantity');

                $merchandise_count = DB::table('reports')
                    ->where('activation_id', $activation_id)
                    ->where('merchandise', 1)
                    ->where('location_id', $location_id)
                    ->where('user_id', $activation_user_location->user_id)
                    ->whereRaw('DATE(created_at) = ?', [$date])
                    ->sum('merchandise_quantity');

                $user_name = "";
                $user = DB::table('users')->where('id', $activation_user_location->user_id)->first();
                if ($user) {
                    $user_name = $user->name;
                }


                array_push($result,
                    array(
                        'id' => $activation_user_location->id,
                        'activation_id' => $activation_id,
                        'user_id' => $activation_user_location->user_id,
                        'user_name' => $user_name,
                        'interaction' => $interaction_count,
                        'sales' => $sales_count,
                        'merchandise' => $merchandise_count,
                        'created_at' => $activation_user_location->created_at,
                        'updated_at' => $activation_user_location->updated_at,
                    ));

            }


        }

        return json_encode(array("result" => $result));
    }


    public function getTeamLeaderCheckIn(Request $request)
    {

        $admin_id = $request->user_id;
        $activation_name = $request->activation_name;
        $date = date('Y-m-d');

        $activation =  DB::table('activations')
            ->where('name', $activation_name)
            ->first();

        $result = array();

        if($activation){

            $activation_id = $activation->id;
            $activation_users =  DB::table('activation_users')
                ->where('activation_id', $activation_id)
                ->where('admin_id', $admin_id)
                ->get();

            foreach ($activation_users as $activation_user) {

                $user_name = "";
                $user = DB::table('users')->where('id', $activation_user->user_id)->first();
                if ($user) {
                    $user_name = $user->name;
                }

                $activation_check_in =  DB::table('activation_check_ins')
                    ->where('activation_id', $activation_id)
                    ->where('user_id', $activation_user->user_id)
                    ->whereRaw('DATE(created_at) = ?', [$date])
                    ->first();

                $check_in = 0;
                if($activation_check_in){
                    $check_in = 1;
                }

                array_push($result,
                    array(
                        'id' => $activation_user->user_id,
                        'activation_id' => $activation_id,
                        'user_id' => $activation_user->user_id,
                        'user_name' => $user_name,
                        'check_in' => $check_in,
                    ));

            }

        }

        return json_encode(array("result" => $result));
    }

    public function teamLeaderCheckInUpdate(Request $request){

        // Get activation id from spinner name
        $user_id = $request->user_id;
        $activation_id = $request->activation_id;
        $admin_id = $request->admin_id;
        $success = 0;

        $activation =  DB::table('activations')
            ->where('id', $activation_id)
            ->first();

        if($activation) {
            //Get client
            $date = date('Y-m-d');
            $activation_check_in = DB::table('activation_check_ins')
                ->where('activation_id', $activation_id)
                ->where('user_id', $user_id)
                ->whereRaw('DATE(created_at) = ?', [$date])
                ->first();
            if ($activation_check_in) {

                $success = 2;

            } else {

                $check_in_object = new ActivationCheckIn();
                $check_in_created = $check_in_object->create([
                    'user_id' => $user_id,
                    'activation_id' => $activation_id,
                    'admin_id' => $admin_id
                ]);

                if ($check_in_created) {
                    $success = 1;
                } else {
                    $success = 0;
                }
            }

        }
        $json_array = array(
            'success' => $success,
        );

        $response = $json_array;
        return json_encode($response);


    }

    public function teamLeaderActivationLocation(Request $request)
    {
        $activation_name= $request->activation_name;
        $team_leader_id = $request->user_id;

        $activation =  DB::table('activations')
            ->where('name', $activation_name)
            ->first();

        $locations = array();

        if($activation){
            $locations =  DB::table('locations')
                ->where('activation_id', $activation->id)
                ->where('team_leader_id', $team_leader_id)
                ->get();

        }

        return json_encode(array("result" => $locations));
    }


    public function teamLeaderActivationProduct(Request $request)
    {
        $activation_name= $request->activation_name;
        $activation =  DB::table('activations')
            ->where('name', $activation_name)
            ->first();

        $products = array();

        if($activation){
            $products =  DB::table('products')
                ->where('activation_id', $activation->id)
                ->get();

        }

        return json_encode(array("result" => $products));
    }

    public function teamLeaderActivationUserLocation(Request $request)
    {
        $activation_id= $request->activation_id;
        $team_leader_id = $request->user_id;
        $location_id = $request->location_id;

        $result = array();

            $activation_user_locations =  DB::table('activation_user_locations')
                ->where('activation_id', $activation_id)
                ->where('location_id', $location_id)
                ->get();

            foreach ($activation_user_locations as $activation_user_location) {

                $location =  DB::table('locations')
                    ->where('id', $location_id)
                    ->where('team_leader_id', $team_leader_id)
                    ->first();

                if($location){

                    $user = DB::table('users')
                        ->where('id', $activation_user_location->user_id)
                        ->first();

                    if ($user) {
                        array_push($result,
                            array(
                                'id' => $user->id,
                                'activation_id' => $activation_id,
                                'name' => $user->name,
                                'phone_number' => $user->phone_number,
                            ));
                    }

                }



            }


        return json_encode(array("result" => $result));
    }


    public function teamLeaderActivationUsers(Request $request)
    {
        $activation_id= $request->activation_id;
        $team_leader_id = $request->user_id;

        $result = array();

        $activation_users =  DB::table('activation_users')
            ->where('activation_id', $activation_id)
            ->where('admin_id', $team_leader_id)
            ->get();


        foreach ($activation_users as $activation_user) {


                $user = DB::table('users')
                    ->where('id', $activation_user->user_id)
                    ->first();

                if ($user) {
                    array_push($result,
                        array(
                            'id' => $user->id,
                            'activation_id' => $activation_id,
                            'name' => $user->name,
                            'phone_number' => $user->phone_number,
                        ));
                }

        }


        return json_encode(array("result" => $result));
    }

    public function teamLeaderActivationUsersUpdate(Request $request){
        $user_id = $request->user_id;
        $activation_id = $request->activation_id;
        $admin_id = $request->admin_id;
        $location_id = $request->location_id;

        $success = 0;

        $activation =  DB::table('activations')
            ->where('id', $activation_id)
            ->first();

        if($activation) {
            $activation_user_location = DB::table('activation_user_locations')
                ->where('activation_id', $activation_id)
                ->where('user_id', $user_id)
                ->where('location_id', $location_id)
                ->first();

            if ($activation_user_location) {

                $success = 2;

            } else {

                $user_location_object = new ActivationUserLocation();
                $user_location_created = $user_location_object->create([
                    'user_id' => $user_id,
                    'activation_id' => $activation_id,
                    'location_id' => $location_id
                ]);

                if ($user_location_created) {
                    $success = 1;
                } else {
                    $success = 0;
                }
            }

        }
        $json_array = array(
            'success' => $success,
        );

        $response = $json_array;
        return json_encode($response);
    }


    public function activationPendingExpenses(Request $request){
        $activation_name= $request->activation_name;
        $team_leader_id = $request->user_id;

        $activation =  DB::table('activations')
            ->where('name', $activation_name)
            ->first();

        $result = array();

        if($activation){

            $expenses =  DB::table('expenses')
                ->where('activation_id', $activation->id)
                ->where('expense_type', '1')
                ->where('admin_id', $team_leader_id)
                ->where('requisition_status', "0")
                ->orderBy('expenses_id', "DESC")
                ->get();


            foreach ($expenses as $expense) {

                $amount = DB::table('expenses_items')
                    ->where('expenses_id', $expense->id)
                    ->sum('item_amount');

                array_push($result,
                    array(
                        'id' => $expense->id,
                        'activation_id' => $expense->id,
                        'expense_type' => $expense->expense_type,
                        'activation_name' => $activation->name,
                        'admin_id' => $expense->admin_id,
                        'requisition_title' => $expense->requisition_title,
                        'amount' => $amount,
                        'required_date' => $expense->required_date,
                        'requisition_status' => $expense->requisition_status,
                        'reconciliation_status' => $expense->reconciliation_status,
                        'submitted_status' => $expense->submitted_status,
                        'created_at' => $expense->created_at,
                        'updated_at' => $expense->updated_at,
                    ));

            }
        }

        return json_encode(array("result" => $result));
    }

    public function activationProcessedExpenses(Request $request){
        $activation_name= $request->activation_name;
        $team_leader_id = $request->user_id;

        $activation =  DB::table('activations')
            ->where('name', $activation_name)
            ->first();

        $result = array();

        if($activation){

            $expenses =  DB::table('expenses')
                ->where('activation_id', $activation->id)
                ->where('expense_type', '1')
                ->where('admin_id', $team_leader_id)
                ->where('requisition_status', "1")
                ->orderBy('expenses_id', "DESC")
                ->get();

            foreach ($expenses as $expense) {

                $amount = DB::table('expenses_items')
                    ->where('expenses_id', $expense->id)
                    ->sum('item_amount');

                array_push($result,
                    array(
                        'id' => $expense->id,
                        'activation_id' => $expense->id,
                        'expense_type' => $expense->expense_type,
                        'activation_name' => $activation->name,
                        'admin_id' => $expense->admin_id,
                        'requisition_title' => $expense->requisition_title,
                        'amount' => $amount,
                        'required_date' => $expense->required_date,
                        'requisition_status' => $expense->requisition_status,
                        'reconciliation_status' => $expense->reconciliation_status,
                        'submitted_status' => $expense->submitted_status,
                        'created_at' => $expense->created_at,
                        'updated_at' => $expense->updated_at,
                    ));

            }
        }

        return json_encode(array("result" => $result));
    }

    public function activationExpensesItems(Request $request){

        $expenses_id = $request->expenses_id;
        $expenses_items =  DB::table('expenses_items')
            ->where('expenses_id', $expenses_id)
            ->get();

        return json_encode(array("result" => $expenses_items));

    }

    public function activationExpensesItemCreate(Request $request){

        $expenses_item = new ExpensesItem();
        $expenses_item_created = $expenses_item->create([
            'expenses_id'=>$request->expenses_id,
            'item_supplier'=>$request->item_supplier,
            'item_name'=>$request->item_name,
            'item_description'=>$request->item_description,
            'item_unit_cost'=>$request->item_unit_cost,
            'item_quantity'=>$request->item_quantity,
            'item_days'=>$request->item_days,
            'item_amount'=>$request->item_amount,
        ]);

        if($expenses_item_created){

            $json_array = array(
                'success' => 1,
            );

            $response = $json_array;
            return json_encode($response);
        }else{
            /**
             * User has failed to register
             */
            $json_array = array(
                'success' => 0,
            );

            $response = $json_array;
            return json_encode($response);
        }


    }

    public function activationExpensesItemEdit(Request $request){

        $expenses_item = DB::table('expenses_items')
            ->where('id', $request->id)
            ->update([
                'item_supplier'=>$request->item_supplier,
                'item_name'=>$request->item_name,
                'item_description'=>$request->item_description,
                'item_unit_cost'=>$request->item_unit_cost,
                'item_quantity'=>$request->item_quantity,
                'item_days'=>$request->item_days,
                'item_amount'=>$request->item_amount,
            ]);

        if($expenses_item){

            $json_array = array(
                'success' => 1,
            );

            $response = $json_array;
            return json_encode($response);
        }else{
            /**
             * User has failed to register
             */
            $json_array = array(
                'success' => 0,
            );

            $response = $json_array;
            return json_encode($response);
        }


    }

    public function activationExpensesItemDelete(Request $request){

        $expenses_item = DB::table('expenses_items')
            ->where('id', $request->id)
            ->delete();

        if($expenses_item){

            $json_array = array(
                'success' => 1,
            );

            $response = $json_array;
            return json_encode($response);
        }else{
            /**
             * User has failed to register
             */
            $json_array = array(
                'success' => 0,
            );

            $response = $json_array;
            return json_encode($response);
        }


    }

    public function activationExpensesCreate(Request $request){

        $activation_name = $request->activation_name;
        $activation =  DB::table('activations')
            ->where('name', $activation_name)
            ->first();

        if($activation){

            $expense = new Expenses();
            $expense_created = $expense->create([
                'activation_id'=>$activation->id,
                'expense_type'=>$request->expense_type,
                'admin_id'=>$request->admin_id,
                'requisition_title'=>$request->requisition_title,
                'required_date'=>$request->required_date,
            ]);

            if($expense_created){

                $json_array = array(
                    'success' => 1,
                );

                $response = $json_array;
                return json_encode($response);
            }else{
                /**
                 * User has failed to register
                 */
                $json_array = array(
                    'success' => 0,
                );

                $response = $json_array;
                return json_encode($response);
            }

        }else{
            $json_array = array(
                'success' => 0,
            );

            $response = $json_array;
            return json_encode($response);
        }

    }


    public function activationExpensesSubmitted(Request $request){

        $expenses =  DB::table('expenses')
            ->where('id', $request->id)
            ->first();

        if($expenses){

            $update= DB::table('expenses')
                ->where('id', $request->id)
                ->update([
                    'submitted_status'=>'1',
                ]);

            if($update){

                $admin = DB::table('admins')
                    ->where('id', $expenses->admin_id)
                    ->first();

                $name = "";
                if($admin){
                    $name = $admin->name;
                }

                $activation = DB::table('activations')
                    ->where('id', $expenses->activation_id)
                    ->first();

                $activation_name = "";
                if($activation){
                    $activation_name = $activation->name;
                }


                $date = $expenses->required_date;
                $amount = DB::table('expenses_items')
                    ->where('expenses_id', $expenses->id)
                    ->sum('item_amount');


                $message    = "ACTIVATION EXPENSES \nName $name\nActivation $activation_name\nAmount $amount \nDate required $date";
                $this->sendSMSMultiple($message);

                $json_array = array(
                    'success' => 1,
                );

                $response = $json_array;
                return json_encode($response);

            }else{

                $json_array = array(
                    'success' => 0,
                );

                $response = $json_array;
                return json_encode($response);
            }


        }else{

            $json_array = array(
                'success' => 0,
            );

            $response = $json_array;
            return json_encode($response);
        }



    }

    public function generalPendingExpenses(Request $request){
        $team_leader_id = $request->user_id;
        $result = array();
        $expenses =  DB::table('expenses')
            ->where('expense_type', '0')
            ->where('admin_id', $team_leader_id)
            ->where('requisition_status', "0")
            ->orderBy('expenses_id', "DESC")
            ->get();


        foreach ($expenses as $expense) {

            $amount = DB::table('expenses_items')
                ->where('expenses_id', $expense->id)
                ->sum('item_amount');

            array_push($result,
                array(
                    'id' => $expense->id,
                    'activation_id' => $expense->id,
                    'expense_type' => $expense->expense_type,
                    'activation_name' => "",
                    'admin_id' => $expense->admin_id,
                    'requisition_title' => $expense->requisition_title,
                    'amount' => $amount,
                    'required_date' => $expense->required_date,
                    'requisition_status' => $expense->requisition_status,
                    'reconciliation_status' => $expense->reconciliation_status,
                    'submitted_status' => $expense->submitted_status,
                    'created_at' => $expense->created_at,
                    'updated_at' => $expense->updated_at,
                ));

        }

        return json_encode(array("result" => $result));
    }

    public function generalProcessedExpenses(Request $request){
        $team_leader_id = $request->user_id;
        $result = array();
        $expenses =  DB::table('expenses')
            ->where('expense_type', '0')
            ->where('admin_id', $team_leader_id)
            ->where('requisition_status', "1")
            ->orderBy('expenses_id', "DESC")
            ->get();


        foreach ($expenses as $expense) {

            $amount = DB::table('expenses_items')
                ->where('expenses_id', $expense->id)
                ->sum('item_amount');

            array_push($result,
                array(
                    'id' => $expense->id,
                    'activation_id' => $expense->id,
                    'expense_type' => $expense->expense_type,
                    'activation_name' => "",
                    'admin_id' => $expense->admin_id,
                    'requisition_title' => $expense->requisition_title,
                    'amount' => $amount,
                    'required_date' => $expense->required_date,
                    'requisition_status' => $expense->requisition_status,
                    'reconciliation_status' => $expense->reconciliation_status,
                    'submitted_status' => $expense->submitted_status,
                    'created_at' => $expense->created_at,
                    'updated_at' => $expense->updated_at,
                ));

        }

        return json_encode(array("result" => $result));
    }

    public function generalExpensesItems(Request $request){

        $expenses_id = $request->expenses_id;
        $expenses_items =  DB::table('expenses_items')
            ->where('expenses_id', $expenses_id)
            ->get();

        return json_encode(array("result" => $expenses_items));

    }

    public function generalExpensesItemCreate(Request $request){

        $expenses_item = new ExpensesItem();
        $expenses_item_created = $expenses_item->create([
            'expenses_id'=>$request->expenses_id,
            'item_supplier'=>$request->item_supplier,
            'item_name'=>$request->item_name,
            'item_description'=>$request->item_description,
            'item_unit_cost'=>$request->item_unit_cost,
            'item_quantity'=>$request->item_quantity,
            'item_days'=>$request->item_days,
            'item_amount'=>$request->item_amount,
        ]);

        if($expenses_item_created){

            $json_array = array(
                'success' => 1,
            );

            $response = $json_array;
            return json_encode($response);
        }else{
            /**
             * User has failed to register
             */
            $json_array = array(
                'success' => 0,
            );

            $response = $json_array;
            return json_encode($response);
        }


    }

    public function generalExpensesItemEdit(Request $request){

        $expenses_item = DB::table('expenses_items')
            ->where('id', $request->id)
            ->update([
                'item_supplier'=>$request->item_supplier,
                'item_name'=>$request->item_name,
                'item_description'=>$request->item_description,
                'item_unit_cost'=>$request->item_unit_cost,
                'item_quantity'=>$request->item_quantity,
                'item_days'=>$request->item_days,
                'item_amount'=>$request->item_amount,
            ]);

        if($expenses_item){

            $json_array = array(
                'success' => 1,
            );

            $response = $json_array;
            return json_encode($response);
        }else{
            /**
             * User has failed to register
             */
            $json_array = array(
                'success' => 0,
            );

            $response = $json_array;
            return json_encode($response);
        }


    }

    public function generalExpensesItemDelete(Request $request){

        $expenses_item = DB::table('expenses_items')
            ->where('id', $request->id)
            ->delete();

        if($expenses_item){

            $json_array = array(
                'success' => 1,
            );

            $response = $json_array;
            return json_encode($response);
        }else{
            /**
             * User has failed to register
             */
            $json_array = array(
                'success' => 0,
            );

            $response = $json_array;
            return json_encode($response);
        }


    }

    public function generalExpensesCreate(Request $request){

            $expense = new Expenses();
            $expense_created = $expense->create([
                'expense_type'=>$request->expense_type,
                'admin_id'=>$request->admin_id,
                'requisition_title'=>$request->requisition_title,
                'required_date'=>$request->required_date,
            ]);

            if($expense_created){

                $json_array = array(
                    'success' => 1,
                );

                $response = $json_array;
                return json_encode($response);
            }else{
                /**
                 * User has failed to register
                 */
                $json_array = array(
                    'success' => 0,
                );

                $response = $json_array;
                return json_encode($response);
            }

    }

    public function generalExpensesSubmitted(Request $request){

        $expenses =  DB::table('expenses')
            ->where('id', $request->id)
            ->first();

        if($expenses){

            $update= DB::table('expenses')
                ->where('id', $request->id)
                ->update([
                    'submitted_status'=>'1',
                ]);

            if($update){

                $admin = DB::table('admins')
                    ->where('id', $expenses->admin_id)
                    ->first();

                $name = "";
                if($admin){
                    $name = $admin->name;
                }
                $date = $expenses->required_date;
                $amount = DB::table('expenses_items')
                    ->where('expenses_id', $expenses->id)
                    ->sum('item_amount');


                $message    = "GENERAL EXPENSES \nName $name\nAmount $amount \nDate required $date";
                $this->sendSMSMultiple($message);

                $json_array = array(
                    'success' => 1,
                );

                $response = $json_array;
                return json_encode($response);

            }else{

                $json_array = array(
                    'success' => 0,
                );

                $response = $json_array;
                return json_encode($response);
            }


        }else{

            $json_array = array(
                'success' => 0,
            );

            $response = $json_array;
            return json_encode($response);
        }


    }


    public function createReconciliationItem(Request $request){

        $item_attachment = "";
        if ($request->hasFile('image')) {
            $this->validate($request, [
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:4048',
            ]);

            $image = $request->file('image');
            $file_name = 'reconcile'.time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/images/reconciliation/');
            $image->move($destinationPath, $file_name);

            $item_attachment = 'http://192.81.215.21/api/image-get-reconciliation/'.$file_name;
        }

        $reconciliation_object = new ExpenseReconciliationItem();
        $reconciliation_created = $reconciliation_object->create([
            'expenses_id'=>$request->expenses_id,
            'item_expenses_id'=>$request->id,
            'item_supplier'=>$request->item_supplier,
            'item_name'=>$request->item_name,
            'item_description'=>$request->item_description,
            'item_unit_cost'=>$request->item_unit_cost,
            'item_quantity'=>$request->item_quantity,
            'item_days'=>$request->item_days,
            'item_amount'=>$request->item_amount,
            'item_attachment'=>$item_attachment,
        ]);
        if($reconciliation_created){

            $update = DB::table('expenses_items')
                ->where('id', $request->id)
                ->update([
                    'reconciliation_status'=>'1',
                ]);

            $json_array = array(
                'success' => '1',
            );
            $response = $json_array;
            return json_encode($response);

        }else{
            $json_array = array(
                'success' => '0',
            );
            $response = $json_array;
            return json_encode($response);
        }

    }

    public function expenseGiveAmount(Request $request){

        $amount_object = new ExpenseGiveAmount();
        $amount_created = $amount_object->create([
            'expense_id'=>$request->expense_id,
            'amount'=>$request->amount,
        ]);
        if($amount_created){

            $json_array = array(
                'success' => '1',
            );
            $response = $json_array;
            return json_encode($response);

        }else{
            $json_array = array(
                'success' => '0',
            );
            $response = $json_array;
            return json_encode($response);
        }

    }

    public function activationReportConfirm(Request $request){

        $activation_report_check =  DB::table('activation_reports')
            ->where('activation_id', $request->activation_id)
            ->where('user_id', $request->user_id)
            ->where('date', $request->date)
            ->first();
        if($activation_report_check){

            $json_array = array(
                'success' => 2,
            );

            $response = $json_array;
            return json_encode($response);

        }else{

            $activation_report = new ActivationReport();
            $activation_report_created = $activation_report->create([
                'activation_id'=>$request->activation_id,
                'user_id'=>$request->user_id,
                'admin_id'=>$request->admin_id,
                'date'=>$request->date,
            ]);

            if($activation_report_created){

                $json_array = array(
                    'success' => 1,
                );

                $response = $json_array;
                return json_encode($response);

            }else{
                /**
                 * User has failed to register
                 */
                $json_array = array(
                    'success' => 0,
                );

                $response = $json_array;
                return json_encode($response);
            }
        }
    }

    public function activationCheckReportConfirm(Request $request){

        $activation_report_check =  DB::table('activation_reports')
            ->where('activation_id', $request->activation_id)
            ->where('user_id', $request->user_id)
            ->where('date', $request->date)
            ->first();
        if($activation_report_check){

            $json_array = array(
                'success' => 1,
            );

            $response = $json_array;
            return json_encode($response);

        }else{

            $json_array = array(
                'success' => 0,
            );

            $response = $json_array;
            return json_encode($response);

        }
    }

    public function sendSMS($phone_number, $message){

        $username   = "alfonesltd";
        $apikey     = "d9d5e35bd248edee61c3b8e00f384b27b45f2c9064c449b62400197636ebdb6c";
        $from = "ALFONES-COM";
        $recipients = "+$phone_number";

        $gateway = new AfricasTalkingGateway($username, $apikey);
        try
        {
            // Thats it, hit send and we'll take care of the rest.
            $results = $gateway->sendMessage($recipients, $message, $from);

            foreach($results as $result) {

                $status = $result->status;
                if($status=='Success'){

                }
                if($status==0){

                }
            }
        }
        catch ( AfricasTalkingGatewayException $e )
        {

        }

    }

    public function sendSMSMultiple($message){

        $username   = "alfonesltd";
        $apikey     = "d9d5e35bd248edee61c3b8e00f384b27b45f2c9064c449b62400197636ebdb6c";
        $from = "ALFONES-COM";
        $recipients = "+254722899646, +254721697302, +254722681396, +254713622058, +254728454300";

        $gateway = new AfricasTalkingGateway($username, $apikey);
        try
        {
            // Thats it, hit send and we'll take care of the rest.
            $results = $gateway->sendMessage($recipients, $message, $from);

            foreach($results as $result) {

                $status = $result->status;
                if($status=='Success'){

                }
                if($status==0){

                }
            }
        }
        catch ( AfricasTalkingGatewayException $e )
        {

        }

    }

    public function submitOutletSales(Request $request){

        $activation =  DB::table('activations')
            ->where('name', $request->activation_name)
            ->first();
        if($activation){

            $activation_id = $activation->id;

        }else{

            $json_array = array(
                'success' => 3,
            );

            $response = $json_array;
            return json_encode($response);
        }

        $location =  DB::table('locations')
            ->where('name', $request->location_name)
            ->first();
        if($location){

            $location_id = $location->id;

        }else{

            $json_array = array(
                'success' => 3,
            );

            $response = $json_array;
            return json_encode($response);
        }

        $product =  DB::table('products')
            ->where('name', $request->product_name)
            ->first();
        if($product){

            $product_id = $product->id;

        }else{

            $json_array = array(
                'success' => 3,
            );

            $response = $json_array;
            return json_encode($response);
        }



        $sales_item_check =  DB::table('activation_outlet_product_summaries')
            ->where('activation_id', $activation_id)
            ->where('location_id', $location_id)
            ->where('product_id', $product_id)
            ->where('date', $request->date)
            ->first();

        if($sales_item_check){

            $json_array = array(
                'success' => 2,
            );
            $response = $json_array;
            return json_encode($response);

        }else{

            $sales_item = new ActivationOutletProductSummary();
            $sales_item_created = $sales_item->create([
                'activation_id'=>$activation_id,
                'location_id'=>$location_id,
                'product_id'=>$product_id,
                'product_price'=>$request->product_price,
                'base_volume'=>$request->base_volume,
                'quantity'=>$request->quantity,
                'admin_id'=>$request->admin_id,
                'date'=>$request->date,
            ]);

            if($sales_item_created){

                $json_array = array(
                    'success' => 1,
                );

                $response = $json_array;
                return json_encode($response);
            }else{
                /**
                 * User has failed to register
                 */
                $json_array = array(
                    'success' => 0,
                );

                $response = $json_array;
                return json_encode($response);
            }
        }


    }

    public function submitOutletStock(Request $request){

        $activation =  DB::table('activations')
            ->where('name', $request->activation_name)
            ->first();
        if($activation){

            $activation_id = $activation->id;

        }else{

            $json_array = array(
                'success' => 3,
            );

            $response = $json_array;
            return json_encode($response);
        }

        $location =  DB::table('locations')
            ->where('name', $request->location_name)
            ->first();
        if($location){

            $location_id = $location->id;

        }else{

            $json_array = array(
                'success' => 3,
            );

            $response = $json_array;
            return json_encode($response);
        }

        $product =  DB::table('products')
            ->where('name', $request->product_name)
            ->first();
        if($product){

            $product_id = $product->id;

        }else{

            $json_array = array(
                'success' => 3,
            );

            $response = $json_array;
            return json_encode($response);
        }

        $stock_item_check =  DB::table('activation_product_stocks')
            ->where('activation_id', $activation_id)
            ->where('location_id', $location_id)
            ->where('product_id', $product_id)
            ->where('date', $request->date)
            ->first();

        if($stock_item_check){

            $json_array = array(
                'success' => 2,
            );
            $response = $json_array;
            return json_encode($response);

        }else{

            $stock_item = new ActivationProductStock();
            $stock_item_created = $stock_item->create([
                'activation_id'=>$activation_id,
                'location_id'=>$location_id,
                'product_id'=>$product_id,
                'opening_stock'=>$request->opening_stock,
                'closing_stock'=>$request->closing_stock,
                'admin_id'=>$request->admin_id,
                'date'=>$request->date,
            ]);

            if($stock_item_created){

                $json_array = array(
                    'success' => 1,
                );

                $response = $json_array;
                return json_encode($response);
            }else{
                /**
                 * User has failed to register
                 */
                $json_array = array(
                    'success' => 0,
                );

                $response = $json_array;
                return json_encode($response);
            }

        }


    }

    /**
     *  WEB FUNCTIONS
     */
    public function adminCreate(Request $request){
        //Get request

        //Check if email exists
        $email = $request->email;
        $email_check =  DB::table('admins')->where('email', $email)->first();
        if($email_check){
            /**
             * admin has failed to register
             */
            $json_array = array(
                'success' => 2,
            );

            $response = $json_array;
            return json_encode($response);
        }

        //Check if phone exists
        $phone_number = $request->phone_number;
        $phone_number_check =  DB::table('admins')->where('phone_number', $phone_number)->first();
        if($phone_number_check){
            /**
             * admin has failed to register
             */
            $json_array = array(
                'success' => 3,
            );

            $response = $json_array;
            return json_encode($response);
        }

        $admin_object = new Admin();
        $admin_created = $admin_object->create([
            'name'=>$request->name,
            'phone_number'=>$request->phone_number,
            'email'=>$request->email,
            'role'=>$request->role,
            'permanent_status'=>$request->permanent_status,
            'national_id'=>$request->national_id,
            'password'=> bcrypt($request->password),
            'is_admin'=> false,
        ]);

        if($admin_created){


            $permissions = array();
            if($request->role == 1){
                // Super admin permissions

                $role = "Super admin";

                $permissions = [
                    'ba-list',
                    'ba-details',
                    'ba-create',
                    'ba-edit',
                    'ba-delete',
                    'admin-list',
                    'admin-details',
                    'admin-create',
                    'admin-edit',
                    'admin-delete',
                    'admin-permission',
                    'client-list',
                    'client-details',
                    'client-create',
                    'client-edit',
                    'client-delete',
                    'division-list',
                    'division-details',
                    'division-create',
                    'division-edit',
                    'division-delete',
                    'region-list',
                    'region-details',
                    'region-create',
                    'region-edit',
                    'region-delete',
                    'activation-list',
                    'activation-details',
                    'activation-create',
                    'activation-edit',
                    'activation-delete',
                    'activation-product-list',
                    'activation-product-details',
                    'activation-product-create',
                    'activation-product-edit',
                    'activation-product-delete',
                    'activation-merchandise-list',
                    'activation-merchandise-details',
                    'activation-merchandise-create',
                    'activation-merchandise-edit',
                    'activation-merchandise-delete',
                    'activation-location-list',
                    'activation-location-details',
                    'activation-location-create',
                    'activation-location-edit',
                    'activation-location-delete',
                    'activation-team-leader-list',
                    'activation-team-leader-details',
                    'activation-team-leader-create',
                    'activation-team-leader-edit',
                    'activation-team-leader-delete',
                    'activation-user-list',
                    'activation-user-details',
                    'activation-user-create',
                    'activation-user-edit',
                    'activation-user-delete',
                    'activation-target-list',
                    'activation-target-details',
                    'activation-target-create',
                    'activation-target-edit',
                    'activation-target-delete',
                    'activation-reports-view',
                    'activation-reports-pdf',
                    'activation-reports-xls',
                    'activation-payroll-view',
                    'activation-payroll-generate',
                    'expenses-list',
                    'expenses-details',
                    'expenses-approve',
                    'expenses-printout',
                    'sms-notification-status',
                ];

            }elseif ($request->role == 2){
                // Admin permissions

                $role = "Admin";

                $permissions = [
                    'ba-list',
                    'ba-details',
                    'ba-create',
                    'ba-edit',
                    'ba-delete',
                    'admin-list',
                    'admin-details',
                    'admin-create',
                    'admin-edit',
                    'admin-delete',
                    'admin-permission',
                    'client-list',
                    'client-details',
                    'client-create',
                    'client-edit',
                    'client-delete',
                    'division-list',
                    'division-details',
                    'division-create',
                    'division-edit',
                    'division-delete',
                    'region-list',
                    'region-details',
                    'region-create',
                    'region-edit',
                    'region-delete',
                    'activation-list',
                    'activation-details',
                    'activation-create',
                    'activation-edit',
                    'activation-delete',
                    'activation-product-list',
                    'activation-product-details',
                    'activation-product-create',
                    'activation-product-edit',
                    'activation-product-delete',
                    'activation-merchandise-list',
                    'activation-merchandise-details',
                    'activation-merchandise-create',
                    'activation-merchandise-edit',
                    'activation-merchandise-delete',
                    'activation-location-list',
                    'activation-location-details',
                    'activation-location-create',
                    'activation-location-edit',
                    'activation-location-delete',
                    'activation-team-leader-list',
                    'activation-team-leader-details',
                    'activation-team-leader-create',
                    'activation-team-leader-edit',
                    'activation-team-leader-delete',
                    'activation-user-list',
                    'activation-user-details',
                    'activation-user-create',
                    'activation-user-edit',
                    'activation-user-delete',
                    'activation-target-list',
                    'activation-target-details',
                    'activation-target-create',
                    'activation-target-edit',
                    'activation-target-delete',
                    'activation-reports-view',
                    'activation-reports-pdf',
                    'activation-reports-xls',
                    'activation-payroll-view',
                    'activation-payroll-generate',
                    'expenses-list',
                    'expenses-details',
                    'expenses-approve',
                    'expenses-printout',
                    'sms-notification-status',
                ];

            }elseif ($request->role == 3){
                // Project manager

                $role = "Project manager";

                $permissions = [
                    'ba-list',
                    'ba-details',
                    'ba-create',
                    'ba-edit',
                    'ba-delete',
                    'admin-list',
                    'client-list',
                    'client-details',
                    'client-create',
                    'client-edit',
                    'client-delete',
                    'activation-list',
                    'activation-details',
                    'activation-create',
                    'activation-edit',
                    'activation-delete',
                    'activation-product-list',
                    'activation-product-details',
                    'activation-product-create',
                    'activation-product-edit',
                    'activation-product-delete',
                    'activation-merchandise-list',
                    'activation-merchandise-details',
                    'activation-merchandise-create',
                    'activation-merchandise-edit',
                    'activation-merchandise-delete',
                    'activation-location-list',
                    'activation-location-details',
                    'activation-location-create',
                    'activation-location-edit',
                    'activation-location-delete',
                    'activation-team-leader-list',
                    'activation-team-leader-details',
                    'activation-team-leader-create',
                    'activation-team-leader-edit',
                    'activation-team-leader-delete',
                    'activation-user-list',
                    'activation-user-details',
                    'activation-user-create',
                    'activation-user-edit',
                    'activation-user-delete',
                    'activation-target-list',
                    'activation-target-details',
                    'activation-target-create',
                    'activation-target-edit',
                    'activation-target-delete',
                    'activation-reports-view',
                    'activation-reports-pdf',
                    'activation-reports-xls',
                    'activation-payroll-view',
                    'activation-payroll-generate',
                ];

            }elseif ($request->role == 4){
                // Team leader
                $role = "Team leader";

                $permissions = [
                    'ba-list',
                    'ba-details',
                    'activation-list',
                    'activation-details',
                    'activation-product-list',
                    'activation-product-details',
                    'activation-merchandise-list',
                    'activation-merchandise-details',
                    'activation-location-list',
                    'activation-location-details',
                    'activation-team-leader-list',
                    'activation-team-leader-details',
                    'activation-target-list',
                    'activation-target-details',
                    'activation-reports-view',
                ];


            }elseif ($request->role == 5){
                // Finance
                $role = "Finance officer";

                $permissions = [
                    'activation-payroll-view',
                    'activation-payroll-generate',
                    'expenses-list',
                    'expenses-details',
                    'expenses-approve',
                    'expenses-printout',
                ];

            }elseif ($request->role == 6){
                // Supervisor
                $role = "Supervisor";

                $permissions = [
                    'ba-list',
                    'ba-details',
                    'ba-create',
                    'ba-edit',
                    'ba-delete',
                    'admin-list',
                    'admin-details',
                    'admin-permission',
                    'product-list',
                    'product-details',
                    'activation-list',
                    'activation-details',
                    'activation-product-list',
                    'activation-product-details',
                    'activation-merchandise-list',
                    'activation-merchandise-details',
                    'activation-location-list',
                    'activation-location-details',
                    'activation-team-leader-list',
                    'activation-team-leader-details',
                    'activation-target-list',
                    'activation-target-details',
                    'activation-reports-view',
                ];


            }elseif($request->role == 7){

                $role = "Operations";

                $permissions = [
                    'ba-list',
                    'ba-details',
                    'ba-create',
                    'ba-edit',
                    'ba-delete',
                    'admin-list',
                    'client-list',
                    'client-details',
                    'client-create',
                    'client-edit',
                    'client-delete',
                    'activation-list',
                    'activation-details',
                    'activation-create',
                    'activation-edit',
                    'activation-delete',
                    'activation-product-list',
                    'activation-product-details',
                    'activation-product-create',
                    'activation-product-edit',
                    'activation-product-delete',
                    'activation-merchandise-list',
                    'activation-merchandise-details',
                    'activation-merchandise-create',
                    'activation-merchandise-edit',
                    'activation-merchandise-delete',
                    'activation-location-list',
                    'activation-location-details',
                    'activation-location-create',
                    'activation-location-edit',
                    'activation-location-delete',
                    'activation-team-leader-list',
                    'activation-team-leader-details',
                    'activation-team-leader-create',
                    'activation-team-leader-edit',
                    'activation-team-leader-delete',
                    'activation-user-list',
                    'activation-user-details',
                    'activation-user-create',
                    'activation-user-edit',
                    'activation-user-delete',
                    'activation-target-list',
                    'activation-target-details',
                    'activation-target-create',
                    'activation-target-edit',
                    'activation-target-delete',
                    'activation-reports-view',
                    'activation-reports-pdf',
                    'activation-reports-xls',
                    'activation-payroll-view',
                    'activation-payroll-generate',
                ];
            }

            $admin_created->syncPermissions($permissions);

            $mail_controller = new MailController();
            $mail_controller->adminWelcomeNote($request->name, $email, $request->password, $role);
            //SEND SMS
            $message = "Welcome to Alfones App\nEmail: $email\nPassword:$request->password";
            $api_controller = new ApiController();
            $api_controller->sendSMS($phone_number, $message);


            $json_array = array(
                'success' => 1,
            );

            $response = $json_array;
            return json_encode($response);
        }else{
            /**
             * admin has failed to register
             */
            $json_array = array(
                'success' => 0,
            );

            $response = $json_array;
            return json_encode($response);
        }

    }

    public function adminDetail(Request $request){

        $admin_id = $request->id;
        $admin =  DB::table('admins')->where('id', $admin_id)->first();
        return response()->json($admin);

    }

    public function adminEdit(Request $request){
        //Get request
        $admin =  DB::table('admins')->where('id', $request->admin_id)->first();
        if($admin){
            /**
             * Update admin
             */
            $update = DB::table('admins')
                ->where('id', $request->admin_id)
                ->update(['name' => $request->name,
                    'phone_number'=>$request->phone_number,
                    'email'=>$request->email,
                    'role'=>$request->role,
                    'permanent_status'=>$request->permanent_status,
                    'national_id'=>$request->national_id]);

            if($update){

                $json_array = array(
                    'success' => 1,
                );

                $response = $json_array;
                return json_encode($response);

            }else{

                $json_array = array(
                    'success' => 0,
                );

                $response = $json_array;
                return json_encode($response);

            }

        }else{

            $json_array = array(
                'success' => 2,
            );

            $response = $json_array;
            return json_encode($response);
        }

    }


    /**
     * Delete admin
     * @param Request $request
     * @return string
     */
    public function adminDelete(Request $request){

        $activity_log = new ActivityLogController();
        $create_log = $activity_log->deleteAdmin($request->admin_id);

        //Get request
        $admin =  DB::table('admins')->where('id', $request->admin_id)->first();
        if($admin){
            /**
             * Update admin
             */
            $conditions = ['id' => $request->admin_id];
            $delete = DB::table('admins')
                ->where($conditions)
                ->delete();

            if($delete){

                $json_array = array(
                    'success' => 1,
                );

                $response = $json_array;
                return json_encode($response);
            }else{

                $json_array = array(
                    'success' => 0,
                );

                $response = $json_array;
                return json_encode($response);
            }

        }else{

            $json_array = array(
                'success' => 2,
            );

            $response = $json_array;
            return json_encode($response);
        }

    }

    public function getProjectManager(){
        $clients = DB::table('admins')
            ->where('role',3)
            ->get();
        return json_encode($clients);

    }

    public function getTeamLeader(){
        $clients = DB::table('admins')
            ->where('role',3)
            ->get();
        return json_encode($clients);

    }

    public function getActivationTeamLeader(Request $request){

        $activation_team_leaders = DB::table('activation_team_leaders')
            ->where('activation_id',$request->id)
            ->get();

        $result = array();

        foreach ($activation_team_leaders as $activation_team_leader){

            $team_leader = DB::table('admins')
                ->where('id',$activation_team_leader->admin_id)
                ->first();
            if($team_leader){
                array_push($result,
                    array(
                        'id' => $team_leader->id,
                        'name' => $team_leader->name,
                        'phone_number' => $team_leader->phone_number,
                        'email' => $team_leader->email,
                        'created_at' => $team_leader->created_at,
                        'updated_at' => $team_leader->updated_at,
                    ));
            }

        }

        return json_encode($result);

    }

    public function createClient(Request $request){
        //Get request

        //Check if email exists
        $email = $request->email;
        $email_check =  DB::table('clients')->where('email', $email)->first();
        if($email_check){
            /**
             * client has failed to register
             */
            $json_array = array(
                'success' => 2,
            );

            $response = $json_array;
            return json_encode($response);
        }

        //Check if phone exists
        $phone_number = $request->phone_number;
        $phone_number_check =  DB::table('clients')->where('phone_number', $phone_number)->first();
        if($phone_number_check){
            /**
             * client has failed to register
             */
            $json_array = array(
                'success' => 3,
            );

            $response = $json_array;
            return json_encode($response);
        }

        $username= str_replace(' ', '.', $request->company_name);
        $username = strtolower($username);

        $client_object = new Client();
        $client_created = $client_object->create([
            'company_name'=>$request->company_name,
            'phone_number'=>$request->phone_number,
            'email'=>$request->email,
            'address'=>$request->address,
            'username'=>$username,
            'password'=> bcrypt($request->password),
        ]);

        if($client_created){

            $mail_controller = new MailController();
            $mail_controller->clientWelcomeNote($request->company_name, $email, $request->password);

            //SEND SMS
            $message = "Welcome to Alfones App\nEmail: $email\nPassword:$request->password";
            $api_controller = new ApiController();
            $api_controller->sendSMS($phone_number, $message);

            $json_array = array(
                'success' => 1,
            );

            $response = $json_array;
            return json_encode($response);
        }else{
            /**
             * client has failed to register
             */
            $json_array = array(
                'success' => 0,
            );

            $response = $json_array;
            return json_encode($response);
        }

    }

    public function clientDetail(Request $request){

        $client_id = $request->id;
        $client =  DB::table('clients')->where('id', $client_id)->first();
        return response()->json($client);


    }

    public function clientEdit(Request $request){
        //Get request

        $client =  DB::table('clients')->where('id', $request->client_id)->first();
        if($client){
            /**
             * Update client
             */
            $update = DB::table('clients')
                ->where('id', $request->client_id)
                ->update(['company_name' => $request->company_name,
                    'phone_number'=>$request->phone_number,
                    'email'=>$request->email,
                    'address'=>$request->address]);

            if($update){

                $json_array = array(
                    'success' => 1,
                );

                $response = $json_array;
                return json_encode($response);

            }else{

                $json_array = array(
                    'success' => 0,
                );

                $response = $json_array;
                return json_encode($response);

            }

        }else{

            $json_array = array(
                'success' => 2,
            );

            $response = $json_array;
            return json_encode($response);
        }

    }


    /**
     * Delete client
     * @param Request $request
     * @return string
     */
    public function clientDelete(Request $request){

        //Get request
        $client =  DB::table('clients')->where('id', $request->client_id)->first();
        if($client){
            /**
             * Update client
             */
            $conditions = ['id' => $request->client_id];
            $delete = DB::table('clients')
                ->where($conditions)
                ->delete();

            if($delete){

                $json_array = array(
                    'success' => 1,
                );

                $response = $json_array;
                return json_encode($response);
            }else{

                $json_array = array(
                    'success' => 0,
                );

                $response = $json_array;
                return json_encode($response);
            }

        }else{

            $json_array = array(
                'success' => 2,
            );

            $response = $json_array;
            return json_encode($response);
        }

    }

    public function getClient(){
        $clients = DB::table('clients')->get();
        return json_encode($clients);

    }

    public function createUser(Request $request){
        //Get request

        //Check if email exists
        $email = $request->email;
        $email_check =  DB::table('users')->where('email', $email)->first();
        if($email_check){
            /**
             * user has failed to register
             */
            $json_array = array(
                'success' => 2,
            );

            $response = $json_array;
            return json_encode($response);
        }

        //Check if phone exists
        $phone_number = $request->phone_number;
        $phone_number_check =  DB::table('users')->where('phone_number', $phone_number)->first();
        if($phone_number_check){
            /**
             * user has failed to register
             */
            $json_array = array(
                'success' => 3,
            );

            $response = $json_array;
            return json_encode($response);
        }

        $user_object = new User();
        $user_created = $user_object->create([
            'name'=>$request->name,
            'date_of_birth'=>$request->date_of_birth,
            'gender'=>$request->gender,
            'phone_number'=>$request->phone_number,
            'national_id'=>$request->national_id,
            'is_verified'=>$request->is_verified,
            'email'=>$request->email,
            'password'=> bcrypt($request->password),
        ]);

        if($user_created){

            //SEND EMAIL
            $mail_controller = new MailController();
            $mail_controller->userWelcomeNote($request->name, $email, $request->password);

            //SEND SMS
            $message = "Welcome to Alfones App\nEmail: $email\nPassword:$request->password";
            $this->sendSMS($phone_number, $message);

            $json_array = array(
                'success' => 1,
            );

            $response = $json_array;
            return json_encode($response);

        }else{
            /**
             * user has failed to register
             */
            $json_array = array(
                'success' => 0,
            );

            $response = $json_array;
            return json_encode($response);
        }

    }

    public function userDetail(Request $request){

        $user_id = $request->id;
        $user =  DB::table('users')->where('id', $user_id)->first();
        return response()->json($user);

    }

    public function userEdit(Request $request){
        //Get request

        $user =  DB::table('users')->where('id', $request->user_id)->first();
        if($user){
            /**
             * Update user
             */
            $update = DB::table('users')
                ->where('id', $request->user_id)
                ->update(['name'=>$request->name,
                    'date_of_birth'=>$request->date_of_birth,
                    'gender'=>$request->gender,
                    'phone_number'=>$request->phone_number,
                    'national_id'=>$request->national_id,
                    'is_verified'=>$request->is_verified,
                    'email'=>$request->email]);

            if($update){

                $json_array = array(
                    'success' => 1,
                );

                $response = $json_array;
                return json_encode($response);

            }else{

                $json_array = array(
                    'success' => 0,
                );

                $response = $json_array;
                return json_encode($response);

            }

        }else{

            $json_array = array(
                'success' => 2,
            );

            $response = $json_array;
            return json_encode($response);
        }

    }


    /**
     * Delete user
     * @param Request $request
     * @return string
     */
    public function userDelete(Request $request){

        //Get request
        $user =  DB::table('users')->where('id', $request->user_id)->first();
        if($user){
            /**
             * Update user
             */
            $conditions = ['id' => $request->user_id];
            $delete = DB::table('users')
                ->where($conditions)
                ->delete();

            if($delete){

                $json_array = array(
                    'success' => 1,
                );

                $response = $json_array;
                return json_encode($response);
            }else{

                $json_array = array(
                    'success' => 0,
                );

                $response = $json_array;
                return json_encode($response);
            }

        }else{

            $json_array = array(
                'success' => 2,
            );

            $response = $json_array;
            return json_encode($response);
        }

    }

    public function createActivation(Request $request)
    {
        $activation_object = new Activation();
        $activation_created = $activation_object->create([
            'name'=>$request->name,
            'description'=>$request->description,
            'project_manager_id'=>$request->project_manager,
            'client_id'=>$request->client,
            'start_date'=>$request->start_date,
            'end_date'=>$request->end_date,
            'ba_unit_cost'=>$request->ba_unit_cost,
            'team_leader_unit_cost'=>$request->team_leader_unit_cost,
            'enable_geo_fence'=>$request->enable_geo_fence,
            'allow_tl_location'=>$request->allow_tl_location,
            'allow_client_portal'=>$request->allow_client_portal,
        ]);


        if($activation_created){
            $json_array = array(
                'success' => 1,
            );

            $response = $json_array;
            return json_encode($response);
        }else{
            /**
             * activation has failed to create
             */
            $json_array = array(
                'success' => 0,
            );

            $response = $json_array;
            return json_encode($response);
        }
    }

    public function activationEdit(Request $request)
    {
        $activation =  DB::table('activations')->where('id', $request->activation_id)->first();
        if($activation){
            /**
             * Update Activation
             */
            $update = DB::table('activations')
                ->where('id', $request->activation_id)
                ->update(['name'=>$request->name,
                    'description'=>$request->description,
                    'project_manager_id'=>$request->project_manager,
                    'client_id'=>$request->client,
                    'start_date'=>$request->start_date,
                    'end_date'=>$request->end_date,
                    'ba_unit_cost'=>$request->ba_unit_cost,
                    'team_leader_unit_cost'=>$request->team_leader_unit_cost,
                    'enable_geo_fence'=>$request->enable_geo_fence,
                    'allow_tl_location'=>$request->allow_tl_location,
                    'allow_client_portal'=>$request->allow_client_portal,
                ]);

            if($update){

                $json_array = array(
                    'success' => 1,
                );

                $response = $json_array;
                return json_encode($response);

            }else{

                $json_array = array(
                    'success' => 0,
                );

                $response = $json_array;
                return json_encode($response);

            }

        }else{

            $json_array = array(
                'success' => 2,
            );

            $response = $json_array;
            return json_encode($response);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function activationDelete(Request $request)
    {
        $activation =  DB::table('activations')->where('id', $request->activation_id)->first();
        if($activation){
            /**
             * Update client
             */
            $conditions = ['id' => $request->activation_id];
            $delete = DB::table('activations')
                ->where($conditions)
                ->delete();

            if($delete){

                $json_array = array(
                    'success' => 1,
                );

                $response = $json_array;
                return json_encode($response);
            }else{

                $json_array = array(
                    'success' => 0,
                );

                $response = $json_array;
                return json_encode($response);
            }

        }else{

            $json_array = array(
                'success' => 2,
            );

            $response = $json_array;
            return json_encode($response);
        }

    }

    public function activationList()
    {
        $activations =  DB::table('activations')
            ->orderBy('created_at', 'DESC')
            ->get();

        return json_encode($activations);
    }

    public function activationDetail(Request $request)
    {
        $activation =  DB::table('activations')->where('id', $request->id)->first();
        return json_encode($activation);
    }

    public function activationProduct(Request $request)
    {
        $products =  DB::table('products')->where('activation_id', $request->id)->get();
        return json_encode($products);
    }

    public function activationMerchandise(Request $request)
    {
        $merchandises =  DB::table('merchandises')->where('activation_id', $request->id)->get();
        return json_encode($merchandises);
    }

    public function activationLocation(Request $request){

        $locations =  DB::table('locations')->where('activation_id', $request->id)->get();

        $result = array();

        foreach ($locations as $location){

            $team_leader = DB::table('admins')
                ->where('id',$location->team_leader_id)
                ->first();

            $team_leader_name = "";
            if($team_leader){
                $team_leader_name =$team_leader->name;
            }

            array_push($result,
                array(
                    'id' => $location->id,
                    'activation_id' => $location->activation_id,
                    'name' => $location->name,
                    'location_identifier' => $location->location_identifier,
                    'team_leader' => $team_leader_name,
                    'date' => $location->date,
                    'duration' => $location->duration,
                    'created_at' => $location->created_at,
                    'updated_at' => $location->updated_at,
                ));

        }

        return json_encode($result);

    }

    public function activationTeamLeader(Request $request)
    {
        $activation_team_leaders =  DB::table('activation_team_leaders')->where('activation_id', $request->id)->get();

        $result = array();

        foreach ($activation_team_leaders as $activation_team_leader){

            $team_leader = DB::table('admins')
                ->where('id',$activation_team_leader->admin_id)
                ->first();

            if($team_leader){
                array_push($result,
                    array(
                        'id' => $team_leader->id,
                        'name' => $team_leader->name,
                        'phone_number' => $team_leader->phone_number,
                        'email' => $team_leader->email,
                        'created_at' => $team_leader->created_at,
                        'updated_at' => $team_leader->updated_at,
                    ));
            }



        }

        return json_encode($result);
    }

    public function activationTeamLeaderCreate(Request $request)
    {

        $activation_id = $request->id;
        $team_leaders = DB::table('admins')
            ->where('role',4)
            ->get();

        $result = array();

        foreach ($team_leaders as $team_leader){

            $team_leader_check = DB::table('activation_team_leaders')
                ->where('admin_id', $team_leader->id)
                ->where('activation_id', $activation_id)
                ->first();

            if(!$team_leader_check){
                array_push($result,
                    array(
                        'id' => $team_leader->id,
                        'name' => $team_leader->name,
                        'phone_number' => $team_leader->phone_number,
                        'email' => $team_leader->email,
                        'created_at' => $team_leader->created_at,
                        'updated_at' => $team_leader->updated_at,
                    ));
            }


        }

        return json_encode($result);
    }

    public function activationTarget(Request $request)
    {
        $activation_targets =  DB::table('activation_targets')->where('activation_id', $request->id)->get();

        $result = array();

        foreach ($activation_targets as $activation_target){

            $location_name = "";
            $location = DB::table('locations')
                ->where('id',$activation_target->location_id)
                ->first();
            if($location){
                $location_name = $location->name;
            }

            array_push($result,
                array(
                    'id' => $activation_target->id,
                    'location_name' => $location_name,
                    'date' => $activation_target->date,
                    'interaction_target' => $activation_target->interaction_target,
                    'sales_target' => $activation_target->sales_target,
                    'created_at' => $activation_target->created_at,
                    'updated_at' => $activation_target->updated_at,
                ));


        }

        return json_encode($result);
    }

    public function activationExpenses(Request $request){

        $activation_expenses = DB::table('expenses')
            ->where('activation_id', $request->activation_id)
            ->where('submitted_status', '1')
            ->orderBy('expenses_id', 'DESC')
            ->get();

        $result_activation = array();

        foreach ($activation_expenses as $activation_expense){

            $admin_name = "";
            $admin = DB::table('admins')
                ->where('id',$activation_expense->admin_id)
                ->first();
            if($admin){
                $admin_name = $admin->name;
            }

            // Get requisition amount
            $activation_expense_sum = DB::table('expenses_items')
                ->where('expenses_id', $activation_expense->id)
                ->sum('item_amount');


            array_push($result_activation,
                array(
                    'id' => $activation_expense->id,
                    'expenses_id' => $activation_expense->expenses_id,
                    'admin_name' => $admin_name,
                    'requisition_title' => $activation_expense->requisition_title,
                    'required_date' => $activation_expense->required_date,
                    'requisition_status' => $activation_expense->requisition_status,
                    'reconciliation_status' => $activation_expense->reconciliation_status,
                    'submitted_status' => $activation_expense->submitted_status,
                    'amount' => $activation_expense_sum,
                    'created_at' => $activation_expense->created_at,
                    'updated_at' => $activation_expense->updated_at,
                ));


        }
        return json_encode($result_activation);

    }

    public function activationUsers(Request $request)
    {
        $activation_id= $request->id;

        $result = array();

        $activation_users =  DB::table('activation_users')
            ->where('activation_id', $activation_id)
            ->get();


        foreach ($activation_users as $activation_user) {


            $user = DB::table('users')
                ->where('id', $activation_user->user_id)
                ->first();

            if ($user) {
                array_push($result,
                    array(
                        'id' => $user->id,
                        'activation_id' => $activation_id,
                        'name' => $user->name,
                        'phone_number' => $user->phone_number,
                        'email' => $user->email,
                    ));
            }

        }


        return json_encode($result);
    }


    public function createTeamLeader(Request $request)
    {

        $team_leader_object = new ActivationTeamLeader();
        $activation_created = $team_leader_object->create([
            'activation_id'=>$request->activation_id,
            'admin_id'=>$request->admin_id
        ]);



        if($team_leader_object){
            $json_array = array(
                'success' => 1,
            );

            $response = $json_array;
            return json_encode($response);
        }else{
            /**
             * activation has failed to create
             */
            $json_array = array(
                'success' => 0,
            );

            $response = $json_array;
            return json_encode($response);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function teamLeaderDetails(Request $request)
    {
        $team_leader_id = $request->id;
        $team_leader =  DB::table('admins')->where('id', $team_leader_id)->first();
        return response()->json($team_leader);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function teamLeaderEdit(Request $request)
    {
        $admins =  DB::table('admins')->where('id', $request->admin_id)->first();
        if($admins){
            /**
             * Update Activation
             */
            $update = DB::table('admins')
                ->where('id', $request->admin_id)
                ->update(['activation_id'=>$request->activation_id,
                    'admin_id'=>$request->admin_id]);

            if($update){

                $json_array = array(
                    'success' => 1,
                );

                $response = $json_array;
                return json_encode($response);

            }else{

                $json_array = array(
                    'success' => 0,
                );

                $response = $json_array;
                return json_encode($response);

            }

        }else{

            $json_array = array(
                'success' => 2,
            );

            $response = $json_array;
            return json_encode($response);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function teamLeaderDelete(Request $request)
    {
        $team_leader =  DB::table('admins')->where('id', $request->admin_id)->first();
        if($team_leader){
            /**
             * Update client
             */
            $conditions = ['id' => $request->admin_id];
            $delete = DB::table('admins')
                ->where($conditions)
                ->delete();

            if($delete){

                $json_array = array(
                    'success' => 1,
                );

                $response = $json_array;
                return json_encode($response);
            }else{

                $json_array = array(
                    'success' => 0,
                );

                $response = $json_array;
                return json_encode($response);
            }

        }else{

            $json_array = array(
                'success' => 2,
            );

            $response = $json_array;
            return json_encode($response);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createProduct(Request $request)
    {
        $product_object = new Product();
        $product_created = $product_object->create([
            'name'=>$request->name,
            'activation_id'=>$request->activation_id,
            'description'=>$request->description,
            'quantity'=>$request->quantity
        ]);


        if($product_created){
            $json_array = array(
                'success' => 1,
            );

            $response = $json_array;
            return json_encode($response);
        }else{
            /**
             * activation has failed to create
             */
            $json_array = array(
                'success' => 0,
            );

            $response = $json_array;
            return json_encode($response);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function productDetail(Request $request)
    {
        $product_id = $request->id;
        $product =  DB::table('products')->where('id', $product_id)->first();
        return response()->json($product);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function productEdit(Request $request)
    {
        $products =  DB::table('products')->where('id', $request->product_id)->first();
        if($products){
            /**
             * Update Activation
             */
            $update = DB::table('products')
                ->where('id', $request->product_id)
                ->update(['name'=>$request->name,
                    'description'=>$request->description,
                    'quantity'=>$request->quantity]);

            if($update){

                $json_array = array(
                    'success' => 1,
                );

                $response = $json_array;
                return json_encode($response);

            }else{

                $json_array = array(
                    'success' => 0,
                );

                $response = $json_array;
                return json_encode($response);

            }

        }else{

            $json_array = array(
                'success' => 2,
            );

            $response = $json_array;
            return json_encode($response);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function productDelete(Request $request)
    {
        $product =  DB::table('products')->where('id', $request->product_id)->first();
        if($product){
            /**
             * Update client
             */
            $conditions = ['id' => $request->product_id];
            $delete = DB::table('products')
                ->where($conditions)
                ->delete();

            if($delete){

                $json_array = array(
                    'success' => 1,
                );

                $response = $json_array;
                return json_encode($response);
            }else{

                $json_array = array(
                    'success' => 0,
                );

                $response = $json_array;
                return json_encode($response);
            }

        }else{

            $json_array = array(
                'success' => 2,
            );

            $response = $json_array;
            return json_encode($response);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function createMerchandise(Request $request)
    {
        $merchandise_object = new Merchandise();
        $merchandise_created = $merchandise_object->create([
            'name'=>$request->name,
            'activation_id'=>$request->activation_id,
            'description'=>$request->description,
            'quantity'=>$request->quantity,
        ]);

        if($merchandise_created){
            $json_array = array(
                'success' => 1,
            );


            $response = $json_array;
            return json_encode($response);
        }else{
            /**
             * client has failed to register
             */
            $json_array = array(
                'success' => 0,
            );

            $response = $json_array;
            return json_encode($response);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function merchandiseDetail(Request $request)
    {
        $merchandise_id = $request->id;
        $merchandise =  DB::table('merchandises')->where('id', $merchandise_id)->first();
        return response()->json($merchandise);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function merchandiseEdit(Request $request)
    {
        $merchandise =  DB::table('merchandises')->where('id', $request->merchandise_id)->first();
        if($merchandise){
            /**
             * Update merchandise
             */
            $update = DB::table('merchandises')
                ->where('id', $request->merchandise_id)
                ->update(['name'=>$request->name,
                    'description'=>$request->description,
                    'quantity'=>$request->quantity]);

            if($update){

                $json_array = array(
                    'success' => 1,
                );

                $response = $json_array;
                return json_encode($response);

            }else{

                $json_array = array(
                    'success' => 0,
                );

                $response = $json_array;
                return json_encode($response);

            }

        }else{

            $json_array = array(
                'success' => 2,
            );

            $response = $json_array;
            return json_encode($response);
        }
    }

    public function merchandiseDelete(Request $request)
    {

        //Get request
        $merchandise =  DB::table('merchandises')->where('id', $request->merchandise_id)->first();
        if($merchandise){
            /**
             * Update client
             */
            $conditions = ['id' => $request->merchandise_id];
            $delete = DB::table('merchandises')
                ->where($conditions)
                ->delete();

            if($delete){

                $json_array = array(
                    'success' => 1,
                );

                $response = $json_array;
                return json_encode($response);
            }else{

                $json_array = array(
                    'success' => 0,
                );

                $response = $json_array;
                return json_encode($response);
            }

        }else{

            $json_array = array(
                'success' => 2,
            );

            $response = $json_array;
            return json_encode($response);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createLocation(Request $request)
    {
        $location_object = new Location();
        $location_created = $location_object->create([
            'activation_id'=>$request->activation_id,
            'name'=>$request->name,
            'location_identifier'=>$request->location_identifier,
            'description'=>$request->description,
            'team_leader_id'=>$request->team_leader_id,
            'date'=>$request->date,
            'division_id'=>$request->division_id,
            'region_id'=>$request->region_id,
            'duration'=>$request->duration,
            'latitude'=>$request->latitude,
            'longitude'=>$request->longitude,
            'enable_geo_fence'=>$request->enable_geo_fence,
            'location_comments'=>$request->location_comments,
        ]);

        if($location_created){
            $json_array = array(
                'success' => 1,
            );


            $response = $json_array;
            return json_encode($response);
        }else{
            /**
             * client has failed to register
             */
            $json_array = array(
                'success' => 0,
            );

            $response = $json_array;
            return json_encode($response);
        }

    }

    public function createLocationMobile(Request $request)
    {

        $activation_name = $request->activation;
        $activation =  DB::table('activations')
            ->where('name', $activation_name)
            ->first();
        if($activation){
            $activation_id = $activation->id;
        }

        $division_name = $request->division;
        $division =  DB::table('divisions')
            ->where('name', $division_name)
            ->first();
        if($division){
            $division_id = $division->id;
        }

        $region_name = $request->region;
        $region =  DB::table('regions')
            ->where('name', $region_name)
            ->first();
        if($region){
            $region_id = $region->id;
        }

        $location_object = new Location();
        $location_created = $location_object->create([
            'activation_id'=>$activation_id,
            'name'=>$request->name,
            'location_identifier'=>$request->location_identifier,
            'description'=>$request->description,
            'team_leader_id'=>$request->team_leader_id,
            'date'=>$request->date,
            'division_id'=>$division_id,
            'region_id'=>$region_id,
            'duration'=>$request->duration,
            'latitude'=>$request->latitude,
            'longitude'=>$request->longitude,
            'location_comments'=>$request->location_comments,
        ]);

        if($location_created){
            $json_array = array(
                'success' => 1,
            );


            $response = $json_array;
            return json_encode($response);
        }else{
            /**
             * client has failed to register
             */
            $json_array = array(
                'success' => 0,
            );

            $response = $json_array;
            return json_encode($response);
        }

    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function locationDetail(Request $request)
    {
        $location_id = $request->id;
        $location =  DB::table('locations')->where('id', $location_id)->first();
        return response()->json($location);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function locationEdit(Request $request)
    {
        $location =  DB::table('locations')->where('id', $request->location_id)->first();
        if($location){
            /**
             * Update location
             */
            $update = DB::table('locations')
                ->where('id', $request->location_id)
                ->update([
                    'name'=>$request->name,
                    'description'=>$request->description,
                    'team_leader_id'=>$request->team_leader_id,
                    'location_identifier'=>$request->location_identifier,
                    'date'=>$request->date,
                    'division_id'=>$request->division_id,
                    'region_id'=>$request->region_id,
                    'duration'=>$request->duration,
                    'latitude'=>$request->latitude,
                    'longitude'=>$request->longitude,
                    'enable_geo_fence'=>$request->enable_geo_fence,
                    'location_comments'=>$request->location_comments,
                ]);

            if($update){

                $json_array = array(
                    'success' => 1,
                );

                $response = $json_array;
                return json_encode($response);

            }else{

                $json_array = array(
                    'success' => 0,
                );

                $response = $json_array;
                return json_encode($response);

            }

        }else{

            $json_array = array(
                'success' => 2,
            );

            $response = $json_array;
            return json_encode($response);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function locationDelete(Request $request)
    {
        //        $activity_log = new ActivityLogController();
        //        $create_log = $activity_log->deleteClient($request->client_id);

        //Get request
        $location =  DB::table('locations')->where('id', $request->location_id)->first();
        if($location){
            /**
             * Update client
             */
            $conditions = ['id' => $request->location_id];
            $delete = DB::table('locations')
                ->where($conditions)
                ->delete();

            if($delete){

                $json_array = array(
                    'success' => 1,
                );

                $response = $json_array;
                return json_encode($response);
            }else{

                $json_array = array(
                    'success' => 0,
                );

                $response = $json_array;
                return json_encode($response);
            }

        }else{

            $json_array = array(
                'success' => 2,
            );

            $response = $json_array;
            return json_encode($response);
        }
    }

    public function createTarget(Request $request)
    {

        $activation_target =  DB::table('activation_targets')
            ->where([
                'location_id'=> $request->location_id,
                'date'=> $request->date,])
            ->first();

        if($activation_target){

            $json_array = array(
                'success' => 2,
            );

            $response = $json_array;
            return json_encode($response);

        }else{
            $target_object = new ActivationTarget();
            $target_created = $target_object->create([
                'activation_id'=>$request->activation_id,
                'location_id'=>$request->location_id,
                'date'=>$request->date,
                'interaction_target'=>$request->interaction_target,
                'sales_target'=>$request->sales_target,
            ]);


            if($target_created){
                $json_array = array(
                    'success' => 1,
                );

                $response = $json_array;
                return json_encode($response);
            }else{
                /**
                 * target has failed to create
                 */
                $json_array = array(
                    'success' => 0,
                );

                $response = $json_array;
                return json_encode($response);
            }
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function targetDetails(Request $request)
    {
        $target_id = $request->id;
        $target =  DB::table('activation_targets')->where('id', $target_id)->first();
        return response()->json($target);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function targetEdit(Request $request)
    {
        $activation_targets =  DB::table('activation_targets')->where('id', $request->activation_target_id)->first();
        if($activation_targets){
            /**
             * Update Activation
             */
            $update = DB::table('activation_targets')
                ->where('id', $request->activation_target_id)
                ->update([
                    'interaction_target'=>$request->interaction_target,
                    'sales_target'=>$request->sales_target]);

            if($update){

                $json_array = array(
                    'success' => 1,
                );

                $response = $json_array;
                return json_encode($response);

            }else{

                $json_array = array(
                    'success' => 0,
                );

                $response = $json_array;
                return json_encode($response);

            }

        }else{

            $json_array = array(
                'success' => 2,
            );

            $response = $json_array;
            return json_encode($response);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function targetDelete(Request $request)
    {
        $target =  DB::table('activation_targets')->where('id', $request->activation_target_id)->first();
        if($target){
            /**
             * Update client
             */
            $conditions = ['id' => $request->activation_target_id];
            $delete = DB::table('activation_targets')
                ->where($conditions)
                ->delete();

            if($delete){

                $json_array = array(
                    'success' => 1,
                );

                $response = $json_array;
                return json_encode($response);
            }else{

                $json_array = array(
                    'success' => 0,
                );

                $response = $json_array;
                return json_encode($response);
            }

        }else{

            $json_array = array(
                'success' => 2,
            );

            $response = $json_array;
            return json_encode($response);
        }
    }

    public function interactionGraph(Request $request){
        // Get date of seven days ago
        $beginning_date = Carbon::now()->subWeek()->toDateString();

        // Get todays date
        $todays_date = Carbon::today()->toDateString();

        $array_labels = array();
        $array_interactions = array();
        while (strtotime($beginning_date) <= strtotime($todays_date)) {

            $interaction_today_count = DB::table('reports')
                ->where('activation_id', $request->id)
                ->whereRaw('DATE(created_at) = ?', [$beginning_date])
                ->count();

            array_push($array_labels, $beginning_date);
            array_push($array_interactions, $interaction_today_count);

            $beginning_date = date ("Y-m-d", strtotime("+1 day", strtotime($beginning_date)));
        }


        $json_array = array(

            'labels' => $array_labels,
            'interactions' => $array_interactions,
        );
        $response = $json_array;
        return json_encode($response);
    }

    public function salesGraph(Request $request){
        // Get date of seven days ago
        $beginning_date = Carbon::now()->subWeek()->toDateString();

        // Get todays date
        $todays_date = Carbon::today()->toDateString();

        $array_labels = array();
        $array_sales = array();
        while (strtotime($beginning_date) <= strtotime($todays_date)) {

            $sale_today_count = DB::table('reports')
                ->where('activation_id', $request->id)
                ->where('product', 1)
                ->whereRaw('DATE(created_at) = ?', [$beginning_date])
                ->sum('product_quantity');

            array_push($array_labels, $beginning_date);
            array_push($array_sales, $sale_today_count);

            $beginning_date = date ("Y-m-d", strtotime("+1 day", strtotime($beginning_date)));
        }


        $json_array = array(

            'labels' => $array_labels,
            'sales' => $array_sales,
        );
        $response = $json_array;
        return json_encode($response);
    }

    public function productSalesGraph(Request $request){
        $array_labels = array();
        $array_sales = array();

        // Get sales per product
        $product_sales = array();
        $activation_products = DB::table('products')
            ->where('activation_id',$request->id)
            ->get();

        foreach ($activation_products as $activation_product) {

            $sales_count = DB::table('reports')
                ->where('activation_id', $request->id)
                ->where('product', 1)
                ->where('product_id', $activation_product->id)
                ->sum('product_quantity');

            array_push($array_labels, $activation_product->name);
            array_push($array_sales, $sales_count);

        }

        $json_array = array(

            'labels' => $array_labels,
            'sales' => $array_sales,
        );
        $response = $json_array;
        return json_encode($response);
    }

    public function merchandiseGraph(Request $request){
        // Get date of seven days ago
        $beginning_date = Carbon::now()->subWeek()->toDateString();

        // Get todays date
        $todays_date = Carbon::today()->toDateString();

        $array_labels = array();
        $array_merchandise = array();
        while (strtotime($beginning_date) <= strtotime($todays_date)) {

            $merchandise_today_count = DB::table('reports')
                ->where('activation_id', $request->id)
                ->where('merchandise', 1)
                ->whereRaw('DATE(created_at) = ?', [$beginning_date])
                ->sum('merchandise_quantity');

            array_push($array_labels, $beginning_date);
            array_push($array_merchandise, $merchandise_today_count);

            $beginning_date = date ("Y-m-d", strtotime("+1 day", strtotime($beginning_date)));
        }


        $json_array = array(

            'labels' => $array_labels,
            'merchandise' => $array_merchandise,
        );
        $response = $json_array;
        return json_encode($response);
    }

    public function merchandiseDistributionGraph(Request $request){
        $array_labels = array();
        $array_distributions = array();

        // Get distributions per merchandise
        $merchandise_distributions = array();
        $activation_merchandises = DB::table('merchandises')
            ->where('activation_id',$request->id)
            ->get();

        foreach ($activation_merchandises as $activation_merchandise) {

            $distributions_count = DB::table('reports')
                ->where('activation_id', $request->id)
                ->where('merchandise', 1)
                ->where('merchandise_id', $activation_merchandise->id)
                ->sum('merchandise_quantity');

            array_push($array_labels, $activation_merchandise->name);
            array_push($array_distributions, $distributions_count);

        }

        $json_array = array(

            'labels' => $array_labels,
            'distributions' => $array_distributions,
        );
        $response = $json_array;
        return json_encode($response);
    }


    public function getActivationExpenses(Request $request){
        $activation =  DB::table('activations')
            ->where('id', $request->activation_id)
            ->first();

        $result = array();

        if($activation){

            $expenses =  DB::table('expenses')
                ->where('activation_id', $activation->id)
                ->where('expense_type', '1')
                ->orderBy('expenses_id', "DESC")
                ->get();

            foreach ($expenses as $expense) {

                $amount = DB::table('expenses_items')
                    ->where('expenses_id', $expense->id)
                    ->sum('item_amount');

                $admin_name = "";
                $admin = DB::table('admins')
                    ->where('id', $expense->admin_id)
                    ->first();
                if($admin){
                    $admin_name = $admin->name;
                }


                array_push($result,
                    array(
                        'id' => $expense->id,
                        'activation_id' => $expense->id,
                        'expense_type' => $expense->expense_type,
                        'activation_name' => $activation->name,
                        'expenses_id' => $expense->expenses_id,
                        'admin_id' => $expense->admin_id,
                        'admin_name' => $admin_name,
                        'requisition_title' => $expense->requisition_title,
                        'amount' => $amount,
                        'required_date' => $expense->required_date,
                        'requisition_status' => $expense->requisition_status,
                        'reconciliation_status' => $expense->reconciliation_status,
                        'submitted_status' => $expense->submitted_status,
                        'created_at' => $expense->created_at,
                        'updated_at' => $expense->updated_at,
                    ));

            }
        }

        return json_encode($result);
    }

    public function divisionList()
    {
        $divisions =  DB::table('divisions')->get();

        $result = array();

        foreach ($divisions as $division){

            array_push($result,
                array(
                    'id' => $division->id,
                    'name' => $division->name,
                    'created_at' => $division->created_at,
                    'updated_at' => $division->updated_at,
                ));


        }

        return json_encode($result);
    }

    public function divisionListMobile()
    {
        $divisions =  DB::table('divisions')->get();

        $result = array();

        foreach ($divisions as $division){

            array_push($result,
                array(
                    'id' => $division->id,
                    'name' => $division->name,
                    'created_at' => $division->created_at,
                    'updated_at' => $division->updated_at,
                ));


        }

        return json_encode(array("result" => $result));
    }

    public function regionListMobile(Request $request)
    {
        $division =  DB::table('divisions')
            ->where('name', $request->division)
            ->first();

        $result = array();

        if($division){

            $regions =  DB::table('regions')
                ->where('division_id', $division->id)
                ->get();
            foreach ($regions as $region){

                array_push($result,
                    array(
                        'id' => $region->id,
                        'name' => $region->name,
                        'created_at' => $region->created_at,
                        'updated_at' => $region->updated_at,
                    ));

            }

        }

        return json_encode(array("result" => $result));
    }

    public function createDivision(Request $request)
    {

        $division =  DB::table('divisions')
            ->where([
                'name'=> $request->name,])
            ->first();

        if($division){

            $json_array = array(
                'success' => 2,
            );

            $response = $json_array;
            return json_encode($response);

        }else{
            $type_object = new Division();
            $type_created = $type_object->create([
                'name'=>$request->name,
            ]);


            if($type_created){
                $json_array = array(
                    'success' => 1,
                );

                $response = $json_array;
                return json_encode($response);
            }else{
                /**
                 * type has failed to create
                 */
                $json_array = array(
                    'success' => 0,
                );

                $response = $json_array;
                return json_encode($response);
            }
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function divisionDetails(Request $request)
    {
        $type_id = $request->id;
        $type =  DB::table('divisions')->where('id', $type_id)->first();
        return response()->json($type);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function divisionEdit(Request $request)
    {
        $divisions =  DB::table('divisions')->where('id', $request->division_id)->first();
        if($divisions){
            /**
             * Update Activation
             */
            $update = DB::table('divisions')
                ->where('id', $request->division_id)
                ->update([
                    'name'=>$request->name]);

            if($update){

                $json_array = array(
                    'success' => 1,
                );

                $response = $json_array;
                return json_encode($response);

            }else{

                $json_array = array(
                    'success' => 0,
                );

                $response = $json_array;
                return json_encode($response);

            }

        }else{

            $json_array = array(
                'success' => 2,
            );

            $response = $json_array;
            return json_encode($response);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function divisionDelete(Request $request)
    {
        $type =  DB::table('divisions')->where('id', $request->division_id)->first();
        if($type){
            /**
             * Update client
             */
            $conditions = ['id' => $request->division_id];
            $delete = DB::table('divisions')
                ->where($conditions)
                ->delete();

            if($delete){

                $json_array = array(
                    'success' => 1,
                );

                $response = $json_array;
                return json_encode($response);
            }else{

                $json_array = array(
                    'success' => 0,
                );

                $response = $json_array;
                return json_encode($response);
            }

        }else{

            $json_array = array(
                'success' => 2,
            );

            $response = $json_array;
            return json_encode($response);
        }
    }

    public function regionList(Request $request)
    {
        $regions =  DB::table('regions')
            ->where('division_id', $request->id)
            ->get();

        $result = array();

        foreach ($regions as $region){

            array_push($result,
                array(
                    'id' => $region->id,
                    'name' => $region->name,
                    'division_id' => $region->division_id,
                    'created_at' => $region->created_at,
                    'updated_at' => $region->updated_at,
                ));


        }

        return json_encode($result);
    }

    public function createRegion(Request $request)
    {

        $region =  DB::table('regions')
            ->where([
                'name'=> $request->name,])
            ->first();

        if($region){

            $json_array = array(
                'success' => 2,
            );

            $response = $json_array;
            return json_encode($response);

        }else{
            $type_object = new Region();
            $type_created = $type_object->create([
                'name'=>$request->name,
                'division_id'=>$request->division_id,
            ]);


            if($type_created){
                $json_array = array(
                    'success' => 1,
                );

                $response = $json_array;
                return json_encode($response);
            }else{
                /**
                 * type has failed to create
                 */
                $json_array = array(
                    'success' => 0,
                );

                $response = $json_array;
                return json_encode($response);
            }
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function regionDetails(Request $request)
    {
        $type_id = $request->id;
        $type =  DB::table('regions')->where('division_id', $type_id)->first();
        return response()->json($type);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function regionEdit(Request $request)
    {
        $regions =  DB::table('regions')->where('id', $request->region_id)->first();
        if($regions){
            /**
             * Update Activation
             */
            $update = DB::table('regions')
                ->where('id', $request->region_id)
                ->update([
                    'name'=>$request->name]);

            if($update){

                $json_array = array(
                    'success' => 1,
                );

                $response = $json_array;
                return json_encode($response);

            }else{

                $json_array = array(
                    'success' => 0,
                );

                $response = $json_array;
                return json_encode($response);

            }

        }else{

            $json_array = array(
                'success' => 2,
            );

            $response = $json_array;
            return json_encode($response);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function regionDelete(Request $request)
    {
        $type =  DB::table('regions')->where('id', $request->region_id)->first();
        if($type){
            /**
             * Update client
             */
            $conditions = ['id' => $request->region_id];
            $delete = DB::table('regions')
                ->where($conditions)
                ->delete();

            if($delete){

                $json_array = array(
                    'success' => 1,
                );

                $response = $json_array;
                return json_encode($response);
            }else{

                $json_array = array(
                    'success' => 0,
                );

                $response = $json_array;
                return json_encode($response);
            }

        }else{

            $json_array = array(
                'success' => 2,
            );

            $response = $json_array;
            return json_encode($response);
        }
    }

    public function getActivationLocationStock(Request $request)
    {
        $activation_product_stocks =  DB::table('activation_product_stocks')
            ->where('date', $request->date)
            ->where('location_id', $request->location_id)
            ->get();

        $result = array();

        foreach ($activation_product_stocks as $activation_product_stock){

            $product_name = "";
            $product = DB::table('products')
                ->where('id', $activation_product_stock->product_id)
                ->first();
            if($product){
                $product_name = $product->name;
            }


            $admin_name = "";
            $admin = DB::table('admins')
                ->where('id', $activation_product_stock->admin_id)
                ->first();
            if($admin){
                $admin_name = $admin->name;
            }

            array_push($result,
                array(
                    'id' => $activation_product_stock->id,
                    'opening_stock' => $activation_product_stock->opening_stock,
                    'closing_stock' => $activation_product_stock->closing_stock,
                    'admin_name' => $admin_name,
                    'product_name' => $product_name,
                    'created_at' => $activation_product_stock->created_at,
                    'updated_at' => $activation_product_stock->updated_at,
                ));


        }

        return json_encode($result);
    }


    public function getActivationLocationStockDetails(Request $request)
    {
        $activation_product_stock =  DB::table('activation_product_stocks')
            ->where('id', $request->id)
            ->first();

        $id = "";
        $opening_stock = "";
        $closing_stock = "";
        $admin_name = "";
        $product_name = "";
        $location_name = "";

        if($activation_product_stock){

            $product_name = "";
            $product = DB::table('products')
                ->where('id', $activation_product_stock->product_id)
                ->first();
            if($product){
                $product_name = $product->name;
            }

            $location_name = "";
            $location = DB::table('locations')
                ->where('id', $activation_product_stock->location_id)
                ->first();
            if($location){
                $location_name = $location->name;
            }


            $admin_name = "";
            $admin = DB::table('admins')
                ->where('id', $activation_product_stock->admin_id)
                ->first();
            if($admin){
                $admin_name = $admin->name;
            }

            $opening_stock = $activation_product_stock->opening_stock;
            $closing_stock = $activation_product_stock->closing_stock;
            $id = $activation_product_stock->id;



        }

        $json_array = array(
            'id' => $id,
            'opening_stock' => $opening_stock,
            'closing_stock' => $closing_stock,
            'admin_name' => $admin_name,
            'product_name' => $product_name,
            'location_name' => $location_name,
            'created_at' => $activation_product_stock->created_at,
            'updated_at' => $activation_product_stock->updated_at,
        );

        $response = $json_array;
        return json_encode($response);

    }

    public function getActivationLocationStockEdit(Request $request){

        $activation_product_stocks =  DB::table('activation_product_stocks')->where('id', $request->product_stock_id)->first();
        if($activation_product_stocks){
            /**
             * Update Activation Stock
             */
            $update = DB::table('activation_product_stocks')
                ->where('id', $request->product_stock_id)
                ->update([
                    'opening_stock'=>$request->opening_stock,
                    'closing_stock'=>$request->closing_stock
                ]);

            if($update){

                $json_array = array(
                    'success' => 1,
                );

                $response = $json_array;
                return json_encode($response);

            }else{

                $json_array = array(
                    'success' => 0,
                );

                $response = $json_array;
                return json_encode($response);

            }

        }else{

            $json_array = array(
                'success' => 2,
            );

            $response = $json_array;
            return json_encode($response);
        }
    }

    public function getActivationLocationStockDelete(Request $request)
    {
        $activation_product_stock =  DB::table('activation_product_stocks')->where('id', $request->product_stock_id)->first();
        if($activation_product_stock){
            /**
             * Update client
             */
            $conditions = ['id' => $request->product_stock_id];
            $delete = DB::table('activation_product_stocks')
                ->where($conditions)
                ->delete();

            if($delete){

                $json_array = array(
                    'success' => 1,
                );

                $response = $json_array;
                return json_encode($response);
            }else{

                $json_array = array(
                    'success' => 0,
                );

                $response = $json_array;
                return json_encode($response);
            }

        }else{

            $json_array = array(
                'success' => 2,
            );

            $response = $json_array;
            return json_encode($response);
        }
    }


    public function getActivationLocationProductSummary(Request $request)
    {
        $activation_outlet_product_summaries =  DB::table('activation_outlet_product_summaries')
            ->where('location_id', $request->location_id)
            ->where('date', $request->date)
            ->get();

        $result = array();

        foreach ($activation_outlet_product_summaries as $activation_outlet_product_summary){

            $product_name = "";
            $product = DB::table('products')
                ->where('id', $activation_outlet_product_summary->product_id)
                ->first();
            if($product){
                $product_name = $product->name;
            }


            $admin_name = "";
            $admin = DB::table('admins')
                ->where('id', $activation_outlet_product_summary->admin_id)
                ->first();
            if($admin){
                $admin_name = $admin->name;
            }

            array_push($result,
                array(
                    'id' => $activation_outlet_product_summary->id,
                    'base_volume' => $activation_outlet_product_summary->base_volume,
                    'quantity' => $activation_outlet_product_summary->quantity,
                    'admin_name' => $admin_name,
                    'product_name' => $product_name,
                    'created_at' => $activation_outlet_product_summary->created_at,
                    'updated_at' => $activation_outlet_product_summary->updated_at,
                ));


        }

        return json_encode($result);
    }


    public function getActivationLocationProductSummaryDetails(Request $request)
    {
        $activation_outlet_product_summary =  DB::table('activation_outlet_product_summaries')
            ->where('id', $request->id)
            ->first();

        $id = "";
        $base_volume = "";
        $quantity = "";
        $admin_name = "";
        $product_name = "";
        $location_name = "";
        $created_at = "";
        $updated_at = "";

        if($activation_outlet_product_summary){

            $product_name = "";
            $product = DB::table('products')
                ->where('id', $activation_outlet_product_summary->product_id)
                ->first();
            if($product){
                $product_name = $product->name;
            }

            $location_name = "";
            $location = DB::table('locations')
                ->where('id', $activation_outlet_product_summary->location_id)
                ->first();
            if($location){
                $location_name = $location->name;
            }


            $admin_name = "";
            $admin = DB::table('admins')
                ->where('id', $activation_outlet_product_summary->admin_id)
                ->first();
            if($admin){
                $admin_name = $admin->name;
            }

            $base_volume = $activation_outlet_product_summary->base_volume;
            $quantity = $activation_outlet_product_summary->quantity;
            $id = $activation_outlet_product_summary->id;
            $created_at = $activation_outlet_product_summary->created_at;
            $updated_at = $activation_outlet_product_summary->updated_at;

        }

        $json_array = array(
            'id' => $id,
            'base_volume' => $base_volume,
            'quantity' => $quantity,
            'admin_name' => $admin_name,
            'product_name' => $product_name,
            'location_name' => $location_name,
            'created_at' => $created_at ,
            'updated_at' => $updated_at,
        );

        $response = $json_array;
        return json_encode($response);

    }

    public function getActivationLocationProductSummaryEdit(Request $request){

        $activation_outlet_product_summaries =  DB::table('activation_outlet_product_summaries')->where('id', $request->product_summary_id)->first();
        if($activation_outlet_product_summaries){
            /**
             * Update Activation ProductSummary
             */
            $update = DB::table('activation_outlet_product_summaries')
                ->where('id', $request->product_summary_id)
                ->update([
                    'base_volume'=>$request->base_volume,
                    'quantity'=>$request->quantity
                ]);

            if($update){

                $json_array = array(
                    'success' => 1,
                );

                $response = $json_array;
                return json_encode($response);

            }else{

                $json_array = array(
                    'success' => 0,
                );

                $response = $json_array;
                return json_encode($response);

            }

        }else{

            $json_array = array(
                'success' => 2,
            );

            $response = $json_array;
            return json_encode($response);
        }
    }

    public function getActivationLocationProductSummaryDelete(Request $request)
    {
        $activation_outlet_product_summary =  DB::table('activation_outlet_product_summaries')->where('id', $request->product_summary_id)->first();
        if($activation_outlet_product_summary){
            /**
             * Update client
             */
            $conditions = ['id' => $request->product_summary_id];
            $delete = DB::table('activation_outlet_product_summaries')
                ->where($conditions)
                ->delete();

            if($delete){

                $json_array = array(
                    'success' => 1,
                );

                $response = $json_array;
                return json_encode($response);
            }else{

                $json_array = array(
                    'success' => 0,
                );

                $response = $json_array;
                return json_encode($response);
            }

        }else{

            $json_array = array(
                'success' => 2,
            );

            $response = $json_array;
            return json_encode($response);
        }
    }


    /**
     * LEAVE DAYS
     */
    public function createLeave(Request $request)
    {
        $leave_object = new UserLeave();
        $leave_created = $leave_object->create([
            'admin_id'=>$request->user_id,
            'date_from'=>$request->start_date,
            'date_to'=>$request->end_date,
            'reasons'=>$request->reasons,
            'description'=>$request->description,
        ]);


        if($leave_created){

            $admin =  DB::table('admins')->where('id', $request->user_id)->first();
            if($admin) {
                $message = "Leave Alert\nName: $admin->name\nFrom: $request->start_date\nTo: $request->end_date";
                $this->sendSMS("254722869529, 254722899646", $message);
            }

            $json_array = array(
                'success' => 1,
            );

            $response = $json_array;
            return json_encode($response);

        }else{
            /**
             * leave has failed to create
             */
            $json_array = array(
                'success' => 0,
            );

            $response = $json_array;
            return json_encode($response);
        }

    }

    public function getLeave()
    {
        $user_leaves =  DB::table('user_leaves')
            ->orderBy('created_at', 'DESC')
            ->get();

        $result = array();

        foreach ($user_leaves as $user_leave){

            $admin_name = "";
            $admin = DB::table('admins')
                ->where('id', $user_leave->admin_id)
                ->first();
            if($admin){
                $admin_name = $admin->name;
            }

            array_push($result,
                array(
                    'id' => $user_leave->id,
                    'name' => $admin_name,
                    'date_from' => $user_leave->date_from,
                    'date_to' => $user_leave->date_to,
                    'reasons' => $user_leave->reasons,
                    'description' => $user_leave->description,
                    'status' => $user_leave->status,
                    'status_report' => $user_leave->status_report,
                    'admin_id' => $user_leave->admin_id,
                    'attachment' => $user_leave->attachment,
                    'created_at' => $user_leave->created_at,
                    'updated_at' => $user_leave->updated_at,
                ));


        }

        return json_encode(array("result" => $result));
    }


    public function getUserLeave(Request $request)
    {
        $user_leaves =  DB::table('user_leaves')
            ->where('admin_id', $request->id)
            ->orderBy('created_at', 'DESC')
            ->get();

        $result = array();

        foreach ($user_leaves as $user_leave){

            $admin_name = "";
            $admin = DB::table('admins')
                ->where('id', $user_leave->admin_id)
                ->first();
            if($admin){
                $admin_name = $admin->name;
            }

            array_push($result,
                array(
                    'id' => $user_leave->id,
                    'name' => $admin_name,
                    'date_from' => $user_leave->date_from,
                    'date_to' => $user_leave->date_to,
                    'reasons' => $user_leave->reasons,
                    'description' => $user_leave->description,
                    'status' => $user_leave->status,
                    'status_report' => $user_leave->status_report,
                    'admin_id' => $user_leave->admin_id,
                    'attachment' => $user_leave->attachment,
                    'created_at' => $user_leave->created_at,
                    'updated_at' => $user_leave->updated_at,
                ));


        }

        return json_encode(array("result" => $result));

    }

    public function leaveDetail(Request $request)
    {
        $user_leave =  DB::table('user_leaves')
            ->where('id', $request->id)
            ->first();

        if($user_leave){

            $admin_name = "";
            $admin = DB::table('admins')
                ->where('id', $user_leave->admin_id)
                ->first();
            if($admin){
                $admin_name = $admin->name;
            }

            $json_array = array(
                'id' => $user_leave->id,
                'name' => $admin_name,
                'date_from' => $user_leave->date_from,
                'date_to' => $user_leave->date_to,
                'reasons' => $user_leave->reasons,
                'description' => $user_leave->description,
                'status' => $user_leave->status,
                'status_reason' => $user_leave->status_reason,
                'admin_id' => $user_leave->admin_id,
                'attachment' => $user_leave->attachment,
                'created_at' => $user_leave->created_at,
                'updated_at' => $user_leave->updated_at,
            );

            return json_encode(array("result" => $json_array));

        }

    }

    public function userLeaveSummary(Request $request)
    {

        $year = date('Y');
        $days_allowed_query = DB::table('leave_days')
            ->where('year', $year)
            ->first();

        $days_allowed = 0;
        if($days_allowed_query){
            $days_allowed = $days_allowed_query->days;
        }

        $year = date('Y');
        $following_year = $year+1;
        $start_date = $year.'-04-01 00:00:00';
        $end_date = $following_year.'-03-31 00:00:00';

        $days_spent = 1;
        $days_spent_queries = DB::table('user_leaves')
            ->where('admin_id', $request->id)
            ->where('status', 1)
            ->whereBetween('created_at', [$start_date, $end_date])
            ->get();

        foreach ($days_spent_queries as $days_spent_query){

            $date_from = $days_spent_query->date_from;
            $date_to = $days_spent_query->date_to;

            $dateDiff= $this->dateDiff($date_from, $date_to);
            $days_spent = $days_spent + $dateDiff;

        }

        $leave_application = DB::table('user_leaves')
            ->where('admin_id', $request->id)
            ->whereBetween('created_at', [$start_date, $end_date])
            ->count();


        $json_array = array(
            'days_allowed' => $days_allowed,
            'days_spent' => $days_spent,
            'leave_application' => $leave_application,
        );

        $response = $json_array;
        return json_encode($response);


    }


    public function editLeave(Request $request)
    {

        $user_leaves =  DB::table('user_leaves')->where('id', $request->id)->first();
        if($user_leaves){
            $update = DB::table('user_leaves')
                ->where('id', $request->id)
                ->update([
                    'date_from'=>$request->date_from,
                    'date_to'=>$request->date_to,
                    'reasons'=>$request->reasons,
                ]);

            if($update){

                $json_array = array(
                    'success' => 1,
                );

                $response = $json_array;
                return json_encode($response);

            }else{

                $json_array = array(
                    'success' => 0,
                );

                $response = $json_array;
                return json_encode($response);

            }

        }else{

            $json_array = array(
                'success' => 2,
            );

            $response = $json_array;
            return json_encode($response);
        }

    }

    public function editLeaveStatus(Request $request)
    {

        $user_leaves =  DB::table('user_leaves')->where('id', $request->id)->first();
        if($user_leaves){
            $update = DB::table('user_leaves')
                ->where('id', $request->id)
                ->update([
                    'status'=>$request->status,
                    'status_reason'=>$request->status_reason,
                ]);

            if($update){

                $json_array = array(
                    'success' => 1,
                );

                $response = $json_array;
                return json_encode($response);

            }else{

                $json_array = array(
                    'success' => 0,
                );

                $response = $json_array;
                return json_encode($response);

            }

        }else{

            $json_array = array(
                'success' => 2,
            );

            $response = $json_array;
            return json_encode($response);
        }

    }

    public function uploadLeaveAttachment(Request $request){

        $item_attachment = "";
        if ($request->hasFile('image')) {
            $this->validate($request, [
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:4048',
            ]);

            $image = $request->file('image');
            $file_name = 'reconcile'.time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/images/leaves/');
            $image->move($destinationPath, $file_name);

            $item_attachment = 'http://192.81.215.21/api/image-get-leave/'.$file_name;
        }

        $update = DB::table('user_leaves')
            ->where('id', $request->id)
            ->update([
                'attachment'=>$item_attachment,
            ]);

        if($update){

            $json_array = array(
                'success' => '1',
            );
            $response = $json_array;
            return json_encode($response);

        }else{
            $json_array = array(
                'success' => '0',
            );
            $response = $json_array;
            return json_encode($response);
        }

    }

    function dateDiff($date1, $date2)
    {
        $date1_ts = strtotime($date1);
        $date2_ts = strtotime($date2);
        $diff = $date2_ts - $date1_ts;
        return round($diff / 86400);
    }

    public function getUserPayslip(Request $request)
    {
        $payslips =  DB::table('payslips')
            ->where('admin_id', $request->id)
            ->orderBy('created_at', 'DESC')
            ->get();

        $result = array();

        foreach ($payslips as $payslip){

            $admin_name = "";
            $employee_no = "";
            $admin = DB::table('admins')
                ->where('id', $payslip->admin_id)
                ->first();
            if($admin){
                $admin_name = $admin->name;
                $employee_no = $admin->employee_no;
            }

            array_push($result,
                array(
                    'id' => $payslip->id,
                    'payslip_id' => $payslip->payslip_id,
                    'admin_id' => $payslip->admin_id,
                    'name' => $admin_name,
                    'employee_no' => $employee_no,
                    'gross_salary' => $payslip->gross_salary,
                    'nssf_contibution' => $payslip->nssf_contibution,
                    'taxable_pay' => $payslip->taxable_pay,
                    'personal_relief' => $payslip->personal_relief,
                    'insurance_relief' => $payslip->insurance_relief,
                    'paye' => $payslip->paye,
                    'nhif_contribution' => $payslip->nhif_contribution,
                    'net_pay' => $payslip->net_pay,
                    'tax_rate' => $payslip->tax_rate,
                    'year' => $payslip->year,
                    'month' => $payslip->month,
                    'created_at' => $payslip->created_at,
                    'updated_at' => $payslip->updated_at,
                ));


         }

        return json_encode(array("result" => $result));

    }

    public function getPayslips(Request $request)
    {
        $payslips =  DB::table('payslips')
            ->where('year', $request->year)
            ->where('month', $request->month)
            ->orderBy('payslip_id', 'DESC')
            ->get();

        $result = array();

        foreach ($payslips as $payslip){

            $admin_name = "";
            $employee_no = "";
            $admin = DB::table('admins')
                ->where('id', $payslip->admin_id)
                ->first();
            if($admin){
                $admin_name = $admin->name;
                $employee_no = $admin->employee_no;
            }

            array_push($result,
                array(
                    'id' => $payslip->id,
                    'payslip_id' => $payslip->payslip_id,
                    'admin_id' => $payslip->admin_id,
                    'name' => $admin_name,
                    'employee_no' => $employee_no,
                    'gross_salary' => $payslip->gross_salary,
                    'nssf_contibution' => $payslip->nssf_contibution,
                    'taxable_pay' => $payslip->taxable_pay,
                    'personal_relief' => $payslip->personal_relief,
                    'insurance_relief' => $payslip->insurance_relief,
                    'paye' => $payslip->paye,
                    'nhif_contribution' => $payslip->nhif_contribution,
                    'net_pay' => $payslip->net_pay,
                    'tax_rate' => $payslip->tax_rate,
                    'year' => $payslip->year,
                    'month' => $payslip->month,
                    'created_at' => $payslip->created_at,
                    'updated_at' => $payslip->updated_at,
                ));


        }

        return json_encode($result);


    }

    public function createPayslip(Request $request){

        $admin_id = $request->admin_id;
        $year = $request->year;
        $month = $request->month;
        $gross_salary = $request->gross_salary;
        $insurance_relief = 0;

        // NSSF CONTRIBUTION
        $nssf_contibution = 200;

        // PERSONAL RELIEF
        $personal_relief = 1408;

        // TAXABLE PAY
        $taxable_pay = $gross_salary - $nssf_contibution;

        // CALCULATE RATE
        $tax_rate = 0;

        if($taxable_pay < 147580){

            $tax_rate = 10;

        }else if($taxable_pay > 147580 && $taxable_pay < 286623){

            $tax_rate = 15;

        }else if($taxable_pay > 286623 && $taxable_pay < 425666){

            $tax_rate = 20;

        }else if($taxable_pay > 425666 && $taxable_pay < 564709){

            $tax_rate = 25;

        }else if($taxable_pay > 564709){

            $tax_rate = 30;

        }

        // CALCULATE TAX PAYABLE
        $tax_payable = $taxable_pay * ($tax_rate/100);

        // CALCULATE PAYE
        $paye = $tax_payable - $personal_relief;

        // NET PAY
        $net_pay = $taxable_pay - $paye;


        // NHIF CONTRIBUTION
        $nhif_contribution = 0;


        $payslip_check =  DB::table('payslips')
            ->where('year', $request->year)
            ->where('month', $request->month)
            ->where('admin_id', $request->admin_id)
            ->first();

        if($payslip_check){

            $json_array = array(
                'success' => 2,
            );

            $response = $json_array;
            return json_encode($response);

        }else{

            $payslip_object = new Payslip();
            $payslip_created = $payslip_object->create([
                'admin_id'=>$request->admin_id,
                'gross_salary'=>$gross_salary,
                'nssf_contibution'=>$nssf_contibution,
                'taxable_pay'=>$taxable_pay,
                'personal_relief'=>$personal_relief,
                'insurance_relief'=> $insurance_relief,
                'paye'=>$paye,
                'nhif_contribution'=>$nhif_contribution,
                'net_pay'=>$net_pay,
                'tax_rate'=>$tax_rate,
                'month'=>$month,
                'year'=>$year,
            ]);


            if($payslip_created){

                $this->savePayslipPDF($payslip_created->id);

                $json_array = array(
                    'success' => 1,
                );

                $response = $json_array;
                return json_encode($response);

            }else{
                /**
                 * leave has failed to create
                 */
                $json_array = array(
                    'success' => 0,
                );

                $response = $json_array;
                return json_encode($response);
            }

        }

    }

    public function savePayslipPDF($id){

        $payslip =  DB::table('payslips')
            ->where('id', $id)
            ->first();

        if($payslip){
            $admin = DB::table('admins')
                ->where('id', $payslip->admin_id)
                ->first();

            $data = [
                'admin'=>$admin,
                'payslip'=>$payslip,
            ];

            $stripped = str_replace(' ', '', $admin->name);
            $pdf = PDF::loadView('admin.activation.report.pdf.payslip', $data);
            $path = public_path().'/printouts/payslips/'.$stripped.'-'.$payslip->month.'-'.$payslip->year.'.pdf';
            $pdf->save($path);

        }

    }

    public function getPermanentEmployees()
    {
        $employees = DB::table('admins')
            ->where('permanent_status',1)
            ->get();

        $result = array();

        foreach ($employees as $employee){

            array_push($result,
                array(
                    'id' => $employee->id,
                    'name' => $employee->name,
                    'phone_number' => $employee->phone_number,
                    'email' => $employee->email,
                    'created_at' => $employee->created_at,
                    'updated_at' => $employee->updated_at,
                ));
        }

        return json_encode($result);
    }


    public function createPerformance(Request $request)
    {
        $performance_report =  DB::table('performance_reports')
            ->where([
                'admin_id'=> $request->admin_id,
                'start_date'=> $request->start_date,
                'end_date'=> $request->end_date ])
            ->first();

        if($performance_report){

            $json_array = array(
                'success' => 2,
            );

            $response = $json_array;
            return json_encode($response);

        }else{

            // Calculate percentage performance
            $target_achievement = $request->target_achievement;
            $percentage_performance = ($target_achievement/4) * 100;

            // Calculate overall performance
            $overall_attendance = $request->overall_attendance;
            $overall_percentage_performance = ($percentage_performance + $overall_attendance) / 2;

            // Calculate cumulative performance
            $last_cumulative_performance = 0;
            $last_record = DB::table('performance_reports')
                ->where('admin_id', $request->admin_id)
                ->latest()
                ->first();

            if($last_record){
                $last_cumulative_performance = $last_record->cumulative_performance;
            }

            $cumulative_performance = 0;
            if($last_cumulative_performance == 0){
                $cumulative_performance = $overall_percentage_performance;
            }else{
                $cumulative_performance = ($overall_percentage_performance + $last_cumulative_performance) / 2;
            }


            $create_object = new PerformanceReport();
            $report_created = $create_object->create([
                'admin_id'=> $request->admin_id,
                'start_date'=> $request->start_date,
                'end_date'=> $request->end_date,
                'on_time'=> $request->on_time,
                'in_full'=> $request->in_full,
                'no_error'=> $request->no_error,
                'target_achievement'=> $request->target_achievement,
                'overall_performance'=> $request->overall_performance,
                'overall_attendance'=> $request->overall_attendance,
                'performance_percentage'=> $percentage_performance,
                'percentage_overall_performance'=> $overall_percentage_performance,
                'cumulative_performance'=> $cumulative_performance,
            ]);


            if($report_created){
                $json_array = array(
                    'success' => 1,
                );

                $response = $json_array;
                return json_encode($response);
            }else{
                /**
                 * type has failed to create
                 */
                $json_array = array(
                    'success' => 0,
                );

                $response = $json_array;
                return json_encode($response);
            }
        }

    }

    public function performanceDetails(Request $request)
    {
        $performance_id = $request->id;
        $performance_report=  DB::table('performance_reports')->where('id', $performance_id)->first();
        return response()->json($performance_report);
    }

    public function performanceEdit(Request $request)
    {
        $performance_reports =  DB::table('performance_reports')->where('id', $request->id)->first();
        if($performance_reports){
            /**
             * Update Activation
             */

            // Calculate percentage performance
            $target_achievement = $request->target_achievement;
            $percentage_performance = ($target_achievement/4) * 100;

            // Calculate overall performance
            $overall_attendance = $request->overall_attendance;
            $overall_percentage_performance = ($percentage_performance + $overall_attendance) / 2;

            // Calculate cumulative performance
            $last_cumulative_performance = 0;
            $last_record = DB::table('performance_reports')
                ->where('admin_id', $request->admin_id)
                ->latest()
                ->first();

            if($last_record){
                $last_cumulative_performance = $last_record->cumulative_performance;
            }

            $cumulative_performance = 0;
            if($last_cumulative_performance == 0){
                $cumulative_performance = $overall_percentage_performance;
            }else{
                $cumulative_performance = ($overall_percentage_performance + $last_cumulative_performance) / 2;
            }

            $update = DB::table('performance_reports')
                ->where('id', $request->id)
                ->update([
                    'admin_id'=> $request->admin_id,
                    'start_date'=> $request->start_date,
                    'end_date'=> $request->end_date,
                    'on_time'=> $request->on_time,
                    'in_full'=> $request->in_full,
                    'no_error'=> $request->no_error,
                    'target_achievement'=> $request->target_achievement,
                    'overall_performance'=> $request->overall_performance,
                    'overall_attendance'=> $request->overall_attendance,
                    'performance_percentage'=> $percentage_performance,
                    'percentage_overall_performance'=> $overall_percentage_performance,
                    'cumulative_performance'=> $cumulative_performance]);

            if($update){

                $json_array = array(
                    'success' => 1,
                );

                $response = $json_array;
                return json_encode($response);

            }else{

                $json_array = array(
                    'success' => 0,
                );

                $response = $json_array;
                return json_encode($response);

            }

        }else{

            $json_array = array(
                'success' => 2,
            );

            $response = $json_array;
            return json_encode($response);
        }
    }


    public function performanceDelete(Request $request)
    {
        $type =  DB::table('performance_reports')->where('id', $request->id)->first();
        if($type){
            /**
             * Update client
             */
            $conditions = ['id' => $request->id];
            $delete = DB::table('performance_reports')
                ->where($conditions)
                ->delete();

            if($delete){

                $json_array = array(
                    'success' => 1,
                );

                $response = $json_array;
                return json_encode($response);
            }else{

                $json_array = array(
                    'success' => 0,
                );

                $response = $json_array;
                return json_encode($response);
            }

        }else{

            $json_array = array(
                'success' => 2,
            );

            $response = $json_array;
            return json_encode($response);
        }
    }

    public function performanceList()
    {
        $performance_reports =  DB::table('performance_reports')
            ->get();

        $result = array();

        foreach ($performance_reports as $performance_report){

            $name = "";
            $admin = DB::table('admins')
                ->where('id', $performance_report->admin_id)
                ->first();
            if($admin){
                $name = $admin->name;
            }

            array_push($result,
                array(
                    'id' => $performance_report->id,
                    'name' => $name,
                    'start_date' => $performance_report->start_date,
                    'end_date' => $performance_report->end_date,
                    'created_at' => $performance_report->created_at,
                    'updated_at' => $performance_report->updated_at,
                ));

        }

        return json_encode($result);
    }

    public function getLeaveAttachment($fileName){
        $path = public_path().'/images/leaves/'.$fileName;
        return Response::download($path);
    }

    public function getActivationAttachment($fileName){
        $path = public_path().'/images/activations/'.$fileName;
        return Response::download($path);
    }

    public function getReconciliationImage($fileName){
        $path = public_path().'/images/reconciliation/'.$fileName;
        return Response::download($path);
    }

    public function getPayslip($fileName){
        $path = public_path().'/printouts/payslips/'.$fileName;
        return Response::download($path);
    }




}

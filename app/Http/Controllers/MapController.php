<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MapController extends Controller
{
    public function index(Request $request)
    {
        $entries = DB::table('reports')
            ->where('id', $request->id)
            ->first();

        if($entries){
            $data = [
                'latitude' => $entries->latitude,
                'longitude' => $entries->longitude
            ];

            return view('maps.index', $data);

        }else{
            echo "No data available";
        }


    }
}

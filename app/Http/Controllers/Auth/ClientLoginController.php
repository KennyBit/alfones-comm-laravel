<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class clientLoginController extends Controller
{
    public function __construct()
    {
        //defining our middleware for this controller
        $this->middleware('guest:client',['except' => ['logout']]);
    }

    //function to show client login form
    public function showLoginForm() {
        return view('auth.client-login');
    }
    //function to login clients
    public function login(Request $request) {
        //validate the form data
        $this->validate($request,[
            'email' => 'required|email',
            'password' => 'required|min:6'
        ]);
        //attempt to login the clients in
        if (Auth::guard('client')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)){
            //if successful redirect to client dashboard
            return redirect()->intended(route('client.dashboard'));
        }
        //if unsuccessfull redirect back to the login for with form data
        return redirect()->back()->withInput($request->only('email','remember'));
    }

    public function logout()
    {
        Auth::guard('client')->logout();

        return redirect('/');
    }

}

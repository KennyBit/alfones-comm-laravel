<?php

namespace App;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

class ActivationReport extends Model
{
    use Uuids;

    protected $fillable = [
        'activation_id','user_id', 'admin_id', 'date'
    ];

    public $incrementing = false;
}

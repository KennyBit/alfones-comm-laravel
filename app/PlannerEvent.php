<?php

namespace App;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

class PlannerEvent extends Model
{
    use Uuids;

    protected $fillable = [
        'user_id', 'date', 'time', 'event', 'notes', 'status'
    ];

    public $incrementing = false;
}

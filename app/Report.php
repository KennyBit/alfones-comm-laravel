<?php

namespace App;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    use Uuids;

    protected $fillable = [
        'activation_id', 'location_id', 'user_id', 'customer_name', 'customer_phone', 'customer_id','product'
        ,'product_id','product_quantity','product_value','extra_field_1','extra_field_2','merchandise','merchandise_id',
        'merchandise_quantity','customer_feedback','image','image_caption','latitude', 'longitude'
    ];

    public $incrementing = false;

    public function getRouteKeyName()
    {
        return 'uuid';
    }
}

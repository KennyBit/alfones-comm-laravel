<?php

namespace App;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

class Merchandise extends Model
{
    use Uuids;

    protected $fillable = [
        'name', 'description', 'quantity', 'activation_id'
    ];

    public $incrementing = false;

    public function getRouteKeyName()
    {
        return 'uuid';
    }
}

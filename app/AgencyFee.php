<?php

namespace App;

use Illuminate\Notifications\Notifiable;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class AgencyFee extends Model
{
    protected $fillable = [
    'agency_fees_percentage',
    'agency_fees',
    'quote_id'
    ];
    
    
    public function agencyFee()
    {
        return $this->belongsTo(Quote::class, 'quote_id');
    }
}

<?php

namespace App;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

class Division extends Model
{
    use Uuids;

    protected $fillable = [
        'name'
    ];

    public $incrementing = false;
}

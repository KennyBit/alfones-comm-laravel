<?php

namespace App;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

class Expenses extends Model
{
    use Uuids;

    protected $fillable = [
        'expense_type', 'activation_id', 'admin_id', 'requisition_title', 'required_date', 'requisition_status', 'reconciliation_status', 'submitted_status'
    ];

    public $incrementing = false;
}

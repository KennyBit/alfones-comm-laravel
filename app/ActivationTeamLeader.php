<?php

namespace App;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

class ActivationTeamLeader extends Model
{
    use Uuids;

    protected $fillable = [
        'activation_id', 'admin_id'
    ];

    public $incrementing = false;

    public function getRouteKeyName()
    {
        return 'uuid';
    }
}

<?php

namespace App;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

class Activation extends Model
{
    use Uuids;

    protected $fillable = [
        'name', 'description', 'client_id', 'project_manager_id', 'ba_unit_cost', 'team_leader_unit_cost', 'start_date', 'end_date', 'status', 'allow_tl_location', 'enable_geo_fence', 'allow_client_portal'
    ];

    public $incrementing = false;

    public function getRouteKeyName()
    {
        return 'uuid';
    }
}
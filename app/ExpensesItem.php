<?php

namespace App;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

class ExpensesItem extends Model
{
    use Uuids;

    protected $fillable = [
        'expenses_id', 'item_supplier', 'item_name', 'item_description', 'item_unit_cost', 'item_quantity', 'item_days','item_amount', 'reconciliation_status'
    ];

    public $incrementing = false;
}

<?php

namespace App;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

class ActivationOutletProductSummary extends Model
{
    use Uuids;

    protected $fillable = [
       'activation_id', 'closing_stock', 'location_id', 'product_id',  'base_volume', 'quantity', 'admin_id', 'date', 'product_price'
    ];

    public $incrementing = false;
}

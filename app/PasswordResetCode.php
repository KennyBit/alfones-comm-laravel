<?php

namespace App;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

class PasswordResetCode extends Model
{
    use Uuids;
    protected $fillable = [
        'user_id', 'code', 'status'
    ];

    public $incrementing = false;
}

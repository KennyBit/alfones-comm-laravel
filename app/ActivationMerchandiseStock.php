<?php

namespace App;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

class ActivationMerchandiseStock extends Model
{
    use Uuids;

    protected $fillable = [
        'activation_id','opening_stock', 'closing_stock', 'location_id', 'merchandise_id', 'admin_id', 'date',
    ];

    public $incrementing = false;
}

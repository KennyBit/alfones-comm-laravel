<?php

namespace App;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

class ActivationProductBaseVolume extends Model
{
    use Uuids;

    protected $fillable = [
        'base_volume', 'closing_stock', 'location_id', 'product_id', 'admin_id', 'date',
    ];

    public $incrementing = false;
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Quote extends Model
{
       protected $fillable = [
        'name',
        'days',
        'client_name',
        'admin_id',
        'date',
        'status',
        
    ];

    public function admin()
    {
        return $this->belongsTo(Admin::class, 'admin_id');
    }

   public function quoteitem()
    {
        return $this->hasMany('App\QuoteItem', 'quote_id');
    }
}


<?php

namespace App;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

class UserPlanner extends Model
{
    use Uuids;

    protected $fillable = [
        'admin_id', 'date_from', 'date_to', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'attachment', 'status',
    ];

    public $incrementing = false;
}

<?php

namespace App;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

class LeaveDay extends Model
{
    use Uuids;

    protected $fillable = [
        'admin_id', 'year', 'days'
    ];

    public $incrementing = false;
}
